This is poke.info, produced by makeinfo version 6.3 from poke.texi.

This manual describes GNU poke (version 2.4, 25 July 2022).

   Copyright (C) 2019-2022 The poke authors.

     You can redistribute it and/or modify this manual under the terms
     of the GNU General Public License as published by the Free Software
     Foundation, either version 3 of the License, or (at your option)
     any later version.
INFO-DIR-SECTION Editors
START-INFO-DIR-ENTRY
* poke: (poke). Interactive editor for binary files.
END-INFO-DIR-ENTRY


Indirect:
poke.info-1: 533
poke.info-2: 301813
poke.info-3: 616666

Tag Table:
(Indirect)
Node: Top533
Node: Introduction9065
Node: Motivation9368
Node: Decode-Compute-Encode10146
Node: Describe-Compute13383
Node: Nomenclature15397
Node: Invoking poke16857
Node: Commanding poke18851
Node: The REPL19437
Node: Evaluation20801
Node: Commands and Dot-Commands22017
Node: Command Files25382
Node: Scripts26722
Node: Setting Up27662
Node: Setting up Hyperlinks27917
Node: Simple Init File31184
Node: Basic Editing31891
Node: Binary Files33206
Node: Files as IO Spaces35588
Node: Dumping File Contents39085
Ref: Dumping File Contents-Footnote-146794
Ref: Dumping File Contents-Footnote-247028
Node: Poking Bytes47238
Node: Values and Variables51593
Node: From Bytes to Integers54191
Ref: From Bytes to Integers-Footnote-159699
Node: Big and Little Endians59936
Ref: Big and Little Endians-Footnote-166118
Node: Negative Integers66173
Node: Weird Integers69676
Node: Unaligned Integers78549
Node: Integers of Different Sizes82675
Node: Offsets and Sizes83235
Ref: Offsets and Sizes-Footnote-189762
Ref: Offsets and Sizes-Footnote-289875
Node: Buffers as IO Spaces89913
Node: Copying Bytes92147
Node: Saving Buffers in Files94478
Node: Character Sets95743
Node: From Bytes to Characters98131
Node: ASCII Strings101241
Node: From Strings to Characters106168
Node: Strings are not Arrays106951
Node: Structuring Data108151
Node: The SBM Format109369
Node: Poking a SBM Image113375
Node: Modifying SBM Images126531
Ref: Modifying SBM Images-Footnote-1141266
Node: Defining Types141304
Node: Pickles144743
Node: Poking Structs154986
Node: How Structs are Built158347
Node: Variables in Structs161581
Node: Functions in Structs163828
Node: Struct Methods166383
Node: Padding and Alignment174020
Node: Esoteric and exoteric padding175081
Node: Reserved fields175828
Node: Payloads179264
Node: Aligning struct fields183664
Node: Padding array elements185381
Node: Dealing with Alternatives188336
Ref: Dealing with Alternatives-Footnote-1194221
Node: Structured Integers194259
Node: Working with Incorrect Data199616
Node: Maps and Map-files202879
Node: Editing using Variables203797
Node: poke Maps208210
Node: Loading Maps211676
Node: Multiple Maps216170
Node: Auto-map218602
Node: Constructing Maps219742
Node: Predefined Maps221323
Node: Writing Pickles221625
Node: Pretty-printers222904
Node: Setters and Getters228299
Node: Writing Binary Utilities230861
Node: Poke Scripts231812
Node: Command-Line Arguments233319
Node: Exiting from Scripts234522
Node: Loading pickles as Modules235498
Node: elfextractor237143
Node: Filters244236
Node: Stream IO Spaces245152
Node: Reading from Streams247054
Node: Writing to Streams248590
Node: pk-strings249470
Node: Configuration250135
Node: pokerc250421
Ref: pokerc-Footnote-1251488
Node: Load Path251574
Node: Styling252655
Node: Time252758
Node: POSIX Time252997
Node: Colors253456
Node: The Color Registry253814
Node: RGB24 Encoding255685
Node: Audio256622
Node: MP3256762
Node: ID3V1 Tags256871
Node: Object Formats259603
Node: ELF259972
Node: Dwarf260062
Node: Programs260156
Node: argp260441
Node: Programming Emacs Modes263730
Node: poke-mode264239
Node: poke-map-mode264504
Node: poke-ras-mode264768
Node: Vim Syntax Highlighting265114
Node: poke.vim265573
Node: Dot-Commands266073
Node: load command267036
Node: source command267706
Node: file command268026
Node: mem command269272
Node: nbd command269753
Node: proc command270660
Node: sub command271026
Node: ios command271429
Node: close command271794
Node: doc command272170
Node: editor command273195
Node: info command273757
Node: set command275839
Node: vm command280662
Node: .vm disassemble281115
Node: .vm profile282056
Node: exit command282545
Node: quit command282866
Node: Commands283049
Node: dump283381
Node: Information dump shows285251
Node: Presentation options for dump286986
Node: copy289166
Node: save290070
Node: extract291407
Node: scrabble291888
Node: The Poke Language293247
Node: Integers294446
Node: Integer Literals295591
Ref: Integer Literals-Footnote-1298437
Node: Characters298582
Node: Booleans299324
Node: Integer Types299623
Node: Casting Integers301813
Node: Relational Operators303393
Node: Arithmetic Operators304032
Node: Bitwise Operators305122
Node: Boolean Operators306343
Node: Integer Attributes306813
Node: Offsets307540
Node: Offset Literals308044
Node: Offset Units308970
Node: Offset Types311792
Node: Casting Offsets312836
Node: Offset Operations313453
Node: Offset Attributes315750
Node: Strings316546
Node: String Literals317271
Node: String Types318150
Node: String Indexing318342
Node: String Concatenation319080
Node: String Attributes320304
Node: String Formatting320997
Node: Arrays321608
Node: Array Literals322317
Node: Array Types323899
Node: Casting Arrays328569
Node: Array Constructors330277
Node: Array Comparison331389
Node: Array Indexing331795
Node: Array Trimming332579
Node: Array Elements334254
Node: Array Concatenation334628
Node: Array Attributes335092
Node: Structs336010
Node: Struct Types337250
Node: Struct Constructors338327
Node: Struct Comparison340381
Node: Field Endianness340804
Node: Accessing Fields342363
Node: Field Constraints342893
Node: Field Initializers344436
Node: Field Labels347887
Node: Pinned Structs349480
Node: The OFFSET variable350259
Node: Integral Structs351213
Node: Unions360603
Node: Union Constructors361731
Node: Optional Fields363199
Node: Casting Structs365142
Node: Declarations in Structs366733
Node: Methods367201
Node: Struct Attributes367787
Node: Types368951
Node: type369185
Node: The any Type369930
Node: The isa Operator370726
Node: Assignments371088
Node: Compound Statements371851
Node: Conditionals372458
Node: if-else372828
Node: Conditional Expression373298
Node: Loops373725
Node: while374111
Node: for374806
Node: for-in375191
Node: Expression Statements376142
Node: Functions376698
Node: Function Declarations377301
Node: Optional Arguments378645
Node: Variadic Functions379423
Node: Calling Functions379818
Node: Function Types381145
Node: Lambdas381935
Node: Function Comparison382562
Node: Function Attributes383089
Node: Endianness383347
Node: set endian383999
Node: Endian in Fields384886
Node: Endian built-ins385154
Node: Mapping389513
Node: IO Spaces390289
Node: open391100
Node: opensub392886
Node: openproc393926
Node: close394358
Node: flush394904
Node: get_ios395579
Node: set_ios396024
Node: iosize396572
Node: ioflags396945
Node: The Map Operator397331
Node: Mapping Simple Types400515
Node: Mapping Structs400957
Node: Mapping Arrays401414
Node: Array maps bounded by number of elements401852
Node: Array maps bounded by size403126
Node: Unbounded array maps406435
Node: Mapped bounds in bounded arrays407917
Node: Mapping Functions409450
Node: Non-strict Mapping410169
Node: Unmapping410937
Node: Exception Handling411482
Node: Exceptions412082
Node: try-catch415032
Node: try-until416035
Node: raise416903
Node: exception-predicate417459
Node: assert419131
Node: Terminal419666
Node: Terminal Colors420195
Node: Terminal Styling421371
Node: Terminal Hyperlinks422551
Node: Printing423133
Node: print423441
Node: printf423913
Node: Comments427383
Node: Multi-line comments427775
Node: Single line comments428057
Node: Vertical separator428417
Node: Modules428753
Node: System430439
Node: getenv430752
Node: rand431100
Node: VM431639
Node: vm_obase432688
Node: vm_set_obase432953
Node: vm_opprint433471
Node: vm_set_opprint433807
Node: vm_oacutoff434189
Node: vm_set_oacutoff434523
Node: vm_odepth434951
Node: vm_set_odepth435278
Node: vm_oindent435735
Node: vm_set_oindent436026
Node: vm_omaps436610
Node: vm_set_omaps436934
Node: vm_omode437259
Node: vm_set_omode437659
Node: Debugging438021
Node: __LINE__ and __FILE__438251
Node: strace438865
Node: The Standard Library439178
Node: Standard Integral Types440063
Node: Standard Offset Types440734
Node: Standard Units441083
Node: Conversion Functions441728
Node: catos442186
Node: stoca442826
Node: atoi443419
Node: ltos444067
Node: Array Functions444476
Node: reverse444787
Node: String Functions445028
Node: ltrim445408
Node: rtrim445744
Node: strchr446096
Node: Sorting Functions446503
Node: qsort446721
Node: CRC Functions447670
Node: Dates and Times448325
Node: Offset Functions450025
Node: alignto450239
Node: The Machine-Interface450538
Node: MI overview451252
Node: Running poke in MI mode452555
Node: MI transport453472
Node: MI protocol454002
Node: MI Requests454488
Node: Request EXIT454740
Node: Request PRINTV454939
Node: MI Responses455228
Node: Response EXIT455495
Node: Response PRINTV455902
Node: MI Events456579
Node: Event INITIALIZE456748
Node: The Poke Virtual Machine457238
Node: PVM Instructions457478
Node: VM instructions458460
Node: Instruction canary459139
Node: Instruction exit459540
Node: Instruction pushend459886
Node: Instruction popend460218
Node: Instruction pushob460607
Node: Instruction popob461001
Node: Instruction pushom461532
Node: Instruction popom461946
Node: Instruction pushoo462497
Node: Instruction popoo462885
Node: Instruction pushoi463310
Node: Instruction popoi463793
Node: Instruction pushod464289
Node: Instruction popod464638
Node: Instruction pushoac465008
Node: Instruction popoac465421
Node: Instruction pushopp465839
Node: Instruction popopp466210
Node: Instruction pushoc466612
Node: Instruction popoc466922
Node: Instruction pushobc467253
Node: Instruction popobc467578
Node: Instruction sync467922
Node: IOS related instructions468363
Node: Instruction open468783
Node: Instruction close469529
Node: Instruction flush470033
Node: Instruction pushios470518
Node: Instruction popios470928
Node: Instruction ioflags471358
Node: Instruction iosize471856
Node: Instruction iogetb472330
Node: Instruction iosetb472932
Node: Function management instructions473512
Node: Instruction call473819
Node: Instruction prolog474242
Node: Instruction return474658
Node: Environment instructions474948
Node: Instruction pushf475349
Node: Instruction popf475750
Node: Instruction pushvar476002
Node: Instruction pushtopvar476430
Node: Instruction popvar476956
Node: Instruction regvar477367
Node: Instruction duc477710
Node: Instruction pec478007
Node: Printing Instructions478285
Node: Instruction indent478952
Node: Instruction printi479372
Node: Instruction printiu479713
Node: Instruction printl480061
Node: Instruction printlu480394
Node: Instruction prints480740
Node: Instruction beghl481019
Node: Instruction endhl481310
Node: Instruction begsc481684
Node: Instruction endsc482089
Node: Format Instructions482424
Node: Instruction formati482973
Node: Instruction formatiu483304
Node: Instruction formatl483670
Node: Instruction formatlu484021
Node: Main stack manipulation instructions484357
Node: Instruction push485021
Node: Instruction drop485295
Node: Instruction drop2485596
Node: Instruction drop3485912
Node: Instruction drop4486235
Node: Instruction swap486555
Node: Instruction nip486849
Node: Instruction nip2487143
Node: Instruction nip3487447
Node: Instruction dup487755
Node: Instruction over488052
Node: Instruction rot488361
Node: Instruction nrot488675
Node: Instruction tuck488995
Node: Instruction quake489315
Node: Instruction revn489621
Node: Instruction pushhi489926
Node: Instruction pushlo490382
Node: Instruction push32490836
Node: Registers manipulation instructions491270
Node: Instruction pushr491609
Node: Instruction popr491887
Node: Instruction setr492207
Node: Return stack manipulation instructions492499
Node: Instruction saver492876
Node: Instruction restorer493187
Node: Instruction tor493551
Node: Instruction fromr493880
Node: Instruction atr494209
Node: Arithmetic instructions494497
Node: Instruction addi495388
Node: Instruction addiu495775
Node: Instruction addlu496337
Node: Instruction subi496671
Node: Instruction subiu496991
Node: Instruction subl497327
Node: Instruction sublu497649
Node: Instruction muli497987
Node: Instruction muliu498307
Node: Instruction mull498643
Node: Instruction mullu498967
Node: Instruction divi499307
Node: Instruction diviu499731
Node: Instruction divl500171
Node: Instruction divlu500597
Node: Instruction modi501039
Node: Instruction modiu501454
Node: Instruction modl501885
Node: Instruction modlu502302
Node: Instruction negi502735
Node: Instruction negiu503048
Node: Instruction negl503380
Node: Instruction neglu503693
Node: Instruction powi504020
Node: Instruction powiu504446
Node: Instruction powl504887
Node: Instruction powlu505313
Node: Relational instructions505729
Node: Instruction eqi506636
Node: Instruction eqiu506946
Node: Instruction eql507297
Node: Instruction eqlu507633
Node: Instruction eqs507984
Node: Instruction nei508317
Node: Instruction neiu508655
Node: Instruction nel509009
Node: Instruction nelu509349
Node: Instruction nes509704
Node: Instruction nn510042
Node: Instruction nnn510363
Node: Instruction lti510687
Node: Instruction ltiu511037
Node: Instruction ltl511411
Node: Instruction ltlu511760
Node: Instruction lei512133
Node: Instruction leiu512493
Node: Instruction lel512877
Node: Instruction lelu513235
Node: Instruction gti513617
Node: Instruction gtiu513971
Node: Instruction gtl514349
Node: Instruction gtlu514701
Node: Instruction gei515073
Node: Instruction geiu515436
Node: Instruction gel515823
Node: Instruction gelu516184
Node: Instruction lts516569
Node: Instruction gts516941
Node: Instruction ges517315
Node: Instruction les517698
Node: Instruction eqc518078
Node: Instruction nec518413
Node: Concatenation instructions518728
Node: Instruction sconc518969
Node: Logical instructions519238
Node: Instruction and519501
Node: Instruction or519778
Node: Instruction not520075
Node: Bitwise instructions520339
Node: Instruction bxori520891
Node: Instruction bxoriu521189
Node: Instruction bxorl521530
Node: Instruction bxorlu521857
Node: Instruction bori522199
Node: Instruction boriu522509
Node: Instruction borl522834
Node: Instruction borlu523145
Node: Instruction bandi523464
Node: Instruction bandiu523779
Node: Instruction bandl524113
Node: Instruction bandlu524433
Node: Instruction bnoti524769
Node: Instruction bnotiu525078
Node: Instruction bnotl525397
Node: Instruction bnotlu525702
Node: Shift instructions525995
Node: Instruction bsli526368
Node: Instruction bsliu526858
Node: Instruction bsll527388
Node: Instruction bsllu527904
Node: Instruction bsri528434
Node: Instruction bsriu528805
Node: Instruction bsrl529190
Node: Instruction bsrlu529561
Node: Compare-and-swap instructions529921
Node: Instruction swapgti530241
Node: Instruction swapgtiu530597
Node: Instruction swapgtl530997
Node: Instruction swapgtlu531383
Node: Branch instructions531756
Node: Instruction ba532189
Node: Instruction bn532424
Node: Instruction bnn532724
Node: Instruction bzi533032
Node: Instruction bziu533336
Node: Instruction bzl533654
Node: Instruction bzlu533958
Node: Instruction bnzi534276
Node: Instruction bnziu534589
Node: Instruction bnzl534916
Node: Instruction bnzlu535231
Node: Conversion instructions535534
Node: Instruction ctos536112
Node: Instruction itoi536450
Node: Instruction itoiu536816
Node: Instruction itol537195
Node: Instruction itolu537559
Node: Instruction iutoi537937
Node: Instruction iutoiu538320
Node: Instruction iutol538716
Node: Instruction iutolu539097
Node: Instruction ltoi539490
Node: Instruction ltoiu539859
Node: Instruction ltol540239
Node: Instruction ltolu540604
Node: Instruction lutoi540983
Node: Instruction lutoiu541367
Node: Instruction lutol541764
Node: Instruction lutolu542104
Node: String instructions542431
Node: Instruction strref542786
Node: Instruction strset543353
Node: Instruction substr543903
Node: Instruction muls544526
Node: Instruction sprops545050
Node: Instruction sproph545403
Node: Instruction spropc545796
Node: Array instructions546096
Node: Instruction mka546435
Node: Instruction ains546986
Node: Instruction arem547728
Node: Instruction aset548194
Node: Instruction aref548776
Node: Instruction arefo549242
Node: Instruction asettb549714
Node: Struct instructions550109
Node: Instruction mksct550546
Node: Instruction sset551238
Node: Instruction sseti551698
Node: Instruction sref552155
Node: Instruction srefo552657
Node: Instruction srefmnt553153
Node: Instruction srefnt553586
Node: Instruction srefi554057
Node: Instruction srefia554545
Node: Instruction srefio555057
Node: Instruction smodi555552
Node: Offset Instructions556025
Node: Instruction mko556344
Node: Instruction ogetm556665
Node: Instruction osetm556956
Node: Instruction ogetu557274
Node: Instruction ogetbt557565
Node: Instructions to handle mapped values557836
Node: Instruction mm558512
Node: Instruction map558806
Node: Instruction unmap559172
Node: Instruction reloc559522
Node: Instruction ureloc560047
Node: Instruction mgets560455
Node: Instruction msets560843
Node: Instruction mgeto561236
Node: Instruction mseto561628
Node: Instruction mgetios562021
Node: Instruction msetios562412
Node: Instruction mgetm562897
Node: Instruction msetm563276
Node: Instruction mgetw563659
Node: Instruction msetw564036
Node: Instruction mgetsel564421
Node: Instruction msetsel564962
Node: Instruction mgetsiz565471
Node: Instruction msetsiz565969
Node: Type related instructions566413
Node: Instruction isa566976
Node: Instruction typof567297
Node: Instruction tyisc567669
Node: Instruction tyissct567990
Node: Instruction mktyv568316
Node: Instruction mktyany568604
Node: Instruction mktyi568896
Node: Instruction mktys569333
Node: Instruction mktyo569604
Node: Instruction mktya570049
Node: Instruction tyagett570497
Node: Instruction tyagetb570822
Node: Instruction mktyc571140
Node: Instruction mktysct571523
Node: Instruction tysctn572025
Node: IO instructions572349
Node: Instruction write572983
Node: Instruction peeki573351
Node: Instruction peekiu573783
Node: Instruction peekl574199
Node: Instruction peeklu574629
Node: Instruction peekdi575045
Node: Instruction peekdiu575429
Node: Instruction peekdl575804
Node: Instruction peekdlu576186
Node: Instruction pokei576558
Node: Instruction pokeiu576981
Node: Instruction pokel577387
Node: Instruction pokelu577806
Node: Instruction pokedi578212
Node: Instruction pokediu578590
Node: Instruction pokedl578959
Node: Instruction pokedlu579335
Node: Instruction peeks579701
Node: Instruction pokes579999
Node: Exceptions handling instructions580273
Node: Instruction pushe580594
Node: Instruction pope580994
Node: Instruction raise581330
Node: Instruction popexite581639
Node: Debugging Instructions581960
Node: Instruction strace582253
Node: Instruction disas582637
Node: Instruction note582967
Node: System Interaction Instructions583361
Node: Instruction getenv583623
Node: Miscellaneous Instructions584047
Node: Instruction nop584369
Node: Instruction rand584580
Node: Instruction time584988
Node: Instruction sleep585374
Node: Instruction siz585950
Node: Instruction sel586234
Node: Table of ASCII Codes586737
Node: GNU Free Documentation License590961
Node: Concept Index616666

End Tag Table
