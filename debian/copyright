Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: poke
Upstream-Contact: Jose E. Marchesi <jemarch@gnu.org>
Source: https://www.gnu.org/software/poke/
Files-Excluded:
 jitter/doc/*
 jitter/example-vms/jitterlisp/doc/*
 jitter/example-vms/structured/doc/*

Files: *
Copyright: 2019-2021 Jose E. Marchesi
License: GPL-3+

Files: gl/*
       gl-libpoke/*
       build-aux/*
       testsuite/config/default.exp
       testsuite/config/unix.exp
       jitter/*
       m4/*
       poke/pk-map-tab.c
       poke/pk-map-tab.h
       maint.mk
       po/poke.pot
       libpoke/pkl-tab.h
       libpoke/pkl-tab.c
Copyright: 1984-2021 Free Software Foundation, Inc.
License: GPL-3+

Files: m4/jitter.m4
       jitter/tests/*
       jitter/machine/*
       jitter/example-build-systems/*
       jitter/templates/*
       jitter/amend-autom4te-output
       jitter/autoconf/jitter.m4
       jitter/jitter/*
       jitter/emacs/jitter-mode.el
       jitter/m4-utility/jitter-script.m4sh
       jitter/jitterc/*
       jitter/scripts/*
       jitter/Makefile.am
       jitter/bin/*
       jitter/example-vms/*
       jitter/configure.ac
       libpoke/pvm-vm.h
       libpoke/pvm-vm1.c
Copyright: 2016-2021 Luca Saiu
License: GPL-3+

Files: gl-libpoke/random_r.c
       gl-libpoke/random.c
Copyright: 1995-2021 Free Software Foundation, Inc.
	   1983 Regents of the University of California.
License: GPL-3+ and BSD-3-Clause

Files: Makefile.in
       aclocal.m4
       maps/Makefile.in
       GNUmakefile
       etc/Makefile.in
       jitter/aclocal.m4
       pickles/Makefile.in
Copyright: 1994-2021 Free Software Foundation, Inc.
License: Expat

Files: gui/Makefile.in
       utils/Makefile.in
       testsuite/Makefile.in
       testsuite/poke.mi-json/Makefile.in
       testsuite/poke.libpoke/Makefile.in
       poke/Makefile.in
       man/Makefile.in
       libpoke/Makefile.in
Copyright: 1994-2018 Free Software Foundation, Inc.
	   2019-2021 Jose E. Marchesi
License: Expat

Files: jitter/Makefile.in
       jitter/configure
       configure
Copyright: 1994-2018 Free Software Foundation, Inc.
	   2016, 2017, 2018, 2019, 2020 Luca Saiu
License: Expat

Files: jitter/bootstrap.conf
Copyright: 2006-2017 Free Software Foundation, Inc.
	   2017, 2020 Luca Saiu
License: GPL-3+

Files: build-aux/install-sh
       jitter/build-aux/install-sh
Copyright: 1994 X Consortium
License: Expat

Files: m4/ax_check_awk_gensub.m4
       m4/ax_need_awk.m4
       m4/ax_try_awk_expout.m4
Copyright: 2009 Francesco Salvestrini <salvestrini@users.sourceforge.net>
License: Expat

Files: m4/tcl.m4
Copyright: 1999-2000 Ajuba Solutions.
	   2002-2005 ActiveState Corporation.
	   2020, 2021 Jose E. Marchesi.
License: Expat

Files: m4/pkg.m4
Copyright: 2004 Scott James Remnant <scott@netsplit.com>
	   2012-2015 Dan Nicholson <dbn.lists@gmail.com>
License: GPL-2+

Files: testsuite/poke.std/std-test.pk
       testsuite/lib/poke-pk.exp
       testsuite/poke.pickles/mbr-test.pk
       testsuite/poke.pickles/id3v1-test.pk
       testsuite/poke.pickles/color-test.pk
       testsuite/poke.pickles/rgb24-test.pk
       testsuite/poke.pickles/argp-test.pk
       testsuite/poke.pickles/pickles.exp
       testsuite/poke.pktest/pktest.exp
       testsuite/poke.libpoke/api.c
       pickles/pktest.pk
       pickles/mbr.pk
       man/poke.1
Copyright: 2020-2021 The poke authors
License: GPL-3+

Files: testsuite/lib/poke.exp
Copyright: 2019, 2020, 2021 Jose E. Marchesi
	   2014-2020 Free Software Foundation
License: GPL-3+

Files: pickles/btf-dump.pk
       pickles/ctf.pk
Copyright: 2021 Oracle, Inc.
License: GPL-3+

Files: pickles/time.pk
Copyright: 2019, 2021 John Darrington
License: GPL-3+

Files: po/Makefile.in.in
Copyright: 1995-1997, 2000-2007, 2009-2010 by Ulrich Drepper <drepper@gnu.ai.mit.edu>
License: Expat

Files: libpoke/ios-buffer.h
       libpoke/ios-buffer.c
       libpoke/ios-dev-stream.c
Copyright: 2020, 2021 Egeyar Bagcioglu
License: GPL-3+

Files: libpoke/ios-dev-nbd.c
Copyright: 2020, 2021 Eric Blake
License: GPL-3+


Files: debian/*
Copyright: 2021-2022 Sergio Durigan Junior <sergiodj@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems a full copy of the GPL 3 can be found at
 /usr/share/common-licenses/GPL-3

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-Clause
 You may use this file under the terms of the BSD license as follows:
 .
 "Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
