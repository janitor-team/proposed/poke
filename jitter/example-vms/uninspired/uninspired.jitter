## An uninspired and unoriginal Jitter VM, useful as an example.

legal-notice
  Copyright (C) 2016, 2017, 2019, 2020, 2021 Luca Saiu
  Written by Luca Saiu

  This file is part of GNU Jitter.

  GNU Jitter is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GNU Jitter is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>.
end


## Global configuration.
#################################################################

early-header-c
  code
    struct exception_handler
    {
      jitter_uint exception;
      uninspired_program_point code;
      jitter_stack_height stack_height;
    };
  end
end

vm
  set prefix "uninspired"
end


## Runtime state customization.
#################################################################

state-struct-runtime-c
  code
  end
end


## Runtime VM data structures.
#################################################################

early-header-c
  code
  #include <string.h> // FIXME: remove after my tests with memcpy

  union vmprefix_currently_unique_kind_register_t
  {
    /* Each of the following fields has the same name and a compatible type (in
       this case exactly the same) as the corresponding fields in union
       jitter_word .  This is useful for meta-instructions whose arguments may be
       either immediates or registers. */
    jitter_int fixnum;
    jitter_uint ufixnum;
    union jitter_word * restrict pointer;
  };

# define VECTOR_ELEMENT_TYPE  float//double//char//int//double //float
# define VECTOR_ELEMENT_NO    2//1024//8//1024//512 //8//512//1024

# define USE_ATTRIBUTE_VECTOR_SIZE

#ifdef USE_ATTRIBUTE_VECTOR_SIZE
  typedef VECTOR_ELEMENT_TYPE
  vector
    __attribute__ ((vector_size (VECTOR_ELEMENT_NO
                                 * sizeof(VECTOR_ELEMENT_TYPE)),
                    aligned));
#else
  typedef VECTOR_ELEMENT_TYPE
  vector [VECTOR_ELEMENT_NO]
    __attribute__ ((aligned));
#endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
  end
end

stack s
  long-name "stack"
  c-element-type "union jitter_word"
  c-initial-value "(union jitter_word) {.ufixnum = 0xaabbccdd}"
  element-no 4096
  tos-optimized
  guard-underflow
  guard-overflow
end

stack h
  long-name "handlers"
  c-element-type "struct exception_handler"
  element-no 4096
  non-tos-optimized
  guard-underflow
  guard-overflow
end

register-class r
  long-name "general_registers"
  c-type "union vmprefix_currently_unique_kind_register_t"
  c-initial-value "(union vmprefix_currently_unique_kind_register_t) {.ufixnum = 0x11223344}"
  fast-register-no 0
end

register-class f
  # c-type "float"
  # c-type "double"
  c-type "VECTOR_ELEMENT_TYPE"
  fast-register-no 0 #2
end

register-class v
  c-type "vector"
  fast-register-no 0 #2
end

register-class c
  c-type "char"
  fast-register-no 0 #2
end


## C code to include.
#################################################################

late-header-c
  code
  end
end

late-c
  code
/* See install-signal-handler below. */
static void
jitter_my_signal_handler (int signal_number)
{
  struct vmprefix_state *s;
  /* For every state... */
  VMPREFIX_FOR_EACH_STATE (s)
    {
      /* ...Mark the signal signal_number as pending... */
      VMPREFIX_STATE_AND_SIGNAL_TO_PENDING_SIGNAL_NOTIFICATION
         (s, signal_number) = true;
      /* ...And record the fact that there is at least one notification to
         handle. */
      VMPREFIX_STATE_TO_PENDING_NOTIFICATIONS (s) = true;
    }
}
  end
end

initialization-c
  code
  end
end

late-c
  code
    #include <stdio.h>
    #include <stdlib.h>

    static const char * const
    vmprefix_printfixnum_decimal_hex_format_string
      __attribute__ ((unused))
      = "%" JITTER_PRIi " 0x%" JITTER_PRIx "\n";

    static const char * const
    vmprefix_printfixnum_format_string
      __attribute__ ((unused))
      = "%" JITTER_PRIi "\n";

    static const char * const
    vmprefix_printdouble_format_string
      = "%f\n";

    static const char * const
    vmprefix_hcf_format_string = "Halting and catching fire\n";

    static const char * const
    vmprefix_newline_string = "\n";

    static const char * const
    vmprefix_print_topmost_format_string = "(%i)%" JITTER_PRIi "  ";
  end
end


## Functions and globals to wrap.
#################################################################

wrapped-functions
  printf
  fflush
  fprintf
  exit
  rand
  jitter_xmalloc
end

wrapped-globals
  vmprefix_printfixnum_format_string
  vmprefix_printdouble_format_string
  vmprefix_printfixnum_decimal_hex_format_string
  vmprefix_hcf_format_string
  vmprefix_print_topmost_format_string
  vmprefix_newline_string
end


## Exception-handling instructions.
#################################################################

instruction push-handler (?n 0 1 2 3 4 5 6 7 8 9 10, ?l)
  code
    struct exception_handler h;
    h.exception = JITTER_ARGU0;
    h.code = JITTER_ARGP1;
    h.stack_height = JITTER_HEIGHT_STACK();
    JITTER_PUSH_HANDLERS(h);
  end
end

instruction drop-handler ()
  code
    JITTER_DROP_HANDLERS();
  end
end

instruction raise (?Rn 0 1 2 3 4 5 6 7 8 9 10)
  branching
  code
    struct exception_handler h;
    do
      {
        h = JITTER_TOP_HANDLERS();
        JITTER_DROP_HANDLERS();
      }
    while (h.exception != JITTER_ARGN0);
    JITTER_SET_HEIGHT_STACK(h.stack_height);
    JITTER_BRANCH(h.code);
  end
end

## (Scratch) f-register instructions.
#################################################################

instruction fset (?Rn, !F)
  code
    JITTER_ARG1 = (double) JITTER_ARGN0;
  end
end

instruction fadd (?F, ?F, !F)
  code
    JITTER_ARG2 = JITTER_ARG0 + JITTER_ARG1;
  end
end

instruction fsub (?F, ?F, !F)
  code
    JITTER_ARG2 = JITTER_ARG0 - JITTER_ARG1;
  end
end

instruction fmul (?F, ?F, !F)
  code
    JITTER_ARG2 = JITTER_ARG0 * JITTER_ARG1;
  end
end

instruction fdiv (?F, ?F, !F)
  code
    JITTER_ARG2 = JITTER_ARG0 / JITTER_ARG1;
  end
end

instruction fincr (!?F)
  code
    JITTER_ARG0 ++;
  end
end

instruction fprint (?F)
  code
    printf (vmprefix_printdouble_format_string, JITTER_ARG0);
  end
end


# ## (Scratch) v-register instructions.
# #################################################################

# instruction movvectorelementno (!R)
#   code
#     JITTER_ARGN0 = VECTOR_ELEMENT_NO;
#   end
# end

# instruction movvectorelementsizeinbytes (!R)
#   code
#     JITTER_ARGN0 = VECTOR_ELEMENT_NO * sizeof (VECTOR_ELEMENT_TYPE);
#   end
# end

# instruction vset (?Rn 0 1, !V)
#   code
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG1 [i] = (double) JITTER_ARGN0;
#   end
# end

# instruction vload (?R, !V)
#   code
#     const VECTOR_ELEMENT_TYPE * restrict p
#       = (VECTOR_ELEMENT_TYPE *) JITTER_ARGP0;
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG1 [i] = p [i];
#   end
# end

# instruction vstore (?V, ?R)
#   code
#     VECTOR_ELEMENT_TYPE * const restrict p
#       = (VECTOR_ELEMENT_TYPE * const restrict) JITTER_ARGP1;
#     /*
#     static int JITTER_CONCATENATE_TWO(counter, JITTER_SPECIALIZED_INSTRUCTION_OPCODE) = 0;
#     printf ("%i. Storing [%p, %p)...\n",
#             JITTER_CONCATENATE_TWO(counter, JITTER_SPECIALIZED_INSTRUCTION_OPCODE) ++,
#             p,
#             ((char*) p) + VECTOR_ELEMENT_NO * sizeof (VECTOR_ELEMENT_TYPE));
#     fflush (stdout);
#     */
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       p [i] = JITTER_ARG0 [i];
#     //printf ("Still alive.\n"); fflush (stdout);
#   end
# end

# instruction vadd (?V, ?V, !V)
#   code
# #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#     JITTER_ARG2 = JITTER_ARG0 + JITTER_ARG1;
# #else
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG2 [i] = JITTER_ARG0 [i] + JITTER_ARG1 [i];
# #endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#   end
# end

# instruction vaddtoscalar (?V, ?F)
#   code
#     VECTOR_ELEMENT_TYPE res = 0;
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       res += JITTER_ARG0 [i];
#     JITTER_ARG1 = res;
#   end
# end

# instruction vmultoscalar (?V, ?F)
#   code
#     VECTOR_ELEMENT_TYPE res = 1;
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       res *= JITTER_ARG0 [i];
#     JITTER_ARG1 = res;
#   end
# end

# instruction vmul (?V, ?V, !V)
#   code
# #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#     JITTER_ARG2 = JITTER_ARG0 * JITTER_ARG1;
# #else
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG2 [i] = JITTER_ARG0 [i] * JITTER_ARG1 [i];
# #endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#   end
# end

# instruction vmulscalar (?F, ?V, !V)
#   code
# #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#     JITTER_ARG2 = JITTER_ARG1 * JITTER_ARG0;
# #else
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG2 [i] = JITTER_ARG0 * JITTER_ARG1 [i];
# #endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#   end
# end

# instruction vmuldot (?V, ?V, !F)
#   code
#     VECTOR_ELEMENT_TYPE sum = 0;
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       sum += JITTER_ARG0 [i] * JITTER_ARG1 [i];
#     JITTER_ARG2 += sum;
#   end
# end

# instruction vincr (!?V)
#   code
# #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#     JITTER_ARG0 += 1;
# #else
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#       JITTER_ARG0 [i] ++;
# #endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
#   end
# end

# instruction vprint (?V)
#   code
#     int i;
#     for (i = 0; i < VECTOR_ELEMENT_NO; i ++)
#     {
#       VECTOR_ELEMENT_TYPE e = JITTER_ARG0 [i];
#       printf (vmprefix_printdouble_format_string, (double) e); // I want to experiment with integer vectors as well.
#     }
#   end
# end


# ## (Scratch) c-register instructions.
# #################################################################

# instruction cset (?Rn 0 1, !C)
#   code
#     JITTER_ARG1 = JITTER_ARGN0;
#   end
# end

# instruction cadd (?C, ?C, !C)
#   code
#     JITTER_ARG2 = JITTER_ARG0 + JITTER_ARG1;
#   end
# end

# instruction cincr (!?C)
#   code
#     JITTER_ARG0 ++;
#   end
# end

# instruction cprint (?C)
#   code
#     printf (vmprefix_printfixnum_format_string,
#             (jitter_int) JITTER_ARG0);
#   end
# end


## Procedure instructions.
#################################################################

instruction procedureprolog ()
  callee
  code
    JITTER_TOP_STACK().label = JITTER_LINK;
  end
end

instruction procedurereturn ()
  returning
  code
    const void * return_address = JITTER_TOP_STACK().label;
    JITTER_DROP_STACK();
    JITTER_RETURN(return_address);
  end
end

instruction procedurecall (?f)
  caller
  code
    JITTER_PUSH_UNSPECIFIED_STACK();
    JITTER_BRANCH_FAST_AND_LINK(JITTER_ARGF0);
  end
end

instruction procedurecallr (?Rl) # ?l would work as well.
  caller
  code
    JITTER_PUSH_UNSPECIFIED_STACK();
    JITTER_BRANCH_AND_LINK(JITTER_ARGP0);
  end
end


## Stack meta-instructions (still dirty, mostly for testing).
#################################################################

# instruction pushreturn (?Rnl 0)
#   code
#     const union jitter_word word = {.pointer = (void* restrict)JITTER_ARG0.pointer};
#     JITTER_PUSH_RETURN(word);
#   end
# end
# instruction popreturn (!R)
#   code
#     JITTER_ARG0.pointer = JITTER_TOP_RETURN().pointer;
#     JITTER_DROP_RETURN();
#   end
# end

# instruction toreturn ()
#   code
#     union jitter_word word = JITTER_TOP_STACK();
#     JITTER_DROP_STACK();
#     JITTER_PUSH_RETURN(word);
#   end
# end
# instruction fromreturn ()
#   code
#     union jitter_word word = JITTER_TOP_RETURN();
#     JITTER_DROP_RETURN();
#     JITTER_PUSH_STACK(word);
#   end
# end

instruction stackpush (?Rnl 0 1)
  code
    union jitter_word w = {.fixnum = JITTER_ARGN0};
    JITTER_PUSH_STACK(w);
  end
end
instruction stackpushunspecified ()
  code
    JITTER_PUSH_UNSPECIFIED_STACK();
  end
end

instruction stackset (?Rnl 0 1)
  code
    JITTER_TOP_STACK().fixnum = JITTER_ARGN0;
  end
end

instruction stackpop (!R)
  code
    JITTER_ARGN0 = JITTER_TOP_STACK().fixnum;
    JITTER_DROP_STACK();
  end
end

instruction stackpeek (!R)
  code
    JITTER_ARGN0 = JITTER_TOP_STACK().fixnum;
  end
end

instruction stackswaptop (?!R)
  code
    jitter_int old_top = JITTER_TOP_STACK().fixnum;
    jitter_int old_register = JITTER_ARGN0;
    JITTER_TOP_STACK().fixnum = old_register;
    JITTER_ARGN0 = old_top;
  end
end

instruction stackdup ()
  code
    JITTER_DUP_STACK();
  end
end

instruction stackover ()
  code
    JITTER_OVER_STACK();
  end
end

instruction stackdrop ()
  code
    JITTER_DROP_STACK();
  end
end

# instruction returndrop ()
#   code
#     JITTER_DROP_RETURN();
#   end
# end

instruction stacknip ()
  code
    JITTER_NIP_STACK();
  end
end

instruction stackswap ()
  code
    JITTER_SWAP_STACK();
  end
end

instruction stackplus ()
  code
#define PLUS(res, a, b) { res.fixnum = a.fixnum + b.fixnum; }
    JITTER_BINARY_STACK(PLUS);
#undef PLUS
  end
end

instruction stacktimes ()
  code
#define TIMES(res, a, b) { res.fixnum = a.fixnum * b.fixnum; }
    JITTER_BINARY_STACK(TIMES);
#undef TIMES
  end
end

instruction stackplusr (?R)
  code
#define PLUSR(res, a) { res.fixnum = a.fixnum + JITTER_ARGN0; }
    JITTER_UNARY_STACK(PLUSR);
#undef PLUSR
  end
end

instruction stackoneplus ()
  code
#define ONEPLUS(res, a) { res.fixnum = a.fixnum + 1; }
    JITTER_UNARY_STACK(ONEPLUS);
#undef ONEPLUS
  end
end

instruction stackoneminus ()
  code
#define ONEMINUS(res, a) { res.fixnum = a.fixnum - 1; }
    JITTER_UNARY_STACK(ONEMINUS);
#undef ONEMINUS
  end
end

instruction stacknot ()
  code
    JITTER_TOP_STACK().fixnum = ! JITTER_TOP_STACK().fixnum;
  end
end

instruction stackif (?f)
  code
    jitter_int condition = JITTER_TOP_STACK().fixnum;
    JITTER_DROP_STACK();
    JITTER_BRANCH_FAST_IF_NONZERO (condition, JITTER_ARGF0);
  end
end

instruction stacknondroppingif (?f)
  code
    jitter_int condition = JITTER_TOP_STACK().fixnum;
    JITTER_BRANCH_FAST_IF_NONZERO (condition, JITTER_ARGF0);
  end
end

instruction stackprint ()
  cold
  non-relocatable
  code
    jitter_int top_number = JITTER_TOP_STACK().fixnum;
    JITTER_DROP_STACK();
    printf (vmprefix_printfixnum_decimal_hex_format_string,
            (jitter_int) top_number, (jitter_int) top_number);
  end
end


## Register move meta-instructions.
#################################################################

#instruction mov (?Rnl 0 1, !R)
instruction mov (?Rnl 0 1, !R)
  code
    JITTER_ARGN1 = JITTER_ARGN0;
  end
end

# instruction swap (?!R, ?!R)
# #  commutative
#   code
#     union jitter_word arg0_copy = JITTER_ARG0;
#     JITTER_ARG0 = JITTER_ARG1;
#     JITTER_ARG1 = arg0_copy;
#   end
# end


## Arithmetic meta-instructions.
#################################################################

instruction add (?Rn 1 -1, ?Rn 1 -1, !R)
#  commutative
#  two-operands
  code
    JITTER_ARGN2 = JITTER_ARGN0 + JITTER_ARGN1;
  end
end

instruction addo (?Rn, ?Rn 1 -1, !R, ?f)
#  commutative
#  two-operands
  code
    JITTER_PLUS_BRANCH_FAST_IF_OVERFLOW (JITTER_ARGN2,
                                         JITTER_ARGN0, JITTER_ARGN1,
                                         JITTER_ARGF3);
  end
end

instruction sub (?Rn 0, ?Rn 1, !R)
#  two-operands
  code
    JITTER_ARGN2 = JITTER_ARGN0 - JITTER_ARGN1;
  end
end

instruction subo (?Rn, ?Rn 1, !R, ?f)
#  two-operands
  code
    JITTER_MINUS_BRANCH_FAST_IF_OVERFLOW (JITTER_ARGN2,
                                          JITTER_ARGN0, JITTER_ARGN1,
                                          JITTER_ARGF3);
  end
end

instruction mul (?Rn 2, ?Rn 2, !R)
##  two-operands
#  commutative
  code
    JITTER_ARGN2 = JITTER_ARGN0 * JITTER_ARGN1;
  end
end

instruction mulo (?Rn, ?Rn 2, !R, ?f)
##  two-operands
#  commutative
  code
    JITTER_TIMES_BRANCH_FAST_IF_OVERFLOW (JITTER_ARGN2,
                                          JITTER_ARGN0, JITTER_ARGN1,
                                          JITTER_ARGF3);
  end
end

instruction div (?Rn, ?Rn 2, !R)
  non-relocatable # FIXME: this should only be non-relocatable on some architectures.
#  two-operands
  code
    JITTER_ARGN2 = JITTER_ARGN0 / JITTER_ARGN1;
  end
end

instruction divo (?Rn, ?Rn 2, !R, ?f)
  # non-relocatable # FIXME: this should only be non-relocatable on some architectures.
##  two-operands
#  commutative
  code
    JITTER_DIVIDED_BRANCH_FAST_IF_OVERFLOW (JITTER_ARGN2,
                                            JITTER_ARGN0, JITTER_ARGN1,
                                            JITTER_ARGF3);
  end
end

instruction mod (?Rn, ?Rn 2, !R)
  non-relocatable # FIXME: this should only be non-relocatable on some architectures.
#  two-operands
  code
    JITTER_ARGN2 = JITTER_ARGN0 % JITTER_ARGN1;
  end
end

instruction modo (?Rn, ?Rn 2, !R, ?f)
  # non-relocatable # FIXME: this should only be non-relocatable on some architectures.
#  commutative
  code
    JITTER_REMAINDER_BRANCH_FAST_IF_OVERFLOW (JITTER_ARGN2,
                                              JITTER_ARGN0, JITTER_ARGN1,
                                              JITTER_ARGF3);
  end
end


# ## Bitwise meta-instructions.
# #################################################################

# instruction and (?Rn 1, ?Rn 1, !R)
# # commutative
# #  two-operands
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 & JITTER_ARGN1;
#   end
# end

# instruction or (?Rn, ?Rn, !R)
# # commutative
# #  two-operands
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 | JITTER_ARGN1;
#   end
# end

# instruction xor (?Rn, ?Rn, !R)
# # commutative
# #  two-operands
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 ^ JITTER_ARGN1;
#   end
# end

# instruction not (?Rn, !R)
#   code
#     JITTER_ARGN1 = ~ JITTER_ARGN0;
#   end
# end

# instruction lshift (?Rn, ?Rn, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 << JITTER_ARGN1;
#   end
# end

# instruction rshifta (?Rn, ?Rn, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 >> JITTER_ARGN1;
#   end
# end

# instruction rshiftl (?Rn, ?Rn, !R)
# #  two-operands
#   code
#     JITTER_ARGU2 = JITTER_ARGU0 >> JITTER_ARGN1;
#   end
# end


## Memory meta-instructions.
#################################################################

instruction loadwithbyteoffset (?R, ?Rn 0 BYTESPERWORD, !R)
#  two-operands # FIXME: not necessarily a good idea, but I want to test rewriting.
  code
    JITTER_ARGN2 = * (jitter_int*)((char*)JITTER_ARGP0 + JITTER_ARGN1);
  end
end

instruction loadwithwordoffset (?R, ?Rn 0 1 2, !R)
#  two-operands # FIXME: not necessarily a good idea, but I want to test rewriting.
  code
    JITTER_ARGN2 = * ((jitter_int*)JITTER_ARGP0 + JITTER_ARGN1);
  end
end

instruction storewithbyteoffset (?Rn, ?R, ?Rn 0 BYTESPERWORD)
  code
    * (jitter_int *)((char*)JITTER_ARGP1 + JITTER_ARGN2) = JITTER_ARGN0;
  end
end

instruction storewithwordoffset (?Rn, ?R, ?Rn 0 1 2)
  code
    * ((jitter_int*)JITTER_ARGP1 + JITTER_ARGN2) = JITTER_ARGN0;
  end
end


## Comparison meta-instructions.
#################################################################

# instruction eq (?Rn 0, ?Rn 0, !R)
# #  commutative
# #  two-operands
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 == JITTER_ARGN1) ? 1 : 0);
#   end
# end

# instruction ne (?Rn 0, ?Rn 0, !R)
#  commutative
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 != JITTER_ARGN1) ? 1 : 0);
#   end
# end

# instruction lt (?Rn 0, ?Rn 0, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 < JITTER_ARGN1) ? 1 : 0);
#   end
# end

# instruction gt (?Rn 0, ?Rn 0, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 > JITTER_ARGN1) ? 1 : 0);
#   end
# end

# instruction le (?Rn 0, ?Rn 0, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 <= JITTER_ARGN1) ? 1 : 0);
#   end
# end

# instruction ge (?Rn 0, ?Rn 0, !R)
# #  two-operands
#   code
#     JITTER_ARGN2 = ((JITTER_ARGN0 >= JITTER_ARGN1) ? 1 : 0);
#   end
# end


## Branching meta-instructions.
#################################################################

instruction b (?f)
  code
    JITTER_BRANCH_FAST (JITTER_ARGF0);
  end
end

instruction br (?R)
  branching
  code
    JITTER_BRANCH (JITTER_ARGP0);
  end
end

# # An instruction I will use to check that residuals are numbered correctly
# # even when fast labels are present.  Not useful in practice.
# instruction testfast (?n, ?f, ?f, ?n)
#   code
#     JITTER_BRANCH_FAST_IF_ZERO (JITTER_ARGN0, JITTER_ARGF1);
#     JITTER_BRANCH_FAST_IF_ZERO (JITTER_ARGN3, JITTER_ARGF2);
#     /*
#     if (JITTER_ARGN0 == 0)
#       JITTER_BRANCH_FAST (JITTER_ARGF1);
#     else if (JITTER_ARGN3 == 0)
#       JITTER_BRANCH_FAST (JITTER_ARGF2);
#     */
#   end
# end

# FIXME: useful for testing, but possibly to keep as well.
instruction bz (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_ZERO (JITTER_ARGN0, JITTER_ARGF1);
  end
end

# FIXME: useful for testing, but possibly to keep as well.
instruction bnz (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_NONZERO (JITTER_ARGN0, JITTER_ARGF1);
  end
end

# FIXME: useful for testing, but possibly to keep as well.
instruction bpos (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_POSITIVE (JITTER_ARGN0, JITTER_ARGF1);
  end
end

# FIXME: useful for testing, but possibly to keep as well.
instruction bnpos (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_NONPOSITIVE (JITTER_ARGN0, JITTER_ARGF1);
  end
end

# FIXME: useful for testing, but possibly to keep as well.
instruction bneg (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_NEGATIVE (JITTER_ARGN0, JITTER_ARGF1);
  end
end

# FIXME: useful for testing, but possibly to keep as well.
instruction bnneg (?Rn, ?f)
  code
    JITTER_BRANCH_FAST_IF_NONNEGATIVE (JITTER_ARGN0, JITTER_ARGF1);
  end
end

instruction beq (?Rn, ?Rn, ?f)
  # FIXME: I would like to be able to express some form of commutativity here,
  # as the two non-label arguments *are* commutative.
  code
    JITTER_BRANCH_FAST_IF_EQUAL (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bne (?Rn 0, ?Rn 0, ?f)
  # FIXME: I would like to be able to express some form of commutativity here,
  # as the two non-label arguments *are* commutative.
  code
    JITTER_BRANCH_FAST_IF_NOTEQUAL (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction blt (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_LESS_SIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bltu (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_LESS_UNSIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bgt (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_GREATER_SIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bgtu (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_GREATER_UNSIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction ble (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTGREATER_SIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bleu (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTGREATER_UNSIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bge (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTLESS_SIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bgeu (?Rn 0 1, ?Rn 0 1, ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTLESS_UNSIGNED (JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction band (?Rn, ?Rn 3 7, ?f)
  code
    JITTER_BRANCH_FAST_IF_AND (JITTER_ARGU0, JITTER_ARGU1, JITTER_ARGF2);
  end
end

instruction bnotand (?Rn, ?Rn 3 7, ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTAND (JITTER_ARGU0, JITTER_ARGU1, JITTER_ARGF2);
  end
end


## Advanced stack handling, and debugging.
#################################################################

# Push n integer values to the stack, each value equal to its depth after
# the instruction completes.
# This is convenient for testing stack effects, by first setting up a known
# stack configuration with this instruction and then using print-topmost
# twice, to print it out before and after a change.
instruction push-depths (?n)
  non-relocatable
  code
    /* Make place for JITTER_ARGN0 new elements on the stack. */
    int i;
    for (i = 0; i < JITTER_ARGN0; i ++)
      JITTER_PUSH_UNSPECIFIED_STACK ();
    /* Set each element at depth i to i . */
    for (i = 0; i < JITTER_ARGN0; i ++)
      {
        union jitter_word w = {.fixnum = i};
        JITTER_SET_AT_DEPTH_STACK (i, w);
      }
  end
end

# An alternative to push-depth, also for testing.
# Push n elements to the stack, the deepest one having value 0, the next
# having value 1, up to the top having value (n - 1) .
instruction push-increasing (?n)
  non-relocatable
  code
    /* Make place for JITTER_ARGN0 new elements on the stack. */
    int i;
    for (i = 0; i < JITTER_ARGN0; i ++)
      JITTER_PUSH_UNSPECIFIED_STACK ();
    /* Fill the new elements. */
    for (i = 0; i < JITTER_ARGN0; i ++)
      {
        union jitter_word w = {.fixnum = JITTER_ARGN0 - i - 1};
        JITTER_SET_AT_DEPTH_STACK (i, w);
      }
  end
end

# Print the topmost n values in the stack, without dropping them.  For
# each element print its depth between parentheses, the element, and two
# spaces.  Add a final newline.
instruction print-topmost (?n)
  non-relocatable
  code
    int i;
    for (i = JITTER_ARGN0 - 1; i >= 0; i --)
      printf (vmprefix_print_topmost_format_string, i,
              JITTER_AT_DEPTH_STACK (i).fixnum);
    printf (vmprefix_newline_string);
  end
end

instruction reverse (?n 0 1 2 3 4 5 6 7 8 9 10
                        11 12 13 14 15 16 17 18 19 20
                        21 22 23 24 25 26 27 28 29 30
                        31 32 33 34 35 36 37 38 39 40
                        41 42 43 44 45 46 47 48 49 50
                        51 52 53 54 55 56 57 58 59 60
                        61 62 63 64 65 66 67 68 69 70)
  code
    JITTER_REVERSE_STACK (JITTER_ARGN0);
  end
end

# Forth-style roll, taking the depth argument from the stack.
instruction rollforth ()
  code
    /* No check on the depth sign, which must be non-negative: this
       is unsafe code. */
    jitter_uint top_number = JITTER_TOP_STACK().fixnum;
    JITTER_DROP_STACK();
    JITTER_ROLL_STACK (top_number);
  end
end

# Forth-style -roll, like rollforth but using the -roll operation.
instruction mrollforth ()
  code
    /* No check on the depth sign, which must be non-negative: this
       is unsafe code. */
    jitter_uint top_number = JITTER_TOP_STACK().fixnum;
    JITTER_DROP_STACK();
    JITTER_MROLL_STACK (top_number);
  end
end


instruction roll (?n 0 1 2 3 4 5 6 7 8 9 10
                     11 12 13 14 15 16 17 18 19 20
                     21 22 23 24 25 26 27 28 29 30
                     31 32 33 34 35 36 37 38 39 40
                     41 42 43 44 45 46 47 48 49 50
                     51 52 53 54 55 56 57 58 59 60
                     61 62 63 64 65 66 67 68 69 70)
  code
    JITTER_ROLL_STACK (JITTER_ARGN0);
  end
end

instruction mroll (?n 0 1 2 3 4 5 6 7 8 9 10
                      11 12 13 14 15 16 17 18 19 20
                      21 22 23 24 25 26 27 28 29 30
                      31 32 33 34 35 36 37 38 39 40
                      41 42 43 44 45 46 47 48 49 50
                      51 52 53 54 55 56 57 58 59 60
                      61 62 63 64 65 66 67 68 69 70)
  code
    JITTER_MROLL_STACK (JITTER_ARGN0);
  end
end

instruction rot ()
  code
    JITTER_ROT_STACK ();
  end
end

instruction mrot ()
  code
    JITTER_MROT_STACK ();
  end
end

# The stack effect is ( a b c -- b a c ).
instruction quake ()
  code
    JITTER_QUAKE_STACK ();
  end
end

# The stack effect is ( a b -- b a b ).
instruction tuck ()
  code
    JITTER_TUCK_STACK ();
  end
end

instruction slide (?n 1 2 3, ?n 1 2 3 4 5)
  code
    JITTER_SLIDE_STACK (JITTER_ARGN0, JITTER_ARGN1);
  end
end

# Like slide above, but taking the arguments from the stack.
instruction slideforth ()
  code
    /* No sign or non-zero checks: this is unsafe code. */
    jitter_uint element_no = JITTER_UNDER_TOP_STACK ().fixnum;
    jitter_uint depth = JITTER_TOP_STACK ().fixnum;
    JITTER_DROP_STACK();
    JITTER_DROP_STACK();
    JITTER_SLIDE_STACK (element_no, depth);
  end
end

instruction whirl (?n 0 # Of course 0 is not useful in practice.
                      1 2 3)
  code
    JITTER_WHIRL_STACK (JITTER_ARGU0);
  end
end

# Like whirl above, but taking the arguments from the stack.
instruction whirlforth ()
  code
    /* No sign checks: this is unsafe code. */
    jitter_uint element_no = JITTER_TOP_STACK ().fixnum;
    JITTER_DROP_STACK();
    JITTER_WHIRL_STACK (element_no);
  end
end

instruction bulge (?n 0 # Of course 0 is not useful in practice.
                      1 2 3)
  code
    JITTER_BULGE_STACK (JITTER_ARGU0);
  end
end

# Like bulge above, but taking the arguments from the stack.
instruction bulgeforth ()
  code
    /* No sign checks: this is unsafe code. */
    jitter_uint element_no = JITTER_TOP_STACK ().fixnum;
    JITTER_DROP_STACK();
    JITTER_BULGE_STACK (element_no);
  end
end


## Safe-point, notification and signal instructions.
#################################################################

# This is not very useful to do in a VM instruction; in normal production
# code this would belong to initialization code, written directly in C.  But
# having a VM instruction is convenient for testing.
instruction install-signal-handler ()
  non-relocatable
  code
    /* Handle SIGINT, using either... */
#if defined (JITTER_HAVE_SIGACTION)
    /* ...The modern, better sigaction API... */
    struct sigaction action;
    sigaction (SIGINT, NULL, & action);
    action.sa_handler = jitter_my_signal_handler;
    sigaction (SIGINT, & action, NULL);
#else
    /* ...Or the traditional, inferior API. */
    signal (SIGINT, jitter_my_signal_handler);
#endif
  end
end

# Explicitly set the pending notification flag, in order to make the next safe
# point branch.
instruction set-pending ()
  code
    JITTER_PENDING_NOTIFICATIONS = true;
  end
end

# Clear the pending signal notification for every signal, plus the general
# pending notification flag.
instruction clear-pending ()
  # This instruction could be relocated, but it is not critical for performance
  # and its disassembly will almost always be a distraction.
  non-relocatable
  code
    int i;
    for (i = 0; i < JITTER_SIGNAL_NO; i ++)
      JITTER_PENDING_SIGNAL_NOTIFICATION (i) = false;
    JITTER_PENDING_NOTIFICATIONS = false;
  end
end

# Print a message naming each singal for which a notification is currently
# pending.
instruction print-pending-signals ()
  non-relocatable
  code
    int i;
    for (i = 0; i < JITTER_SIGNAL_NO; i ++)
      if (JITTER_PENDING_SIGNAL_NOTIFICATION (i))
        printf ("Handling signal %i\n", (int) i);
  end
end

# Do nothing if there are no pending notifications.  In the uncommon case where
# a notification is pending fast-branch to the given label.
instruction safe-point (?f)
  code
    JITTER_BRANCH_FAST_IF_NONZERO (JITTER_PENDING_NOTIFICATIONS, JITTER_ARGF0);
  end
end



## System meta-instructions.
#################################################################

instruction mallocwords (?Rn, !R)
  cold
  non-relocatable
  code
    JITTER_ARGP1 = jitter_xmalloc (sizeof (void *) * JITTER_ARGN0);
    /*
    size_t allocated_bytes = sizeof (void *) * JITTER_ARGN0;
    printf ("malloced: [%p, %p) (%lu bytes or %lu words)\n",
            JITTER_ARGP1, ((char*)JITTER_ARGP1) + allocated_bytes,
            (unsigned long) allocated_bytes,
            (unsigned long) (allocated_bytes / sizeof (void*)));
    fflush (stdout);
    */
  end
end

instruction exit (?Rn)
  cold
  non-relocatable
  code
    exit ((long)JITTER_ARGN0);
  end
end


# ## Debugging or special meta-instructions.
# #################################################################

instruction nop ()
  code
    /* Do nothing. */
  end
end

instruction printfixnum (?Rn)
  cold
  non-relocatable
  code
    printf (vmprefix_printfixnum_format_string, (jitter_int) JITTER_ARGN0);
/*
    printf (vmprefix_printfixnum_decimal_hex_format_string,
            (jitter_int) JITTER_ARGN0,
            (jitter_int) JITTER_ARGN0);
*/
    fflush (NULL);
  end
end

instruction hcf ()
  cold
  non-relocatable
  code
    printf (vmprefix_hcf_format_string);
    while (true)
      /* do nothing */;
  end
end

instruction random (!R)
  cold
  non-relocatable
  code
    JITTER_ARGN0 = rand ();
  end
end

instruction endvm ()
  cold
  branching # JITTER_EXIT is a branching macro.
  code
    JITTER_EXIT ();
  end
end


## End.
#################################################################
