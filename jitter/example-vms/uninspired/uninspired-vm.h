/* This machine-generated file includes source code from GNU Jitter.

   Copyright (C) 2016-2021 Luca Saiu
   Written by Luca Saiu

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>.


  Copyright (C) 2016, 2017, 2019, 2020, 2021 Luca Saiu
  Written by Luca Saiu

  This file is part of GNU Jitter.

  GNU Jitter is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GNU Jitter is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>.

*/

/* User-specified code, initial header part: beginning. */

/* User-specified code, initial header part: end */

/* VM library: main header file.

   Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Luca Saiu
   Written by Luca Saiu

   This file is part of GNU Jitter.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm.h" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.h
   template from Jitter, with added code implementing the uninspired VM. */




/* This multiple-inclusion guard is opened here in the template, and will be
   closed at the end of the generated code.  It is normal to find no matching
   #endif in the template file.  */
#ifndef UNINSPIRED_VM_H_
#define UNINSPIRED_VM_H_


/* This is the main VM header to use from hand-written code.
 * ************************************************************************** */

#include <stdio.h>
#include <stdbool.h>

#include <jitter/jitter.h>
#include <jitter/jitter-hash.h>
#include <jitter/jitter-stack.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-routine.h>
//#include <jitter/jitter-specialize.h> // FIXME: what about only declaring jitter_specialize in another header, and not including this?
#include <jitter/jitter-disassemble.h>
#include <jitter/jitter-vm.h>
#include <jitter/jitter-profile.h>
#include <jitter/jitter-data-locations.h>
#include <jitter/jitter-arithmetic.h>
#include <jitter/jitter-bitwise.h>
#include <jitter/jitter-signals.h>
#include <jitter/jitter-list.h>




/* Initialization and finalization.
 * ************************************************************************** */

/* Initialize the runtime state for the uninspired VM.  This needs to be called
   before using VM routines or VM states in any way. */
void
uninspired_initialize (void);

/* Finalize the runtime state, freeing some resources.  After calling this no
   use of VM routines or states is allowed.  It is possible to re-initialize
   after finalizing; these later re-initializations might be more efficient than
   the first initialization. */
void
uninspired_finalize (void);




/* State data structure initialization and finalization.
 * ************************************************************************** */

/* The machine state is separated into the backing and the more compact runtime
   data structures, to be allocated in registers as far as possible.  These are
   just a forward-declarations: the actual definitions are machine-generated. */
struct uninspired_state_backing;
struct uninspired_state_runtime;

/* A data structure containing both the backing and the runtime state.  This is
   a forward-declaration: the actual definition will come after both are
   defined. */
struct uninspired_state;

/* Initialize the pointed VM state data structure, or fail fatally.  The
   function definition is machine-generated, even if it may include user code.
   The state backing and runtime are initialized at the same time, and in fact
   the distinction between them is invisible to the VM user.
   The version not specifying a given number of slow registers per class
   sets slow registers to be initially zero. */
void
uninspired_state_initialize (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));
void
uninspired_state_initialize_with_slow_registers (struct uninspired_state *state,
                                               jitter_uint
                                               slow_register_no_per_class)
  __attribute__ ((nonnull (1)));

/* Finalize the pointed VM state data structure, or fail fatally.  The function
   definition is machine-generated, even if it may include user code.  The state
   backing and runtime are finalized at the same time. */
void
uninspired_state_finalize (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));

/* The make/destroy counterparts of the initialize/finalize functions above. */
struct uninspired_state *
uninspired_state_make (void)
  __attribute__ ((returns_nonnull));
struct uninspired_state *
uninspired_state_make_with_slow_registers (jitter_uint slow_register_no_per_class)
  __attribute__ ((returns_nonnull));
void
uninspired_state_destroy (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));

/* Reset the pointed VM state, restoring its initial content.  This is cheaper
   than finalising and re-initialising a state. */
void
uninspired_state_reset (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));




/* State data structure: iteration.
 * ************************************************************************** */

/* The header of a doubly-linked list linking every state for the uninspired VM
   together.  This global is automatically wrapped, and therefore also
   accessible from VM instruction code. */
extern struct jitter_list_header * const
uninspired_states;

/* A pointer to the current state, only accessible from VM code.  This is usable
   for pointer comparison when iterating over states. */
#define UNINSPIRED_OWN_STATE                           \
  ((struct uninspired_state *) jitter_original_state)

/* Given an l-value of type struct uninspired_state * (usually a variable name)
   expand to a for loop statement iterating over every existing uninspired state
   using the l-value as iteration variable.  The expansion will execute the
   statement immediately following the macro call with the l-value in scope;
   in order words the loop body is not a macro argument, but follows the macro
   use.
   The l-value may be evaluated an unspecified number of times.
   This macro is safe to use within VM instruction code.
   For example:
     struct uninspired_state *s;
     UNINSPIRED_FOR_EACH_STATE (s)
       printf ("This is a state: %p\n", s); // (but printf unsafe in VM code) */
#define UNINSPIRED_FOR_EACH_STATE(jitter_state_iteration_lvalue)     \
  for ((jitter_state_iteration_lvalue)                             \
          = uninspired_states->first;                                \
       (jitter_state_iteration_lvalue)                             \
          != NULL;                                                 \
       (jitter_state_iteration_lvalue)                             \
         = (jitter_state_iteration_lvalue)->links.next)            \
    /* Here comes the body supplied by the user: no semicolon. */




/* Mutable routine initialization.
 * ************************************************************************** */

/* Return a freshly-allocated empty mutable routine for the uninspired VM. */
struct jitter_mutable_routine*
uninspired_make_mutable_routine (void)
  __attribute__ ((returns_nonnull));

/* Mutable routine finalization is actually VM-independent, but a definition of
   uninspired_destroy_mutable_routine is provided below as a macro, for cosmetic
   reasons. */


/* Mutable routines: code generation C API.
 * ************************************************************************** */

/* This is the preferred way of adding a new VM instruction to a pointed
   routine, more efficient than uninspired_mutable_routine_append_instruction_name
   even if only usable when the VM instruction opcode is known at compile time.
   The unspecialized instruction name must be explicitly mangled by the user as
   per the rules in jitterc_mangle.c .  For example an instruction named foo_bar
   can be added to the routine pointed by p with any one of
     uninspired_mutable_routine_append_instruction_name (p, "foo_bar");
   ,
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION (p, foo_ubar);
   , and
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID 
        (p, uninspired_meta_instruction_id_foo_ubar);
   .
   The string "foo_bar" is not mangled, but the token foo_ubar is. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION(                 \
          routine_p, instruction_mangled_name_root)                  \
  do                                                                 \
    {                                                                \
      jitter_mutable_routine_append_meta_instruction                 \
         ((routine_p),                                               \
          uninspired_meta_instructions                                 \
          + JITTER_CONCATENATE_TWO(uninspired_meta_instruction_id_,    \
                                   instruction_mangled_name_root));  \
    }                                                                \
  while (false)

/* Append the unspecialized instruction whose id is given to the pointed routine.
   The id must be a case of enum uninspired_meta_instruction_id ; such cases have
   a name starting with uninspired_meta_instruction_id_ .
   This is slightly less convenient to use than UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION
   but more general, as the instruction id is allowed to be a non-constant C
   expression. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID(_jitter_routine_p,       \
                                                       _jitter_instruction_id)  \
  do                                                                            \
    {                                                                           \
      jitter_mutable_routine_append_instruction_id                              \
         ((_jitter_routine_p),                                                  \
          uninspired_meta_instructions,                                           \
          UNINSPIRED_META_INSTRUCTION_NO,                                         \
          (_jitter_instruction_id));                                            \
    }                                                                           \
  while (false)

/* This is the preferred way of appending a register argument to the instruction
   being added to the pointed routine, more convenient than directly using
   uninspired_mutable_routine_append_register_id_parameter , even if only usable
   when the register class is known at compile time.  Here the register class is
   only provided as a letter, but both the routine pointer and the register
   index are arbitrary C expressions.
   For example, in
     UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER (p, r,
                                                         variable_to_index (x));
   the second macro argument "r" represents the register class named "r", and
   not the value of a variable named r. */
#define UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER(routine_p,     \
                                                           class_letter,  \
                                                           index)         \
  do                                                                      \
    {                                                                     \
      uninspired_mutable_routine_append_register_parameter                  \
         ((routine_p),                                                    \
          & JITTER_CONCATENATE_TWO(uninspired_register_class_,              \
                                   class_letter),                         \
          (index));                                                       \
    }                                                                     \
  while (false)




/* Routine unified API: initialization.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: initialization", and the
   implementation of the unified routine API in <jitter/jitter-routine.h> . */

#define uninspired_make_routine uninspired_make_mutable_routine




/* Routine unified API: code generation C API.
 * ************************************************************************** */

/* See the comments above in "Mutable routines: code generation C API". */

#define UNINSPIRED_ROUTINE_APPEND_INSTRUCTION  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION
#define UNINSPIRED_ROUTINE_APPEND_INSTRUCTION_ID  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_INSTRUCTION_ID
#define UNINSPIRED_ROUTINE_APPEND_REGISTER_PARAMETER  \
  UNINSPIRED_MUTABLE_ROUTINE_APPEND_REGISTER_PARAMETER




/* Array: special-purpose data.
 * ************************************************************************** */

/* The Array is a convenient place to store special-purpose data, accessible in
   an efficient way from a VM routine.
   Every item in special-purpose data is thread-local. */

/* The special-purpose data struct.  Every Array contains one of these at unbiased
   offset UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET from the unbiased
   beginning of the array.
   This entire struct is aligned to at least sizeof (jitter_int) bytes.  The
   entire struct is meant to be always accessed through a pointer-to-volatile,
   as its content may be altered from signal handlers and from different
   threads.  In particualar the user should use the macro
     UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   defined below and the macros defined from it as accessors.
   VM code accessing special-purpose data for its own state should use
     UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA
   and the macros defined from it. */
struct jitter_special_purpose_state_data
{
  /* Notification fields.
   * ***************************************************************** */

  /* This is a Boolean flag, held as a word-sized datum so as to ensure
     atomicity in access.  It is also aligned to at least sizeof (jitter_int)
     bytes.
     Non-zero means that there is at least one notification pending, zero means
     that there are no notifications.  The flag specifies no other details: it
     is meant to be fast to check, with detailed information about each pending
     notification available elsewhere.
     It is the receiver's responsibility to periodically poll for notifications
     in application-specific "safe-points":
     A check can be inserted, for example, in all of these program points:
     a) at every backward branch;
     b) at every procedure entry;
     c) right after a call to each blocking primitive (as long as primitives
       can be interrupted).
     Safe-point checks are designed to be short and fast in the common case.  In
     the common case no action is required, and the VM routine should simply
     fall through.  If an action is required then control should branch off to a
     handler, where the user may implement the required behavior.
     It is mandatory that, as long as notifications can arrive, this field
     is reset to zero (when handling pending notifications) only by a thread
     running VM code in the state containing this struct.
     Other threads are allowed to set this to non-zero, in order to send a
     notification.  */
  jitter_int pending_notifications;

  /* Information about pending signal notifications.  If any signal is pending
     then pending_notifications must also be set, so that a notification check
     can always just quickly check pending_notifications, and then look at more
     details (including in pending_signal_notifications) only in the rare case
     of pending_notifications being true. */
  struct jitter_signal_notification *pending_signal_notifications;


  /* Profiling instrumentation fields.
   * ***************************************************************** */
  struct jitter_profile_runtime profile_runtime;
};




/* The Array and volatility.
 * ************************************************************************** */

/* Some fields of The Array, seen from VM code, are meant to be volatile, since
   they can be set by signal handlers or by other threads.  However it is
   acceptable to not see such changes immediately after they occur (notifications
   will get delayed, but not lost) and always accessing such data through a
   volatile struct is suboptimal.

   Non-VM code does need a volatile qualifier.

   Advanced dispatches already need a trick using inline assembly to make the
   base pointer (a biased pointer to The Array beginning) appear to
   spontaneously change beween instruction.  That is sufficient to express the
   degree of volatility required for this purpose.
   Simple dispatches, on targets where inline assembly may not be available at
   all, will use an actual volatile qualifier. */
#if defined (JITTER_DISPATCH_SWITCH)               \
    || defined (JITTER_DISPATCH_DIRECT_THREADING)
# define UNINSPIRED_ARRAY_VOLATILE_QUALIFIER volatile
#elif defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
      || defined (JITTER_DISPATCH_NO_THREADING)
# define UNINSPIRED_ARRAY_VOLATILE_QUALIFIER /* nothing */
#else
# error "unknown dispatch: this should not happen"
#endif /* dispatch conditional */




/* Array element access: residuals, transfers, slow registers, and more.
 * ************************************************************************** */

/* In order to cover a wider range of addresses with simple base + register
   addressing the base does not necessarily point to the beginning of the Array;
   instead the base points to the beginning of the Array plus JITTER_ARRAY_BIAS
   bytes.
   FIXME: define the bias as a value appropriate to each architecture.  I think
   I should just move the definition to jitter-machine.h and provide a default
   here, in case the definition is missing on some architecture. */

/* FIXME: Horrible, horrible, horrible temporary workaround!

   This is a temporary workaround, very ugly and fragile, to compensate
   a limitation in jitter-specialize.c , which I will need to rewrite anyway.
   The problem is that jitter-specialize.c patches snippets to load non-label
   residuals in a VM-independent way based only on slow-register/memory residual
   indices, which is incorrect.  By using this particular bias I am cancelling
   that error.
   Test case, on a machine having only one register residual and a VM having just
     one fast register:
     [luca@moore ~/repos/jitter/_build/native-gcc-9]$ Q=bin/uninspired--no-threading; make $Q && echo 'mov 2, %r1' | libtool --mode=execute valgrind $Q --disassemble - --print-locations
   If this bias is wrong the slow-register accesses in mov/nR/%rR will use two
   different offsets, one for reading and another for writing.  With this
   workaround they will be the same.
   Good, with workadound (biased offset 0x0 from the base in %rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x20 (21 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 20 00 00 00 	movq   $0x20,0x0(%rbx)
        0x0000000004effb3e 48 8b 13             	movq   (%rbx),%rdx
        0x0000000004effb41 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1)
   Bad, with JITTER_ARRAY_BIAS defined as zero: first write at 0x0(%rbx)
                                                then read at 0x10(%rbx):
    # 0x4a43d38: mov/nR/%rR 0x2, 0x30 (22 bytes):
        0x0000000004effb30 41 bc 02 00 00 00    	movl   $0x2,%r12d
        0x0000000004effb36 48 c7 43 00 30 00 00 00 	movq   $0x30,0x0(%rbx)
        0x0000000004effb3e 48 8b 53 10          	movq   0x10(%rbx),%rdx
        0x0000000004effb42 4c 89 24 13          	movq   %r12,(%rbx,%rdx,1) */
#define JITTER_ARRAY_BIAS \
  (sizeof (struct jitter_special_purpose_state_data))
//#define JITTER_ARRAY_BIAS //0//(((jitter_int) 1 << 15))//(((jitter_int) 1 << 31))//0//0//16//0

/* Array-based globals are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define UNINSPIRED_GLOBAL_NO 0

/* Transfer registers are not implemented yet.  For the purpose of computing
   Array offsets I will say they are zero. */
#define UNINSPIRED_TRANSFER_REGISTER_NO 0

/* Define macros holding offsets in bytes for the first global, memory residual
   and transfer register, from an initial Array pointer.
   In general we have to keep into account:
   - globals (word-sized);
   - special-purpose state data;
   - memory residuals (word-sized);
   - transfer registers (word-sized);
   - slow registers (uninspired_any_register-sized and aligned).
   Notice that memory
   residuals (meaning residuals stored in The Array) are zero on dispatching
   modes different from no-threading.  This relies on
   UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY , defined below, which in its turn depends
   on UNINSPIRED_MAX_RESIDUAL_ARITY, which is machine-generated. */
#define UNINSPIRED_FIRST_GLOBAL_UNBIASED_OFFSET  \
  0
#define UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET  \
  (UNINSPIRED_FIRST_GLOBAL_UNBIASED_OFFSET                     \
   + sizeof (jitter_int) * UNINSPIRED_GLOBAL_NO)
#define UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET   \
  (UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   + sizeof (struct jitter_special_purpose_state_data))
#define UNINSPIRED_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
  (UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET               \
   + sizeof (jitter_int) * UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY)
#define UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET          \
  JITTER_NEXT_MULTIPLE_OF_POSITIVE                            \
     (UNINSPIRED_FIRST_TRANSFER_REGISTER_UNBIASED_OFFSET        \
      + sizeof (jitter_int) * UNINSPIRED_TRANSFER_REGISTER_NO,  \
      sizeof (union uninspired_any_register))

/* Expand to the offset of the special-purpose data struct from the Array
   biased beginning. */
#define UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_OFFSET       \
  (UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET   \
   - JITTER_ARRAY_BIAS)

/* Given an expression evaluating to the Array unbiased beginning, expand to
   an expression evaluating to a pointer to its special-purpose data.
   This is convenient for accessing special-purpose data from outside the
   state -- for example, to set the pending notification flag for another
   thread.
   There are two versions of this feature:
     UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
   is meant to be used to access state data for some other thread, or in
   general out of VM code.
     UNINSPIRED_OWN_SPECIAL_PURPOSE_STATE_DATA
   is for VM code accessing its own special-purpose data. */
#define UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE(qualifier,      \
                                                             array_address)  \
  ((qualifier struct jitter_special_purpose_state_data *)                    \
   (((char *) (array_address))                                               \
    + UNINSPIRED_SPECIAL_PURPOSE_STATE_DATA_UNBIASED_OFFSET))
#define UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA(array_address)       \
  UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE (volatile,         \
                                                        (array_address))
#define UNINSPIRED_OWN_SPECIAL_PURPOSE_STATE_DATA          \
  UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA_PRIVATE   \
     (UNINSPIRED_ARRAY_VOLATILE_QUALIFIER,                 \
      ((char *) jitter_array_base) - JITTER_ARRAY_BIAS)

/* Given a state pointer, expand to an expression evaluating to a pointer to
   the state's special-purpose data.  This is meant for threads accessing
   other threads' special-purpose data, typically to set notifications. */
#define UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA(state_p)  \
  (UNINSPIRED_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA                \
     ((state_p)->uninspired_state_backing.jitter_array))

/* Given a state pointer, expand to an expression evaluating to the
   pending_notification field for the state as an l-value.  This is meant for
   threads sending notifications to other threads. */
#define UNINSPIRED_STATE_TO_PENDING_NOTIFICATIONS(state_p)   \
  (UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)  \
     ->pending_notifications)

/* Given a state pointer and a signal, expand to an l-value evaluating to a the
   pending field of the struct jitter_signal_notification element for the given
   signal in the pointed state.  This is meant for threads sending signal
   notifications to other threads and for C handler function. */
#define UNINSPIRED_STATE_AND_SIGNAL_TO_PENDING_SIGNAL_NOTIFICATION(state_p,    \
                                                                 signal_id)  \
  (((UNINSPIRED_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)                   \
       ->pending_signal_notifications)                                        \
    + (signal_id))->pending)


/* Expand to the offset of the i-th register of class c in bytes from the Array
   beginning.
   The c argument must be a literal C (one-character) identifier.
   The i argument should always be a compile-time constant for performance, and
   it is in generated code.
   The i-th c-class register must be slow, otherwise the offset will be
   incorrect -- in fact fast registers are, hopefully, not in memory at all.

   Slow registers come in the Array ordered first by index, then by class.  For
   example if there are three classes "r" with 4 fast registers, "f" with 7 fast
   registers and "q" with 2 fast registers, slow registers can be accessed in
   this order:
     r4, f7, q2, r5, r8, q3, r6, r9, q4, and so on.
   Each contiguous group of slow registers spanning every class and starting
   from the first class (here for example <r5, r6, q3>) is called a "rank".
   This organization is convenient since changing the number of slow registers
   doesn't invalidate any offset computed in the past: the Array can simply be
   resized and its base pointer updated, without changing the code accessing it.

   This relies on macro such as UNINSPIRED_REGISTER_CLASS_NO and
   UNINSPIRED_REGISTER_?_FAST_REGISTER_NO and , defined below in machine-generated
   code. */
#define UNINSPIRED_SLOW_REGISTER_UNBIASED_OFFSET(c, i)                     \
  (UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union uninspired_any_register)                               \
      * (UNINSPIRED_REGISTER_CLASS_NO                                      \
         * ((i) - JITTER_CONCATENATE_THREE(UNINSPIRED_REGISTER_, c,        \
                                           _FAST_REGISTER_NO))           \
         + JITTER_CONCATENATE_THREE(UNINSPIRED_REGISTER_, c, _CLASS_ID))))

/* Expand to the offset of the i-th register of class c in bytes from the base,
   keeping the bias into account. */
#define UNINSPIRED_SLOW_REGISTER_OFFSET(c, i)                              \
  (UNINSPIRED_SLOW_REGISTER_UNBIASED_OFFSET(c, i) - JITTER_ARRAY_BIAS)

/* Expand to the Array size in bytes, assuming the given number of slow
   registers per class.  This is an allocation size, ignoring the bias. */
#define UNINSPIRED_ARRAY_SIZE(slow_register_per_class_no)                  \
  (UNINSPIRED_FIRST_SLOW_REGISTER_UNBIASED_OFFSET                          \
   + (sizeof (union uninspired_any_register)                               \
      * UNINSPIRED_REGISTER_CLASS_NO                                       \
      * (slow_register_per_class_no)))




/* Residual access.
 * ************************************************************************** */

/* How many residuals we can have at most in memory, which is to say,
   without counting residuals kept in reserved registers.

   Implementation note: it would be wrong here to use a CPP conditional based on
   the value of UNINSPIRED_MAX_RESIDUAL_ARITY , as I was doing in a preliminary
   version.  That lead to a tricky bug, since UNINSPIRED_MAX_RESIDUAL_ARITY ,
   which is defined below but is not yet available here, simply counted as 0
   for the purposes of evaluating the CPP condititional. */
#ifdef JITTER_DISPATCH_NO_THREADING
  /* We are using no-threading dispatch.  If there are no more residuals
     than reserved residual registers then we never need to keep any in
     memory.  Otherwise we need to keep as many residuals in memory as the
     total number of residuals minus how many registers are reserved for
     them. */
# define UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY                          \
    ((UNINSPIRED_MAX_RESIDUAL_ARITY <= JITTER_RESIDUAL_REGISTER_NO)    \
     ? 0                                                             \
     : (UNINSPIRED_MAX_RESIDUAL_ARITY - JITTER_RESIDUAL_REGISTER_NO))
#else // Not no-threading.
  /* No registers are reserved for residuals in this dispatch; even if
     in fact all residuals are memory residuals they don't count here, since
     residuals are not held in The Array in this dispatch. */
# define UNINSPIRED_MAX_MEMORY_RESIDUAL_ARITY  \
  0
#endif // #ifdef JITTER_DISPATCH_NO_THREADING

#ifdef JITTER_DISPATCH_NO_THREADING
/* Expand to the offset from the base, in bytes, of the i-th residual.  The
   given index must be greater than or equal to JITTER_RESIDUAL_REGISTER_NO;
   residuals with indices lower than that number are not stored in The Array
   at all.
   This is not useful with any of the other dispatches, where residuals
   directly follow each VM instruction opcode or thread.  For good performance i
   should always be a compile-time constant, as it is in machine-generated
   code.
   Residuals always have the size of a jitter word, even if some register class
   may be wider. */
/* FIXME: if later I use a different policy than simply checking
   JITTER_RESIDUAL_REGISTER_NO to decide how many residuals to keep in
   registers, then I have to change this or meet very nasty bugs. */
# define UNINSPIRED_RESIDUAL_UNBIASED_OFFSET(i)                      \
    (UNINSPIRED_FIRST_MEMORY_RESIDUAL_UNBIASED_OFFSET                \
     + (sizeof (jitter_int) * (i - JITTER_RESIDUAL_REGISTER_NO)))
# define UNINSPIRED_RESIDUAL_OFFSET(i)  \
    (UNINSPIRED_RESIDUAL_UNBIASED_OFFSET(i) - JITTER_ARRAY_BIAS)
#endif // #ifdef JITTER_DISPATCH_NO_THREADING



/* Mutable routine text frontend.
 * ************************************************************************** */

/* Parse VM code from the given file or string into the pointed VM routine,
   which is allowed but not required to be empty.
   These are simple wrappers around functions implemented in the Bison file. */
void
uninspired_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
  __attribute__ ((nonnull (1, 2)));




/* Unified routine text frontend.
 * ************************************************************************** */

/* The C wrappers for the ordinary API can be reused for the unified API, since
   it internally works with mutable routines. */
#define uninspired_parse_routine_from_file_star  \
  uninspired_parse_mutable_routine_from_file_star
#define uninspired_parse_routine_from_file  \
  uninspired_parse_mutable_routine_from_file
#define uninspired_parse_routine_from_string  \
  uninspired_parse_mutable_routine_from_string




/* Machine-generated data structures.
 * ************************************************************************** */

/* Declare a few machine-generated data structures, which together define a VM. */

/* Threads or pointers to native code blocks of course don't exist with
   switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
/* Every possible thread, indexed by enum jitter_specialized_instruction_opcode .
   This is used at specialization time, and the user shouldn't need to touch
   it. */
extern const jitter_thread *
uninspired_threads;

/* VM instruction end label.  These are not all reachable at run time, but
   having them in a global array might prevent older GCCs from being too clever
   in reordering blocks. */
extern const jitter_thread *
uninspired_thread_ends;

/* The size, in chars, of each thread's native code.  The elements are in the
   same order of uninspired_threads.  Sizes could conceptually be of type size_t ,
   but in order to be defensive I'm storing pointer differences as signed
   values, so that we may catch compilation problems: if any VM instruction end
   *precedes* its VM instruction beginning, then the compiler has reordered
   labels, which would have disastrous effects with replicated code. */
extern const long *
uninspired_thread_sizes;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* This is defined in the machine-generated vm/meta-instructions.c . */
extern struct jitter_hash_table
uninspired_meta_instruction_hash;

/* An array specifying every existing meta-instruction, defined in the order of
   enum uninspired_meta_instruction_id .  This is defined in vm/meta-instructions.c ,
   which is machine-generated. */
extern const struct jitter_meta_instruction
uninspired_meta_instructions [];

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
extern const int
uninspired_specialized_instruction_to_unspecialized_instruction [];

/* How many residual parameters each specialized instruction has.  The
   actual array definition is machine-generated. */
extern const size_t
uninspired_specialized_instruction_residual_arities [];

/* An array of bitmasks, one per specialized instruction.  Each bitmask holds
   one bit per residual argument, counting from the least significant (the first
   residual arg maps to element & (1 << 0), the second to element & (1 << 1),
   and so on).
   Each bit is 1 if and only if the corresponding residual argument is a label
   or a fast label.
   Only residual arguments are counted: for example a specialized instruction
   foo_n1_lR_r2 would have a mask with the *first* bit set. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
uninspired_specialized_instruction_label_bitmasks [];

/* Like uninspired_specialized_instruction_label_bitmasks , but for fast labels
   only.
   The actual definition is conditionalized so as to appear only when
   needed according to the dispatch. */
extern const unsigned long // FIXME: possibly use a shorter type when possible
uninspired_specialized_instruction_fast_label_bitmasks [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is relocatable. */
extern const bool
uninspired_specialized_instruction_relocatables [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a caller. */
extern const bool
uninspired_specialized_instruction_callers [];

/* An array of booleans in which each element is true iff the specialized
   instruction whose opcode is the index is a callee. */
extern const bool
uninspired_specialized_instruction_callees [];

/* This big array of strings contains the name of each specialized instruction,
   in the order of enum uninspired_specialized_instruction_opcode . */
extern const char* const
uninspired_specialized_instruction_names [];


/* A pointer to a struct containing const pointers to the structures above, plus
   sizes; there will be only one instance of this per VM, machine-generated.
   Each program data structure contains a pointer to that instance, so that
   VM-independent functions, given a program, will have everything needed to
   work.  The one instance of struct jitter_vm for the uninspired VM. */
extern struct jitter_vm * const
uninspired_vm;

/* A pointer to a struct containing VM-specific parameters set in part when
   calling jitterc and in part when compiling the generated C code, such as the
   dispatch and the number of fast registers.  The data is fully
   initialized only after a call to uninspired_initialize . */
extern const
struct jitter_vm_configuration * const
uninspired_vm_configuration;




/* Compatibility macros.
 * ************************************************************************** */

/* It is convenient, for future extensibility, to expose an interface in which
   some VM-independent functions and data structures actually look as if they
   were specific to the user VM. */

/* What the user refers to as struct uninspired_mutable_routine is actually a
   struct jitter_mutable_routine , whose definition is VM-independent. */
#define uninspired_mutable_routine jitter_mutable_routine

/* Same for executable routines. */
#define uninspired_executable_routine jitter_executable_routine

/* Same for unified routines. */
#define uninspired_routine jitter_routine

/* Destroy a non-executable routine (routine initialization is actually
   VM-specific). */
#define uninspired_destroy_mutable_routine jitter_destroy_mutable_routine

/* Destroy a unified routine. */
#define uninspired_destroy_routine jitter_destroy_routine

/* Pin a unified routine. */
#define uninspired_pin_routine jitter_pin_routine

/* Unpin a unified routine. */
#define uninspired_unpin_routine jitter_unpin_routine

/* Print VM configuration. */
#define uninspired_print_vm_configuration jitter_print_vm_configuration

/* Generic routine construction API. */
#define uninspired_label \
  jitter_label
#define uninspired_fresh_label \
  jitter_fresh_label

/* Mutable routine option API. */
#define uninspired_set_mutable_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define uninspired_set_mutable_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define uninspired_set_mutable_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define uninspired_set_mutable_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define uninspired_set_mutable_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: ordinary API. */
#define uninspired_mutable_routine_print \
  jitter_mutable_routine_print
#define uninspired_executable_routine_disassemble \
  jitter_executable_routine_disassemble

/* Mutable routine construction API. */
#define uninspired_mutable_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define uninspired_mutable_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define uninspired_mutable_routine_append_label \
  jitter_mutable_routine_append_label
#define uninspired_mutable_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define uninspired_mutable_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define uninspired_mutable_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define uninspired_mutable_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define uninspired_mutable_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define uninspired_mutable_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define uninspired_mutable_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define uninspired_mutable_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define uninspired_destroy_executable_routine \
  jitter_destroy_executable_routine

/* Making executable routines from mutable routines. */
#define uninspired_make_executable_routine \
  jitter_make_executable_routine

/* Unified routine option API. */
#define uninspired_set_routine_option_slow_literals_only \
  jitter_set_mutable_routine_option_slow_literals_only
#define uninspired_set_routine_option_slow_registers_only \
  jitter_set_mutable_routine_option_slow_registers_only
#define uninspired_set_routine_option_slow_literals_and_registers_only \
  jitter_set_mutable_routine_option_slow_literals_and_registers_only
#define uninspired_set_routine_option_add_final_exitvm \
  jitter_set_mutable_routine_option_add_final_exitvm
#define uninspired_set_routine_option_optimization_rewriting \
  jitter_set_mutable_routine_option_optimization_rewriting

/* Printing and disassembling: unified API.  These do not follow the pattern of
   the rest: wrapped identifiers here are the names of C functions specific to
   the unified API */
#define uninspired_routine_print \
  jitter_routine_print
#define uninspired_routine_disassemble \
  jitter_routine_disassemble

/* Unified routine construction API. */
#define uninspired_routine_append_instruction_name \
  jitter_mutable_routine_append_instruction_name
#define uninspired_routine_append_meta_instruction \
  jitter_mutable_routine_append_meta_instruction
#define uninspired_routine_append_label \
  jitter_mutable_routine_append_label
#define uninspired_routine_append_symbolic_label \
  jitter_mutable_routine_append_symbolic_label
#define uninspired_routine_append_register_parameter \
  jitter_mutable_routine_append_register_parameter
#define uninspired_routine_append_literal_parameter \
  jitter_mutable_routine_append_literal_parameter
#define uninspired_routine_append_signed_literal_parameter \
  jitter_mutable_routine_append_signed_literal_parameter
#define uninspired_routine_append_unsigned_literal_parameter \
  jitter_mutable_routine_append_unsigned_literal_parameter
#define uninspired_routine_append_pointer_literal_parameter \
  jitter_mutable_routine_append_pointer_literal_parameter
#define uninspired_routine_append_label_parameter \
  jitter_mutable_routine_append_label_parameter
#define uninspired_routine_append_symbolic_label_parameter \
  jitter_mutable_routine_append_symbolic_label_parameter

/* Mutable routine destruction. */
#define uninspired_destroy_routine                                           \
  /* This does not follow the pattern of the rest: the wrapped identifier  \
     here is the name of a C function specific to the unified API. */      \
  jitter_destroy_routine

/* The unified API has no facility to explicitly make executable routines: their
   very existence is hidden.  For this reason some of the macros above, such
   uninspired_make_executable_routine, have no unified counterpart here. */

/* Defects and replacements. */
void
uninspired_defect_print_summary (jitter_print_context cx)
  __attribute__ ((nonnull (1)));
void
uninspired_defect_print (jitter_print_context cx,
                       unsigned indentation_column_no)
  __attribute__ ((nonnull (1)));
void
uninspired_defect_print_replacement_table (jitter_print_context cx,
                                         unsigned indentation_column_no)
  __attribute__ ((nonnull (1)));

/* Profiling.  Apart from uninspired_state_profile, which returns a pointer to
   the profile within a pointed state structure, everything else here has the
   same API as the functionality in jitter/jitter-profile.h , without the VM
   pointer.
   Notice that this API does nothing useful onless at least one of the CPP
   macros UNINSPIRED_PROFILE_COUNT or UNINSPIRED_PROFILE_SAMPLE is defined. */
#define uninspired_profile_runtime  \
  jitter_profile_runtime /* the struct name */
#define uninspired_profile  \
  jitter_profile /* the struct name */
// FIXME: no: distinguish between struct jitter_profile_runtime and its user-friendly variant
struct jitter_profile_runtime *
uninspired_state_profile_runtime (struct uninspired_state *s)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct uninspired_profile_runtime*
uninspired_profile_runtime_make (void)
  __attribute__ ((returns_nonnull));
void
uninspired_profile_runtime_destroy (struct uninspired_profile_runtime *p)
  __attribute__ ((nonnull (1)));
#define uninspired_profile_destroy jitter_profile_destroy
void
uninspired_profile_runtime_clear (struct uninspired_profile_runtime *p)
  __attribute__ ((nonnull (1)));
void
uninspired_profile_runtime_merge_from (struct uninspired_profile_runtime *to,
                                     const struct uninspired_profile_runtime *from)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_profile_runtime_merge_from_state (struct uninspired_profile_runtime *to,
                                   const struct uninspired_state *from_state)
  __attribute__ ((nonnull (1, 2)));
struct uninspired_profile *
uninspired_profile_unspecialized_from_runtime
   (const struct uninspired_profile_runtime *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
struct uninspired_profile *
uninspired_profile_specialized_from_runtime (const struct uninspired_profile_runtime
                                           *p)
  __attribute__ ((returns_nonnull, nonnull (1)));
void
uninspired_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct uninspired_profile_runtime *p)
  __attribute__ ((nonnull (1, 2)));
void
uninspired_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct uninspired_profile_runtime
                                            *p)
  __attribute__ ((nonnull (1, 2)));




/* Register class types.
 * ************************************************************************** */

/* Return a pointer to a statically allocated register class descriptor, given
   the register class character, or NULL if the character does not represent a
   valid register class.

   A constant array indexed by a character would have been more efficient, but
   relying on character ordering is not portable, at least in theory.  A
   non-constant array could be initialized in a portable way, but that would
   probably not be worth the trouble. */
const struct jitter_register_class *
uninspired_register_class_character_to_register_class (char c)
  __attribute__ ((pure));


/* A constant array of constant pointers to every existing register class
   descriptor, ordered by class id; each pointer within the array refers the
   only existing class descriptor for its class.  The number of elements is
   UNINSPIRED_REGISTER_CLASS_NO , but that is not declared because the definition
   of UNINSPIRED_REGISTER_CLASS_NO comes later in generated code.

   This is useful when the user code enumerates every existing register class,
   particularly for debugging. */
extern const struct jitter_register_class * const
uninspired_regiter_classes [];




/* Array re-allocation.
 * ************************************************************************** */

/* Make the Array in the pointed state large enough to accommodate the given
   number of slow reigsters per class, adjusting the Array pointer as needed
   and recording information about the new size in the state; change nothing
   if the array is already large enough.  Return the new base.
   For example passing 3 as the value of slow_register_no would make
   place for three slow registers per register class: if the current VM had two
   classes 'r' and 'f' than the function would ensure that the Array can hold
   three 'r' and three 'f' slow registers, independently from the number
   of fast 'r' or 'f' registers.
   Any new elements allocated in the Array are left uninitialized, but its old
   content remains valid. */
char *
uninspired_make_place_for_slow_registers (struct uninspired_state *s,
                                        jitter_int slow_register_no_per_class)
  __attribute__ ((noinline));




/* **************************************************************************
 * Evrything following this point is for internal use only.
 * ************************************************************************** */




/* Replacement tables.
 * ************************************************************************** */

/* It is harmless to declare these unconditionally, even if they only used when
   defect replacement is available.  See jitter/jitter-defect.h . */

/* The worst-case replacement table.  This is a global constant array, having
   one element per specialised instruction. */
extern const jitter_uint
uninspired_worst_case_replacement_table [];

/* An array whose first defective_specialized_instruction_no elements contain
   the specialized_instruction_ids of defective instructions; the remaining
   elements are set to -1.  This array is initialised by
   jitter_fill_replacement_table . */
extern jitter_int
uninspired_defective_specialized_instructions [];

/* The actual replacement table, to be filled at initialization time. */
extern jitter_uint
uninspired_replacement_table [];

/* An array of specialized instruction ids, holding the specialized instruction
   ids of any non-replacement specialized instruction which is "call-related",
   which is to say any of caller, callee or returning.  These are useful for
   defect replacements, since any defect in even just one of them requires
   replacing them all. */
extern const jitter_uint
uninspired_call_related_specialized_instruction_ids [];

/* The size of uninspired_call_related_specialized_instruction_ids in elements. */
extern const jitter_uint
uninspired_call_related_specialized_instruction_id_no;

/* An array of Booleans in which each element is true iff the specialized
   instruction whose opcode is the index is call-related. */
extern const bool
uninspired_specialized_instruction_call_relateds [];




/* Instruction rewriter.
 * ************************************************************************** */

/* Try to apply each rewrite rule in order and run the first one that matches,
   if any, on the pointed program.  When a rule fires the following ones are not
   checked but if a rule, after removing the last few instructions, adds another
   one, the addition will trigger another rewrite in its turn, and so on until
   no more rewriting is possible.  The rewriting process is inherently
   recursive.

   The implementation of this function is machine-generated, but the user can
   add her own code in the rewriter-c block, which ends up near the beginning of
   this function body, right after JITTTER_REWRITE_FUNCTION_PROLOG_ .  The
   formal argument seen from the body is named jitter_mutable_routine_p .

   Rationale: the argument is named differently in the body in order to keep
   the namespace conventions and, more importantly, to encourage the user to
   read this comment.

   The user must *not* append labels to the VM routines during rewriting: that
   would break it.  The user is responsible for destroying any instruction she
   removes, including their arguments.  The user can assume that
   jitter_rewritable_instruction_no is strictly greater than zero. */
void
uninspired_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p);




/* Program points at run time in executable routines.
 * ************************************************************************** */

/* Provide a nice name for a program point type which looks VM-dependent. */
typedef jitter_program_point
uninspired_program_point;

/* Again, provide a VM-dependent alias for an actually VM-independent macro. */
#define UNINSPIRED_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)  \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING(_jitter_executable_routine_ptr)




/* Program points at run time in routines: unified routine API.
 * ************************************************************************** */

/* Like UNINSPIRED_EXECUTABLE_ROUTINE_BEGINNING for the unified routine API. */
#define UNINSPIRED_ROUTINE_BEGINNING(_jitter_routine)                \
  JITTER_EXECUTABLE_ROUTINE_BEGINNING                              \
     (jitter_routine_make_executable_if_needed (_jitter_routine))



/* Executing code from an executable routine.
 * ************************************************************************** */

/* Make sure that the pointed state has enough slow registers to run the pointed
   executable routine; if that is not the case, allocate more slow registers. */
void
uninspired_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the given program point (which must belong to some
   executable routine), in the pointed VM state.
   Return the VM execution state at the end.

   Since no executable routine is given this cannot automatically guarantee that
   the slow registers in the pointed state are in sufficient number; it is the
   user's responsibility to check, if needed.

   This function is also usable with the unified routine API. */
enum uninspired_exit_status
uninspired_branch_to_program_point (uninspired_program_point p,
                                  struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* Run VM code starting from the beginning of the pointed executable routine,
   in the pointed VM state.
   Return the VM execution state at the end.
   This does ensure that the slow registers in the pointed state are in
   sufficient number, by calling uninspired_ensure_enough_slow_registers_for .
   This function is slightly less efficient than
   uninspired_branch_to_program_point , and uninspired_branch_to_program_point
   should be preferred in contexts where C code repeatedly calls VM code. */
enum uninspired_exit_status
uninspired_execute_executable_routine (const struct jitter_executable_routine *er,
                                     struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));




/* Executing code: unified routine API.
 * ************************************************************************** */

/* Like uninspired_ensure_enough_slow_registers_for_executable_routine , with the
   unified API. */
void
uninspired_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));

/* uninspired_branch_to_program_point , declared above, is also usable with the
   unified routine API. */

/* Like uninspired_execute_executable_routine, for a unified routine. */
enum uninspired_exit_status
uninspired_execute_routine (jitter_routine r,
                          struct uninspired_state *s)
  __attribute__ ((nonnull (1, 2)));




/* VM exit status.
 * ************************************************************************** */

/* A value of this type is returned by a VM after execution, and the last
   returned value is also held in the VM state in order to make consistency
   checks. */
enum uninspired_exit_status
  {
    /* This state has never been used for execution.  This is the initial value
       within the state, and is never returned after execution. */
    uninspired_exit_status_never_executed = 0,

    /* The state is being used in execution right now; this is never returned by
       the executor.  It is an error (checked for) to execute code with a VM
       state containing this exit status, which shows that there has been a
       problem -- likely VM code was exited via longjmp, skipping the proper
       cleanup. */
    uninspired_exit_status_being_executed = 1,

    /* Some VM code has been executed.  It is now possible to execute more code
       (including the same code again) in the same state. */
    uninspired_exit_status_exited = 2,

    /* Code execution has been interrupted for debugging, but can be resumed. */
    uninspired_exit_status_debug = 3,
  };




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

/* Dump human-readable information about data locations to the given print
   context.
   This is a trivial VM-dependent wrapper around jitter_dump_data_locations,
   which does not require a struct jitter_vm pointer as input. */
void
uninspired_dump_data_locations (jitter_print_context output)
  __attribute__ ((nonnull (1)));




/* Sample profiling: internal API.
 * ************************************************************************** */

/* The functions in this sections are used internally by vm2.c, only when
   sample-profiling is enabled.  In fact these functions are not defined at all
   otherwise. */

/* Initialise global sampling-related structures. */
// FIXME: no: distinguish struct jitter_profile_runtime and struct jitter_profile
void
uninspired_profile_sample_initialize (void);

/* Begin sampling. */
void
uninspired_profile_sample_start (struct uninspired_state *state)
  __attribute__ ((nonnull (1)));

/* Stop sampling. */
void
uninspired_profile_sample_stop (void);




/* User macros to access VM state data structures (out of VM instructions).
 * ************************************************************************** */

/* Notice that these macros are to be used out of VM instruction code blocks:
   for use within instructions see the alternative definitions of
   _JITTER_STATE_RUNTIME_FIELD and _JITTER_STATE_BACKING_FIELD in
   jitter-executor.h .  The alternative definitions are *not* compatible: the
   macros defined here have one more argument, the VM state structure. */

/* Given a VM state pointer and a state runtime field name expand to an l-value
   referring the named field in the given VM state runtime.
   This macro is not usable within VM instruction code blocks: see the commmnt
   above. */
#define UNINSPIRED_STATE_RUNTIME_FIELD(state_p /* see the comment above */,  \
                                     field_name)                           \
  ((state_p)->_uninspired_xOxBm2j5vO_state_runtime.field_name)

/* Given a VM state pointer and a state backing field name expand to an l-value
   referring the named field in the given VM state backing.
   This macro is not usable within VM instruction code blocks: see the commmnt
   above. */
#define UNINSPIRED_STATE_BACKING_FIELD(state_p /* see the comment above */,  \
                                     field_name)                           \
  ((state_p)->uninspired_state_backing.field_name)




/* Machine-generated code.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated header files,
   but having too many files would be inconvenient for the user to compile and
   link.  For this reason we generate a single header. */

/* User-specified code, early header part: beginning. */

    struct exception_handler
    {
      jitter_uint exception;
      uninspired_program_point code;
      jitter_stack_height stack_height;
    };
  
  #include <string.h> // FIXME: remove after my tests with memcpy

  union uninspired_currently_unique_kind_register_t
  {
    /* Each of the following fields has the same name and a compatible type (in
       this case exactly the same) as the corresponding fields in union
       jitter_word .  This is useful for meta-instructions whose arguments may be
       either immediates or registers. */
    jitter_int fixnum;
    jitter_uint ufixnum;
    union jitter_word * restrict pointer;
  };

# define VECTOR_ELEMENT_TYPE  float//double//char//int//double //float
# define VECTOR_ELEMENT_NO    2//1024//8//1024//512 //8//512//1024

# define USE_ATTRIBUTE_VECTOR_SIZE

#ifdef USE_ATTRIBUTE_VECTOR_SIZE
  typedef VECTOR_ELEMENT_TYPE
  vector
    __attribute__ ((vector_size (VECTOR_ELEMENT_NO
                                 * sizeof(VECTOR_ELEMENT_TYPE)),
                    aligned));
#else
  typedef VECTOR_ELEMENT_TYPE
  vector [VECTOR_ELEMENT_NO]
    __attribute__ ((aligned));
#endif // #ifdef USE_ATTRIBUTE_VECTOR_SIZE
  
/* User-specified code, early header part: end */

/* Configuration data for struct jitter_vm_configuration. */
#define UNINSPIRED_VM_NAME JITTER_STRINGIFY(Uninspired)
#define UNINSPIRED_LOWER_CASE_PREFIX "uninspired"
#define UNINSPIRED_UPPER_CASE_PREFIX "UNINSPIRED"
#define UNINSPIRED_HASH_PREFIX "_uninspired_xOxBm2j5vO"
#define UNINSPIRED_DISPATCH_HUMAN_READABLE \
  JITTER_DISPATCH_NAME_STRING
#define UNINSPIRED_MAX_FAST_REGISTER_NO_PER_CLASS 1
#define UNINSPIRED_MAX_NONRESIDUAL_LITERAL_NO -1


/* For each register class define the register type, a unique index, and the
   number of fast registers.  Indices are useful for computing slow register
   offsets.  For each register class declare a global register class
   descriptor, convenient to use when generating unspecialized instructions
   from the C API.*/
typedef
union uninspired_currently_unique_kind_register_t uninspired_register_r;
#define UNINSPIRED_REGISTER_r_CLASS_ID 0
#define UNINSPIRED_REGISTER_r_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_r;
typedef
VECTOR_ELEMENT_TYPE uninspired_register_f;
#define UNINSPIRED_REGISTER_f_CLASS_ID 1
#define UNINSPIRED_REGISTER_f_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_f;
typedef
vector uninspired_register_v;
#define UNINSPIRED_REGISTER_v_CLASS_ID 2
#define UNINSPIRED_REGISTER_v_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_v;
typedef
char uninspired_register_c;
#define UNINSPIRED_REGISTER_c_CLASS_ID 3
#define UNINSPIRED_REGISTER_c_FAST_REGISTER_NO 0
extern const struct jitter_register_class
uninspired_register_class_c;

/* How many register classes we have. */
#define UNINSPIRED_REGISTER_CLASS_NO  4

/* A union large enough to hold a register of any class, or a machine word. */
union uninspired_any_register
{
  /* In any case the union must be at least as large as a machine word. */
  jitter_int jitter_unused_field;

  uninspired_register_r r /* A r-class register */;
  uninspired_register_f f /* A f-class register */;
  uninspired_register_v v /* A v-class register */;
  uninspired_register_c c /* A c-class register */;
};

/* An enumeration of all uninspired register classes. */
enum uninspired_register_class_id
  {
    uninspired_register_class_id_r = UNINSPIRED_REGISTER_r_CLASS_ID,
    uninspired_register_class_id_f = UNINSPIRED_REGISTER_f_CLASS_ID,
    uninspired_register_class_id_v = UNINSPIRED_REGISTER_v_CLASS_ID,
    uninspired_register_class_id_c = UNINSPIRED_REGISTER_c_CLASS_ID,

    /* The number of register class ids, not valid as a class id itself. */
    uninspired_register_class_id_no = UNINSPIRED_REGISTER_CLASS_NO
  };

/* A macro expanding to a statement initialising a rank of slow
   registers.  The argument has type union uninspired_any_register *
   and points to the first register in a rank. */
#define UNINSPIRED_INITIALIZE_SLOW_REGISTER_RANK(rank) \
  do \
    { \
      union uninspired_any_register *_jitter_rank __attribute__ ((unused)) \
        = (rank); \
      _jitter_rank [0].r = (union uninspired_currently_unique_kind_register_t) ((union uninspired_currently_unique_kind_register_t) {.ufixnum = 0x11223344}); \
      /* f-class registers need no initialisation. */ \
      /* v-class registers need no initialisation. */ \
      /* c-class registers need no initialisation. */ \
    } \
  while (false)


#ifndef UNINSPIRED_STATE_H_
#define UNINSPIRED_STATE_H_

//#include <jitter/jitter.h>

/* Early C code from the user for the state definition. */
/* End of the early C code from the user for the state definition. */

/* The VM state backing. */
struct uninspired_state_backing
{
  /* The Array.  This initial pointer is kept in the backing, since it is
     not normally needed at run time.  By subtracting JITTER_ARRAY_BIAS from
     it (as a pointer to char) we get the base pointer. */
  char *jitter_array;

  /* How many slow registers per class the Array can hold, without being
     reallocated.  This number is always the same for evey class. */
  jitter_int jitter_slow_register_no_per_class;

  /* The initial VM program point.  This is not part of the runtime,
     in fact with no-threading dispatch there is not even a copy of this
     datum being kept up to date during execution, anywhere; this field
     serves to keep track of where execution should *continue* from at the
     next execution.  It will become more useful when debubbing is
     implemented. */
  uninspired_program_point initial_program_point;

  /* The exit status. */
  enum uninspired_exit_status exit_status;

  /* Stack backing data structures. */
  struct jitter_stack_backing jitter_stack_stack_backing;
  struct jitter_stack_backing jitter_stack_handlers_backing;

  /* State backing fields added in C by the user. */

  /* End of the state backing fields added in C by the user. */
};

/* The VM state runtime data structure, using memory from the VM state backing. */
struct uninspired_state_runtime
{
  /* A link register for branch-and-link operations.  This field must *not*
     be accessed from user code, as it may not exist on all dispatches.
     It is only used internally for JITTER_PROCEDURE_PROLOG .

     With no-threading on arthitectures supporting procedures some
     hardware-dependent resource such as a designed register (general-
     purpose or not, reserved or not) or a stack location will be used
     instead of this, normally; however even with no-threading we need
     this for defect replacement: if any call-related instruction turns
     out to be defective they will all be replaced in order to keep their
     calling conventions compatible, and the replacement will use
     this. */
  union jitter_word _jitter_link;

  /* With recent GCC versions (as of Summer 2017) the *last* declared fields
     are the most likely to be allocated in registers; this is why VM registers
     are in reverse order here.  The first few fast registers will be the "fastest"
     ones, allocated in hardware registers; they may be followed by other fast
     fast allocated on the stack at known offsets, with intermediate performance; then
     come the slow registers.  In critical code the users should prefer a register with as
     small an index as possible for best performance. */

  /* Stack runtime data structures. */
  JITTER_STACK_TOS_DECLARATION(union jitter_word, stack);
  JITTER_STACK_NTOS_DECLARATION(struct exception_handler, handlers);

  /* State runtime fields added in C by the user. */

  
  /* End of the state runtime fields added in C by the user. */
};

/* A struct holding both the backing and the runtime part of the VM state. */
struct uninspired_state
{
  /* Pointers to the previous and next VM state for this VM. */
  struct jitter_list_links links;

  /* Each state data structure contains its backing. */
  struct uninspired_state_backing uninspired_state_backing;

  /* Each state data structure contains its runtime data structures,
     which the compiler will try to keep in registers as far as
     possible.  Runtime structures are allowed to point to memory
     from the backing (which is particularly important for stacks),
     but the backing itself is not copied into registers at
     execution time.
     It is important for this identifier not to be directly used in
     user code, since at some points during execution the data stored
     struct field may be out of date.  In order to prevent this kind
     of mistakes this field has a hard-to-predict name. */
  struct uninspired_state_runtime _uninspired_xOxBm2j5vO_state_runtime;
};
#endif // #ifndef UNINSPIRED_STATE_H_
#ifndef UNINSPIRED_META_INSTRUCTIONS_H_
#define UNINSPIRED_META_INSTRUCTIONS_H_

enum uninspired_meta_instruction_id
  {
    uninspired_meta_instruction_id_add = 0,
    uninspired_meta_instruction_id_addo = 1,
    uninspired_meta_instruction_id_b = 2,
    uninspired_meta_instruction_id_band = 3,
    uninspired_meta_instruction_id_beq = 4,
    uninspired_meta_instruction_id_bge = 5,
    uninspired_meta_instruction_id_bgeu = 6,
    uninspired_meta_instruction_id_bgt = 7,
    uninspired_meta_instruction_id_bgtu = 8,
    uninspired_meta_instruction_id_ble = 9,
    uninspired_meta_instruction_id_bleu = 10,
    uninspired_meta_instruction_id_blt = 11,
    uninspired_meta_instruction_id_bltu = 12,
    uninspired_meta_instruction_id_bne = 13,
    uninspired_meta_instruction_id_bneg = 14,
    uninspired_meta_instruction_id_bnneg = 15,
    uninspired_meta_instruction_id_bnotand = 16,
    uninspired_meta_instruction_id_bnpos = 17,
    uninspired_meta_instruction_id_bnz = 18,
    uninspired_meta_instruction_id_bpos = 19,
    uninspired_meta_instruction_id_br = 20,
    uninspired_meta_instruction_id_bulge = 21,
    uninspired_meta_instruction_id_bulgeforth = 22,
    uninspired_meta_instruction_id_bz = 23,
    uninspired_meta_instruction_id_clear_mpending = 24,
    uninspired_meta_instruction_id_div = 25,
    uninspired_meta_instruction_id_divo = 26,
    uninspired_meta_instruction_id_drop_mhandler = 27,
    uninspired_meta_instruction_id_endvm = 28,
    uninspired_meta_instruction_id_exit = 29,
    uninspired_meta_instruction_id_exitvm = 30,
    uninspired_meta_instruction_id_fadd = 31,
    uninspired_meta_instruction_id_fdiv = 32,
    uninspired_meta_instruction_id_fincr = 33,
    uninspired_meta_instruction_id_fmul = 34,
    uninspired_meta_instruction_id_fprint = 35,
    uninspired_meta_instruction_id_fset = 36,
    uninspired_meta_instruction_id_fsub = 37,
    uninspired_meta_instruction_id_hcf = 38,
    uninspired_meta_instruction_id_install_msignal_mhandler = 39,
    uninspired_meta_instruction_id_loadwithbyteoffset = 40,
    uninspired_meta_instruction_id_loadwithwordoffset = 41,
    uninspired_meta_instruction_id_mallocwords = 42,
    uninspired_meta_instruction_id_mod = 43,
    uninspired_meta_instruction_id_modo = 44,
    uninspired_meta_instruction_id_mov = 45,
    uninspired_meta_instruction_id_mroll = 46,
    uninspired_meta_instruction_id_mrollforth = 47,
    uninspired_meta_instruction_id_mrot = 48,
    uninspired_meta_instruction_id_mul = 49,
    uninspired_meta_instruction_id_mulo = 50,
    uninspired_meta_instruction_id_nop = 51,
    uninspired_meta_instruction_id_print_mpending_msignals = 52,
    uninspired_meta_instruction_id_print_mtopmost = 53,
    uninspired_meta_instruction_id_printfixnum = 54,
    uninspired_meta_instruction_id_procedurecall = 55,
    uninspired_meta_instruction_id_procedurecallr = 56,
    uninspired_meta_instruction_id_procedureprolog = 57,
    uninspired_meta_instruction_id_procedurereturn = 58,
    uninspired_meta_instruction_id_push_mdepths = 59,
    uninspired_meta_instruction_id_push_mhandler = 60,
    uninspired_meta_instruction_id_push_mincreasing = 61,
    uninspired_meta_instruction_id_quake = 62,
    uninspired_meta_instruction_id_raise = 63,
    uninspired_meta_instruction_id_random = 64,
    uninspired_meta_instruction_id_reverse = 65,
    uninspired_meta_instruction_id_roll = 66,
    uninspired_meta_instruction_id_rollforth = 67,
    uninspired_meta_instruction_id_rot = 68,
    uninspired_meta_instruction_id_safe_mpoint = 69,
    uninspired_meta_instruction_id_set_mpending = 70,
    uninspired_meta_instruction_id_slide = 71,
    uninspired_meta_instruction_id_slideforth = 72,
    uninspired_meta_instruction_id_stackdrop = 73,
    uninspired_meta_instruction_id_stackdup = 74,
    uninspired_meta_instruction_id_stackif = 75,
    uninspired_meta_instruction_id_stacknip = 76,
    uninspired_meta_instruction_id_stacknondroppingif = 77,
    uninspired_meta_instruction_id_stacknot = 78,
    uninspired_meta_instruction_id_stackoneminus = 79,
    uninspired_meta_instruction_id_stackoneplus = 80,
    uninspired_meta_instruction_id_stackover = 81,
    uninspired_meta_instruction_id_stackpeek = 82,
    uninspired_meta_instruction_id_stackplus = 83,
    uninspired_meta_instruction_id_stackplusr = 84,
    uninspired_meta_instruction_id_stackpop = 85,
    uninspired_meta_instruction_id_stackprint = 86,
    uninspired_meta_instruction_id_stackpush = 87,
    uninspired_meta_instruction_id_stackpushunspecified = 88,
    uninspired_meta_instruction_id_stackset = 89,
    uninspired_meta_instruction_id_stackswap = 90,
    uninspired_meta_instruction_id_stackswaptop = 91,
    uninspired_meta_instruction_id_stacktimes = 92,
    uninspired_meta_instruction_id_storewithbyteoffset = 93,
    uninspired_meta_instruction_id_storewithwordoffset = 94,
    uninspired_meta_instruction_id_sub = 95,
    uninspired_meta_instruction_id_subo = 96,
    uninspired_meta_instruction_id_tuck = 97,
    uninspired_meta_instruction_id_unreachable = 98,
    uninspired_meta_instruction_id_whirl = 99,
    uninspired_meta_instruction_id_whirlforth = 100
  };

#define UNINSPIRED_META_INSTRUCTION_NO 101

/* The longest meta-instruction name length, not mangled, without
   counting the final '\0' character. */
#define UNINSPIRED_MAX_META_INSTRUCTION_NAME_LENGTH 22

#endif // #ifndef UNINSPIRED_META_INSTRUCTIONS_H_
#ifndef UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_
#define UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_

enum uninspired_specialized_instruction_opcode
  {
    uninspired_specialized_instruction_opcode__eINVALID = 0,
    uninspired_specialized_instruction_opcode__eBEGINBASICBLOCK = 1,
    uninspired_specialized_instruction_opcode__eEXITVM = 2,
    uninspired_specialized_instruction_opcode__eDATALOCATIONS = 3,
    uninspired_specialized_instruction_opcode__eNOP = 4,
    uninspired_specialized_instruction_opcode__eUNREACHABLE0 = 5,
    uninspired_specialized_instruction_opcode__eUNREACHABLE1 = 6,
    uninspired_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE = 7,
    uninspired_specialized_instruction_opcode_add___rrR___rrR___rrR = 8,
    uninspired_specialized_instruction_opcode_add___rrR__n1___rrR = 9,
    uninspired_specialized_instruction_opcode_add___rrR__n_m1___rrR = 10,
    uninspired_specialized_instruction_opcode_add___rrR__nR___rrR = 11,
    uninspired_specialized_instruction_opcode_add__n1___rrR___rrR = 12,
    uninspired_specialized_instruction_opcode_add__n1__n1___rrR = 13,
    uninspired_specialized_instruction_opcode_add__n1__n_m1___rrR = 14,
    uninspired_specialized_instruction_opcode_add__n1__nR___rrR = 15,
    uninspired_specialized_instruction_opcode_add__n_m1___rrR___rrR = 16,
    uninspired_specialized_instruction_opcode_add__n_m1__n1___rrR = 17,
    uninspired_specialized_instruction_opcode_add__n_m1__n_m1___rrR = 18,
    uninspired_specialized_instruction_opcode_add__n_m1__nR___rrR = 19,
    uninspired_specialized_instruction_opcode_add__nR___rrR___rrR = 20,
    uninspired_specialized_instruction_opcode_add__nR__n1___rrR = 21,
    uninspired_specialized_instruction_opcode_add__nR__n_m1___rrR = 22,
    uninspired_specialized_instruction_opcode_add__nR__nR___rrR = 23,
    uninspired_specialized_instruction_opcode_addo___rrR___rrR___rrR__fR = 24,
    uninspired_specialized_instruction_opcode_addo___rrR__n1___rrR__fR = 25,
    uninspired_specialized_instruction_opcode_addo___rrR__n_m1___rrR__fR = 26,
    uninspired_specialized_instruction_opcode_addo___rrR__nR___rrR__fR = 27,
    uninspired_specialized_instruction_opcode_addo__nR___rrR___rrR__fR = 28,
    uninspired_specialized_instruction_opcode_addo__nR__n1___rrR__fR = 29,
    uninspired_specialized_instruction_opcode_addo__nR__n_m1___rrR__fR = 30,
    uninspired_specialized_instruction_opcode_addo__nR__nR___rrR__fR = 31,
    uninspired_specialized_instruction_opcode_b__fR = 32,
    uninspired_specialized_instruction_opcode_band___rrR___rrR__fR = 33,
    uninspired_specialized_instruction_opcode_band___rrR__n3__fR = 34,
    uninspired_specialized_instruction_opcode_band___rrR__n7__fR = 35,
    uninspired_specialized_instruction_opcode_band___rrR__nR__fR = 36,
    uninspired_specialized_instruction_opcode_band__nR___rrR__fR = 37,
    uninspired_specialized_instruction_opcode_band__nR__n3__fR = 38,
    uninspired_specialized_instruction_opcode_band__nR__n7__fR = 39,
    uninspired_specialized_instruction_opcode_band__nR__nR__fR = 40,
    uninspired_specialized_instruction_opcode_beq___rrR___rrR__fR = 41,
    uninspired_specialized_instruction_opcode_beq___rrR__nR__fR = 42,
    uninspired_specialized_instruction_opcode_beq__nR___rrR__fR = 43,
    uninspired_specialized_instruction_opcode_beq__nR__nR__fR = 44,
    uninspired_specialized_instruction_opcode_bge___rrR___rrR__fR = 45,
    uninspired_specialized_instruction_opcode_bge___rrR__n0__fR = 46,
    uninspired_specialized_instruction_opcode_bge___rrR__n1__fR = 47,
    uninspired_specialized_instruction_opcode_bge___rrR__nR__fR = 48,
    uninspired_specialized_instruction_opcode_bge__n0___rrR__fR = 49,
    uninspired_specialized_instruction_opcode_bge__n0__n0__fR = 50,
    uninspired_specialized_instruction_opcode_bge__n0__n1__fR = 51,
    uninspired_specialized_instruction_opcode_bge__n0__nR__fR = 52,
    uninspired_specialized_instruction_opcode_bge__n1___rrR__fR = 53,
    uninspired_specialized_instruction_opcode_bge__n1__n0__fR = 54,
    uninspired_specialized_instruction_opcode_bge__n1__n1__fR = 55,
    uninspired_specialized_instruction_opcode_bge__n1__nR__fR = 56,
    uninspired_specialized_instruction_opcode_bge__nR___rrR__fR = 57,
    uninspired_specialized_instruction_opcode_bge__nR__n0__fR = 58,
    uninspired_specialized_instruction_opcode_bge__nR__n1__fR = 59,
    uninspired_specialized_instruction_opcode_bge__nR__nR__fR = 60,
    uninspired_specialized_instruction_opcode_bgeu___rrR___rrR__fR = 61,
    uninspired_specialized_instruction_opcode_bgeu___rrR__n0__fR = 62,
    uninspired_specialized_instruction_opcode_bgeu___rrR__n1__fR = 63,
    uninspired_specialized_instruction_opcode_bgeu___rrR__nR__fR = 64,
    uninspired_specialized_instruction_opcode_bgeu__n0___rrR__fR = 65,
    uninspired_specialized_instruction_opcode_bgeu__n0__n0__fR = 66,
    uninspired_specialized_instruction_opcode_bgeu__n0__n1__fR = 67,
    uninspired_specialized_instruction_opcode_bgeu__n0__nR__fR = 68,
    uninspired_specialized_instruction_opcode_bgeu__n1___rrR__fR = 69,
    uninspired_specialized_instruction_opcode_bgeu__n1__n0__fR = 70,
    uninspired_specialized_instruction_opcode_bgeu__n1__n1__fR = 71,
    uninspired_specialized_instruction_opcode_bgeu__n1__nR__fR = 72,
    uninspired_specialized_instruction_opcode_bgeu__nR___rrR__fR = 73,
    uninspired_specialized_instruction_opcode_bgeu__nR__n0__fR = 74,
    uninspired_specialized_instruction_opcode_bgeu__nR__n1__fR = 75,
    uninspired_specialized_instruction_opcode_bgeu__nR__nR__fR = 76,
    uninspired_specialized_instruction_opcode_bgt___rrR___rrR__fR = 77,
    uninspired_specialized_instruction_opcode_bgt___rrR__n0__fR = 78,
    uninspired_specialized_instruction_opcode_bgt___rrR__n1__fR = 79,
    uninspired_specialized_instruction_opcode_bgt___rrR__nR__fR = 80,
    uninspired_specialized_instruction_opcode_bgt__n0___rrR__fR = 81,
    uninspired_specialized_instruction_opcode_bgt__n0__n0__fR = 82,
    uninspired_specialized_instruction_opcode_bgt__n0__n1__fR = 83,
    uninspired_specialized_instruction_opcode_bgt__n0__nR__fR = 84,
    uninspired_specialized_instruction_opcode_bgt__n1___rrR__fR = 85,
    uninspired_specialized_instruction_opcode_bgt__n1__n0__fR = 86,
    uninspired_specialized_instruction_opcode_bgt__n1__n1__fR = 87,
    uninspired_specialized_instruction_opcode_bgt__n1__nR__fR = 88,
    uninspired_specialized_instruction_opcode_bgt__nR___rrR__fR = 89,
    uninspired_specialized_instruction_opcode_bgt__nR__n0__fR = 90,
    uninspired_specialized_instruction_opcode_bgt__nR__n1__fR = 91,
    uninspired_specialized_instruction_opcode_bgt__nR__nR__fR = 92,
    uninspired_specialized_instruction_opcode_bgtu___rrR___rrR__fR = 93,
    uninspired_specialized_instruction_opcode_bgtu___rrR__n0__fR = 94,
    uninspired_specialized_instruction_opcode_bgtu___rrR__n1__fR = 95,
    uninspired_specialized_instruction_opcode_bgtu___rrR__nR__fR = 96,
    uninspired_specialized_instruction_opcode_bgtu__n0___rrR__fR = 97,
    uninspired_specialized_instruction_opcode_bgtu__n0__n0__fR = 98,
    uninspired_specialized_instruction_opcode_bgtu__n0__n1__fR = 99,
    uninspired_specialized_instruction_opcode_bgtu__n0__nR__fR = 100,
    uninspired_specialized_instruction_opcode_bgtu__n1___rrR__fR = 101,
    uninspired_specialized_instruction_opcode_bgtu__n1__n0__fR = 102,
    uninspired_specialized_instruction_opcode_bgtu__n1__n1__fR = 103,
    uninspired_specialized_instruction_opcode_bgtu__n1__nR__fR = 104,
    uninspired_specialized_instruction_opcode_bgtu__nR___rrR__fR = 105,
    uninspired_specialized_instruction_opcode_bgtu__nR__n0__fR = 106,
    uninspired_specialized_instruction_opcode_bgtu__nR__n1__fR = 107,
    uninspired_specialized_instruction_opcode_bgtu__nR__nR__fR = 108,
    uninspired_specialized_instruction_opcode_ble___rrR___rrR__fR = 109,
    uninspired_specialized_instruction_opcode_ble___rrR__n0__fR = 110,
    uninspired_specialized_instruction_opcode_ble___rrR__n1__fR = 111,
    uninspired_specialized_instruction_opcode_ble___rrR__nR__fR = 112,
    uninspired_specialized_instruction_opcode_ble__n0___rrR__fR = 113,
    uninspired_specialized_instruction_opcode_ble__n0__n0__fR = 114,
    uninspired_specialized_instruction_opcode_ble__n0__n1__fR = 115,
    uninspired_specialized_instruction_opcode_ble__n0__nR__fR = 116,
    uninspired_specialized_instruction_opcode_ble__n1___rrR__fR = 117,
    uninspired_specialized_instruction_opcode_ble__n1__n0__fR = 118,
    uninspired_specialized_instruction_opcode_ble__n1__n1__fR = 119,
    uninspired_specialized_instruction_opcode_ble__n1__nR__fR = 120,
    uninspired_specialized_instruction_opcode_ble__nR___rrR__fR = 121,
    uninspired_specialized_instruction_opcode_ble__nR__n0__fR = 122,
    uninspired_specialized_instruction_opcode_ble__nR__n1__fR = 123,
    uninspired_specialized_instruction_opcode_ble__nR__nR__fR = 124,
    uninspired_specialized_instruction_opcode_bleu___rrR___rrR__fR = 125,
    uninspired_specialized_instruction_opcode_bleu___rrR__n0__fR = 126,
    uninspired_specialized_instruction_opcode_bleu___rrR__n1__fR = 127,
    uninspired_specialized_instruction_opcode_bleu___rrR__nR__fR = 128,
    uninspired_specialized_instruction_opcode_bleu__n0___rrR__fR = 129,
    uninspired_specialized_instruction_opcode_bleu__n0__n0__fR = 130,
    uninspired_specialized_instruction_opcode_bleu__n0__n1__fR = 131,
    uninspired_specialized_instruction_opcode_bleu__n0__nR__fR = 132,
    uninspired_specialized_instruction_opcode_bleu__n1___rrR__fR = 133,
    uninspired_specialized_instruction_opcode_bleu__n1__n0__fR = 134,
    uninspired_specialized_instruction_opcode_bleu__n1__n1__fR = 135,
    uninspired_specialized_instruction_opcode_bleu__n1__nR__fR = 136,
    uninspired_specialized_instruction_opcode_bleu__nR___rrR__fR = 137,
    uninspired_specialized_instruction_opcode_bleu__nR__n0__fR = 138,
    uninspired_specialized_instruction_opcode_bleu__nR__n1__fR = 139,
    uninspired_specialized_instruction_opcode_bleu__nR__nR__fR = 140,
    uninspired_specialized_instruction_opcode_blt___rrR___rrR__fR = 141,
    uninspired_specialized_instruction_opcode_blt___rrR__n0__fR = 142,
    uninspired_specialized_instruction_opcode_blt___rrR__n1__fR = 143,
    uninspired_specialized_instruction_opcode_blt___rrR__nR__fR = 144,
    uninspired_specialized_instruction_opcode_blt__n0___rrR__fR = 145,
    uninspired_specialized_instruction_opcode_blt__n0__n0__fR = 146,
    uninspired_specialized_instruction_opcode_blt__n0__n1__fR = 147,
    uninspired_specialized_instruction_opcode_blt__n0__nR__fR = 148,
    uninspired_specialized_instruction_opcode_blt__n1___rrR__fR = 149,
    uninspired_specialized_instruction_opcode_blt__n1__n0__fR = 150,
    uninspired_specialized_instruction_opcode_blt__n1__n1__fR = 151,
    uninspired_specialized_instruction_opcode_blt__n1__nR__fR = 152,
    uninspired_specialized_instruction_opcode_blt__nR___rrR__fR = 153,
    uninspired_specialized_instruction_opcode_blt__nR__n0__fR = 154,
    uninspired_specialized_instruction_opcode_blt__nR__n1__fR = 155,
    uninspired_specialized_instruction_opcode_blt__nR__nR__fR = 156,
    uninspired_specialized_instruction_opcode_bltu___rrR___rrR__fR = 157,
    uninspired_specialized_instruction_opcode_bltu___rrR__n0__fR = 158,
    uninspired_specialized_instruction_opcode_bltu___rrR__n1__fR = 159,
    uninspired_specialized_instruction_opcode_bltu___rrR__nR__fR = 160,
    uninspired_specialized_instruction_opcode_bltu__n0___rrR__fR = 161,
    uninspired_specialized_instruction_opcode_bltu__n0__n0__fR = 162,
    uninspired_specialized_instruction_opcode_bltu__n0__n1__fR = 163,
    uninspired_specialized_instruction_opcode_bltu__n0__nR__fR = 164,
    uninspired_specialized_instruction_opcode_bltu__n1___rrR__fR = 165,
    uninspired_specialized_instruction_opcode_bltu__n1__n0__fR = 166,
    uninspired_specialized_instruction_opcode_bltu__n1__n1__fR = 167,
    uninspired_specialized_instruction_opcode_bltu__n1__nR__fR = 168,
    uninspired_specialized_instruction_opcode_bltu__nR___rrR__fR = 169,
    uninspired_specialized_instruction_opcode_bltu__nR__n0__fR = 170,
    uninspired_specialized_instruction_opcode_bltu__nR__n1__fR = 171,
    uninspired_specialized_instruction_opcode_bltu__nR__nR__fR = 172,
    uninspired_specialized_instruction_opcode_bne___rrR___rrR__fR = 173,
    uninspired_specialized_instruction_opcode_bne___rrR__n0__fR = 174,
    uninspired_specialized_instruction_opcode_bne___rrR__nR__fR = 175,
    uninspired_specialized_instruction_opcode_bne__n0___rrR__fR = 176,
    uninspired_specialized_instruction_opcode_bne__n0__n0__fR = 177,
    uninspired_specialized_instruction_opcode_bne__n0__nR__fR = 178,
    uninspired_specialized_instruction_opcode_bne__nR___rrR__fR = 179,
    uninspired_specialized_instruction_opcode_bne__nR__n0__fR = 180,
    uninspired_specialized_instruction_opcode_bne__nR__nR__fR = 181,
    uninspired_specialized_instruction_opcode_bneg___rrR__fR = 182,
    uninspired_specialized_instruction_opcode_bneg__nR__fR = 183,
    uninspired_specialized_instruction_opcode_bnneg___rrR__fR = 184,
    uninspired_specialized_instruction_opcode_bnneg__nR__fR = 185,
    uninspired_specialized_instruction_opcode_bnotand___rrR___rrR__fR = 186,
    uninspired_specialized_instruction_opcode_bnotand___rrR__n3__fR = 187,
    uninspired_specialized_instruction_opcode_bnotand___rrR__n7__fR = 188,
    uninspired_specialized_instruction_opcode_bnotand___rrR__nR__fR = 189,
    uninspired_specialized_instruction_opcode_bnotand__nR___rrR__fR = 190,
    uninspired_specialized_instruction_opcode_bnotand__nR__n3__fR = 191,
    uninspired_specialized_instruction_opcode_bnotand__nR__n7__fR = 192,
    uninspired_specialized_instruction_opcode_bnotand__nR__nR__fR = 193,
    uninspired_specialized_instruction_opcode_bnpos___rrR__fR = 194,
    uninspired_specialized_instruction_opcode_bnpos__nR__fR = 195,
    uninspired_specialized_instruction_opcode_bnz___rrR__fR = 196,
    uninspired_specialized_instruction_opcode_bnz__nR__fR = 197,
    uninspired_specialized_instruction_opcode_bpos___rrR__fR = 198,
    uninspired_specialized_instruction_opcode_bpos__nR__fR = 199,
    uninspired_specialized_instruction_opcode_br___rrR = 200,
    uninspired_specialized_instruction_opcode_bulge__n0 = 201,
    uninspired_specialized_instruction_opcode_bulge__n1 = 202,
    uninspired_specialized_instruction_opcode_bulge__n2 = 203,
    uninspired_specialized_instruction_opcode_bulge__n3 = 204,
    uninspired_specialized_instruction_opcode_bulge__nR = 205,
    uninspired_specialized_instruction_opcode_bulgeforth = 206,
    uninspired_specialized_instruction_opcode_bz___rrR__fR = 207,
    uninspired_specialized_instruction_opcode_bz__nR__fR = 208,
    uninspired_specialized_instruction_opcode_clear_mpending__retR = 209,
    uninspired_specialized_instruction_opcode_div___rrR___rrR___rrR__retR = 210,
    uninspired_specialized_instruction_opcode_div___rrR__n2___rrR__retR = 211,
    uninspired_specialized_instruction_opcode_div___rrR__nR___rrR__retR = 212,
    uninspired_specialized_instruction_opcode_div__nR___rrR___rrR__retR = 213,
    uninspired_specialized_instruction_opcode_div__nR__n2___rrR__retR = 214,
    uninspired_specialized_instruction_opcode_div__nR__nR___rrR__retR = 215,
    uninspired_specialized_instruction_opcode_divo___rrR___rrR___rrR__fR = 216,
    uninspired_specialized_instruction_opcode_divo___rrR__n2___rrR__fR = 217,
    uninspired_specialized_instruction_opcode_divo___rrR__nR___rrR__fR = 218,
    uninspired_specialized_instruction_opcode_divo__nR___rrR___rrR__fR = 219,
    uninspired_specialized_instruction_opcode_divo__nR__n2___rrR__fR = 220,
    uninspired_specialized_instruction_opcode_divo__nR__nR___rrR__fR = 221,
    uninspired_specialized_instruction_opcode_drop_mhandler = 222,
    uninspired_specialized_instruction_opcode_endvm = 223,
    uninspired_specialized_instruction_opcode_exit___rrR__retR = 224,
    uninspired_specialized_instruction_opcode_exit__nR__retR = 225,
    uninspired_specialized_instruction_opcode_exitvm = 226,
    uninspired_specialized_instruction_opcode_fadd___rfR___rfR___rfR = 227,
    uninspired_specialized_instruction_opcode_fdiv___rfR___rfR___rfR = 228,
    uninspired_specialized_instruction_opcode_fincr___rfR = 229,
    uninspired_specialized_instruction_opcode_fmul___rfR___rfR___rfR = 230,
    uninspired_specialized_instruction_opcode_fprint___rfR = 231,
    uninspired_specialized_instruction_opcode_fset___rrR___rfR = 232,
    uninspired_specialized_instruction_opcode_fset__nR___rfR = 233,
    uninspired_specialized_instruction_opcode_fsub___rfR___rfR___rfR = 234,
    uninspired_specialized_instruction_opcode_hcf__retR = 235,
    uninspired_specialized_instruction_opcode_install_msignal_mhandler__retR = 236,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR___rrR___rrR = 237,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n0___rrR = 238,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__n8___rrR = 239,
    uninspired_specialized_instruction_opcode_loadwithbyteoffset___rrR__nR___rrR = 240,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR___rrR___rrR = 241,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n0___rrR = 242,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n1___rrR = 243,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__n2___rrR = 244,
    uninspired_specialized_instruction_opcode_loadwithwordoffset___rrR__nR___rrR = 245,
    uninspired_specialized_instruction_opcode_mallocwords___rrR___rrR__retR = 246,
    uninspired_specialized_instruction_opcode_mallocwords__nR___rrR__retR = 247,
    uninspired_specialized_instruction_opcode_mod___rrR___rrR___rrR__retR = 248,
    uninspired_specialized_instruction_opcode_mod___rrR__n2___rrR__retR = 249,
    uninspired_specialized_instruction_opcode_mod___rrR__nR___rrR__retR = 250,
    uninspired_specialized_instruction_opcode_mod__nR___rrR___rrR__retR = 251,
    uninspired_specialized_instruction_opcode_mod__nR__n2___rrR__retR = 252,
    uninspired_specialized_instruction_opcode_mod__nR__nR___rrR__retR = 253,
    uninspired_specialized_instruction_opcode_modo___rrR___rrR___rrR__fR = 254,
    uninspired_specialized_instruction_opcode_modo___rrR__n2___rrR__fR = 255,
    uninspired_specialized_instruction_opcode_modo___rrR__nR___rrR__fR = 256,
    uninspired_specialized_instruction_opcode_modo__nR___rrR___rrR__fR = 257,
    uninspired_specialized_instruction_opcode_modo__nR__n2___rrR__fR = 258,
    uninspired_specialized_instruction_opcode_modo__nR__nR___rrR__fR = 259,
    uninspired_specialized_instruction_opcode_mov___rrR___rrR = 260,
    uninspired_specialized_instruction_opcode_mov__n0___rrR = 261,
    uninspired_specialized_instruction_opcode_mov__n1___rrR = 262,
    uninspired_specialized_instruction_opcode_mov__nR___rrR = 263,
    uninspired_specialized_instruction_opcode_mov__lR___rrR = 264,
    uninspired_specialized_instruction_opcode_mroll__n0 = 265,
    uninspired_specialized_instruction_opcode_mroll__n1 = 266,
    uninspired_specialized_instruction_opcode_mroll__n2 = 267,
    uninspired_specialized_instruction_opcode_mroll__n3 = 268,
    uninspired_specialized_instruction_opcode_mroll__n4 = 269,
    uninspired_specialized_instruction_opcode_mroll__n5 = 270,
    uninspired_specialized_instruction_opcode_mroll__n6 = 271,
    uninspired_specialized_instruction_opcode_mroll__n7 = 272,
    uninspired_specialized_instruction_opcode_mroll__n8 = 273,
    uninspired_specialized_instruction_opcode_mroll__n9 = 274,
    uninspired_specialized_instruction_opcode_mroll__n10 = 275,
    uninspired_specialized_instruction_opcode_mroll__n11 = 276,
    uninspired_specialized_instruction_opcode_mroll__n12 = 277,
    uninspired_specialized_instruction_opcode_mroll__n13 = 278,
    uninspired_specialized_instruction_opcode_mroll__n14 = 279,
    uninspired_specialized_instruction_opcode_mroll__n15 = 280,
    uninspired_specialized_instruction_opcode_mroll__n16 = 281,
    uninspired_specialized_instruction_opcode_mroll__n17 = 282,
    uninspired_specialized_instruction_opcode_mroll__n18 = 283,
    uninspired_specialized_instruction_opcode_mroll__n19 = 284,
    uninspired_specialized_instruction_opcode_mroll__n20 = 285,
    uninspired_specialized_instruction_opcode_mroll__n21 = 286,
    uninspired_specialized_instruction_opcode_mroll__n22 = 287,
    uninspired_specialized_instruction_opcode_mroll__n23 = 288,
    uninspired_specialized_instruction_opcode_mroll__n24 = 289,
    uninspired_specialized_instruction_opcode_mroll__n25 = 290,
    uninspired_specialized_instruction_opcode_mroll__n26 = 291,
    uninspired_specialized_instruction_opcode_mroll__n27 = 292,
    uninspired_specialized_instruction_opcode_mroll__n28 = 293,
    uninspired_specialized_instruction_opcode_mroll__n29 = 294,
    uninspired_specialized_instruction_opcode_mroll__n30 = 295,
    uninspired_specialized_instruction_opcode_mroll__n31 = 296,
    uninspired_specialized_instruction_opcode_mroll__n32 = 297,
    uninspired_specialized_instruction_opcode_mroll__n33 = 298,
    uninspired_specialized_instruction_opcode_mroll__n34 = 299,
    uninspired_specialized_instruction_opcode_mroll__n35 = 300,
    uninspired_specialized_instruction_opcode_mroll__n36 = 301,
    uninspired_specialized_instruction_opcode_mroll__n37 = 302,
    uninspired_specialized_instruction_opcode_mroll__n38 = 303,
    uninspired_specialized_instruction_opcode_mroll__n39 = 304,
    uninspired_specialized_instruction_opcode_mroll__n40 = 305,
    uninspired_specialized_instruction_opcode_mroll__n41 = 306,
    uninspired_specialized_instruction_opcode_mroll__n42 = 307,
    uninspired_specialized_instruction_opcode_mroll__n43 = 308,
    uninspired_specialized_instruction_opcode_mroll__n44 = 309,
    uninspired_specialized_instruction_opcode_mroll__n45 = 310,
    uninspired_specialized_instruction_opcode_mroll__n46 = 311,
    uninspired_specialized_instruction_opcode_mroll__n47 = 312,
    uninspired_specialized_instruction_opcode_mroll__n48 = 313,
    uninspired_specialized_instruction_opcode_mroll__n49 = 314,
    uninspired_specialized_instruction_opcode_mroll__n50 = 315,
    uninspired_specialized_instruction_opcode_mroll__n51 = 316,
    uninspired_specialized_instruction_opcode_mroll__n52 = 317,
    uninspired_specialized_instruction_opcode_mroll__n53 = 318,
    uninspired_specialized_instruction_opcode_mroll__n54 = 319,
    uninspired_specialized_instruction_opcode_mroll__n55 = 320,
    uninspired_specialized_instruction_opcode_mroll__n56 = 321,
    uninspired_specialized_instruction_opcode_mroll__n57 = 322,
    uninspired_specialized_instruction_opcode_mroll__n58 = 323,
    uninspired_specialized_instruction_opcode_mroll__n59 = 324,
    uninspired_specialized_instruction_opcode_mroll__n60 = 325,
    uninspired_specialized_instruction_opcode_mroll__n61 = 326,
    uninspired_specialized_instruction_opcode_mroll__n62 = 327,
    uninspired_specialized_instruction_opcode_mroll__n63 = 328,
    uninspired_specialized_instruction_opcode_mroll__n64 = 329,
    uninspired_specialized_instruction_opcode_mroll__n65 = 330,
    uninspired_specialized_instruction_opcode_mroll__n66 = 331,
    uninspired_specialized_instruction_opcode_mroll__n67 = 332,
    uninspired_specialized_instruction_opcode_mroll__n68 = 333,
    uninspired_specialized_instruction_opcode_mroll__n69 = 334,
    uninspired_specialized_instruction_opcode_mroll__n70 = 335,
    uninspired_specialized_instruction_opcode_mroll__nR = 336,
    uninspired_specialized_instruction_opcode_mrollforth = 337,
    uninspired_specialized_instruction_opcode_mrot = 338,
    uninspired_specialized_instruction_opcode_mul___rrR___rrR___rrR = 339,
    uninspired_specialized_instruction_opcode_mul___rrR__n2___rrR = 340,
    uninspired_specialized_instruction_opcode_mul___rrR__nR___rrR = 341,
    uninspired_specialized_instruction_opcode_mul__n2___rrR___rrR = 342,
    uninspired_specialized_instruction_opcode_mul__n2__n2___rrR = 343,
    uninspired_specialized_instruction_opcode_mul__n2__nR___rrR = 344,
    uninspired_specialized_instruction_opcode_mul__nR___rrR___rrR = 345,
    uninspired_specialized_instruction_opcode_mul__nR__n2___rrR = 346,
    uninspired_specialized_instruction_opcode_mul__nR__nR___rrR = 347,
    uninspired_specialized_instruction_opcode_mulo___rrR___rrR___rrR__fR = 348,
    uninspired_specialized_instruction_opcode_mulo___rrR__n2___rrR__fR = 349,
    uninspired_specialized_instruction_opcode_mulo___rrR__nR___rrR__fR = 350,
    uninspired_specialized_instruction_opcode_mulo__nR___rrR___rrR__fR = 351,
    uninspired_specialized_instruction_opcode_mulo__nR__n2___rrR__fR = 352,
    uninspired_specialized_instruction_opcode_mulo__nR__nR___rrR__fR = 353,
    uninspired_specialized_instruction_opcode_nop = 354,
    uninspired_specialized_instruction_opcode_print_mpending_msignals__retR = 355,
    uninspired_specialized_instruction_opcode_print_mtopmost__nR__retR = 356,
    uninspired_specialized_instruction_opcode_printfixnum___rrR__retR = 357,
    uninspired_specialized_instruction_opcode_printfixnum__nR__retR = 358,
    uninspired_specialized_instruction_opcode_procedurecall__fR__retR = 359,
    uninspired_specialized_instruction_opcode_procedurecallr___rrR__retR = 360,
    uninspired_specialized_instruction_opcode_procedurecallr__lR__retR = 361,
    uninspired_specialized_instruction_opcode_procedureprolog = 362,
    uninspired_specialized_instruction_opcode_procedurereturn = 363,
    uninspired_specialized_instruction_opcode_push_mdepths__nR__retR = 364,
    uninspired_specialized_instruction_opcode_push_mhandler__n0__lR = 365,
    uninspired_specialized_instruction_opcode_push_mhandler__n1__lR = 366,
    uninspired_specialized_instruction_opcode_push_mhandler__n2__lR = 367,
    uninspired_specialized_instruction_opcode_push_mhandler__n3__lR = 368,
    uninspired_specialized_instruction_opcode_push_mhandler__n4__lR = 369,
    uninspired_specialized_instruction_opcode_push_mhandler__n5__lR = 370,
    uninspired_specialized_instruction_opcode_push_mhandler__n6__lR = 371,
    uninspired_specialized_instruction_opcode_push_mhandler__n7__lR = 372,
    uninspired_specialized_instruction_opcode_push_mhandler__n8__lR = 373,
    uninspired_specialized_instruction_opcode_push_mhandler__n9__lR = 374,
    uninspired_specialized_instruction_opcode_push_mhandler__n10__lR = 375,
    uninspired_specialized_instruction_opcode_push_mhandler__nR__lR = 376,
    uninspired_specialized_instruction_opcode_push_mincreasing__nR__retR = 377,
    uninspired_specialized_instruction_opcode_quake = 378,
    uninspired_specialized_instruction_opcode_raise___rrR = 379,
    uninspired_specialized_instruction_opcode_raise__n0 = 380,
    uninspired_specialized_instruction_opcode_raise__n1 = 381,
    uninspired_specialized_instruction_opcode_raise__n2 = 382,
    uninspired_specialized_instruction_opcode_raise__n3 = 383,
    uninspired_specialized_instruction_opcode_raise__n4 = 384,
    uninspired_specialized_instruction_opcode_raise__n5 = 385,
    uninspired_specialized_instruction_opcode_raise__n6 = 386,
    uninspired_specialized_instruction_opcode_raise__n7 = 387,
    uninspired_specialized_instruction_opcode_raise__n8 = 388,
    uninspired_specialized_instruction_opcode_raise__n9 = 389,
    uninspired_specialized_instruction_opcode_raise__n10 = 390,
    uninspired_specialized_instruction_opcode_raise__nR = 391,
    uninspired_specialized_instruction_opcode_random___rrR__retR = 392,
    uninspired_specialized_instruction_opcode_reverse__n0 = 393,
    uninspired_specialized_instruction_opcode_reverse__n1 = 394,
    uninspired_specialized_instruction_opcode_reverse__n2 = 395,
    uninspired_specialized_instruction_opcode_reverse__n3 = 396,
    uninspired_specialized_instruction_opcode_reverse__n4 = 397,
    uninspired_specialized_instruction_opcode_reverse__n5 = 398,
    uninspired_specialized_instruction_opcode_reverse__n6 = 399,
    uninspired_specialized_instruction_opcode_reverse__n7 = 400,
    uninspired_specialized_instruction_opcode_reverse__n8 = 401,
    uninspired_specialized_instruction_opcode_reverse__n9 = 402,
    uninspired_specialized_instruction_opcode_reverse__n10 = 403,
    uninspired_specialized_instruction_opcode_reverse__n11 = 404,
    uninspired_specialized_instruction_opcode_reverse__n12 = 405,
    uninspired_specialized_instruction_opcode_reverse__n13 = 406,
    uninspired_specialized_instruction_opcode_reverse__n14 = 407,
    uninspired_specialized_instruction_opcode_reverse__n15 = 408,
    uninspired_specialized_instruction_opcode_reverse__n16 = 409,
    uninspired_specialized_instruction_opcode_reverse__n17 = 410,
    uninspired_specialized_instruction_opcode_reverse__n18 = 411,
    uninspired_specialized_instruction_opcode_reverse__n19 = 412,
    uninspired_specialized_instruction_opcode_reverse__n20 = 413,
    uninspired_specialized_instruction_opcode_reverse__n21 = 414,
    uninspired_specialized_instruction_opcode_reverse__n22 = 415,
    uninspired_specialized_instruction_opcode_reverse__n23 = 416,
    uninspired_specialized_instruction_opcode_reverse__n24 = 417,
    uninspired_specialized_instruction_opcode_reverse__n25 = 418,
    uninspired_specialized_instruction_opcode_reverse__n26 = 419,
    uninspired_specialized_instruction_opcode_reverse__n27 = 420,
    uninspired_specialized_instruction_opcode_reverse__n28 = 421,
    uninspired_specialized_instruction_opcode_reverse__n29 = 422,
    uninspired_specialized_instruction_opcode_reverse__n30 = 423,
    uninspired_specialized_instruction_opcode_reverse__n31 = 424,
    uninspired_specialized_instruction_opcode_reverse__n32 = 425,
    uninspired_specialized_instruction_opcode_reverse__n33 = 426,
    uninspired_specialized_instruction_opcode_reverse__n34 = 427,
    uninspired_specialized_instruction_opcode_reverse__n35 = 428,
    uninspired_specialized_instruction_opcode_reverse__n36 = 429,
    uninspired_specialized_instruction_opcode_reverse__n37 = 430,
    uninspired_specialized_instruction_opcode_reverse__n38 = 431,
    uninspired_specialized_instruction_opcode_reverse__n39 = 432,
    uninspired_specialized_instruction_opcode_reverse__n40 = 433,
    uninspired_specialized_instruction_opcode_reverse__n41 = 434,
    uninspired_specialized_instruction_opcode_reverse__n42 = 435,
    uninspired_specialized_instruction_opcode_reverse__n43 = 436,
    uninspired_specialized_instruction_opcode_reverse__n44 = 437,
    uninspired_specialized_instruction_opcode_reverse__n45 = 438,
    uninspired_specialized_instruction_opcode_reverse__n46 = 439,
    uninspired_specialized_instruction_opcode_reverse__n47 = 440,
    uninspired_specialized_instruction_opcode_reverse__n48 = 441,
    uninspired_specialized_instruction_opcode_reverse__n49 = 442,
    uninspired_specialized_instruction_opcode_reverse__n50 = 443,
    uninspired_specialized_instruction_opcode_reverse__n51 = 444,
    uninspired_specialized_instruction_opcode_reverse__n52 = 445,
    uninspired_specialized_instruction_opcode_reverse__n53 = 446,
    uninspired_specialized_instruction_opcode_reverse__n54 = 447,
    uninspired_specialized_instruction_opcode_reverse__n55 = 448,
    uninspired_specialized_instruction_opcode_reverse__n56 = 449,
    uninspired_specialized_instruction_opcode_reverse__n57 = 450,
    uninspired_specialized_instruction_opcode_reverse__n58 = 451,
    uninspired_specialized_instruction_opcode_reverse__n59 = 452,
    uninspired_specialized_instruction_opcode_reverse__n60 = 453,
    uninspired_specialized_instruction_opcode_reverse__n61 = 454,
    uninspired_specialized_instruction_opcode_reverse__n62 = 455,
    uninspired_specialized_instruction_opcode_reverse__n63 = 456,
    uninspired_specialized_instruction_opcode_reverse__n64 = 457,
    uninspired_specialized_instruction_opcode_reverse__n65 = 458,
    uninspired_specialized_instruction_opcode_reverse__n66 = 459,
    uninspired_specialized_instruction_opcode_reverse__n67 = 460,
    uninspired_specialized_instruction_opcode_reverse__n68 = 461,
    uninspired_specialized_instruction_opcode_reverse__n69 = 462,
    uninspired_specialized_instruction_opcode_reverse__n70 = 463,
    uninspired_specialized_instruction_opcode_reverse__nR = 464,
    uninspired_specialized_instruction_opcode_roll__n0 = 465,
    uninspired_specialized_instruction_opcode_roll__n1 = 466,
    uninspired_specialized_instruction_opcode_roll__n2 = 467,
    uninspired_specialized_instruction_opcode_roll__n3 = 468,
    uninspired_specialized_instruction_opcode_roll__n4 = 469,
    uninspired_specialized_instruction_opcode_roll__n5 = 470,
    uninspired_specialized_instruction_opcode_roll__n6 = 471,
    uninspired_specialized_instruction_opcode_roll__n7 = 472,
    uninspired_specialized_instruction_opcode_roll__n8 = 473,
    uninspired_specialized_instruction_opcode_roll__n9 = 474,
    uninspired_specialized_instruction_opcode_roll__n10 = 475,
    uninspired_specialized_instruction_opcode_roll__n11 = 476,
    uninspired_specialized_instruction_opcode_roll__n12 = 477,
    uninspired_specialized_instruction_opcode_roll__n13 = 478,
    uninspired_specialized_instruction_opcode_roll__n14 = 479,
    uninspired_specialized_instruction_opcode_roll__n15 = 480,
    uninspired_specialized_instruction_opcode_roll__n16 = 481,
    uninspired_specialized_instruction_opcode_roll__n17 = 482,
    uninspired_specialized_instruction_opcode_roll__n18 = 483,
    uninspired_specialized_instruction_opcode_roll__n19 = 484,
    uninspired_specialized_instruction_opcode_roll__n20 = 485,
    uninspired_specialized_instruction_opcode_roll__n21 = 486,
    uninspired_specialized_instruction_opcode_roll__n22 = 487,
    uninspired_specialized_instruction_opcode_roll__n23 = 488,
    uninspired_specialized_instruction_opcode_roll__n24 = 489,
    uninspired_specialized_instruction_opcode_roll__n25 = 490,
    uninspired_specialized_instruction_opcode_roll__n26 = 491,
    uninspired_specialized_instruction_opcode_roll__n27 = 492,
    uninspired_specialized_instruction_opcode_roll__n28 = 493,
    uninspired_specialized_instruction_opcode_roll__n29 = 494,
    uninspired_specialized_instruction_opcode_roll__n30 = 495,
    uninspired_specialized_instruction_opcode_roll__n31 = 496,
    uninspired_specialized_instruction_opcode_roll__n32 = 497,
    uninspired_specialized_instruction_opcode_roll__n33 = 498,
    uninspired_specialized_instruction_opcode_roll__n34 = 499,
    uninspired_specialized_instruction_opcode_roll__n35 = 500,
    uninspired_specialized_instruction_opcode_roll__n36 = 501,
    uninspired_specialized_instruction_opcode_roll__n37 = 502,
    uninspired_specialized_instruction_opcode_roll__n38 = 503,
    uninspired_specialized_instruction_opcode_roll__n39 = 504,
    uninspired_specialized_instruction_opcode_roll__n40 = 505,
    uninspired_specialized_instruction_opcode_roll__n41 = 506,
    uninspired_specialized_instruction_opcode_roll__n42 = 507,
    uninspired_specialized_instruction_opcode_roll__n43 = 508,
    uninspired_specialized_instruction_opcode_roll__n44 = 509,
    uninspired_specialized_instruction_opcode_roll__n45 = 510,
    uninspired_specialized_instruction_opcode_roll__n46 = 511,
    uninspired_specialized_instruction_opcode_roll__n47 = 512,
    uninspired_specialized_instruction_opcode_roll__n48 = 513,
    uninspired_specialized_instruction_opcode_roll__n49 = 514,
    uninspired_specialized_instruction_opcode_roll__n50 = 515,
    uninspired_specialized_instruction_opcode_roll__n51 = 516,
    uninspired_specialized_instruction_opcode_roll__n52 = 517,
    uninspired_specialized_instruction_opcode_roll__n53 = 518,
    uninspired_specialized_instruction_opcode_roll__n54 = 519,
    uninspired_specialized_instruction_opcode_roll__n55 = 520,
    uninspired_specialized_instruction_opcode_roll__n56 = 521,
    uninspired_specialized_instruction_opcode_roll__n57 = 522,
    uninspired_specialized_instruction_opcode_roll__n58 = 523,
    uninspired_specialized_instruction_opcode_roll__n59 = 524,
    uninspired_specialized_instruction_opcode_roll__n60 = 525,
    uninspired_specialized_instruction_opcode_roll__n61 = 526,
    uninspired_specialized_instruction_opcode_roll__n62 = 527,
    uninspired_specialized_instruction_opcode_roll__n63 = 528,
    uninspired_specialized_instruction_opcode_roll__n64 = 529,
    uninspired_specialized_instruction_opcode_roll__n65 = 530,
    uninspired_specialized_instruction_opcode_roll__n66 = 531,
    uninspired_specialized_instruction_opcode_roll__n67 = 532,
    uninspired_specialized_instruction_opcode_roll__n68 = 533,
    uninspired_specialized_instruction_opcode_roll__n69 = 534,
    uninspired_specialized_instruction_opcode_roll__n70 = 535,
    uninspired_specialized_instruction_opcode_roll__nR = 536,
    uninspired_specialized_instruction_opcode_rollforth = 537,
    uninspired_specialized_instruction_opcode_rot = 538,
    uninspired_specialized_instruction_opcode_safe_mpoint__fR = 539,
    uninspired_specialized_instruction_opcode_set_mpending = 540,
    uninspired_specialized_instruction_opcode_slide__n1__n1 = 541,
    uninspired_specialized_instruction_opcode_slide__n1__n2 = 542,
    uninspired_specialized_instruction_opcode_slide__n1__n3 = 543,
    uninspired_specialized_instruction_opcode_slide__n1__n4 = 544,
    uninspired_specialized_instruction_opcode_slide__n1__n5 = 545,
    uninspired_specialized_instruction_opcode_slide__n1__nR = 546,
    uninspired_specialized_instruction_opcode_slide__n2__n1 = 547,
    uninspired_specialized_instruction_opcode_slide__n2__n2 = 548,
    uninspired_specialized_instruction_opcode_slide__n2__n3 = 549,
    uninspired_specialized_instruction_opcode_slide__n2__n4 = 550,
    uninspired_specialized_instruction_opcode_slide__n2__n5 = 551,
    uninspired_specialized_instruction_opcode_slide__n2__nR = 552,
    uninspired_specialized_instruction_opcode_slide__n3__n1 = 553,
    uninspired_specialized_instruction_opcode_slide__n3__n2 = 554,
    uninspired_specialized_instruction_opcode_slide__n3__n3 = 555,
    uninspired_specialized_instruction_opcode_slide__n3__n4 = 556,
    uninspired_specialized_instruction_opcode_slide__n3__n5 = 557,
    uninspired_specialized_instruction_opcode_slide__n3__nR = 558,
    uninspired_specialized_instruction_opcode_slide__nR__n1 = 559,
    uninspired_specialized_instruction_opcode_slide__nR__n2 = 560,
    uninspired_specialized_instruction_opcode_slide__nR__n3 = 561,
    uninspired_specialized_instruction_opcode_slide__nR__n4 = 562,
    uninspired_specialized_instruction_opcode_slide__nR__n5 = 563,
    uninspired_specialized_instruction_opcode_slide__nR__nR = 564,
    uninspired_specialized_instruction_opcode_slideforth = 565,
    uninspired_specialized_instruction_opcode_stackdrop = 566,
    uninspired_specialized_instruction_opcode_stackdup = 567,
    uninspired_specialized_instruction_opcode_stackif__fR = 568,
    uninspired_specialized_instruction_opcode_stacknip = 569,
    uninspired_specialized_instruction_opcode_stacknondroppingif__fR = 570,
    uninspired_specialized_instruction_opcode_stacknot = 571,
    uninspired_specialized_instruction_opcode_stackoneminus = 572,
    uninspired_specialized_instruction_opcode_stackoneplus = 573,
    uninspired_specialized_instruction_opcode_stackover = 574,
    uninspired_specialized_instruction_opcode_stackpeek___rrR = 575,
    uninspired_specialized_instruction_opcode_stackplus = 576,
    uninspired_specialized_instruction_opcode_stackplusr___rrR = 577,
    uninspired_specialized_instruction_opcode_stackpop___rrR = 578,
    uninspired_specialized_instruction_opcode_stackprint__retR = 579,
    uninspired_specialized_instruction_opcode_stackpush___rrR = 580,
    uninspired_specialized_instruction_opcode_stackpush__n0 = 581,
    uninspired_specialized_instruction_opcode_stackpush__n1 = 582,
    uninspired_specialized_instruction_opcode_stackpush__nR = 583,
    uninspired_specialized_instruction_opcode_stackpush__lR = 584,
    uninspired_specialized_instruction_opcode_stackpushunspecified = 585,
    uninspired_specialized_instruction_opcode_stackset___rrR = 586,
    uninspired_specialized_instruction_opcode_stackset__n0 = 587,
    uninspired_specialized_instruction_opcode_stackset__n1 = 588,
    uninspired_specialized_instruction_opcode_stackset__nR = 589,
    uninspired_specialized_instruction_opcode_stackset__lR = 590,
    uninspired_specialized_instruction_opcode_stackswap = 591,
    uninspired_specialized_instruction_opcode_stackswaptop___rrR = 592,
    uninspired_specialized_instruction_opcode_stacktimes = 593,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR___rrR = 594,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__n0 = 595,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__n8 = 596,
    uninspired_specialized_instruction_opcode_storewithbyteoffset___rrR___rrR__nR = 597,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR___rrR = 598,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__n0 = 599,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__n8 = 600,
    uninspired_specialized_instruction_opcode_storewithbyteoffset__nR___rrR__nR = 601,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR___rrR = 602,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n0 = 603,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n1 = 604,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__n2 = 605,
    uninspired_specialized_instruction_opcode_storewithwordoffset___rrR___rrR__nR = 606,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR___rrR = 607,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n0 = 608,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n1 = 609,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__n2 = 610,
    uninspired_specialized_instruction_opcode_storewithwordoffset__nR___rrR__nR = 611,
    uninspired_specialized_instruction_opcode_sub___rrR___rrR___rrR = 612,
    uninspired_specialized_instruction_opcode_sub___rrR__n1___rrR = 613,
    uninspired_specialized_instruction_opcode_sub___rrR__nR___rrR = 614,
    uninspired_specialized_instruction_opcode_sub__n0___rrR___rrR = 615,
    uninspired_specialized_instruction_opcode_sub__n0__n1___rrR = 616,
    uninspired_specialized_instruction_opcode_sub__n0__nR___rrR = 617,
    uninspired_specialized_instruction_opcode_sub__nR___rrR___rrR = 618,
    uninspired_specialized_instruction_opcode_sub__nR__n1___rrR = 619,
    uninspired_specialized_instruction_opcode_sub__nR__nR___rrR = 620,
    uninspired_specialized_instruction_opcode_subo___rrR___rrR___rrR__fR = 621,
    uninspired_specialized_instruction_opcode_subo___rrR__n1___rrR__fR = 622,
    uninspired_specialized_instruction_opcode_subo___rrR__nR___rrR__fR = 623,
    uninspired_specialized_instruction_opcode_subo__nR___rrR___rrR__fR = 624,
    uninspired_specialized_instruction_opcode_subo__nR__n1___rrR__fR = 625,
    uninspired_specialized_instruction_opcode_subo__nR__nR___rrR__fR = 626,
    uninspired_specialized_instruction_opcode_tuck = 627,
    uninspired_specialized_instruction_opcode_unreachable = 628,
    uninspired_specialized_instruction_opcode_whirl__n0 = 629,
    uninspired_specialized_instruction_opcode_whirl__n1 = 630,
    uninspired_specialized_instruction_opcode_whirl__n2 = 631,
    uninspired_specialized_instruction_opcode_whirl__n3 = 632,
    uninspired_specialized_instruction_opcode_whirl__nR = 633,
    uninspired_specialized_instruction_opcode_whirlforth = 634,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo___rrR___rrR___rrR__fR__retR = 635,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo___rrR__n1___rrR__fR__retR = 636,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo___rrR__n_m1___rrR__fR__retR = 637,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo___rrR__nR___rrR__fR__retR = 638,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo__nR___rrR___rrR__fR__retR = 639,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo__nR__n1___rrR__fR__retR = 640,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo__nR__n_m1___rrR__fR__retR = 641,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_maddo__nR__nR___rrR__fR__retR = 642,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mb__fR__retR = 643,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband___rrR___rrR__fR__retR = 644,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband___rrR__n3__fR__retR = 645,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband___rrR__n7__fR__retR = 646,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband___rrR__nR__fR__retR = 647,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband__nR___rrR__fR__retR = 648,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband__nR__n3__fR__retR = 649,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband__nR__n7__fR__retR = 650,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mband__nR__nR__fR__retR = 651,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbeq___rrR___rrR__fR__retR = 652,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbeq___rrR__nR__fR__retR = 653,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbeq__nR___rrR__fR__retR = 654,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbeq__nR__nR__fR__retR = 655,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR___rrR__fR__retR = 656,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__n0__fR__retR = 657,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__n1__fR__retR = 658,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge___rrR__nR__fR__retR = 659,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n0___rrR__fR__retR = 660,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__n0__fR__retR = 661,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__n1__fR__retR = 662,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n0__nR__fR__retR = 663,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n1___rrR__fR__retR = 664,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n1__n0__fR__retR = 665,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n1__n1__fR__retR = 666,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__n1__nR__fR__retR = 667,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__nR___rrR__fR__retR = 668,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__n0__fR__retR = 669,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__n1__fR__retR = 670,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbge__nR__nR__fR__retR = 671,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu___rrR___rrR__fR__retR = 672,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu___rrR__n0__fR__retR = 673,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu___rrR__n1__fR__retR = 674,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu___rrR__nR__fR__retR = 675,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n0___rrR__fR__retR = 676,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n0__n0__fR__retR = 677,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n0__n1__fR__retR = 678,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n0__nR__fR__retR = 679,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n1___rrR__fR__retR = 680,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n1__n0__fR__retR = 681,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n1__n1__fR__retR = 682,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__n1__nR__fR__retR = 683,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__nR___rrR__fR__retR = 684,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__nR__n0__fR__retR = 685,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__nR__n1__fR__retR = 686,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgeu__nR__nR__fR__retR = 687,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt___rrR___rrR__fR__retR = 688,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt___rrR__n0__fR__retR = 689,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt___rrR__n1__fR__retR = 690,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt___rrR__nR__fR__retR = 691,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n0___rrR__fR__retR = 692,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n0__n0__fR__retR = 693,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n0__n1__fR__retR = 694,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n0__nR__fR__retR = 695,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n1___rrR__fR__retR = 696,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n1__n0__fR__retR = 697,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n1__n1__fR__retR = 698,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__n1__nR__fR__retR = 699,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__nR___rrR__fR__retR = 700,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__nR__n0__fR__retR = 701,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__nR__n1__fR__retR = 702,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgt__nR__nR__fR__retR = 703,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu___rrR___rrR__fR__retR = 704,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu___rrR__n0__fR__retR = 705,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu___rrR__n1__fR__retR = 706,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu___rrR__nR__fR__retR = 707,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n0___rrR__fR__retR = 708,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n0__n0__fR__retR = 709,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n0__n1__fR__retR = 710,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n0__nR__fR__retR = 711,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n1___rrR__fR__retR = 712,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n1__n0__fR__retR = 713,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n1__n1__fR__retR = 714,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__n1__nR__fR__retR = 715,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__nR___rrR__fR__retR = 716,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__nR__n0__fR__retR = 717,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__nR__n1__fR__retR = 718,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbgtu__nR__nR__fR__retR = 719,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble___rrR___rrR__fR__retR = 720,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__n0__fR__retR = 721,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__n1__fR__retR = 722,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble___rrR__nR__fR__retR = 723,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n0___rrR__fR__retR = 724,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n0__n0__fR__retR = 725,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n0__n1__fR__retR = 726,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n0__nR__fR__retR = 727,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n1___rrR__fR__retR = 728,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n1__n0__fR__retR = 729,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n1__n1__fR__retR = 730,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__n1__nR__fR__retR = 731,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__nR___rrR__fR__retR = 732,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__nR__n0__fR__retR = 733,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__nR__n1__fR__retR = 734,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mble__nR__nR__fR__retR = 735,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu___rrR___rrR__fR__retR = 736,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu___rrR__n0__fR__retR = 737,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu___rrR__n1__fR__retR = 738,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu___rrR__nR__fR__retR = 739,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n0___rrR__fR__retR = 740,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n0__n0__fR__retR = 741,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n0__n1__fR__retR = 742,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n0__nR__fR__retR = 743,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n1___rrR__fR__retR = 744,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n1__n0__fR__retR = 745,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n1__n1__fR__retR = 746,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__n1__nR__fR__retR = 747,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__nR___rrR__fR__retR = 748,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__nR__n0__fR__retR = 749,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__nR__n1__fR__retR = 750,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbleu__nR__nR__fR__retR = 751,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt___rrR___rrR__fR__retR = 752,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt___rrR__n0__fR__retR = 753,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt___rrR__n1__fR__retR = 754,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt___rrR__nR__fR__retR = 755,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n0___rrR__fR__retR = 756,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n0__n0__fR__retR = 757,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n0__n1__fR__retR = 758,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n0__nR__fR__retR = 759,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n1___rrR__fR__retR = 760,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n1__n0__fR__retR = 761,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n1__n1__fR__retR = 762,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__n1__nR__fR__retR = 763,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__nR___rrR__fR__retR = 764,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__nR__n0__fR__retR = 765,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__nR__n1__fR__retR = 766,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mblt__nR__nR__fR__retR = 767,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu___rrR___rrR__fR__retR = 768,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu___rrR__n0__fR__retR = 769,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu___rrR__n1__fR__retR = 770,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu___rrR__nR__fR__retR = 771,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n0___rrR__fR__retR = 772,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n0__n0__fR__retR = 773,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n0__n1__fR__retR = 774,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n0__nR__fR__retR = 775,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n1___rrR__fR__retR = 776,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n1__n0__fR__retR = 777,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n1__n1__fR__retR = 778,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__n1__nR__fR__retR = 779,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__nR___rrR__fR__retR = 780,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__nR__n0__fR__retR = 781,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__nR__n1__fR__retR = 782,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbltu__nR__nR__fR__retR = 783,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR___rrR__fR__retR = 784,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__n0__fR__retR = 785,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne___rrR__nR__fR__retR = 786,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__n0___rrR__fR__retR = 787,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__n0__fR__retR = 788,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__n0__nR__fR__retR = 789,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__nR___rrR__fR__retR = 790,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__n0__fR__retR = 791,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbne__nR__nR__fR__retR = 792,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbneg___rrR__fR__retR = 793,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbneg__nR__fR__retR = 794,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnneg___rrR__fR__retR = 795,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnneg__nR__fR__retR = 796,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand___rrR___rrR__fR__retR = 797,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand___rrR__n3__fR__retR = 798,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand___rrR__n7__fR__retR = 799,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand___rrR__nR__fR__retR = 800,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand__nR___rrR__fR__retR = 801,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand__nR__n3__fR__retR = 802,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand__nR__n7__fR__retR = 803,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnotand__nR__nR__fR__retR = 804,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnpos___rrR__fR__retR = 805,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnpos__nR__fR__retR = 806,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnz___rrR__fR__retR = 807,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbnz__nR__fR__retR = 808,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbpos___rrR__fR__retR = 809,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbpos__nR__fR__retR = 810,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbr___rrR__retR = 811,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbz___rrR__fR__retR = 812,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mbz__nR__fR__retR = 813,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo___rrR___rrR___rrR__fR__retR = 814,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo___rrR__n2___rrR__fR__retR = 815,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo___rrR__nR___rrR__fR__retR = 816,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo__nR___rrR___rrR__fR__retR = 817,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo__nR__n2___rrR__fR__retR = 818,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mdivo__nR__nR___rrR__fR__retR = 819,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mendvm__retR = 820,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR = 821,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo___rrR___rrR___rrR__fR__retR = 822,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo___rrR__n2___rrR__fR__retR = 823,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo___rrR__nR___rrR__fR__retR = 824,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo__nR___rrR___rrR__fR__retR = 825,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo__nR__n2___rrR__fR__retR = 826,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmodo__nR__nR___rrR__fR__retR = 827,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo___rrR___rrR___rrR__fR__retR = 828,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo___rrR__n2___rrR__fR__retR = 829,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo___rrR__nR___rrR__fR__retR = 830,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo__nR___rrR___rrR__fR__retR = 831,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo__nR__n2___rrR__fR__retR = 832,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mmulo__nR__nR___rrR__fR__retR = 833,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mprocedurecall__fR__retR = 834,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mprocedurecallr___rrR__retR = 835,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mprocedurecallr__lR__retR = 836,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mprocedureprolog__retR = 837,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mprocedurereturn__retR = 838,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise___rrR__retR = 839,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n0__retR = 840,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n1__retR = 841,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n2__retR = 842,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n3__retR = 843,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n4__retR = 844,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n5__retR = 845,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n6__retR = 846,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n7__retR = 847,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n8__retR = 848,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n9__retR = 849,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__n10__retR = 850,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mraise__nR__retR = 851,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msafe_mpoint__fR__retR = 852,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mstackif__fR__retR = 853,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_mstacknondroppingif__fR__retR = 854,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo___rrR___rrR___rrR__fR__retR = 855,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo___rrR__n1___rrR__fR__retR = 856,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo___rrR__nR___rrR__fR__retR = 857,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo__nR___rrR___rrR__fR__retR = 858,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo__nR__n1___rrR__fR__retR = 859,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_msubo__nR__nR___rrR__fR__retR = 860,
    uninspired_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR = 861
  };

#define UNINSPIRED_SPECIALIZED_INSTRUCTION_NO 862

#endif // #ifndef UNINSPIRED_SPECIALIZED_INSTRUCTIONS_H_
/* How many residuals we can have at most.  This, with some dispatches,
   is needed to compute a slow register offset from the base. */
#define UNINSPIRED_MAX_RESIDUAL_ARITY  5

/* Stack operations.
 * ************************************************************************** */

/* The following stack operations (with the initial state
   pointer argument) can be used *out* of instruction code
   blocks, in non-VM code.
   Macros with the same names are available from instruction
   code blocks, but those alternative definitions lack the first
   argument: the state they operate on is always the current
   state -- in particular, its runtime. */

/* Wrapper definition of the top operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_TOP_STACK(state_p)  \
  JITTER_STACK_TOS_TOP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the under_top operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_UNDER_TOP_STACK(state_p)  \
  JITTER_STACK_TOS_UNDER_TOP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the at_depth operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_AT_DEPTH_STACK(state_p, x0)  \
  JITTER_STACK_TOS_AT_DEPTH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the at_nonzero_depth operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_AT_NONZERO_DEPTH_STACK(state_p, x0)  \
  JITTER_STACK_TOS_AT_NONZERO_DEPTH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the set_at_depth operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_SET_AT_DEPTH_STACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SET_AT_DEPTH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0, x1)
/* Wrapper definition of the set_at_nonzero_depth operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_SET_AT_NONZERO_DEPTH_STACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SET_AT_NONZERO_DEPTH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0, x1)
/* Wrapper definition of the push_unspecified operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_PUSH_UNSPECIFIED_STACK(state_p)  \
  JITTER_STACK_TOS_PUSH_UNSPECIFIED (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the push operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_PUSH_STACK(state_p, x0)  \
  JITTER_STACK_TOS_PUSH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the under_push_unspecified operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_UNDER_PUSH_UNSPECIFIED_STACK(state_p)  \
  JITTER_STACK_TOS_UNDER_PUSH_UNSPECIFIED (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the under_push operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_UNDER_PUSH_STACK(state_p, x0)  \
  JITTER_STACK_TOS_UNDER_PUSH (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the drop operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_DROP_STACK(state_p)  \
  JITTER_STACK_TOS_DROP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the dup operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_DUP_STACK(state_p)  \
  JITTER_STACK_TOS_DUP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the swap operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_SWAP_STACK(state_p)  \
  JITTER_STACK_TOS_SWAP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the quake operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_QUAKE_STACK(state_p)  \
  JITTER_STACK_TOS_QUAKE (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the over operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_OVER_STACK(state_p)  \
  JITTER_STACK_TOS_OVER (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the tuck operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_TUCK_STACK(state_p)  \
  JITTER_STACK_TOS_TUCK (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the nip operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_NIP_STACK(state_p)  \
  JITTER_STACK_TOS_NIP (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the rot operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_ROT_STACK(state_p)  \
  JITTER_STACK_TOS_ROT (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the mrot operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_MROT_STACK(state_p)  \
  JITTER_STACK_TOS_MROT (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the roll operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_ROLL_STACK(state_p, x0)  \
  JITTER_STACK_TOS_ROLL (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the mroll operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_MROLL_STACK(state_p, x0)  \
  JITTER_STACK_TOS_MROLL (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the slide operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_SLIDE_STACK(state_p, x0, x1)  \
  JITTER_STACK_TOS_SLIDE (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0, x1)
/* Wrapper definition of the whirl operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_WHIRL_STACK(state_p, x0)  \
  JITTER_STACK_TOS_WHIRL (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the bulge operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_BULGE_STACK(state_p, x0)  \
  JITTER_STACK_TOS_BULGE (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the height operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_HEIGHT_STACK(state_p)  \
  JITTER_STACK_TOS_HEIGHT (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    )
/* Wrapper definition of the set_height operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_SET_HEIGHT_STACK(state_p, x0)  \
  JITTER_STACK_TOS_SET_HEIGHT (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the reverse operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_REVERSE_STACK(state_p, x0)  \
  JITTER_STACK_TOS_REVERSE (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the unary operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_UNARY_STACK(state_p, x0)  \
  JITTER_STACK_TOS_UNARY (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the binary operation for the
   TOS-optimized stack "stack". */
#define UNINSPIRED_BINARY_STACK(state_p, x0)  \
  JITTER_STACK_TOS_BINARY (union jitter_word,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    stack  \
    , x0)
/* Wrapper definition of the top operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_TOP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_TOP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the under_top operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_UNDER_TOP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_UNDER_TOP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the at_depth operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_AT_DEPTH_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_AT_DEPTH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the at_nonzero_depth operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_AT_NONZERO_DEPTH_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_AT_NONZERO_DEPTH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the set_at_depth operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_SET_AT_DEPTH_HANDLERS(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SET_AT_DEPTH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0, x1)
/* Wrapper definition of the set_at_nonzero_depth operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_SET_AT_NONZERO_DEPTH_HANDLERS(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SET_AT_NONZERO_DEPTH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0, x1)
/* Wrapper definition of the push_unspecified operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_PUSH_UNSPECIFIED_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_PUSH_UNSPECIFIED (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the push operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_PUSH_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_PUSH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the under_push_unspecified operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_UNDER_PUSH_UNSPECIFIED_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_UNDER_PUSH_UNSPECIFIED (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the under_push operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_UNDER_PUSH_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_UNDER_PUSH (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the drop operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_DROP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_DROP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the dup operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_DUP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_DUP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the swap operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_SWAP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_SWAP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the quake operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_QUAKE_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_QUAKE (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the over operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_OVER_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_OVER (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the tuck operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_TUCK_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_TUCK (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the nip operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_NIP_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_NIP (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the rot operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_ROT_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_ROT (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the mrot operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_MROT_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_MROT (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the roll operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_ROLL_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_ROLL (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the mroll operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_MROLL_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_MROLL (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the slide operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_SLIDE_HANDLERS(state_p, x0, x1)  \
  JITTER_STACK_NTOS_SLIDE (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0, x1)
/* Wrapper definition of the whirl operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_WHIRL_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_WHIRL (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the bulge operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_BULGE_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_BULGE (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the height operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_HEIGHT_HANDLERS(state_p)  \
  JITTER_STACK_NTOS_HEIGHT (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    )
/* Wrapper definition of the set_height operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_SET_HEIGHT_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_SET_HEIGHT (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the reverse operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_REVERSE_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_REVERSE (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the unary operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_UNARY_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_UNARY (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)
/* Wrapper definition of the binary operation for the
   non-TOS-optimized stack "handlers". */
#define UNINSPIRED_BINARY_HANDLERS(state_p, x0)  \
  JITTER_STACK_NTOS_BINARY (struct exception_handler,  \
    (state_p)->_uninspired_xOxBm2j5vO_state_runtime. /* not an error */,  \
    handlers  \
    , x0)

/* User-specified code, late header part: beginning. */

  
/* User-specified code, late header part: end */


/* Close the multiple-inclusion guard opened in the template. */
#endif // #ifndef UNINSPIRED_VM_H_
