## The Jittery VM for Structured.

legal-notice
  Copyright (C) 2017, 2019, 2020, 2021 Luca Saiu
  Copyright (C) 2021 pEp Foundation
  Written by Luca Saiu

  This file is part of Structured, a GNU Jitter example.

  Structured is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Structured is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Structured.  If not, see <http://www.gnu.org/licenses/>.
end


## Global configuration.
#################################################################

vm
  set prefix "structuredvm"
#  tos-stack "jitter_int" "mainstack"
end


## Register classes and stacks.
#################################################################

register-class r
  c-type "union jitter_word"
  c-initial-value "(union jitter_word) {.fixnum = 0}"
  long-name "general-register"
  fast-register-no 0 #3 #0 #5
  slow-registers
end

stack s
  long-name "mainstack"
  c-element-type "jitter_int"
  tos-optimized
  guard-underflow
  guard-overflow
end


## Functions and globals to wrap.
#################################################################

wrapped-functions
  printf
  structured_input
end

wrapped-globals
  structured_fixnum_format_string
end

early-c
  code
#   include <stdio.h>
  end
end
late-c
  code
    static const char *structured_fixnum_format_string = "%" JITTER_PRIi "\n";
  end
end


## Custom literal argument printer.
#################################################################

printer-c
  code
static void
structured_literal_printer (jitter_print_context out, jitter_uint u)
{
  jitter_print_begin_class (out, "structuredvm-number");
  jitter_print_jitter_uint (out, 10, u);
  jitter_print_end_class (out);
}
  end
end


## Utility functions in C.
#################################################################

late-c
  code
static jitter_int
structured_input (void)
{
#define STRUCTURED_PROMPT "> "
  printf (STRUCTURED_PROMPT);
  jitter_int res;
  int scanf_result;
  while ((scanf_result = scanf ("%" JITTER_PRIi, & res)) != 1)
    {
      printf ("Invalid input: expecting an integer.\n" STRUCTURED_PROMPT);
    }
  return res;
#undef STRUCTURED_PROMPT
}
  end
end


## Stack instructions.
#################################################################

instruction drop-stack ()
  code
    JITTER_DROP_MAINSTACK();
  end
end

instruction push-unspecified-stack ()
  code
    JITTER_PUSH_UNSPECIFIED_MAINSTACK();
  end
end

instruction push-stack (?Rnl 0 1 -1 2 structured_literal_printer)
  code
    jitter_int k = JITTER_ARGN0;
    JITTER_PUSH_MAINSTACK(k);
  end
end

instruction pop-stack (!R)
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    JITTER_ARGN0 = top;
  end
end

instruction print-stack ()
  non-relocatable
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    printf (structured_fixnum_format_string, top);
  end
end

instruction input-stack ()
  non-relocatable
  code
    JITTER_PUSH_MAINSTACK (structured_input ());
  end
end

instruction plus-stack ()
  code
#define F(res, a, b) { res = a + b; }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction uminus-stack ()
  code
    JITTER_TOP_MAINSTACK() = - JITTER_TOP_MAINSTACK();
  end
end

instruction minus-stack ()
  code
#define F(res, a, b) { res = a - b; }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction times-stack ()
  code
#define F(res, a, b) { res = a * b; }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction divided-stack ()
  non-relocatable # FIXME: this should be non-relocatable only on some architectures.
  code
#define F(res, a, b) { res = a / b; }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction remainder-stack ()
  non-relocatable # FIXME: this should be non-relocatable only on some architectures.
  code
#define F(res, a, b) { res = a % b; }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction equal-stack ()
  code
#define F(res, a, b) { res = (a == b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction different-stack ()
  code
#define F(res, a, b) { res = (a != b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction less-stack ()
  code
#define F(res, a, b) { res = (a < b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction lessorequal-stack ()
  code
#define F(res, a, b) { res = (a <= b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction greater-stack ()
  code
#define F(res, a, b) { res = (a > b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction greaterorequal-stack ()
  code
#define F(res, a, b) { res = (a >= b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction logicaland-stack ()
  code
#define F(res, a, b) { res = (a && b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction logicalor-stack ()
  code
#define F(res, a, b) { res = (a || b); }
    JITTER_BINARY_MAINSTACK(F);
#undef F
  end
end

instruction logicalnot-stack ()
  code
    JITTER_TOP_MAINSTACK() = ! JITTER_TOP_MAINSTACK();
  end
end

instruction isnonzero-stack ()
  code
    JITTER_TOP_MAINSTACK() = JITTER_TOP_MAINSTACK() ? 1 : 0;
  end
end

instruction b (?f)
  code
    JITTER_BRANCH_FAST(JITTER_ARGF0);
  end
end

instruction bt-stack (?f)
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    JITTER_BRANCH_FAST_IF_NONZERO(top, JITTER_ARGF0);
  end
end

instruction bf-stack (?f)
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    JITTER_BRANCH_FAST_IF_ZERO(top, JITTER_ARGF0);
  end
end


## Stack instructions: rewriting.
#################################################################

# rule
# # pop-push-to-move
#   rewrite
#     add ?R, ?Rn, ?R
#     nop
#   into
# #    copy-to-r ?R
# end

rule not-not--nothing rewrite logicalnot-stack; logicalnot-stack into end
rule not-bf--bt rewrite logicalnot-stack; bf-stack $a into bt-stack $a end
rule not-bt--bf rewrite logicalnot-stack; bt-stack $a into bf-stack $a end
rule less-not--greaterorequal rewrite less-stack; logicalnot-stack into greaterorequal-stack end
rule lessorequal-not--greater rewrite lessorequal-stack; logicalnot-stack into greater-stack end
rule greater-not--lessorequal rewrite greater-stack; logicalnot-stack into lessorequal-stack end
rule greaterorequal-not--less rewrite greaterorequal-stack; logicalnot-stack into less-stack end
rule pop-push--copytor rewrite pop-stack R $a; push-stack R $a into copy-to-r-stack $a end
rule push-pop-- rewrite push-stack R $a; pop-stack R $a into end
rule push-pop--movr rewrite push-stack R $a; pop-stack R $b into mov $a, $b end
rule push-pop--movn rewrite push-stack n $a; pop-stack R $b into mov $a, $b end
rule push-plus--plusi rewrite push-stack n $a; plus-stack into plusi-stack $a end
rule push-minus--minusi rewrite push-stack n $a; minus-stack into minusi-stack $a end
# rule push-plusi--push rewrite push-stack n $a; plusi-stack n $b into push-stack plus-stack($a, $b) end

rule push-equal--equali rewrite push-stack n $a; equal-stack into equali-stack $a end
rule equali-bt--beqi rewrite equali-stack n $a; bt-stack $b into beqi-stack $a, $b end
rule equali-bf--bneqi rewrite equali-stack n $a; bf-stack $b into bneqi-stack $a, $b end

rule push-drop-- rewrite push-stack $a; drop-stack into end
rule push-push--push-dup rewrite push-stack $a; push-stack $a into push-stack $a; dup-stack end

# rule push-bf-- rewrite push-stack N $a; bf-stack $b into when notequal ($a, 0) end
rule push-bf--b rewrite push-stack 0; bf-stack $a into b $a end
# rule push-bt--b rewrite push-stack N $a; bt-stack $b into b $b when notequal ($a, 0) end
rule push-bt-- rewrite push-stack 0; bt-stack $a into end

rule plusi-minusi-- rewrite plusi-stack $a; minusi-stack $a into end
rule minusi-plusi-- rewrite minusi-stack $a; plusi-stack $a into end
rule plusi-- rewrite plusi-stack 0 into end
rule minusi-- rewrite minusi-stack 0 into end

# rule equali0-to-not rewrite equali $a into not when is_zero ($a) end

rule push-swap--underpush rewrite push-stack $a; swap-stack into underpush-stack $a end

## Optimized instructions (scratch).
#################################################################

instruction dup-stack ()
  code
    JITTER_DUP_MAINSTACK();
  end
end

instruction copy-to-r-stack (!R)
  code
    JITTER_ARGN0 = JITTER_TOP_MAINSTACK();
  end
end

instruction plusi-stack (?n -1 1 2 structured_literal_printer)
  code
    JITTER_TOP_MAINSTACK() += JITTER_ARGN0;
  end
end

instruction minusi-stack (?n 1 2 structured_literal_printer)
  code
    JITTER_TOP_MAINSTACK() -= JITTER_ARGN0;
  end
end

# instruction minusr-stack (?R)
#   code
#     JITTER_TOP_MAINSTACK() -= JITTER_ARGN0;
#   end
# end

instruction equali-stack (?n 0 1 2 structured_literal_printer)
  code
    JITTER_TOP_MAINSTACK() = (JITTER_TOP_MAINSTACK() == JITTER_ARGN0);
  end
end

# instruction differenti-stack (?n 0 1 2 structured_literal_printer)
#   code
#     JITTER_TOP_MAINSTACK() = (JITTER_TOP_MAINSTACK() != JITTER_ARGN0);
#   end
# end

instruction beqi-stack (?n -1 0 1 2 structured_literal_printer, ?f)
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    JITTER_BRANCH_FAST_IF_EQUAL(top, JITTER_ARGN0, JITTER_ARGF1);
  end
end

instruction bneqi-stack (?n -1 0 1 2 structured_literal_printer, ?f)
  code
    jitter_int top = JITTER_TOP_MAINSTACK();
    JITTER_DROP_MAINSTACK();
    JITTER_BRANCH_FAST_IF_NOTEQUAL(top, JITTER_ARGN0, JITTER_ARGF1);
  end
end

# instruction beqr-stack (?R, ?f)
#   code
#     jitter_int top = JITTER_TOP_MAINSTACK();
#     JITTER_DROP_MAINSTACK();
#     JITTER_BRANCH_FAST_IF_EQUAL(top, JITTER_ARGN0, JITTER_ARGF1);
#   end
# end

# instruction bneqr-stack (?R, ?f)
#   code
#     jitter_int top = JITTER_TOP_MAINSTACK();
#     JITTER_DROP_MAINSTACK();
#     JITTER_BRANCH_FAST_IF_NOTEQUAL(top, JITTER_ARGN0, JITTER_ARGF1);
#   end
# end

# instruction bger-stack (?R, ?f)
#   code
#     jitter_int top = JITTER_TOP_MAINSTACK();
#     JITTER_DROP_MAINSTACK();
#     JITTER_BRANCH_FAST_IF_NOTLESS_SIGNED(top, JITTER_ARGN0, JITTER_ARGF1);
#   end
# end

# instruction pushr-minusr-pop-stack (?R, ?R, !R)
#   code
#     JITTER_ARGN2 = JITTER_ARGN0 - JITTER_ARGN1;
#   end
# end

# instruction pushr-beqr-stack (?R, ?R, ?f)
#   code
#     JITTER_BRANCH_FAST_IF_EQUAL(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
#   end
# end

# instruction pushr-bger-stack (?R, ?R, ?f)
#   code
#     JITTER_BRANCH_FAST_IF_NOTLESS_SIGNED(JITTER_ARGN0, JITTER_ARGN1,
#                                          JITTER_ARGF2);
#   end
# end

# This is useful for binding the next parameter in a callee, without touching
# the return address at the top.
instruction underpop-stack (!R)
  code
    JITTER_ARGN0 = JITTER_UNDER_TOP_MAINSTACK();
    JITTER_NIP_MAINSTACK();
  end
end

# This is useful for passing parameters to a tail call, without disturbing the
# (old) return address on the top.
instruction underpush-stack (?Rn)
  code
    JITTER_UNDER_PUSH_MAINSTACK(JITTER_ARGN0);
  end
end

# Return to the return address, which is at the undertop at the beginning of
# the instruction, and nip, leaving the procedure result on the top.
instruction return-to-undertop ()
  returning
  code
    structuredvm_label return_address = JITTER_UNDER_TOP_MAINSTACK();
    JITTER_NIP_MAINSTACK();
    JITTER_RETURN (return_address);
  end
end

# This is needed in tail calls for stack code: see structured_translate_call in
# structured-code-generator-stack.c .
instruction swap-stack ()
  code
    JITTER_SWAP_MAINSTACK();
  end
end


instruction procedure-prolog ()
  callee
  code
    JITTER_PUSH_MAINSTACK ((jitter_uint) JITTER_LINK);
  end
end

instruction call (?f)
  caller
  code
    JITTER_BRANCH_FAST_AND_LINK (JITTER_ARGF0);
  end
end

## Register instructions.
#################################################################

instruction mov (?Rn 0 1 -1 2 structured_literal_printer, !R)
  code
    JITTER_ARGN1 = JITTER_ARGN0;
  end
end

instruction plus (?Rn 1 2 structured_literal_printer,
                  ?Rn 1 2 structured_literal_printer,
                  !R)
  code
    JITTER_ARGN2 = JITTER_ARGN0 + JITTER_ARGN1;
  end
end

instruction minus (?Rn structured_literal_printer,
                   ?Rn 1 2 structured_literal_printer,
                   !R)
  code
    JITTER_ARGN2 = JITTER_ARGN0 - JITTER_ARGN1;
  end
end

instruction times (?Rn structured_literal_printer,
                   ?Rn 2 structured_literal_printer,
                   !R)
  code
    JITTER_ARGN2 = JITTER_ARGN0 * JITTER_ARGN1;
  end
end

instruction divided (?Rn structured_literal_printer,
                     ?Rn 2 structured_literal_printer,
                     !R)
  non-relocatable # FIXME: this should be non-relocatable only on some architectures.
  code
    JITTER_ARGN2 = JITTER_ARGN0 / JITTER_ARGN1;
  end
end

instruction remainder (?Rn structured_literal_printer,
                       ?Rn 2 structured_literal_printer,
                       !R)
  non-relocatable # FIXME: this should be non-relocatable only on some architectures.
  code
    JITTER_ARGN2 = JITTER_ARGN0 % JITTER_ARGN1;
  end
end

instruction uminus (?Rn structured_literal_printer,
                    !R)
  code
    JITTER_ARGN1 = - JITTER_ARGN0;
  end
end

instruction be (?Rn 0 structured_literal_printer,
                ?Rn 0 structured_literal_printer,
                ?f)
  code
    JITTER_BRANCH_FAST_IF_EQUAL(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bne (?Rn 0 structured_literal_printer,
                 ?Rn 0 structured_literal_printer,
                 ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTEQUAL(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bl (?Rn 0 structured_literal_printer,
                ?Rn 0 structured_literal_printer,
                ?f)
  code
    JITTER_BRANCH_FAST_IF_LESS_SIGNED(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction ble (?Rn 0 structured_literal_printer,
                 ?Rn 0 structured_literal_printer,
                 ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTGREATER_SIGNED(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bg (?Rn 0 structured_literal_printer,
                ?Rn 0 structured_literal_printer,
                ?f)
  code
    JITTER_BRANCH_FAST_IF_GREATER_SIGNED(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction bge (?Rn 0 structured_literal_printer,
                 ?Rn 0 structured_literal_printer,
                 ?f)
  code
    JITTER_BRANCH_FAST_IF_NOTLESS_SIGNED(JITTER_ARGN0, JITTER_ARGN1, JITTER_ARGF2);
  end
end

instruction print (?Rn structured_literal_printer)
  non-relocatable
  code
    printf (structured_fixnum_format_string, JITTER_ARGN0);
  end
end

instruction input (!R)
  non-relocatable
  code
    JITTER_ARGN0 = structured_input ();
  end
end
