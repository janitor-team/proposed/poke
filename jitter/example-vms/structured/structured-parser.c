/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.6.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         STRUCTURED_STYPE
#define YYLTYPE         STRUCTURED_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         structured_parse
#define yylex           structured_lex
#define yyerror         structured_error
#define yydebug         structured_debug
#define yynerrs         structured_nerrs

/* First part of user prologue.  */
#line 24 "../../jitter/example-vms/structured/structured.y"

#include <stdio.h>
#include <ctype.h>
#include <jitter/jitter-malloc.h>
#include <jitter/jitter-fatal.h>
#include <jitter/jitter-parse-int.h>
#include <jitter/jitter-string.h>

#include "structured-syntax.h"
#include "structured-parser.h"
#include "structured-scanner.h"

/* This is currently a fatal error.  I could longjmp away instead. */
static void
structured_error (YYLTYPE *locp, struct structured_program *p,
                  yyscan_t scanner, char *message)
  __attribute__ ((noreturn));

#define STRUCTURED_PARSE_ERROR(message)                            \
  do                                                               \
    {                                                              \
      structured_error (structured_get_lloc (structured_scanner),  \
                        p, structured_scanner, message);           \
    }                                                              \
  while (false)

/* What would be yytext in a non-reentrant scanner. */
#define STRUCTURED_TEXT \
  (structured_get_text (structured_scanner))

 /* What would be yylineno in a non-reentrant scanner. */
#define STRUCTURED_LINENO \
  (structured_get_lineno (structured_scanner))

/* A copy of what would be yytext in a non-reentrant scanner. */
#define STRUCTURED_TEXT_COPY \
  (jitter_clone_string (STRUCTURED_TEXT))

/* Initialise the fields of the pointed program, except for the main statement.
   The name will be copied.  */
static void
structured_initialize_program (struct structured_program *p,
                               const char *file_name)
{
  p->source_file_name = jitter_clone_string (file_name);
  p->procedures = NULL;
  p->procedure_no = 0;
  /* Do not initialise p->main_statement . */
}

/* Return a pointer to a fresh malloc-allocated expression of the given case.
   No field is initialized but case_. */
static struct structured_expression*
structured_make_expression (enum structured_expression_case case_)
{
  struct structured_expression *res
    = jitter_xmalloc (sizeof (struct structured_expression));
  res->case_ = case_;

  return res;
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given binary primitive and operands.  Every field is
   initalized. */
static struct structured_expression*
structured_make_binary (enum structured_primitive primitive,
                        struct structured_expression *operand_0,
                        struct structured_expression *operand_1)
{
  struct structured_expression *res
    = structured_make_expression (structured_expression_case_primitive);
  res->primitive = primitive;
  res->primitive_operand_0 = operand_0;
  res->primitive_operand_1 = operand_1;
  return res;
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given nullary primitive.  Every field is initalized. */
static struct structured_expression*
structured_make_nullary (enum structured_primitive primitive)
{
  return structured_make_binary (primitive, NULL, NULL);
}

/* Return a pointer to a fresh malloc-allocated expression of the primitive
   case, with the given unary primitive and operand.  Every field is
   initalized. */
static struct structured_expression*
structured_make_unary (enum structured_primitive primitive,
                       struct structured_expression *operand_0)
{
  return structured_make_binary (primitive, operand_0, NULL);
}

/* Return a pointer to a fresh malloc-allocated statement of the given case.
   No field is initialized but case_. */
static struct structured_statement*
structured_make_statement (enum structured_statement_case case_)
{
  struct structured_statement *res
    = jitter_xmalloc (sizeof (struct structured_statement));
  res->case_ = case_;

  return res;
}

/* Return a pointer to a fresh malloc-allocated statement containing a sequence
   setting the given variable to the pointed expression, and then the pointed
   statement. */
static struct structured_statement*
structured_make_block (structured_variable v,
                       struct structured_expression *e,
                       struct structured_statement *body)
{
  struct structured_statement *sequence
    = structured_make_statement (structured_statement_case_sequence);
  struct structured_statement *assignment
    = structured_make_statement (structured_statement_case_assignment);
  assignment->assignment_variable = v;
  assignment->assignment_expression = e;
  sequence->sequence_statement_0 = assignment;
  sequence->sequence_statement_1 = body;
  return sequence;
}

/* Add an element at the end of the pointed array of pointers, which is
   currently allocated with malloc and of size *element_no (in elements), by
   using realloc.  Add new_pointer as the new value at the end.  Increment the
   pointed size. */
static void
structured_append_pointer (void ***pointers, size_t *element_no,
                           void *new_pointer)
{
  * pointers = jitter_xrealloc (* pointers,
                                sizeof (void *) * ((* element_no) + 1));
  (* pointers) [* element_no] = new_pointer;
  (* element_no) ++;
}

/* Return a pointer to a fresh malloc-allocated procedure with the given name
   and zero formals.  The body is undefined.
   Only used as a helper for structured_program_append_procedure . */
static struct structured_procedure *
structured_make_procedure (const char *procedure_name)
{
  struct structured_procedure *res
    = jitter_xmalloc (sizeof (struct structured_procedure));
  res->procedure_name = jitter_clone_string (procedure_name);
  res->formals = NULL;
  res->formal_no = 0;
  /* Do not initialise res->body . */
  return res;
}

/* Append a fresh procedure with the name given in the pointed string to the
   pointed program. */
static void
structured_program_append_procedure (struct structured_program *p,
                                     const char *procedure_name)
{
  structured_append_pointer ((void ***) & p->procedures, & p->procedure_no,
                             structured_make_procedure (procedure_name));
}

/* Destructively append a formal with a copy of the pointed string as name to
   the pointed procedure. */
static void
structured_procedure_append_formal (struct structured_procedure *p,
                                    const char *new_formal_name)
{
  int i;
  for (i = 0; i < p->formal_no; i ++)
    if (! strcmp (p->formals [i], new_formal_name))
      jitter_fatal ("duplicated formal name %s in %s",
                    p->procedure_name, new_formal_name);
  structured_append_pointer ((void ***) & p->formals, & p->formal_no,
                             jitter_clone_string (new_formal_name));
}

/* Return a pointer to the last procedure of the pointed program.  This assumes
   that there is at least one procedure. */
static struct structured_procedure *
structured_last_procedure (struct structured_program *p)
{
  if (p->procedure_no == 0)
    jitter_fatal ("structured_last_procedure: no procedure exists");
  return p->procedures [p->procedure_no - 1];
}

/* These are used internally, when parsing sequences. */
struct structured_sequence
{
  /* A malloc-allocated array of pointers to malloc-allocated objects. */
  void **pointers;

  /* The number of pointers of the previous array. */
  size_t pointer_no;
};

/* Initialise the pointed actuals structure to be empty. */
static void
structured_initialize_sequence (struct structured_sequence *s)
{
  s->pointers = NULL;
  s->pointer_no = 0;
}

/* Return a pointer to a fresh malloc-allocated sequence structure, initialised
   to contain zero elements. */
static struct structured_sequence *
structured_make_sequence (void)
{
  struct structured_sequence *res
    = jitter_xmalloc (sizeof (struct structured_sequence));
  structured_initialize_sequence (res);
  return res;
}



#line 301 "../../jitter/example-vms/structured/structured-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
# define YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef STRUCTURED_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define STRUCTURED_DEBUG 1
#  else
#   define STRUCTURED_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define STRUCTURED_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined STRUCTURED_DEBUG */
#if STRUCTURED_DEBUG
extern int structured_debug;
#endif
/* "%code requires" blocks.  */
#line 277 "../../jitter/example-vms/structured/structured.y"

/* Simplified error-reporting facilities calling structured_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by structured_error . */
void
structured_scan_error (void *structured_scanner) __attribute__ ((noreturn));

struct structured_program *
structured_parse_file_star (FILE *input_file);

struct structured_program *
structured_parse_file (const char *input_file_name);

#line 358 "../../jitter/example-vms/structured/structured-parser.c"

/* Token kinds.  */
#ifndef STRUCTURED_TOKENTYPE
# define STRUCTURED_TOKENTYPE
  enum structured_tokentype
  {
    STRUCTURED_EMPTY = -2,
    STRUCTURED_EOF = 0,            /* "end of file"  */
    STRUCTURED_error = 256,        /* error  */
    STRUCTURED_UNDEF = 257,        /* "invalid token"  */
    PROCEDURE = 258,               /* PROCEDURE  */
    RETURN = 259,                  /* RETURN  */
    BEGIN_ = 260,                  /* BEGIN_  */
    END = 261,                     /* END  */
    SKIP = 262,                    /* SKIP  */
    VAR = 263,                     /* VAR  */
    PRINT = 264,                   /* PRINT  */
    INPUT = 265,                   /* INPUT  */
    SET_TO = 266,                  /* SET_TO  */
    SEMICOLON = 267,               /* SEMICOLON  */
    COMMA = 268,                   /* COMMA  */
    IF = 269,                      /* IF  */
    THEN = 270,                    /* THEN  */
    ELSE = 271,                    /* ELSE  */
    ELIF = 272,                    /* ELIF  */
    WHILE = 273,                   /* WHILE  */
    DO = 274,                      /* DO  */
    REPEAT = 275,                  /* REPEAT  */
    UNTIL = 276,                   /* UNTIL  */
    OPEN_PAREN = 277,              /* OPEN_PAREN  */
    CLOSE_PAREN = 278,             /* CLOSE_PAREN  */
    UNDEFINED = 279,               /* UNDEFINED  */
    VARIABLE = 280,                /* VARIABLE  */
    DECIMAL_LITERAL = 281,         /* DECIMAL_LITERAL  */
    TRUE = 282,                    /* TRUE  */
    FALSE = 283,                   /* FALSE  */
    PLUS = 284,                    /* PLUS  */
    MINUS = 285,                   /* MINUS  */
    TIMES = 286,                   /* TIMES  */
    DIVIDED = 287,                 /* DIVIDED  */
    REMAINDER = 288,               /* REMAINDER  */
    EQUAL = 289,                   /* EQUAL  */
    DIFFERENT = 290,               /* DIFFERENT  */
    LESS = 291,                    /* LESS  */
    LESS_OR_EQUAL = 292,           /* LESS_OR_EQUAL  */
    GREATER = 293,                 /* GREATER  */
    GREATER_OR_EQUAL = 294,        /* GREATER_OR_EQUAL  */
    LOGICAL_OR = 295,              /* LOGICAL_OR  */
    LOGICAL_AND = 296,             /* LOGICAL_AND  */
    LOGICAL_NOT = 297,             /* LOGICAL_NOT  */
    UNARY_MINUS = 298              /* UNARY_MINUS  */
  };
  typedef enum structured_tokentype structured_token_kind_t;
#endif
/* Token kinds.  */
#define STRUCTURED_EOF 0
#define STRUCTURED_error 256
#define STRUCTURED_UNDEF 257
#define PROCEDURE 258
#define RETURN 259
#define BEGIN_ 260
#define END 261
#define SKIP 262
#define VAR 263
#define PRINT 264
#define INPUT 265
#define SET_TO 266
#define SEMICOLON 267
#define COMMA 268
#define IF 269
#define THEN 270
#define ELSE 271
#define ELIF 272
#define WHILE 273
#define DO 274
#define REPEAT 275
#define UNTIL 276
#define OPEN_PAREN 277
#define CLOSE_PAREN 278
#define UNDEFINED 279
#define VARIABLE 280
#define DECIMAL_LITERAL 281
#define TRUE 282
#define FALSE 283
#define PLUS 284
#define MINUS 285
#define TIMES 286
#define DIVIDED 287
#define REMAINDER 288
#define EQUAL 289
#define DIFFERENT 290
#define LESS 291
#define LESS_OR_EQUAL 292
#define GREATER 293
#define GREATER_OR_EQUAL 294
#define LOGICAL_OR 295
#define LOGICAL_AND 296
#define LOGICAL_NOT 297
#define UNARY_MINUS 298

/* Value type.  */
#if ! defined STRUCTURED_STYPE && ! defined STRUCTURED_STYPE_IS_DECLARED
union STRUCTURED_STYPE
{
#line 292 "../../jitter/example-vms/structured/structured.y"

  jitter_int literal;
  structured_variable variable;
  struct structured_expression *expression;
  struct structured_statement *statement;
  struct structured_sequence *pointers;

#line 471 "../../jitter/example-vms/structured/structured-parser.c"

};
typedef union STRUCTURED_STYPE STRUCTURED_STYPE;
# define STRUCTURED_STYPE_IS_TRIVIAL 1
# define STRUCTURED_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined STRUCTURED_LTYPE && ! defined STRUCTURED_LTYPE_IS_DECLARED
typedef struct STRUCTURED_LTYPE STRUCTURED_LTYPE;
struct STRUCTURED_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define STRUCTURED_LTYPE_IS_DECLARED 1
# define STRUCTURED_LTYPE_IS_TRIVIAL 1
#endif



int structured_parse (struct structured_program *p, void* structured_scanner);

#endif /* !YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_PROCEDURE = 3,                  /* PROCEDURE  */
  YYSYMBOL_RETURN = 4,                     /* RETURN  */
  YYSYMBOL_BEGIN_ = 5,                     /* BEGIN_  */
  YYSYMBOL_END = 6,                        /* END  */
  YYSYMBOL_SKIP = 7,                       /* SKIP  */
  YYSYMBOL_VAR = 8,                        /* VAR  */
  YYSYMBOL_PRINT = 9,                      /* PRINT  */
  YYSYMBOL_INPUT = 10,                     /* INPUT  */
  YYSYMBOL_SET_TO = 11,                    /* SET_TO  */
  YYSYMBOL_SEMICOLON = 12,                 /* SEMICOLON  */
  YYSYMBOL_COMMA = 13,                     /* COMMA  */
  YYSYMBOL_IF = 14,                        /* IF  */
  YYSYMBOL_THEN = 15,                      /* THEN  */
  YYSYMBOL_ELSE = 16,                      /* ELSE  */
  YYSYMBOL_ELIF = 17,                      /* ELIF  */
  YYSYMBOL_WHILE = 18,                     /* WHILE  */
  YYSYMBOL_DO = 19,                        /* DO  */
  YYSYMBOL_REPEAT = 20,                    /* REPEAT  */
  YYSYMBOL_UNTIL = 21,                     /* UNTIL  */
  YYSYMBOL_OPEN_PAREN = 22,                /* OPEN_PAREN  */
  YYSYMBOL_CLOSE_PAREN = 23,               /* CLOSE_PAREN  */
  YYSYMBOL_UNDEFINED = 24,                 /* UNDEFINED  */
  YYSYMBOL_VARIABLE = 25,                  /* VARIABLE  */
  YYSYMBOL_DECIMAL_LITERAL = 26,           /* DECIMAL_LITERAL  */
  YYSYMBOL_TRUE = 27,                      /* TRUE  */
  YYSYMBOL_FALSE = 28,                     /* FALSE  */
  YYSYMBOL_PLUS = 29,                      /* PLUS  */
  YYSYMBOL_MINUS = 30,                     /* MINUS  */
  YYSYMBOL_TIMES = 31,                     /* TIMES  */
  YYSYMBOL_DIVIDED = 32,                   /* DIVIDED  */
  YYSYMBOL_REMAINDER = 33,                 /* REMAINDER  */
  YYSYMBOL_EQUAL = 34,                     /* EQUAL  */
  YYSYMBOL_DIFFERENT = 35,                 /* DIFFERENT  */
  YYSYMBOL_LESS = 36,                      /* LESS  */
  YYSYMBOL_LESS_OR_EQUAL = 37,             /* LESS_OR_EQUAL  */
  YYSYMBOL_GREATER = 38,                   /* GREATER  */
  YYSYMBOL_GREATER_OR_EQUAL = 39,          /* GREATER_OR_EQUAL  */
  YYSYMBOL_LOGICAL_OR = 40,                /* LOGICAL_OR  */
  YYSYMBOL_LOGICAL_AND = 41,               /* LOGICAL_AND  */
  YYSYMBOL_LOGICAL_NOT = 42,               /* LOGICAL_NOT  */
  YYSYMBOL_UNARY_MINUS = 43,               /* UNARY_MINUS  */
  YYSYMBOL_YYACCEPT = 44,                  /* $accept  */
  YYSYMBOL_program = 45,                   /* program  */
  YYSYMBOL_formals = 46,                   /* formals  */
  YYSYMBOL_non_empty_formals = 47,         /* non_empty_formals  */
  YYSYMBOL_48_1 = 48,                      /* $@1  */
  YYSYMBOL_actuals = 49,                   /* actuals  */
  YYSYMBOL_non_empty_actuals = 50,         /* non_empty_actuals  */
  YYSYMBOL_procedure_definition = 51,      /* procedure_definition  */
  YYSYMBOL_52_2 = 52,                      /* $@2  */
  YYSYMBOL_statement = 53,                 /* statement  */
  YYSYMBOL_if_statement = 54,              /* if_statement  */
  YYSYMBOL_if_statement_rest = 55,         /* if_statement_rest  */
  YYSYMBOL_statements = 56,                /* statements  */
  YYSYMBOL_one_or_more_statements = 57,    /* one_or_more_statements  */
  YYSYMBOL_block = 58,                     /* block  */
  YYSYMBOL_block_rest = 59,                /* block_rest  */
  YYSYMBOL_optional_initialization = 60,   /* optional_initialization  */
  YYSYMBOL_expression = 61,                /* expression  */
  YYSYMBOL_if_expression = 62,             /* if_expression  */
  YYSYMBOL_if_expression_rest = 63,        /* if_expression_rest  */
  YYSYMBOL_literal = 64,                   /* literal  */
  YYSYMBOL_variable = 65,                  /* variable  */
  YYSYMBOL_optional_skip = 66,             /* optional_skip  */
  YYSYMBOL_begin = 67,                     /* begin  */
  YYSYMBOL_end = 68                        /* end  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL \
             && defined STRUCTURED_STYPE_IS_TRIVIAL && STRUCTURED_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  40
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   432

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  44
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  25
/* YYNRULES -- Number of rules.  */
#define YYNRULES  71
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  143

#define YYMAXUTOK   298


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43
};

#if STRUCTURED_DEBUG
  /* YYRLINEYYN -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   346,   346,   348,   352,   354,   358,   361,   360,   367,
     368,   373,   377,   384,   383,   390,   392,   396,   399,   404,
     407,   409,   411,   426,   430,   440,   448,   451,   456,   462,
     463,   468,   470,   474,   479,   486,   488,   494,   495,   500,
     502,   505,   508,   510,   512,   514,   516,   518,   520,   522,
     524,   526,   528,   530,   532,   534,   536,   544,   552,   554,
     556,   566,   576,   581,   586,   588,   590,   595,   599,   601,
     607,   614
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if STRUCTURED_DEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "PROCEDURE", "RETURN",
  "BEGIN_", "END", "SKIP", "VAR", "PRINT", "INPUT", "SET_TO", "SEMICOLON",
  "COMMA", "IF", "THEN", "ELSE", "ELIF", "WHILE", "DO", "REPEAT", "UNTIL",
  "OPEN_PAREN", "CLOSE_PAREN", "UNDEFINED", "VARIABLE", "DECIMAL_LITERAL",
  "TRUE", "FALSE", "PLUS", "MINUS", "TIMES", "DIVIDED", "REMAINDER",
  "EQUAL", "DIFFERENT", "LESS", "LESS_OR_EQUAL", "GREATER",
  "GREATER_OR_EQUAL", "LOGICAL_OR", "LOGICAL_AND", "LOGICAL_NOT",
  "UNARY_MINUS", "$accept", "program", "formals", "non_empty_formals",
  "$@1", "actuals", "non_empty_actuals", "procedure_definition", "$@2",
  "statement", "if_statement", "if_statement_rest", "statements",
  "one_or_more_statements", "block", "block_rest",
  "optional_initialization", "expression", "if_expression",
  "if_expression_rest", "literal", "variable", "optional_skip", "begin",
  "end", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298
};
#endif

#define YYPACT_NINF (-67)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-69)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACTSTATE-NUM -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     374,    -7,   126,   -67,   -67,    -7,   145,   145,   145,   393,
     -67,     1,   374,   393,   -67,   -67,    -8,    12,   393,   -67,
     -67,   -67,   145,   145,   -67,   -67,   -67,   -67,   145,   145,
     162,   -67,     7,   -67,    -3,   176,   -67,   219,   335,     9,
     -67,   -67,   -67,   145,   145,   -67,    26,    11,   246,   -67,
     391,   -67,   -67,   -67,   145,   145,   145,   145,   145,   145,
     145,   145,   145,   145,   145,   145,   145,   145,   145,    -5,
     -67,   393,   393,   145,   192,    13,    22,   273,   -67,   -67,
      -7,   145,   -67,   231,   231,   144,     8,     8,   -24,   -24,
     -24,   -24,   -24,   -24,    -1,   -67,    14,   273,   393,    -7,
     -67,    -4,    26,   206,   -67,   -67,   145,    15,   -67,    37,
     312,   -67,   -67,   -67,   393,   145,   -67,   -67,   -67,   -67,
     273,   393,   -67,   145,   145,   -67,    26,   259,    33,    -7,
      62,   286,   -67,   393,    39,   -67,   -67,   145,    -4,   -67,
     312,   -67,   -67
};

  /* YYDEFACTSTATE-NUM -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
      29,     0,     0,    70,    69,     0,     0,     0,     0,    29,
      67,     0,    29,    31,     2,    30,     0,     0,    29,    13,
      59,    18,     0,     0,    39,    64,    65,    66,     0,     0,
       0,    40,    41,    33,    37,     0,    21,     0,     0,     0,
       1,     3,    32,     0,     9,    15,     0,     0,     0,    43,
       0,    46,    58,    17,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     9,     0,     0,
      19,    29,    29,     0,     0,     0,    10,    11,    71,    20,
       4,     0,    42,    44,    45,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    57,    56,     0,    38,    29,     0,
      34,     0,     0,     0,    16,    24,     0,     0,     5,     6,
       0,    60,    35,    36,    29,     0,    25,    26,    22,    23,
      12,    29,     7,     0,     0,    61,     0,     0,     0,     0,
       0,     0,    28,    29,     0,     8,    63,     0,     0,    14,
       0,    27,    62
};

  /* YYPGOTONTERM-NUM.  */
static const yytype_int8 yypgoto[] =
{
     -67,    55,   -67,   -60,   -67,     3,   -67,   -67,   -67,   -67,
     -67,   -66,    16,    60,   -25,   -67,   -67,    -2,   -67,   -65,
     -67,    10,   -67,   -67,   -37
};

  /* YYDEFGOTONTERM-NUM.  */
static const yytype_int16 yydefgoto[] =
{
      -1,    11,   107,   108,   129,    75,    76,    12,    47,    13,
      36,   116,    14,    15,    33,   100,    69,    77,    49,   125,
      31,    32,    17,    18,   117
};

  /* YYTABLEYYPACT[STATE-NUM] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      30,    40,    78,    43,    35,    37,    38,    98,    99,    79,
      16,    19,   114,   115,    44,    34,    65,    66,    10,    16,
      48,    50,    16,    16,    45,    39,    51,    52,    16,    67,
      73,    68,    78,    80,    46,   106,   105,   111,   121,   134,
      66,    74,    59,    60,    61,    62,    63,    64,    65,    66,
     122,   139,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,   118,    97,    41,   136,   135,
      96,   103,   141,    42,   113,   142,     0,     0,     0,   110,
       0,    16,    16,     0,     0,     0,     0,   101,   102,   132,
     109,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,   120,     0,     0,     0,    16,    34,
       0,     0,     0,   127,   112,     0,     0,     0,     0,     0,
       0,   130,   131,     0,    16,     0,     0,     0,     0,     0,
     126,    16,     0,     0,     0,   140,    20,   128,    21,   109,
      22,     0,     0,    16,     0,     0,     0,     0,    23,   138,
      24,    10,    25,    26,    27,    20,    28,     0,     0,    22,
       0,     0,     0,     0,     0,     0,     0,    23,    29,    24,
      10,    25,    26,    27,    53,    28,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,     0,    29,    70,     0,
       0,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,   104,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,   119,     0,
       0,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    71,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    81,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,     0,   133,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,   137,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,   123,   124,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    72,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,     1,     2,     3,
       0,     4,     5,     6,     0,     0,   -68,     0,     7,     0,
       0,     0,     8,     0,     9,     0,     0,     2,     3,    10,
       4,     5,     6,     0,     0,   -68,     0,     7,     0,     0,
       0,     8,     0,     9,    82,     0,     0,     0,    10,     0,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66
};

static const yytype_int16 yycheck[] =
{
       2,     0,     6,    11,     6,     7,     8,    12,    13,    46,
       0,     1,    16,    17,    22,     5,    40,    41,    25,     9,
      22,    23,    12,    13,    12,     9,    28,    29,    18,    22,
      21,    34,     6,    22,    18,    13,    23,    23,    23,     6,
      41,    43,    34,    35,    36,    37,    38,    39,    40,    41,
      13,    12,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,   102,    68,    12,     6,   129,
      67,    73,   138,    13,    99,   140,    -1,    -1,    -1,    81,
      -1,    71,    72,    -1,    -1,    -1,    -1,    71,    72,   126,
      80,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,   106,    -1,    -1,    -1,    98,    99,
      -1,    -1,    -1,   115,    98,    -1,    -1,    -1,    -1,    -1,
      -1,   123,   124,    -1,   114,    -1,    -1,    -1,    -1,    -1,
     114,   121,    -1,    -1,    -1,   137,    10,   121,    12,   129,
      14,    -1,    -1,   133,    -1,    -1,    -1,    -1,    22,   133,
      24,    25,    26,    27,    28,    10,    30,    -1,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    42,    24,
      25,    26,    27,    28,    12,    30,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    -1,    42,    12,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    12,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    12,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    15,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    15,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    -1,    15,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    15,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    16,    17,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    19,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,     3,     4,     5,
      -1,     7,     8,     9,    -1,    -1,    12,    -1,    14,    -1,
      -1,    -1,    18,    -1,    20,    -1,    -1,     4,     5,    25,
       7,     8,     9,    -1,    -1,    12,    -1,    14,    -1,    -1,
      -1,    18,    -1,    20,    23,    -1,    -1,    -1,    25,    -1,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41
};

  /* YYSTOSSTATE-NUM -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     4,     5,     7,     8,     9,    14,    18,    20,
      25,    45,    51,    53,    56,    57,    65,    66,    67,    65,
      10,    12,    14,    22,    24,    26,    27,    28,    30,    42,
      61,    64,    65,    58,    65,    61,    54,    61,    61,    56,
       0,    45,    57,    11,    22,    12,    56,    52,    61,    62,
      61,    61,    61,    12,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    22,    34,    60,
      12,    15,    19,    21,    61,    49,    50,    61,     6,    68,
      22,    15,    23,    61,    61,    61,    61,    61,    61,    61,
      61,    61,    61,    61,    61,    61,    49,    61,    12,    13,
      59,    56,    56,    61,    12,    23,    13,    46,    47,    65,
      61,    23,    56,    58,    16,    17,    55,    68,    68,    12,
      61,    23,    13,    16,    17,    63,    56,    61,    56,    48,
      61,    61,    68,    15,     6,    47,     6,    15,    56,    12,
      61,    55,    63
};

  /* YYR1YYN -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    44,    45,    45,    46,    46,    47,    48,    47,    49,
      49,    50,    50,    52,    51,    53,    53,    53,    53,    53,
      53,    53,    53,    53,    53,    54,    55,    55,    55,    56,
      56,    57,    57,    57,    58,    59,    59,    60,    60,    61,
      61,    61,    61,    61,    61,    61,    61,    61,    61,    61,
      61,    61,    61,    61,    61,    61,    61,    61,    61,    61,
      61,    62,    63,    63,    64,    64,    64,    65,    66,    66,
      67,    68
};

  /* YYR2YYN -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     2,     0,     1,     1,     0,     4,     0,
       1,     1,     3,     0,     9,     2,     4,     3,     2,     3,
       3,     2,     5,     5,     4,     4,     1,     5,     3,     0,
       1,     1,     2,     2,     3,     2,     2,     0,     2,     1,
       1,     1,     3,     2,     3,     3,     2,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     2,     1,
       4,     4,     5,     3,     1,     1,     1,     1,     0,     1,
       1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = STRUCTURED_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == STRUCTURED_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, p, structured_scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use STRUCTURED_error or STRUCTURED_UNDEF. */
#define YYERRCODE STRUCTURED_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if STRUCTURED_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YY_LOCATION_PRINT
#  if defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#   define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

#  else
#   define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#  endif
# endif /* !defined YY_LOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, p, structured_scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct structured_program *p, void* structured_scanner)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (yylocationp);
  YYUSE (p);
  YYUSE (structured_scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct structured_program *p, void* structured_scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YY_LOCATION_PRINT (yyo, *yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, p, structured_scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct structured_program *p, void* structured_scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), p, structured_scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, p, structured_scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !STRUCTURED_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !STRUCTURED_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct structured_program *p, void* structured_scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (p);
  YYUSE (structured_scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct structured_program *p, void* structured_scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined STRUCTURED_LTYPE_IS_TRIVIAL && STRUCTURED_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize;

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yynerrs = 0;
  yystate = 0;
  yyerrstatus = 0;

  yystacksize = YYINITDEPTH;
  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;


  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = STRUCTURED_EMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == STRUCTURED_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, structured_scanner);
    }

  if (yychar <= STRUCTURED_EOF)
    {
      yychar = STRUCTURED_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == STRUCTURED_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = STRUCTURED_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = STRUCTURED_EMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 347 "../../jitter/example-vms/structured/structured.y"
  { p->main_statement = (yyvsp[0].statement); }
#line 1807 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 6:
#line 359 "../../jitter/example-vms/structured/structured.y"
    { structured_procedure_append_formal (structured_last_procedure (p), (yyvsp[0].variable)); }
#line 1813 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 7:
#line 361 "../../jitter/example-vms/structured/structured.y"
    { structured_procedure_append_formal (structured_last_procedure (p), (yyvsp[-1].variable)); }
#line 1819 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 9:
#line 367 "../../jitter/example-vms/structured/structured.y"
  { (yyval.pointers) = structured_make_sequence (); }
#line 1825 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 10:
#line 369 "../../jitter/example-vms/structured/structured.y"
  { (yyval.pointers) = (yyvsp[0].pointers); }
#line 1831 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 11:
#line 374 "../../jitter/example-vms/structured/structured.y"
  { (yyval.pointers) = structured_make_sequence ();
    structured_append_pointer ((void ***) & (yyval.pointers)->pointers, & (yyval.pointers)->pointer_no,
                               (yyvsp[0].expression)); }
#line 1839 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 12:
#line 378 "../../jitter/example-vms/structured/structured.y"
  { structured_append_pointer ((void ***) & (yyval.pointers)->pointers, & (yyval.pointers)->pointer_no,
                               (yyvsp[0].expression)); }
#line 1846 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 13:
#line 384 "../../jitter/example-vms/structured/structured.y"
    { structured_program_append_procedure (p, (yyvsp[0].variable)); }
#line 1852 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 14:
#line 386 "../../jitter/example-vms/structured/structured.y"
    { structured_last_procedure (p)->body = (yyvsp[-2].statement); }
#line 1858 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 15:
#line 391 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1864 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 16:
#line 393 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_assignment);
    (yyval.statement)->assignment_variable = (yyvsp[-3].variable);
    (yyval.statement)->assignment_expression = (yyvsp[-1].expression); }
#line 1872 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 17:
#line 397 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_return);
    (yyval.statement)->return_result = (yyvsp[-1].expression); }
#line 1879 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 18:
#line 400 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_return);
    struct structured_expression *e
      = structured_make_expression (structured_expression_case_undefined);
    (yyval.statement)->return_result = e; }
#line 1888 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 19:
#line 405 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_print);
    (yyval.statement)->print_expression = (yyvsp[-1].expression); }
#line 1895 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 20:
#line 408 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[-1].statement); }
#line 1901 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 21:
#line 410 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 1907 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 22:
#line 412 "../../jitter/example-vms/structured/structured.y"
  { /* Parse "while A do B end" as "if A then repeat B until not A else
       skip". */
    struct structured_statement *r
      = structured_make_statement (structured_statement_case_repeat_until);
    r->repeat_until_body = (yyvsp[-1].statement);
    /* FIXME: clone $2 into a separate heap object, if I want to be able to free
       ASTs. */
    r->repeat_until_guard
      = structured_make_unary (structured_primitive_logical_not, (yyvsp[-3].expression));
    (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = r;
    (yyval.statement)->if_then_else_else_branch
      = structured_make_statement (structured_statement_case_skip); }
#line 1926 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 23:
#line 427 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_repeat_until);
    (yyval.statement)->repeat_until_body = (yyvsp[-3].statement);
    (yyval.statement)->repeat_until_guard = (yyvsp[-1].expression); }
#line 1934 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 24:
#line 431 "../../jitter/example-vms/structured/structured.y"
    { (yyval.statement) = structured_make_statement (structured_statement_case_call);
      (yyval.statement)->callee = (yyvsp[-3].variable);
      (yyval.statement)->actuals = (struct structured_expression **) (yyvsp[-1].pointers)->pointers;
      (yyval.statement)->actual_no = (yyvsp[-1].pointers)->pointer_no;
      /* FIXME: I could free $3 if I cared about not leaking memory at
         parsing time. */}
#line 1945 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 25:
#line 441 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = (yyvsp[-1].statement);
    (yyval.statement)->if_then_else_else_branch = (yyvsp[0].statement); }
#line 1954 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 26:
#line 449 "../../jitter/example-vms/structured/structured.y"
  { /* Parse "if A then B end" as "if A then B else skip end". */
    (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1961 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 27:
#line 452 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_if_then_else);
    (yyval.statement)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.statement)->if_then_else_then_branch = (yyvsp[-1].statement);
    (yyval.statement)->if_then_else_else_branch = (yyvsp[0].statement); }
#line 1970 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 28:
#line 457 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[-1].statement); }
#line 1976 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 29:
#line 462 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_skip); }
#line 1982 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 30:
#line 464 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 1988 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 31:
#line 469 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 1994 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 32:
#line 471 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_sequence);
    (yyval.statement)->sequence_statement_0 = (yyvsp[-1].statement);
    (yyval.statement)->sequence_statement_1 = (yyvsp[0].statement); }
#line 2002 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 33:
#line 475 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 2008 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 34:
#line 480 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = structured_make_statement (structured_statement_case_block);
    (yyval.statement)->block_variable = (yyvsp[-2].variable);
    (yyval.statement)->block_body = structured_make_block ((yyvsp[-2].variable), (yyvsp[-1].expression), (yyvsp[0].statement)); }
#line 2016 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 35:
#line 487 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 2022 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 36:
#line 489 "../../jitter/example-vms/structured/structured.y"
  { (yyval.statement) = (yyvsp[0].statement); }
#line 2028 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 37:
#line 494 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_undefined); }
#line 2034 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 38:
#line 496 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = (yyvsp[0].expression); }
#line 2040 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 39:
#line 501 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_undefined); }
#line 2046 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 40:
#line 503 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->literal = (yyvsp[0].literal); }
#line 2053 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 41:
#line 506 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_variable);
    (yyval.expression)->variable = (yyvsp[0].variable); }
#line 2060 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 42:
#line 509 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = (yyvsp[-1].expression); }
#line 2066 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 43:
#line 511 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = (yyvsp[0].expression); }
#line 2072 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 44:
#line 513 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_plus, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2078 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 45:
#line 515 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_minus, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2084 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 46:
#line 517 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_unary (structured_primitive_unary_minus, (yyvsp[0].expression)); }
#line 2090 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 47:
#line 519 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_times, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2096 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 48:
#line 521 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_divided, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2102 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 49:
#line 523 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_remainder, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2108 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 50:
#line 525 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2114 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 51:
#line 527 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_different, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2120 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 52:
#line 529 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_less, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2126 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 53:
#line 531 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_less_or_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2132 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 54:
#line 533 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_greater, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2138 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 55:
#line 535 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_binary (structured_primitive_greater_or_equal, (yyvsp[-2].expression), (yyvsp[0].expression)); }
#line 2144 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 56:
#line 537 "../../jitter/example-vms/structured/structured.y"
  { /* Parse "A and B" as "if A then B else false end". */
    (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-2].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[0].expression);
    (yyval.expression)->if_then_else_else_branch
      = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->if_then_else_else_branch->literal = 0; }
#line 2156 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 57:
#line 545 "../../jitter/example-vms/structured/structured.y"
  { /* Parse "A or B" as "if A then true else B end". */
    (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-2].expression);
    (yyval.expression)->if_then_else_then_branch
      = structured_make_expression (structured_expression_case_literal);
    (yyval.expression)->if_then_else_then_branch->literal = 1;
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 2168 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 58:
#line 553 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_unary (structured_primitive_logical_not, (yyvsp[0].expression)); }
#line 2174 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 59:
#line 555 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_nullary (structured_primitive_input); }
#line 2180 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 60:
#line 557 "../../jitter/example-vms/structured/structured.y"
    { (yyval.expression) = structured_make_expression (structured_expression_case_call);
      (yyval.expression)->callee = (yyvsp[-3].variable);
      (yyval.expression)->actuals = (struct structured_expression **) (yyvsp[-1].pointers)->pointers;
      (yyval.expression)->actual_no = (yyvsp[-1].pointers)->pointer_no;
      /* FIXME: I could free $3 if I cared about not leaking memory at
         parsing time. */}
#line 2191 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 61:
#line 567 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[-1].expression);
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 2200 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 62:
#line 577 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = structured_make_expression (structured_expression_case_if_then_else);
    (yyval.expression)->if_then_else_condition = (yyvsp[-3].expression);
    (yyval.expression)->if_then_else_then_branch = (yyvsp[-1].expression);
    (yyval.expression)->if_then_else_else_branch = (yyvsp[0].expression); }
#line 2209 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 63:
#line 582 "../../jitter/example-vms/structured/structured.y"
  { (yyval.expression) = (yyvsp[-1].expression); }
#line 2215 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 64:
#line 587 "../../jitter/example-vms/structured/structured.y"
  { (yyval.literal) = jitter_string_to_long_long_unsafe (STRUCTURED_TEXT); }
#line 2221 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 65:
#line 589 "../../jitter/example-vms/structured/structured.y"
  { (yyval.literal) = 1; }
#line 2227 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 66:
#line 591 "../../jitter/example-vms/structured/structured.y"
  { (yyval.literal) = 0; }
#line 2233 "../../jitter/example-vms/structured/structured-parser.c"
    break;

  case 67:
#line 596 "../../jitter/example-vms/structured/structured.y"
  { (yyval.variable) = STRUCTURED_TEXT_COPY; }
#line 2239 "../../jitter/example-vms/structured/structured-parser.c"
    break;


#line 2243 "../../jitter/example-vms/structured/structured-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == STRUCTURED_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (&yylloc, p, structured_scanner, YY_("syntax error"));
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= STRUCTURED_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == STRUCTURED_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, p, structured_scanner);
          yychar = STRUCTURED_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, p, structured_scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, p, structured_scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != STRUCTURED_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, p, structured_scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, p, structured_scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 618 "../../jitter/example-vms/structured/structured.y"


void
structured_error (YYLTYPE *locp, struct structured_program *p, yyscan_t structured_scanner,
                 char *message)
{
  printf ("%s:%i: %s near \"%s\".\n",
          (p != NULL) ? p->source_file_name : "<INPUT>",
          structured_get_lineno (structured_scanner), message, STRUCTURED_TEXT);
  exit (EXIT_FAILURE);
}

void
structured_scan_error (void *structured_scanner)
{
  struct structured_program *p = NULL; /* A little hack to have p in scope. */
  STRUCTURED_PARSE_ERROR("scan error");
}

static struct structured_program *
structured_parse_file_star_with_name (FILE *input_file, const char *file_name)
{
  yyscan_t scanner;
  structured_lex_init (&scanner);
  structured_set_in (input_file, scanner);

  struct structured_program *res
    = jitter_xmalloc (sizeof (struct structured_program));
  structured_initialize_program (res, file_name);
  /* FIXME: if I ever make parsing errors non-fatal, call structured_lex_destroy before
     returning, and finalize the program -- which might be incomplete! */
  if (structured_parse (res, scanner))
    structured_error (structured_get_lloc (scanner), res, scanner, "parse error");
  structured_set_in (NULL, scanner);
  structured_lex_destroy (scanner);

  return res;
}

struct structured_program *
structured_parse_file_star (FILE *input_file)
{
  return structured_parse_file_star_with_name (input_file, "<stdin>");
}

struct structured_program *
structured_parse_file (const char *input_file_name)
{
  FILE *f;
  if ((f = fopen (input_file_name, "r")) == NULL)
    jitter_fatal ("failed opening file %s", input_file_name);

  /* FIXME: if I ever make parse errors non-fatal, I'll need to close the file
     before returning. */
  struct structured_program *res
    = structured_parse_file_star_with_name (f, input_file_name);
  fclose (f);
  return res;
}
