/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
# define YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef STRUCTURED_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define STRUCTURED_DEBUG 1
#  else
#   define STRUCTURED_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define STRUCTURED_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined STRUCTURED_DEBUG */
#if STRUCTURED_DEBUG
extern int structured_debug;
#endif
/* "%code requires" blocks.  */
#line 277 "../../jitter/example-vms/structured/structured.y"

/* Simplified error-reporting facilities calling structured_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by structured_error . */
void
structured_scan_error (void *structured_scanner) __attribute__ ((noreturn));

struct structured_program *
structured_parse_file_star (FILE *input_file);

struct structured_program *
structured_parse_file (const char *input_file_name);

#line 71 "../../jitter/example-vms/structured/structured-parser.h"

/* Token kinds.  */
#ifndef STRUCTURED_TOKENTYPE
# define STRUCTURED_TOKENTYPE
  enum structured_tokentype
  {
    STRUCTURED_EMPTY = -2,
    STRUCTURED_EOF = 0,            /* "end of file"  */
    STRUCTURED_error = 256,        /* error  */
    STRUCTURED_UNDEF = 257,        /* "invalid token"  */
    PROCEDURE = 258,               /* PROCEDURE  */
    RETURN = 259,                  /* RETURN  */
    BEGIN_ = 260,                  /* BEGIN_  */
    END = 261,                     /* END  */
    SKIP = 262,                    /* SKIP  */
    VAR = 263,                     /* VAR  */
    PRINT = 264,                   /* PRINT  */
    INPUT = 265,                   /* INPUT  */
    SET_TO = 266,                  /* SET_TO  */
    SEMICOLON = 267,               /* SEMICOLON  */
    COMMA = 268,                   /* COMMA  */
    IF = 269,                      /* IF  */
    THEN = 270,                    /* THEN  */
    ELSE = 271,                    /* ELSE  */
    ELIF = 272,                    /* ELIF  */
    WHILE = 273,                   /* WHILE  */
    DO = 274,                      /* DO  */
    REPEAT = 275,                  /* REPEAT  */
    UNTIL = 276,                   /* UNTIL  */
    OPEN_PAREN = 277,              /* OPEN_PAREN  */
    CLOSE_PAREN = 278,             /* CLOSE_PAREN  */
    UNDEFINED = 279,               /* UNDEFINED  */
    VARIABLE = 280,                /* VARIABLE  */
    DECIMAL_LITERAL = 281,         /* DECIMAL_LITERAL  */
    TRUE = 282,                    /* TRUE  */
    FALSE = 283,                   /* FALSE  */
    PLUS = 284,                    /* PLUS  */
    MINUS = 285,                   /* MINUS  */
    TIMES = 286,                   /* TIMES  */
    DIVIDED = 287,                 /* DIVIDED  */
    REMAINDER = 288,               /* REMAINDER  */
    EQUAL = 289,                   /* EQUAL  */
    DIFFERENT = 290,               /* DIFFERENT  */
    LESS = 291,                    /* LESS  */
    LESS_OR_EQUAL = 292,           /* LESS_OR_EQUAL  */
    GREATER = 293,                 /* GREATER  */
    GREATER_OR_EQUAL = 294,        /* GREATER_OR_EQUAL  */
    LOGICAL_OR = 295,              /* LOGICAL_OR  */
    LOGICAL_AND = 296,             /* LOGICAL_AND  */
    LOGICAL_NOT = 297,             /* LOGICAL_NOT  */
    UNARY_MINUS = 298              /* UNARY_MINUS  */
  };
  typedef enum structured_tokentype structured_token_kind_t;
#endif
/* Token kinds.  */
#define STRUCTURED_EOF 0
#define STRUCTURED_error 256
#define STRUCTURED_UNDEF 257
#define PROCEDURE 258
#define RETURN 259
#define BEGIN_ 260
#define END 261
#define SKIP 262
#define VAR 263
#define PRINT 264
#define INPUT 265
#define SET_TO 266
#define SEMICOLON 267
#define COMMA 268
#define IF 269
#define THEN 270
#define ELSE 271
#define ELIF 272
#define WHILE 273
#define DO 274
#define REPEAT 275
#define UNTIL 276
#define OPEN_PAREN 277
#define CLOSE_PAREN 278
#define UNDEFINED 279
#define VARIABLE 280
#define DECIMAL_LITERAL 281
#define TRUE 282
#define FALSE 283
#define PLUS 284
#define MINUS 285
#define TIMES 286
#define DIVIDED 287
#define REMAINDER 288
#define EQUAL 289
#define DIFFERENT 290
#define LESS 291
#define LESS_OR_EQUAL 292
#define GREATER 293
#define GREATER_OR_EQUAL 294
#define LOGICAL_OR 295
#define LOGICAL_AND 296
#define LOGICAL_NOT 297
#define UNARY_MINUS 298

/* Value type.  */
#if ! defined STRUCTURED_STYPE && ! defined STRUCTURED_STYPE_IS_DECLARED
union STRUCTURED_STYPE
{
#line 292 "../../jitter/example-vms/structured/structured.y"

  jitter_int literal;
  structured_variable variable;
  struct structured_expression *expression;
  struct structured_statement *statement;
  struct structured_sequence *pointers;

#line 184 "../../jitter/example-vms/structured/structured-parser.h"

};
typedef union STRUCTURED_STYPE STRUCTURED_STYPE;
# define STRUCTURED_STYPE_IS_TRIVIAL 1
# define STRUCTURED_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined STRUCTURED_LTYPE && ! defined STRUCTURED_LTYPE_IS_DECLARED
typedef struct STRUCTURED_LTYPE STRUCTURED_LTYPE;
struct STRUCTURED_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define STRUCTURED_LTYPE_IS_DECLARED 1
# define STRUCTURED_LTYPE_IS_TRIVIAL 1
#endif



int structured_parse (struct structured_program *p, void* structured_scanner);

#endif /* !YY_STRUCTURED_JITTER_EXAMPLE_VMS_STRUCTURED_STRUCTURED_PARSER_H_INCLUDED  */
