/* JitterLisp: configuration-dependent definition and CPP macro validation.

   Copyright (C) 2017, 2018 Luca Saiu
   Written by Luca Saiu

   This file is part of the JitterLisp language implementation, distributed as
   an example along with GNU Jitter under the same license.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef JITTERLISP_CONFIG_H_
#define JITTERLISP_CONFIG_H_

/* Validate memory management macros.
 * ************************************************************************** */

/* Check that exactly one memory management was selected.  This is currently
   done from the compiler command line, when using Jitter's Makefile.am . */
#if ((defined(JITTERLISP_BOEHM_GC)   \
      + defined(JITTERLISP_LITTER))  \
     != 1)
  /* There are zero definitions, or more than one. */
# error "exactly one of the following CPP macros must be defined:"
# error "- JITTERLISP_BOEHM_GC"
# error "- JITTERLISP_LITTER"
#endif // #if ((defined(JITTERLISP_BOEHM_GC) ...

#endif // #ifndef JITTERLISP_CONFIG_H_
