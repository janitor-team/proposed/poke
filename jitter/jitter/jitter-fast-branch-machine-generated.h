/* This header is machine-generated.  Its actual source is in
   scripts/generate-fast-branches.in.m4sh
, within the Jitter source distribution. */

#ifndef JITTER_FAST_BRANCH_MACHINE_GENERATED_H_
#define JITTER_FAST_BRANCH_MACHINE_GENERATED_H_

/* Condition-evaluating macros, expanding to C expressions. */
#define _JITTER_C_CONDITION_NEVER_(x) \
  0
#define _JITTER_C_CONDITION_ALWAYS_(x) \
  1
#define _JITTER_C_CONDITION_ZERO_(x) \
  (! (x))
#define _JITTER_C_CONDITION_NONZERO_(x) \
  (x)
#define _JITTER_C_CONDITION_POSITIVE_(x) \
  ((jitter_int) (x) > 0)
#define _JITTER_C_CONDITION_NONPOSITIVE_(x) \
  ((jitter_int) (x) <= 0)
#define _JITTER_C_CONDITION_NEGATIVE_(x) \
  ((jitter_int) (x) < 0)
#define _JITTER_C_CONDITION_NONNEGATIVE_(x) \
  ((jitter_int) (x) >= 0)
#define _JITTER_C_CONDITION_EQUAL_(x, y) \
  ((x) == (y))
#define _JITTER_C_CONDITION_NOTEQUAL_(x, y) \
  ((x) != (y))
#define _JITTER_C_CONDITION_LESS_UNSIGNED_(x, y) \
  ((x) < (y))
#define _JITTER_C_CONDITION_LESS_SIGNED_(x, y) \
  ((x) < (y))
#define _JITTER_C_CONDITION_GREATER_UNSIGNED_(x, y) \
  ((x) > (y))
#define _JITTER_C_CONDITION_GREATER_SIGNED_(x, y) \
  ((x) > (y))
#define _JITTER_C_CONDITION_NOTLESS_UNSIGNED_(x, y) \
  ((x) >= (y))
#define _JITTER_C_CONDITION_NOTLESS_SIGNED_(x, y) \
  ((x) >= (y))
#define _JITTER_C_CONDITION_NOTGREATER_UNSIGNED_(x, y) \
  ((x) <= (y))
#define _JITTER_C_CONDITION_NOTGREATER_SIGNED_(x, y) \
  ((x) <= (y))
#define _JITTER_C_CONDITION_AND_(x, y) \
  ((x) & (y))
#define _JITTER_C_CONDITION_NOTAND_(x, y) \
  (! ((x) & (y)))

/* Add missing low-level conditional fast branch definitions. */
#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_(x, _jitter_tgt) \
    /* Never branch. */
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_ZERO_ ((x)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_(x, _jitter_tgt) \
    if ((x)) \
                { \
                  _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
                }
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_POSITIVE_ ((x)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NONPOSITIVE_ ((x)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NEGATIVE_ ((x)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_(x, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NONNEGATIVE_ ((x)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_EQUAL_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_EQUAL_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_EQUAL_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_EQUAL_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTEQUAL_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTEQUAL_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTEQUAL_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NOTEQUAL_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_UNSIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_UNSIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_UNSIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_LESS_UNSIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_SIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_SIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_SIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_LESS_SIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_UNSIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_UNSIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_UNSIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_GREATER_UNSIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_SIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_SIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_SIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_GREATER_SIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_UNSIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_UNSIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_UNSIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NOTLESS_UNSIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_SIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_SIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_SIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NOTLESS_SIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_UNSIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_UNSIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_UNSIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NOTGREATER_UNSIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_SIGNED_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_SIGNED_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_SIGNED_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_NOTGREATER_SIGNED_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_AND_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_AND_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_AND_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ ((jitter_uint) _JITTER_C_CONDITION_AND_ ((x), (y)), _jitter_tgt)
#endif

#if ! defined (JITTER_DISPATCH_NO_THREADING)
  /* Forget any machine-specific definition unless the dispatch is
     no-threading; this makes each port simpler, since machine-specific
     headers can define low-level fast branches unconditionally. */
# undef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTAND_
#endif
#ifndef _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTAND_
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTAND_(x, y, _jitter_tgt) \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ (_JITTER_C_CONDITION_AND_ ((x), (y)), _jitter_tgt)
#endif


/* High-level conditional fast branches. */
#define _JITTER_BRANCH_FAST_IF_NEVER(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NEVER_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_ALWAYS(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_ALWAYS_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_ZERO(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_ZERO_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NONZERO(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NONZERO_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_POSITIVE(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_POSITIVE_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NONPOSITIVE(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NONPOSITIVE_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NEGATIVE(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NEGATIVE_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NONNEGATIVE(x, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NONNEGATIVE_ (_jitter_x); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_EQUAL(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_EQUAL_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_EQUAL_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTEQUAL(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTEQUAL_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTEQUAL_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_LESS_UNSIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_LESS_UNSIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_UNSIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_LESS_SIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (x); \
      const jitter_int _jitter_y = (jitter_int) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_LESS_SIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_LESS_SIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_GREATER_UNSIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_GREATER_UNSIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_UNSIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_GREATER_SIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (x); \
      const jitter_int _jitter_y = (jitter_int) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_GREATER_SIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEGATIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_POSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_GREATER_SIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTLESS_UNSIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTLESS_UNSIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_UNSIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTLESS_SIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (x); \
      const jitter_int _jitter_y = (jitter_int) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTLESS_SIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTLESS_SIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTGREATER_UNSIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTGREATER_UNSIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_UNSIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTGREATER_SIGNED(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (x); \
      const jitter_int _jitter_y = (jitter_int) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTGREATER_SIGNED_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONNEGATIVE_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONPOSITIVE_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTGREATER_SIGNED_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_AND(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_AND_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NEVER_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_AND_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)
#define _JITTER_BRANCH_FAST_IF_NOTAND(x, y, _jitter_tgt) \
  do \
    { \
      /* Evaluate arguments, once and for all. */ \
      const jitter_uint _jitter_x = (jitter_uint) (x); \
      const jitter_uint _jitter_y = (jitter_uint) (y); \
      /* "Evaluate" the condition in C, just to see if it's a known constant. \
         The condition of this if statement is a constant expression, and will \
         be optimized away in either case. */ \
      const bool _jitter_condition \
        = _JITTER_C_CONDITION_NOTAND_ (_jitter_x, _jitter_y); \
      const bool _jitter_condition_known \
        = __builtin_constant_p (_jitter_condition); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch. */ \
          if (_jitter_condition) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (_jitter_tgt); \
            } \
        } \
      else if (__builtin_constant_p (_jitter_x) && _jitter_x == 0) \
        { \
          /* _jitter_x is known to be zero, but _jitter_y is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_ \
             (_jitter_y, _jitter_tgt); \
        } \
      else if (__builtin_constant_p (_jitter_y) && _jitter_y == 0) \
        { \
          /* _jitter_y is known to be zero, but _jitter_x is unknown. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ALWAYS_ \
             (_jitter_x, _jitter_tgt); \
        } \
      else \
        { \
          /* The condition is not a known constant. */ \
          _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NOTAND_ \
             (_jitter_x, _jitter_y, _jitter_tgt); \
        } \
    } \
  while (0)

/* High-level operate-and-branch-on-overflow operations. */
/* The operation which may overflow, in C. */
#define _JITTER_C_OPERATION_PLUS_(x, y) \
  (((jitter_int) (x) + (jitter_int) (y)))

/* The overflow condition, in C. */
#define _JITTER_C_CONDITION_PLUS_OVERFLOWS_(x, y) \
  ((jitter_uint) \
   JITTER_WOULD_PLUS_OVERFLOW \
      (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))

/* The negative overflow (which is to say non-overflow) condition, in C, only
   defined if we have a macro available for it; it will be available for the
   operations where computing the negative condition is more efficient.
   Example: remainder.
     BRANCH_FAST_IF_NONZERO (b == 0, label)
   is slower than
     BRANCH_FAST_IF_ZERO (b, label)
   ; notice that the first macro argument is an assembly operand, which GCC has
   to compile as is. */
#if defined (JITTER_WOULD_PLUS_NOT_OVERFLOW)
# define _JITTER_C_CONDITION_PLUS_DOES_NOT_OVERFLOW_(x, y) \
    ((jitter_uint) \
     JITTER_WOULD_PLUS_NOT_OVERFLOW \
        (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))
#endif

/* If the branch-on-overflow and operate-and-branch-on-overflow low-level
   primitives are both missing for PLUS , define one. */
#if (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_)    \
     && ! defined (_JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_))
  /* Use the negative condition if available.  It is only available when it is
     preferable; see the comment above. */
# if defined (_JITTER_C_CONDITION_PLUS_DOES_NOT_OVERFLOW_)
  /* Use the negative condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
         (_JITTER_C_CONDITION_PLUS_DOES_NOT_OVERFLOW_ (opd0, opd1), \
          (tgt))
# else
  /* Use the positive condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
         (_JITTER_C_CONDITION_PLUS_OVERFLOWS_ (opd0, opd1), \
          (tgt))
# endif /* positive or negative */
#endif

/* At this point we definitely have one of the branch-on-overflow and
   operate-and-branch-on-overflow low-level primitives for PLUS .
   Using the one we have, define the other. */
#if (defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_)    \
     && defined (_JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_))
# error "both _JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_ and"
# error "_JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_ are defined."
# error "The machine-specific header should define only one: the other"
# error "will be automatically defined here, based on the one supplied by you."
#elif (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_) \
       && defined (_JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_))
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_(opd0, opd1, tgt)   \
    /* Use the operate-and-branch-on-overflow primitive and just throw away  \
       the result. */                                                        \
    jitter_int _jitter_unused_result __attribute__ ((unused));               \
    /* Prevent uninitialized-variable warnings. */                           \
    asm ("": "=X" (_jitter_unused_result));                                  \
    _JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_ \
       (_jitter_unused_result, (opd0), (opd1), (tgt))
#elif (! defined (_JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_) \
       && defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_))
# define _JITTER_LOW_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_(res, opd0, opd1, tgt) \
    /* Fast-branch away if the operation would overflow. */ \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_PLUS_OVERFLOWS_ \
       ((opd0), (opd1), (tgt)); \
    /* If we are still here the operation will not overflow.  Do it. */ \
    (res) = _JITTER_C_OPERATION_PLUS_ ((opd0), (opd1))
#else
# error "impossible"
#endif // which PLUS_OVERFLOWS low-level primitive is defined

/* This is the operate-and-branch-on-overflow high-level primitive for
   PLUS . */
#define _JITTER_PLUS_BRANCH_FAST_IF_OVERFLOW(res, opd0, opd1, tgt) \
  do \
    { \
      /* Evaluate the arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (opd0); \
      const jitter_int _jitter_y = (jitter_int) (opd1); \
      \
      /* Check if the condition's value is a known constant, before actually \
         computing it.  I have my own macros for this which appear to be more \
         accurate than just __builtin_constant_p (will the thing overflow); \
         when my macros say that the condition is a known constant it always \
         appears to actually be for GCC as well -- making the fast branch \
         either disappear or become unconditional.  This helps with \
         performance, but is not required for correctness. */ \
      const bool _jitter_condition_known \
        = JITTER_PLUS_OVERFLOWS_KNOWN_CONSTANT_GCC (_jitter_x, _jitter_y); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch.  If we do not branch \
             then we have to assign the result. */ \
          if (_JITTER_C_CONDITION_PLUS_OVERFLOWS_ (_jitter_x, _jitter_y)) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (tgt); \
            } \
          else \
            (res) = _JITTER_C_OPERATION_PLUS_ (_jitter_x, _jitter_y); \
        } \
      else \
        { \
          /* The condition is not a known constant.  The overflow check \
             will not be optimized away, but the operation might still be \
             rewritable into a cheaper one (with overflow checking); this \
             is why the expansion uses a middle-level, instead of a low-level, \
             primitive. */ \
          _JITTER_MIDDLE_LEVEL_PLUS_BRANCH_FAST_IF_OVERFLOW_ \
             ((res), _jitter_x, _jitter_y, (tgt)); \
        } \
    } \
  while (false)

/* This is the branch-on-overflow high-level primitive for PLUS
   , which conditionally branches but does not yield a result. */
#define _JITTER_BRANCH_FAST_IF_PLUS_OVERFLOWS(opd0, opd1, tgt) \
  do \
    { \
      /* Here we are only interested in branching or not branching; compute a \
         useless result, to be ignored.  GCC will be able to optimize that away, \
         unless the result is required for computing the overflow condition \
         itself. */ \
      jitter_int _jitter_unused_result __attribute__ ((unused)); \
      _JITTER_PLUS_BRANCH_FAST_IF_OVERFLOW \
         (_jitter_unused_result, (opd0), (opd1), (tgt)); \
    } \
  while (0)

/* The operation which may overflow, in C. */
#define _JITTER_C_OPERATION_MINUS_(x, y) \
  (((jitter_int) (x) - (jitter_int) (y)))

/* The overflow condition, in C. */
#define _JITTER_C_CONDITION_MINUS_OVERFLOWS_(x, y) \
  ((jitter_uint) \
   JITTER_WOULD_MINUS_OVERFLOW \
      (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))

/* The negative overflow (which is to say non-overflow) condition, in C, only
   defined if we have a macro available for it; it will be available for the
   operations where computing the negative condition is more efficient.
   Example: remainder.
     BRANCH_FAST_IF_NONZERO (b == 0, label)
   is slower than
     BRANCH_FAST_IF_ZERO (b, label)
   ; notice that the first macro argument is an assembly operand, which GCC has
   to compile as is. */
#if defined (JITTER_WOULD_MINUS_NOT_OVERFLOW)
# define _JITTER_C_CONDITION_MINUS_DOES_NOT_OVERFLOW_(x, y) \
    ((jitter_uint) \
     JITTER_WOULD_MINUS_NOT_OVERFLOW \
        (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))
#endif

/* If the branch-on-overflow and operate-and-branch-on-overflow low-level
   primitives are both missing for MINUS , define one. */
#if (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_)    \
     && ! defined (_JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_))
  /* Use the negative condition if available.  It is only available when it is
     preferable; see the comment above. */
# if defined (_JITTER_C_CONDITION_MINUS_DOES_NOT_OVERFLOW_)
  /* Use the negative condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
         (_JITTER_C_CONDITION_MINUS_DOES_NOT_OVERFLOW_ (opd0, opd1), \
          (tgt))
# else
  /* Use the positive condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
         (_JITTER_C_CONDITION_MINUS_OVERFLOWS_ (opd0, opd1), \
          (tgt))
# endif /* positive or negative */
#endif

/* At this point we definitely have one of the branch-on-overflow and
   operate-and-branch-on-overflow low-level primitives for MINUS .
   Using the one we have, define the other. */
#if (defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_)    \
     && defined (_JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_))
# error "both _JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_ and"
# error "_JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_ are defined."
# error "The machine-specific header should define only one: the other"
# error "will be automatically defined here, based on the one supplied by you."
#elif (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_) \
       && defined (_JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_))
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_(opd0, opd1, tgt)   \
    /* Use the operate-and-branch-on-overflow primitive and just throw away  \
       the result. */                                                        \
    jitter_int _jitter_unused_result __attribute__ ((unused));               \
    /* Prevent uninitialized-variable warnings. */                           \
    asm ("": "=X" (_jitter_unused_result));                                  \
    _JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_ \
       (_jitter_unused_result, (opd0), (opd1), (tgt))
#elif (! defined (_JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_) \
       && defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_))
# define _JITTER_LOW_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_(res, opd0, opd1, tgt) \
    /* Fast-branch away if the operation would overflow. */ \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_MINUS_OVERFLOWS_ \
       ((opd0), (opd1), (tgt)); \
    /* If we are still here the operation will not overflow.  Do it. */ \
    (res) = _JITTER_C_OPERATION_MINUS_ ((opd0), (opd1))
#else
# error "impossible"
#endif // which MINUS_OVERFLOWS low-level primitive is defined

/* This is the operate-and-branch-on-overflow high-level primitive for
   MINUS . */
#define _JITTER_MINUS_BRANCH_FAST_IF_OVERFLOW(res, opd0, opd1, tgt) \
  do \
    { \
      /* Evaluate the arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (opd0); \
      const jitter_int _jitter_y = (jitter_int) (opd1); \
      \
      /* Check if the condition's value is a known constant, before actually \
         computing it.  I have my own macros for this which appear to be more \
         accurate than just __builtin_constant_p (will the thing overflow); \
         when my macros say that the condition is a known constant it always \
         appears to actually be for GCC as well -- making the fast branch \
         either disappear or become unconditional.  This helps with \
         performance, but is not required for correctness. */ \
      const bool _jitter_condition_known \
        = JITTER_MINUS_OVERFLOWS_KNOWN_CONSTANT_GCC (_jitter_x, _jitter_y); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch.  If we do not branch \
             then we have to assign the result. */ \
          if (_JITTER_C_CONDITION_MINUS_OVERFLOWS_ (_jitter_x, _jitter_y)) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (tgt); \
            } \
          else \
            (res) = _JITTER_C_OPERATION_MINUS_ (_jitter_x, _jitter_y); \
        } \
      else \
        { \
          /* The condition is not a known constant.  The overflow check \
             will not be optimized away, but the operation might still be \
             rewritable into a cheaper one (with overflow checking); this \
             is why the expansion uses a middle-level, instead of a low-level, \
             primitive. */ \
          _JITTER_MIDDLE_LEVEL_MINUS_BRANCH_FAST_IF_OVERFLOW_ \
             ((res), _jitter_x, _jitter_y, (tgt)); \
        } \
    } \
  while (false)

/* This is the branch-on-overflow high-level primitive for MINUS
   , which conditionally branches but does not yield a result. */
#define _JITTER_BRANCH_FAST_IF_MINUS_OVERFLOWS(opd0, opd1, tgt) \
  do \
    { \
      /* Here we are only interested in branching or not branching; compute a \
         useless result, to be ignored.  GCC will be able to optimize that away, \
         unless the result is required for computing the overflow condition \
         itself. */ \
      jitter_int _jitter_unused_result __attribute__ ((unused)); \
      _JITTER_MINUS_BRANCH_FAST_IF_OVERFLOW \
         (_jitter_unused_result, (opd0), (opd1), (tgt)); \
    } \
  while (0)

/* The operation which may overflow, in C. */
#define _JITTER_C_OPERATION_TIMES_(x, y) \
  (((jitter_int) (x) * (jitter_int) (y)))

/* The overflow condition, in C. */
#define _JITTER_C_CONDITION_TIMES_OVERFLOWS_(x, y) \
  ((jitter_uint) \
   JITTER_WOULD_TIMES_OVERFLOW \
      (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))

/* The negative overflow (which is to say non-overflow) condition, in C, only
   defined if we have a macro available for it; it will be available for the
   operations where computing the negative condition is more efficient.
   Example: remainder.
     BRANCH_FAST_IF_NONZERO (b == 0, label)
   is slower than
     BRANCH_FAST_IF_ZERO (b, label)
   ; notice that the first macro argument is an assembly operand, which GCC has
   to compile as is. */
#if defined (JITTER_WOULD_TIMES_NOT_OVERFLOW)
# define _JITTER_C_CONDITION_TIMES_DOES_NOT_OVERFLOW_(x, y) \
    ((jitter_uint) \
     JITTER_WOULD_TIMES_NOT_OVERFLOW \
        (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))
#endif

/* If the branch-on-overflow and operate-and-branch-on-overflow low-level
   primitives are both missing for TIMES , define one. */
#if (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_)    \
     && ! defined (_JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_))
  /* Use the negative condition if available.  It is only available when it is
     preferable; see the comment above. */
# if defined (_JITTER_C_CONDITION_TIMES_DOES_NOT_OVERFLOW_)
  /* Use the negative condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
         (_JITTER_C_CONDITION_TIMES_DOES_NOT_OVERFLOW_ (opd0, opd1), \
          (tgt))
# else
  /* Use the positive condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
         (_JITTER_C_CONDITION_TIMES_OVERFLOWS_ (opd0, opd1), \
          (tgt))
# endif /* positive or negative */
#endif

/* At this point we definitely have one of the branch-on-overflow and
   operate-and-branch-on-overflow low-level primitives for TIMES .
   Using the one we have, define the other. */
#if (defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_)    \
     && defined (_JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_))
# error "both _JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_ and"
# error "_JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_ are defined."
# error "The machine-specific header should define only one: the other"
# error "will be automatically defined here, based on the one supplied by you."
#elif (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_) \
       && defined (_JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_))
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_(opd0, opd1, tgt)   \
    /* Use the operate-and-branch-on-overflow primitive and just throw away  \
       the result. */                                                        \
    jitter_int _jitter_unused_result __attribute__ ((unused));               \
    /* Prevent uninitialized-variable warnings. */                           \
    asm ("": "=X" (_jitter_unused_result));                                  \
    _JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_ \
       (_jitter_unused_result, (opd0), (opd1), (tgt))
#elif (! defined (_JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_) \
       && defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_))
# define _JITTER_LOW_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_(res, opd0, opd1, tgt) \
    /* Fast-branch away if the operation would overflow. */ \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_TIMES_OVERFLOWS_ \
       ((opd0), (opd1), (tgt)); \
    /* If we are still here the operation will not overflow.  Do it. */ \
    (res) = _JITTER_C_OPERATION_TIMES_ ((opd0), (opd1))
#else
# error "impossible"
#endif // which TIMES_OVERFLOWS low-level primitive is defined

/* This is the operate-and-branch-on-overflow high-level primitive for
   TIMES . */
#define _JITTER_TIMES_BRANCH_FAST_IF_OVERFLOW(res, opd0, opd1, tgt) \
  do \
    { \
      /* Evaluate the arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (opd0); \
      const jitter_int _jitter_y = (jitter_int) (opd1); \
      \
      /* Check if the condition's value is a known constant, before actually \
         computing it.  I have my own macros for this which appear to be more \
         accurate than just __builtin_constant_p (will the thing overflow); \
         when my macros say that the condition is a known constant it always \
         appears to actually be for GCC as well -- making the fast branch \
         either disappear or become unconditional.  This helps with \
         performance, but is not required for correctness. */ \
      const bool _jitter_condition_known \
        = JITTER_TIMES_OVERFLOWS_KNOWN_CONSTANT_GCC (_jitter_x, _jitter_y); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch.  If we do not branch \
             then we have to assign the result. */ \
          if (_JITTER_C_CONDITION_TIMES_OVERFLOWS_ (_jitter_x, _jitter_y)) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (tgt); \
            } \
          else \
            (res) = _JITTER_C_OPERATION_TIMES_ (_jitter_x, _jitter_y); \
        } \
      else \
        { \
          /* The condition is not a known constant.  The overflow check \
             will not be optimized away, but the operation might still be \
             rewritable into a cheaper one (with overflow checking); this \
             is why the expansion uses a middle-level, instead of a low-level, \
             primitive. */ \
          _JITTER_MIDDLE_LEVEL_TIMES_BRANCH_FAST_IF_OVERFLOW_ \
             ((res), _jitter_x, _jitter_y, (tgt)); \
        } \
    } \
  while (false)

/* This is the branch-on-overflow high-level primitive for TIMES
   , which conditionally branches but does not yield a result. */
#define _JITTER_BRANCH_FAST_IF_TIMES_OVERFLOWS(opd0, opd1, tgt) \
  do \
    { \
      /* Here we are only interested in branching or not branching; compute a \
         useless result, to be ignored.  GCC will be able to optimize that away, \
         unless the result is required for computing the overflow condition \
         itself. */ \
      jitter_int _jitter_unused_result __attribute__ ((unused)); \
      _JITTER_TIMES_BRANCH_FAST_IF_OVERFLOW \
         (_jitter_unused_result, (opd0), (opd1), (tgt)); \
    } \
  while (0)

/* The operation which may overflow, in C. */
#define _JITTER_C_OPERATION_DIVIDED_(x, y) \
  (((jitter_int) (x) / (jitter_int) (y)))

/* The overflow condition, in C. */
#define _JITTER_C_CONDITION_DIVIDED_OVERFLOWS_(x, y) \
  ((jitter_uint) \
   JITTER_WOULD_DIVIDED_OVERFLOW \
      (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))

/* The negative overflow (which is to say non-overflow) condition, in C, only
   defined if we have a macro available for it; it will be available for the
   operations where computing the negative condition is more efficient.
   Example: remainder.
     BRANCH_FAST_IF_NONZERO (b == 0, label)
   is slower than
     BRANCH_FAST_IF_ZERO (b, label)
   ; notice that the first macro argument is an assembly operand, which GCC has
   to compile as is. */
#if defined (JITTER_WOULD_DIVIDED_NOT_OVERFLOW)
# define _JITTER_C_CONDITION_DIVIDED_DOES_NOT_OVERFLOW_(x, y) \
    ((jitter_uint) \
     JITTER_WOULD_DIVIDED_NOT_OVERFLOW \
        (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))
#endif

/* If the branch-on-overflow and operate-and-branch-on-overflow low-level
   primitives are both missing for DIVIDED , define one. */
#if (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_)    \
     && ! defined (_JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_))
  /* Use the negative condition if available.  It is only available when it is
     preferable; see the comment above. */
# if defined (_JITTER_C_CONDITION_DIVIDED_DOES_NOT_OVERFLOW_)
  /* Use the negative condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
         (_JITTER_C_CONDITION_DIVIDED_DOES_NOT_OVERFLOW_ (opd0, opd1), \
          (tgt))
# else
  /* Use the positive condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
         (_JITTER_C_CONDITION_DIVIDED_OVERFLOWS_ (opd0, opd1), \
          (tgt))
# endif /* positive or negative */
#endif

/* At this point we definitely have one of the branch-on-overflow and
   operate-and-branch-on-overflow low-level primitives for DIVIDED .
   Using the one we have, define the other. */
#if (defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_)    \
     && defined (_JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_))
# error "both _JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_ and"
# error "_JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_ are defined."
# error "The machine-specific header should define only one: the other"
# error "will be automatically defined here, based on the one supplied by you."
#elif (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_) \
       && defined (_JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_))
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_(opd0, opd1, tgt)   \
    /* Use the operate-and-branch-on-overflow primitive and just throw away  \
       the result. */                                                        \
    jitter_int _jitter_unused_result __attribute__ ((unused));               \
    /* Prevent uninitialized-variable warnings. */                           \
    asm ("": "=X" (_jitter_unused_result));                                  \
    _JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_ \
       (_jitter_unused_result, (opd0), (opd1), (tgt))
#elif (! defined (_JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_) \
       && defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_))
# define _JITTER_LOW_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_(res, opd0, opd1, tgt) \
    /* Fast-branch away if the operation would overflow. */ \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_DIVIDED_OVERFLOWS_ \
       ((opd0), (opd1), (tgt)); \
    /* If we are still here the operation will not overflow.  Do it. */ \
    (res) = _JITTER_C_OPERATION_DIVIDED_ ((opd0), (opd1))
#else
# error "impossible"
#endif // which DIVIDED_OVERFLOWS low-level primitive is defined

/* This is the operate-and-branch-on-overflow high-level primitive for
   DIVIDED . */
#define _JITTER_DIVIDED_BRANCH_FAST_IF_OVERFLOW(res, opd0, opd1, tgt) \
  do \
    { \
      /* Evaluate the arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (opd0); \
      const jitter_int _jitter_y = (jitter_int) (opd1); \
      \
      /* Check if the condition's value is a known constant, before actually \
         computing it.  I have my own macros for this which appear to be more \
         accurate than just __builtin_constant_p (will the thing overflow); \
         when my macros say that the condition is a known constant it always \
         appears to actually be for GCC as well -- making the fast branch \
         either disappear or become unconditional.  This helps with \
         performance, but is not required for correctness. */ \
      const bool _jitter_condition_known \
        = JITTER_DIVIDED_OVERFLOWS_KNOWN_CONSTANT_GCC (_jitter_x, _jitter_y); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch.  If we do not branch \
             then we have to assign the result. */ \
          if (_JITTER_C_CONDITION_DIVIDED_OVERFLOWS_ (_jitter_x, _jitter_y)) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (tgt); \
            } \
          else \
            (res) = _JITTER_C_OPERATION_DIVIDED_ (_jitter_x, _jitter_y); \
        } \
      else \
        { \
          /* The condition is not a known constant.  The overflow check \
             will not be optimized away, but the operation might still be \
             rewritable into a cheaper one (with overflow checking); this \
             is why the expansion uses a middle-level, instead of a low-level, \
             primitive. */ \
          _JITTER_MIDDLE_LEVEL_DIVIDED_BRANCH_FAST_IF_OVERFLOW_ \
             ((res), _jitter_x, _jitter_y, (tgt)); \
        } \
    } \
  while (false)

/* This is the branch-on-overflow high-level primitive for DIVIDED
   , which conditionally branches but does not yield a result. */
#define _JITTER_BRANCH_FAST_IF_DIVIDED_OVERFLOWS(opd0, opd1, tgt) \
  do \
    { \
      /* Here we are only interested in branching or not branching; compute a \
         useless result, to be ignored.  GCC will be able to optimize that away, \
         unless the result is required for computing the overflow condition \
         itself. */ \
      jitter_int _jitter_unused_result __attribute__ ((unused)); \
      _JITTER_DIVIDED_BRANCH_FAST_IF_OVERFLOW \
         (_jitter_unused_result, (opd0), (opd1), (tgt)); \
    } \
  while (0)

/* The operation which may overflow, in C. */
#define _JITTER_C_OPERATION_REMAINDER_(x, y) \
  (((jitter_int) (x) % (jitter_int) (y)))

/* The overflow condition, in C. */
#define _JITTER_C_CONDITION_REMAINDER_OVERFLOWS_(x, y) \
  ((jitter_uint) \
   JITTER_WOULD_REMAINDER_OVERFLOW \
      (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))

/* The negative overflow (which is to say non-overflow) condition, in C, only
   defined if we have a macro available for it; it will be available for the
   operations where computing the negative condition is more efficient.
   Example: remainder.
     BRANCH_FAST_IF_NONZERO (b == 0, label)
   is slower than
     BRANCH_FAST_IF_ZERO (b, label)
   ; notice that the first macro argument is an assembly operand, which GCC has
   to compile as is. */
#if defined (JITTER_WOULD_REMAINDER_NOT_OVERFLOW)
# define _JITTER_C_CONDITION_REMAINDER_DOES_NOT_OVERFLOW_(x, y) \
    ((jitter_uint) \
     JITTER_WOULD_REMAINDER_NOT_OVERFLOW \
        (jitter_uint, jitter_int, (x), (y), JITTER_BITS_PER_WORD))
#endif

/* If the branch-on-overflow and operate-and-branch-on-overflow low-level
   primitives are both missing for REMAINDER , define one. */
#if (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_)    \
     && ! defined (_JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_))
  /* Use the negative condition if available.  It is only available when it is
     preferable; see the comment above. */
# if defined (_JITTER_C_CONDITION_REMAINDER_DOES_NOT_OVERFLOW_)
  /* Use the negative condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_ZERO_ \
         (_JITTER_C_CONDITION_REMAINDER_DOES_NOT_OVERFLOW_ (opd0, opd1), \
          (tgt))
# else
  /* Use the positive condition. */
#   define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_(opd0, opd1, tgt) \
      _JITTER_LOW_LEVEL_BRANCH_FAST_IF_NONZERO_ \
         (_JITTER_C_CONDITION_REMAINDER_OVERFLOWS_ (opd0, opd1), \
          (tgt))
# endif /* positive or negative */
#endif

/* At this point we definitely have one of the branch-on-overflow and
   operate-and-branch-on-overflow low-level primitives for REMAINDER .
   Using the one we have, define the other. */
#if (defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_)    \
     && defined (_JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_))
# error "both _JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_ and"
# error "_JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_ are defined."
# error "The machine-specific header should define only one: the other"
# error "will be automatically defined here, based on the one supplied by you."
#elif (! defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_) \
       && defined (_JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_))
# define _JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_(opd0, opd1, tgt)   \
    /* Use the operate-and-branch-on-overflow primitive and just throw away  \
       the result. */                                                        \
    jitter_int _jitter_unused_result __attribute__ ((unused));               \
    /* Prevent uninitialized-variable warnings. */                           \
    asm ("": "=X" (_jitter_unused_result));                                  \
    _JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_ \
       (_jitter_unused_result, (opd0), (opd1), (tgt))
#elif (! defined (_JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_) \
       && defined (_JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_))
# define _JITTER_LOW_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_(res, opd0, opd1, tgt) \
    /* Fast-branch away if the operation would overflow. */ \
    _JITTER_LOW_LEVEL_BRANCH_FAST_IF_REMAINDER_OVERFLOWS_ \
       ((opd0), (opd1), (tgt)); \
    /* If we are still here the operation will not overflow.  Do it. */ \
    (res) = _JITTER_C_OPERATION_REMAINDER_ ((opd0), (opd1))
#else
# error "impossible"
#endif // which REMAINDER_OVERFLOWS low-level primitive is defined

/* This is the operate-and-branch-on-overflow high-level primitive for
   REMAINDER . */
#define _JITTER_REMAINDER_BRANCH_FAST_IF_OVERFLOW(res, opd0, opd1, tgt) \
  do \
    { \
      /* Evaluate the arguments, once and for all. */ \
      const jitter_int _jitter_x = (jitter_int) (opd0); \
      const jitter_int _jitter_y = (jitter_int) (opd1); \
      \
      /* Check if the condition's value is a known constant, before actually \
         computing it.  I have my own macros for this which appear to be more \
         accurate than just __builtin_constant_p (will the thing overflow); \
         when my macros say that the condition is a known constant it always \
         appears to actually be for GCC as well -- making the fast branch \
         either disappear or become unconditional.  This helps with \
         performance, but is not required for correctness. */ \
      const bool _jitter_condition_known \
        = JITTER_REMAINDER_OVERFLOWS_KNOWN_CONSTANT_GCC (_jitter_x, _jitter_y); \
      if (_jitter_condition_known) \
        { \
          /* The condition is a known constant, so this if will turn into \
             either nothing or an unconditional branch.  If we do not branch \
             then we have to assign the result. */ \
          if (_JITTER_C_CONDITION_REMAINDER_OVERFLOWS_ (_jitter_x, _jitter_y)) \
            { \
              _JITTER_LOW_LEVEL_BRANCH_FAST_ (tgt); \
            } \
          else \
            (res) = _JITTER_C_OPERATION_REMAINDER_ (_jitter_x, _jitter_y); \
        } \
      else \
        { \
          /* The condition is not a known constant.  The overflow check \
             will not be optimized away, but the operation might still be \
             rewritable into a cheaper one (with overflow checking); this \
             is why the expansion uses a middle-level, instead of a low-level, \
             primitive. */ \
          _JITTER_MIDDLE_LEVEL_REMAINDER_BRANCH_FAST_IF_OVERFLOW_ \
             ((res), _jitter_x, _jitter_y, (tgt)); \
        } \
    } \
  while (false)

/* This is the branch-on-overflow high-level primitive for REMAINDER
   , which conditionally branches but does not yield a result. */
#define _JITTER_BRANCH_FAST_IF_REMAINDER_OVERFLOWS(opd0, opd1, tgt) \
  do \
    { \
      /* Here we are only interested in branching or not branching; compute a \
         useless result, to be ignored.  GCC will be able to optimize that away, \
         unless the result is required for computing the overflow condition \
         itself. */ \
      jitter_int _jitter_unused_result __attribute__ ((unused)); \
      _JITTER_REMAINDER_BRANCH_FAST_IF_OVERFLOW \
         (_jitter_unused_result, (opd0), (opd1), (tgt)); \
    } \
  while (0)



#endif // #ifndef JITTER_FAST_BRANCH_MACHINE_GENERATED_H_
