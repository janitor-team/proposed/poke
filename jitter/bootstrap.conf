# Bootstrap configuration.

# Copyright (C) 2006-2017 Free Software Foundation, Inc.
# Copyright (C) 2017, 2020, 2021 Luca Saiu
# Updated by Luca Saiu for use in GNU Jitter.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ChangeLog and .in file generation.
# ################################################################

# Make an empty ChangeLog if we have none, early on.  This way autoreconf will
# not complain.
bootstrap_post_import_hook ()
{
  if ! test -r ChangeLog; then
    echo 'The real ChangeLog will be generated from git' \
      > ChangeLog
  fi

  # Expand away M4sh macros into *.in files.
  unm4sh_all

  # Make an empty directory to contain config.h.in .
  if ! test -d config-private; then
    mkdir config-private \
      || (echo 'could not make config-private/'; exit 1)
  fi
}


# M4sh machinery.
# ################################################################

# For every portable-shell script to be generated take SCRIPT.in.m4sh and
# generate SCRIPT.in from that, on the machine where bootstrap is run; then
# aclocal will be able to generate SCRIPT from SCRIPT.in by replacing
# @-substitutions, even on a user machine with weaker tools installed.
unm4sh_all ()
{
  # Generate FILE.in from each FILE.in.m4sh .
  unm4sh "bin/jitter-config.in.m4sh"
  unm4sh "scripts/generate-fast-branches.in.m4sh"
  unm4sh "scripts/emulator.in.m4sh"
}

# Given a .in.m4sh file, build a .in file with the same extension by expanding
# M4sh macros into portable shell.  The result will still need to be processed
# by aclocal, on the user machine, for @-substitution.
unm4sh ()
{
  source="$1"
  top_srcdir=$(dirname "$0")
  destination_basename=$(basename "$source" .m4sh)
  destination_relative_dirname=$(dirname "$source")
  destination_dirname=$top_srcdir/$destination_relative_dirname
  destination=$destination_dirname/$destination_basename

  echo "Building $destination from $source expanding away M4sh macros..."
  # At least here on the machine where we are running bootstrap we can assume
  # autom4te to be there, as we depend on Autoconf anyway; on the user's machine
  # the situation is different, and configure will check.
  autom4te \
    --language=M4sh --prepend-include="$top_srcdir/m4-utility" \
    --output="$destination" "$source"

  # Amend the generated file by adding a shebang line.  See the comment in
  # amend-autom4te-output for an explanation.
  "${top_srcdir}/amend-autom4te-output" "$destination"
}


# Typical bootstrap.conf settings, with few customizations.
# ################################################################

# I prefer copies to symlinks, as I may want to move copies of the working
# directory across machines.
copy="true"

# Copy one of the dummy machine-specific directories into a subdirectory
# just named "dummy".  This makes path names shorter, which helps the dist
# target.
rm -rf machine/dummy 2> /dev/null || true
cp -af machine/dummy--minimal-threading machine/dummy

# Delete .version , used for git-version-gen , from the source directory.
# Doing it here ensures that I don't forget it; I'm used to calling
# ./bootstrap from the source directory rather than simply autoreconf ,
# so this will work for me.
rm -f .version

# My non-default Gnulib directories:
m4_base="build-aux"
build_aux="build-aux"
source_base="gnulib-local"
tests_base="gnulib-local-tests"
doc_base="gnulib-local-doc"

# I like to call the Gnulib library just "gnulib"; the default, as of
# 2017-02-26, seems to depend on the package name (*not* its tarname),
# which gets messy with automake when such name contains spaces.  This
# solution is cleaner.
gnulib_tool_option_extras='--lib libgnulib'

# # Enabling conditional dependencies in Gnulib should make executables
# # (Jitter libraries do not depend on Gnulib) smaller on GNU and on
# # other systems which are not horribly limited.
# gnulib_tool_option_extras="${gnulib_tool_option_extras} --conditional-dependencies"
# FIXME: the line above makes full sense, but for some reason it leads to a link
# failure in JitterLisp on GNU, with rpl_gettimeofday (not needed on GNU) not
# found.  I may investigate the issue later.
gnulib_tool_option_extras="${gnulib_tool_option_extras}"

# gnulib modules used by this package.
gnulib_modules="
array-list
argp
c-ctype
gendocs
git-version-gen
gitlog-to-changelog
mkdtemp
xalloc
xlist
"
# I had problems like this when cross-linking to mipsel-uclibc:
# gnulib-local/libgnulib.a(vasnprintf.o):vasnprintf.c:(.text+0x17e8): more undefined references to `wctomb' follow
# Before introducing Gnulib there was no such issue.
# The problem does *not* exist when cross-compiling to sussman (mipsel, glibc).
#
# I've since solved the problem by regenerating my cross tools with crosstool-ng
# , this time enabling wchar_t support, which was disabled before.  I'm still a
# little skeptical that that was the "correct" solution.

# Additional xgettext options to use.  Use "\\\newline" to break lines.
XGETTEXT_OPTIONS=$XGETTEXT_OPTIONS'\\\
 --from-code=UTF-8\\\
 --flag=asprintf:2:c-format --flag=vasprintf:2:c-format\\\
 --flag=asnprintf:3:c-format --flag=vasnprintf:3:c-format\\\
 --flag=wrapf:1:c-format\\\
'

# If "AM_GNU_GETTEXT(external" or "AM_GNU_GETTEXT([external]"
# appears in configure.ac, exclude some unnecessary files.
# Without grep's -E option (not portable enough, pre-configure),
# the following test is ugly.  Also, this depends on the existence
# of configure.ac, not the obsolescent-named configure.in.  But if
# you're using this infrastructure, you should care about such things.

gettext_external=0
grep '^[	 ]*AM_GNU_GETTEXT(external\>' configure.ac > /dev/null &&
  gettext_external=1
grep '^[	 ]*AM_GNU_GETTEXT(\[external\]' configure.ac > /dev/null &&
  gettext_external=1

if test $gettext_external = 1; then
  # Gettext supplies these files, but we don't need them since
  # we don't have an intl subdirectory.
  excluded_files='
      m4/glibc2.m4
      m4/intdiv0.m4
      m4/lcmessage.m4
      m4/lock.m4
      m4/printf-posix.m4
      m4/size_max.m4
      m4/uintmax_t.m4
      m4/ulonglong.m4
      m4/visibility.m4
      m4/xsize.m4
  '
fi

# Build prerequisites
buildreq="\
autoconf   2.62
automake   1.14
bison      3.0.0
flex       2.5.37
help2man   -
git        1.5.5
tar        -
"
