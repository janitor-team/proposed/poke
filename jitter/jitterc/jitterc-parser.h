/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED
# define YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef JITTERC_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define JITTERC_DEBUG 1
#  else
#   define JITTERC_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define JITTERC_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined JITTERC_DEBUG */
#if JITTERC_DEBUG
extern int jitterc_debug;
#endif
/* "%code requires" blocks.  */
#line 201 "../../jitter/jitterc/jitterc.y"

/* The value associated to a bare_argument nonterminal -- which is to say, an
   argument without a mode.  This is only used within the parser, but needs go
   the header as well as one of the %type cases. */
struct jitterc_bare_argument
{
  /* The argument kind. */
  enum jitterc_instruction_argument_kind kind;

  /* The register letter, lower-case.  Only meaningful if the kind contains the
     register case. */
  char register_class_letter;
};

/* A code block to copy in the output.  This is only used within the parser, but
   needs go the header as well as one of the %type cases. */
struct jitterc_code_block
{
  /* The line number where the code block begins, in the Jitter VM specification
     file.  This is useful for friendlier error reporting thru the #line CPP
     feature. */
  int line_number;

  /* A malloc-allocated string. */
  char *code;
};

/* Simplified error-reporting facilities calling jitterc_error, suitable to be
   called from the scanner and the parser without the complicated and
   irrelevant parameters needed by jitterc_error . */
void
jitterc_scan_error (void *jitterc_scanner) __attribute__ ((noreturn));

/* Return a pointer to a fresh VM data structure parsed from the pointed stream,
   or fail fatally.  Don't generate #line directives iff generate_line is false.
   Rationale: unfortunately some C code generation already happens in the
   parser, so generate_line must be supplied early. */
struct jitterc_vm *
jitterc_parse_file_star (FILE *input_file, bool generate_line);

/* Like jitterc_parse_file_star, but parsing from a file whose pathname is
   given. */
struct jitterc_vm *
jitterc_parse_file (const char *input_file_name, bool generate_line);

#line 103 "../../jitter/jitterc/jitterc-parser.h"

/* Token kinds.  */
#ifndef JITTERC_TOKENTYPE
# define JITTERC_TOKENTYPE
  enum jitterc_tokentype
  {
    JITTERC_EMPTY = -2,
    JITTERC_EOF = 0,               /* "end of file"  */
    JITTERC_error = 256,           /* error  */
    JITTERC_UNDEF = 257,           /* "invalid token"  */
    VM = 258,                      /* VM  */
    LEGAL_NOTICE = 259,            /* LEGAL_NOTICE  */
    END = 260,                     /* END  */
    CODE = 261,                    /* CODE  */
    STRING = 262,                  /* STRING  */
    SET = 263,                     /* SET  */
    INITIAL_HEADER_C = 264,        /* INITIAL_HEADER_C  */
    INITIAL_VM1_C = 265,           /* INITIAL_VM1_C  */
    INITIAL_VM2_C = 266,           /* INITIAL_VM2_C  */
    INITIAL_VM_MAIN_C = 267,       /* INITIAL_VM_MAIN_C  */
    EARLY_HEADER_C = 268,          /* EARLY_HEADER_C  */
    LATE_HEADER_C = 269,           /* LATE_HEADER_C  */
    PRINTER_C = 270,               /* PRINTER_C  */
    REWRITER_C = 271,              /* REWRITER_C  */
    EARLY_C = 272,                 /* EARLY_C  */
    LATE_C = 273,                  /* LATE_C  */
    INITIALIZATION_C = 274,        /* INITIALIZATION_C  */
    FINALIZATION_C = 275,          /* FINALIZATION_C  */
    STATE_EARLY_C = 276,           /* STATE_EARLY_C  */
    STATE_BACKING_STRUCT_C = 277,  /* STATE_BACKING_STRUCT_C  */
    STATE_RUNTIME_STRUCT_C = 278,  /* STATE_RUNTIME_STRUCT_C  */
    STATE_INITIALIZATION_C = 279,  /* STATE_INITIALIZATION_C  */
    STATE_RESET_C = 280,           /* STATE_RESET_C  */
    STATE_FINALIZATION_C = 281,    /* STATE_FINALIZATION_C  */
    INSTRUCTION_BEGINNING_C = 282, /* INSTRUCTION_BEGINNING_C  */
    INSTRUCTION_END_C = 283,       /* INSTRUCTION_END_C  */
    BARE_ARGUMENT = 284,           /* BARE_ARGUMENT  */
    IDENTIFIER = 285,              /* IDENTIFIER  */
    WRAPPED_FUNCTIONS = 286,       /* WRAPPED_FUNCTIONS  */
    WRAPPED_GLOBALS = 287,         /* WRAPPED_GLOBALS  */
    INSTRUCTION = 288,             /* INSTRUCTION  */
    OPEN_PAREN = 289,              /* OPEN_PAREN  */
    CLOSE_PAREN = 290,             /* CLOSE_PAREN  */
    COMMA = 291,                   /* COMMA  */
    SEMICOLON = 292,               /* SEMICOLON  */
    IN = 293,                      /* IN  */
    OUT = 294,                     /* OUT  */
    RULE = 295,                    /* RULE  */
    WHEN = 296,                    /* WHEN  */
    REWRITE = 297,                 /* REWRITE  */
    INTO = 298,                    /* INTO  */
    TRUE_ = 299,                   /* TRUE_  */
    FALSE_ = 300,                  /* FALSE_  */
    RULE_PLACEHOLDER = 301,        /* RULE_PLACEHOLDER  */
    HOT = 302,                     /* HOT  */
    COLD = 303,                    /* COLD  */
    RELOCATABLE = 304,             /* RELOCATABLE  */
    NON_RELOCATABLE = 305,         /* NON_RELOCATABLE  */
    NON_BRANCHING = 306,           /* NON_BRANCHING  */
    BRANCHING = 307,               /* BRANCHING  */
    CALLER = 308,                  /* CALLER  */
    CALLEE = 309,                  /* CALLEE  */
    RETURNING = 310,               /* RETURNING  */
    COMMUTATIVE = 311,             /* COMMUTATIVE  */
    NON_COMMUTATIVE = 312,         /* NON_COMMUTATIVE  */
    TWO_OPERANDS = 313,            /* TWO_OPERANDS  */
    REGISTER_CLASS = 314,          /* REGISTER_CLASS  */
    FAST_REGISTER_NO = 315,        /* FAST_REGISTER_NO  */
    REGISTER_OR_STACK_LETTER = 316, /* REGISTER_OR_STACK_LETTER  */
    SLOW_REGISTERS = 317,          /* SLOW_REGISTERS  */
    NO_SLOW_REGISTERS = 318,       /* NO_SLOW_REGISTERS  */
    STACK = 319,                   /* STACK  */
    C_TYPE = 320,                  /* C_TYPE  */
    C_INITIAL_VALUE = 321,         /* C_INITIAL_VALUE  */
    C_ELEMENT_TYPE = 322,          /* C_ELEMENT_TYPE  */
    LONG_NAME = 323,               /* LONG_NAME  */
    ELEMENT_NO = 324,              /* ELEMENT_NO  */
    NON_TOS_OPTIMIZED = 325,       /* NON_TOS_OPTIMIZED  */
    TOS_OPTIMIZED = 326,           /* TOS_OPTIMIZED  */
    NO_GUARD_OVERFLOW = 327,       /* NO_GUARD_OVERFLOW  */
    NO_GUARD_UNDERFLOW = 328,      /* NO_GUARD_UNDERFLOW  */
    GUARD_OVERFLOW = 329,          /* GUARD_OVERFLOW  */
    GUARD_UNDERFLOW = 330,         /* GUARD_UNDERFLOW  */
    FIXNUM = 331,                  /* FIXNUM  */
    BITSPERWORD = 332,             /* BITSPERWORD  */
    BYTESPERWORD = 333,            /* BYTESPERWORD  */
    LGBYTESPERWORD = 334           /* LGBYTESPERWORD  */
  };
  typedef enum jitterc_tokentype jitterc_token_kind_t;
#endif
/* Token kinds.  */
#define JITTERC_EOF 0
#define JITTERC_error 256
#define JITTERC_UNDEF 257
#define VM 258
#define LEGAL_NOTICE 259
#define END 260
#define CODE 261
#define STRING 262
#define SET 263
#define INITIAL_HEADER_C 264
#define INITIAL_VM1_C 265
#define INITIAL_VM2_C 266
#define INITIAL_VM_MAIN_C 267
#define EARLY_HEADER_C 268
#define LATE_HEADER_C 269
#define PRINTER_C 270
#define REWRITER_C 271
#define EARLY_C 272
#define LATE_C 273
#define INITIALIZATION_C 274
#define FINALIZATION_C 275
#define STATE_EARLY_C 276
#define STATE_BACKING_STRUCT_C 277
#define STATE_RUNTIME_STRUCT_C 278
#define STATE_INITIALIZATION_C 279
#define STATE_RESET_C 280
#define STATE_FINALIZATION_C 281
#define INSTRUCTION_BEGINNING_C 282
#define INSTRUCTION_END_C 283
#define BARE_ARGUMENT 284
#define IDENTIFIER 285
#define WRAPPED_FUNCTIONS 286
#define WRAPPED_GLOBALS 287
#define INSTRUCTION 288
#define OPEN_PAREN 289
#define CLOSE_PAREN 290
#define COMMA 291
#define SEMICOLON 292
#define IN 293
#define OUT 294
#define RULE 295
#define WHEN 296
#define REWRITE 297
#define INTO 298
#define TRUE_ 299
#define FALSE_ 300
#define RULE_PLACEHOLDER 301
#define HOT 302
#define COLD 303
#define RELOCATABLE 304
#define NON_RELOCATABLE 305
#define NON_BRANCHING 306
#define BRANCHING 307
#define CALLER 308
#define CALLEE 309
#define RETURNING 310
#define COMMUTATIVE 311
#define NON_COMMUTATIVE 312
#define TWO_OPERANDS 313
#define REGISTER_CLASS 314
#define FAST_REGISTER_NO 315
#define REGISTER_OR_STACK_LETTER 316
#define SLOW_REGISTERS 317
#define NO_SLOW_REGISTERS 318
#define STACK 319
#define C_TYPE 320
#define C_INITIAL_VALUE 321
#define C_ELEMENT_TYPE 322
#define LONG_NAME 323
#define ELEMENT_NO 324
#define NON_TOS_OPTIMIZED 325
#define TOS_OPTIMIZED 326
#define NO_GUARD_OVERFLOW 327
#define NO_GUARD_UNDERFLOW 328
#define GUARD_OVERFLOW 329
#define GUARD_UNDERFLOW 330
#define FIXNUM 331
#define BITSPERWORD 332
#define BYTESPERWORD 333
#define LGBYTESPERWORD 334

/* Value type.  */
#if ! defined JITTERC_STYPE && ! defined JITTERC_STYPE_IS_DECLARED
union JITTERC_STYPE
{
#line 248 "../../jitter/jitterc/jitterc.y"

  char character;
  char* string;
  gl_list_t string_list;
  enum jitterc_instruction_argument_mode mode;
  struct jitterc_bare_argument bare_argument;
  jitter_int fixnum;
  bool boolean;
  struct jitterc_code_block code_block;
  struct jitterc_code_block legal_notice;

  struct jitterc_argument_pattern *argument_pattern;
  struct jitterc_template_expression *template_expression;
  struct jitterc_instruction_pattern *instruction_pattern;
  struct jitterc_instruction_template *instruction_template;

  /* List elements are pointers to struct jitterc_argument_pattern . */
  gl_list_t argument_patterns;

  /* List elements are pointers to struct jitterc_template_expression . */
  gl_list_t template_expressions;

  /* List elements are pointers to struct jitterc_instruction_pattern . */
  gl_list_t instruction_patterns;

  /* List elements are pointers to struct jitterc_instruction_template . */
  gl_list_t instruction_templates;

  /* Register-class section contents and stack section contents consist in
     pointers to VM structs holding all the data. */
  struct jitterc_register_class *register_class;
  struct jitterc_stack *stack;

#line 314 "../../jitter/jitterc/jitterc-parser.h"

};
typedef union JITTERC_STYPE JITTERC_STYPE;
# define JITTERC_STYPE_IS_TRIVIAL 1
# define JITTERC_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined JITTERC_LTYPE && ! defined JITTERC_LTYPE_IS_DECLARED
typedef struct JITTERC_LTYPE JITTERC_LTYPE;
struct JITTERC_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define JITTERC_LTYPE_IS_DECLARED 1
# define JITTERC_LTYPE_IS_TRIVIAL 1
#endif



int jitterc_parse (struct jitterc_vm *vm, void* jitterc_scanner);

#endif /* !YY_JITTERC_JITTER_JITTERC_JITTERC_PARSER_H_INCLUDED  */
