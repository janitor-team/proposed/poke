/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.6.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         PKL_TAB_STYPE
#define YYLTYPE         PKL_TAB_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         pkl_tab_parse
#define yylex           pkl_tab_lex
#define yyerror         pkl_tab_error
#define yydebug         pkl_tab_debug
#define yynerrs         pkl_tab_nerrs

/* First part of user prologue.  */
#line 35 "pkl-tab.y"

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <xalloc.h>
#include <assert.h>
#include <string.h>
#include <gettext.h>
#define _(str) gettext (str)

#include "pk-utils.h"

#include "pkl.h"
#include "pkl-diag.h"
#include "pkl-ast.h"
#include "pkl-parser.h" /* For struct pkl_parser.  */

#include "pvm.h"

#define PKL_TAB_LTYPE pkl_ast_loc
#define YYDEBUG 1
#include "pkl-tab.h"
#include "pkl-lex.h"

#ifdef PKL_DEBUG
# include "pkl-gen.h"
#endif

#define scanner (pkl_parser->scanner)

/* YYLLOC_DEFAULT -> default code for computing locations.  */

#define PKL_AST_CHILDREN_STEP 12


/* Emit an error.  */

static void
pkl_tab_error (YYLTYPE *llocp,
               struct pkl_parser *pkl_parser,
               char const *err)
{
    pkl_error (pkl_parser->compiler, pkl_parser->ast, *llocp, "%s", err);
}

/* These are used in the defun_or_method rule.  */

#define IS_DEFUN 0
#define IS_METHOD 1

/* Register an argument in the compile-time environment.  This is used
   by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_arg (struct pkl_parser *parser, pkl_ast_node arg)
{
  pkl_ast_node arg_decl;
  pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

  pkl_ast_node dummy
    = pkl_ast_make_integer (parser->ast, 0);
  PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

  arg_decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                arg_identifier,
                                dummy,
                                NULL /* source */);
  PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

  if (!pkl_env_register (parser->env,
                         PKL_ENV_NS_MAIN,
                         PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                         arg_decl))
    {
      pkl_error (parser->compiler, parser->ast,PKL_AST_LOC (arg_identifier),
                 "duplicated argument name `%s' in function declaration",
                 PKL_AST_IDENTIFIER_POINTER (arg_identifier));
      /* Make sure to pop the function frame.  */
      parser->env = pkl_env_pop_frame (parser->env);
      return 0;
    }

  return 1;
}

/* Assert statement is a syntatic sugar that transforms to invocation of
   _pkl_assert function with appropriate arguments.

   This function accepts AST nodes corresponding to the condition and
   optional message of the assert statement, and also the location info
   of the statement.

   Returns NULL on failure, and expression statement AST node on success.  */

static pkl_ast_node
pkl_make_assertion (struct pkl_parser *p, pkl_ast_node cond, pkl_ast_node msg,
                    struct pkl_ast_loc stmt_loc)
{
  pkl_ast_node vfunc, call, call_arg;
  /* _pkl_assert args */
  pkl_ast_node arg_cond, arg_msg, arg_fname, arg_line, arg_col;

  /* Make variable for `_pkl_assert` function */
  {
    const char *name = "_pkl_assert";
    pkl_ast_node vfunc_init;
    int back, over;

    vfunc_init = pkl_env_lookup (p->env, PKL_ENV_NS_MAIN, name, &back, &over);
    if (!vfunc_init
        || (PKL_AST_DECL_KIND (vfunc_init) != PKL_AST_DECL_KIND_FUNC))
      {
        pkl_error (p->compiler, p->ast, stmt_loc, "undefined function '%s'",
                   name);
        return NULL;
      }
    vfunc = pkl_ast_make_var (p->ast, pkl_ast_make_identifier (p->ast, name),
                              vfunc_init, back, over);
  }

  /* First argument of _pkl_assert: condition */
  arg_cond = pkl_ast_make_funcall_arg (p->ast, cond, NULL);
  PKL_AST_LOC (arg_cond) = PKL_AST_LOC (cond);

  /* Second argument of _pkl_assert: user message */
  if (msg == NULL)
    {
      msg = pkl_ast_make_string (p->ast, "");
      PKL_AST_TYPE (msg) = ASTREF (pkl_ast_make_string_type (p->ast));
    }
  arg_msg = pkl_ast_make_funcall_arg (p->ast, msg, NULL);
  arg_msg = ASTREF (arg_msg);
  PKL_AST_LOC (arg_msg) = PKL_AST_LOC (msg);

  /* Third argument of _pkl_assert: file name */
  {
    pkl_ast_node fname
        = pkl_ast_make_string (p->ast, p->filename ? p->filename : "<stdin>");

    PKL_AST_TYPE (fname) = ASTREF (pkl_ast_make_string_type (p->ast));
    arg_fname = pkl_ast_make_funcall_arg (p->ast, fname, NULL);
    arg_fname = ASTREF (arg_fname);
  }

  /* Fourth argument of _pkl_assert: line */
  {
    pkl_ast_node line = pkl_ast_make_integer (p->ast, stmt_loc.first_line);

    PKL_AST_TYPE (line) = ASTREF (pkl_ast_make_integral_type (p->ast, 64, 0));
    arg_line = pkl_ast_make_funcall_arg (p->ast, line, NULL);
    arg_line = ASTREF (arg_line);
  }

  /* Fifth argument of _pkl_assert: column */
  {
    pkl_ast_node col = pkl_ast_make_integer (p->ast, stmt_loc.first_column);

    PKL_AST_TYPE (col) = ASTREF (pkl_ast_make_integral_type (p->ast, 64, 0));
    arg_col = pkl_ast_make_funcall_arg (p->ast, col, NULL);
    arg_col = ASTREF (arg_col);
  }

  call_arg = pkl_ast_chainon (arg_line, arg_col);
  call_arg = pkl_ast_chainon (arg_fname, call_arg);
  call_arg = pkl_ast_chainon (arg_msg, call_arg);
  call_arg = pkl_ast_chainon (arg_cond, call_arg);
  call = pkl_ast_make_funcall (p->ast, vfunc, call_arg);
  return pkl_ast_make_exp_stmt (p->ast, call);
}

#if 0
/* Register a list of arguments in the compile-time environment.  This
   is used by function specifiers and try-catch statements.

   Return 0 if there was an error registering, 1 otherwise.  */

static int
pkl_register_args (struct pkl_parser *parser, pkl_ast_node arg_list)
{
  pkl_ast_node arg;

  for (arg = arg_list; arg; arg = PKL_AST_CHAIN (arg))
    {
      pkl_ast_node arg_decl;
      pkl_ast_node arg_identifier = PKL_AST_FUNC_ARG_IDENTIFIER (arg);

      pkl_ast_node dummy
        = pkl_ast_make_integer (parser->ast, 0);
      PKL_AST_TYPE (dummy) = ASTREF (PKL_AST_FUNC_ARG_TYPE (arg));

      arg_decl = pkl_ast_make_decl (parser->ast,
                                    PKL_AST_DECL_KIND_VAR,
                                    arg_identifier,
                                    dummy,
                                    NULL /* source */);
      PKL_AST_LOC (arg_decl) = PKL_AST_LOC (arg);

      if (!pkl_env_register (parser->env,
                             PKL_ENV_NS_MAIN,
                             PKL_AST_IDENTIFIER_POINTER (arg_identifier),
                             arg_decl))
        {
          pkl_error (parser->compiler, parser->ast, PKL_AST_LOC (arg_identifier),
                     "duplicated argument name `%s' in function declaration",
                     PKL_AST_IDENTIFIER_POINTER (arg_identifier));
          /* Make sure to pop the function frame.  */
          parser->env = pkl_env_pop_frame (parser->env);
          return 0;
        }
    }

  return 1;
}
#endif

/* Register N dummy entries in the compilation environment.  */

static void
pkl_register_dummies (struct pkl_parser *parser, int n)
{
  int i;
  for (i = 0; i < n; ++i)
    {
      char *name;
      pkl_ast_node id;
      pkl_ast_node decl;
      int r;

      asprintf (&name, "@*UNUSABLE_OFF_%d*@", i);
      id = pkl_ast_make_identifier (parser->ast, name);
      decl = pkl_ast_make_decl (parser->ast,
                                PKL_AST_DECL_KIND_VAR,
                                id, NULL /* initial */,
                                NULL /* source */);

      r = pkl_env_register (parser->env, PKL_ENV_NS_MAIN, name, decl);
      assert (r);
    }
}

/* Load a module, given its name.
   If the module file cannot be read, return 1.
   If there is a parse error loading the module, return 2.
   Otherwise, return 0.  */

static int
load_module (struct pkl_parser *parser,
             const char *module, pkl_ast_node *node,
             int filename_p, char **filename)
{
  char *module_filename = NULL;
  pkl_ast ast;
  FILE *fp;

  module_filename = pkl_resolve_module (parser->compiler,
                                        module,
                                        filename_p);
  if (module_filename == NULL)
    /* No file found.  */
    return 1;

  fp = fopen (module_filename, "rb");
  if (!fp)
    {
      free (module_filename);
      return 1;
    }

  /* Parse the file, using the given environment.  The declarations
     found in the parsed file are appended to that environment, so
     nothing extra should be done about that.  */
  if (pkl_parse_file (parser->compiler, &parser->env, &ast, fp,
                      module_filename)
      != 0)
    {
      fclose (fp);
      free (module_filename);
      return 2;
    }

  /* Add the module to the compiler's list of loaded modules.  */
  pkl_add_module (parser->compiler, module_filename);

  /* However, the AST nodes shall be appended explicitly, which is
     achieved by returning them to the caller in the NODE
     argument.  */
  *node = PKL_AST_PROGRAM_ELEMS (ast->ast);

  /* Dirty hack is dirty, but it works.  */
  PKL_AST_PROGRAM_ELEMS (ast->ast) = NULL;
  pkl_ast_free (ast);

  /* Set the `filename' output argument if needed.  */
  if (filename)
    *filename = strdup (module_filename);

  fclose (fp);
  free (module_filename);
  return 0;
}


#line 384 "pkl-tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
# define YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PKL_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PKL_TAB_DEBUG 1
#  else
#   define PKL_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PKL_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PKL_TAB_DEBUG */
#if PKL_TAB_DEBUG
extern int pkl_tab_debug;
#endif

/* Token kinds.  */
#ifndef PKL_TAB_TOKENTYPE
# define PKL_TAB_TOKENTYPE
  enum pkl_tab_tokentype
  {
    PKL_TAB_EMPTY = -2,
    PKL_TAB_EOF = 0,               /* "end of file"  */
    PKL_TAB_error = 256,           /* error  */
    PKL_TAB_UNDEF = 257,           /* "invalid token"  */
    INTEGER = 258,                 /* "integer literal"  */
    INTEGER_OVERFLOW = 259,        /* INTEGER_OVERFLOW  */
    CHAR = 260,                    /* "character literal"  */
    STR = 261,                     /* "string"  */
    IDENTIFIER = 262,              /* "identifier"  */
    TYPENAME = 263,                /* "type name"  */
    UNIT = 264,                    /* "offset unit"  */
    OFFSET = 265,                  /* "offset"  */
    ENUM = 266,                    /* "keyword `enum'"  */
    PINNED = 267,                  /* "keyword `pinned'"  */
    STRUCT = 268,                  /* "keyword `struct'"  */
    token = 269,                   /* token  */
    UNION = 270,                   /* "keyword `union'"  */
    CONST = 271,                   /* "keyword `const'"  */
    CONTINUE = 272,                /* "keyword `continue'"  */
    ELSE = 273,                    /* "keyword `else'"  */
    IF = 274,                      /* "keyword `if'"  */
    WHILE = 275,                   /* "keyword `while"  */
    UNTIL = 276,                   /* "keyword `until'"  */
    FOR = 277,                     /* "keyword `for'"  */
    IN = 278,                      /* "keyword `in'"  */
    WHERE = 279,                   /* "keyword `where'"  */
    SIZEOF = 280,                  /* "keyword `sizeof'"  */
    TYPEOF = 281,                  /* "keyword `typeof'"  */
    ASSERT = 282,                  /* "keyword `assert'"  */
    ERR = 283,                     /* "token"  */
    ALIEN = 284,                   /* ALIEN  */
    INTCONSTR = 285,               /* "int type constructor"  */
    UINTCONSTR = 286,              /* "uint type constructor"  */
    OFFSETCONSTR = 287,            /* "offset type constructor"  */
    DEFUN = 288,                   /* "keyword `fun'"  */
    DEFSET = 289,                  /* "keyword `defset'"  */
    DEFTYPE = 290,                 /* "keyword `type'"  */
    DEFVAR = 291,                  /* "keyword `var'"  */
    DEFUNIT = 292,                 /* "keyword `unit'"  */
    METHOD = 293,                  /* "keyword `method'"  */
    RETURN = 294,                  /* "keyword `return'"  */
    BREAK = 295,                   /* "keyword `break'"  */
    STRING = 296,                  /* "string type specifier"  */
    TRY = 297,                     /* "keyword `try'"  */
    CATCH = 298,                   /* "keyword `catch'"  */
    RAISE = 299,                   /* "keyword `raise'"  */
    VOID = 300,                    /* "void type specifier"  */
    ANY = 301,                     /* "any type specifier"  */
    PRINT = 302,                   /* "keyword `print'"  */
    PRINTF = 303,                  /* "keyword `printf'"  */
    LOAD = 304,                    /* "keyword `load'"  */
    LAMBDA = 305,                  /* "keyword `lambda'"  */
    FORMAT = 306,                  /* "keyword `format'"  */
    BUILTIN_RAND = 307,            /* BUILTIN_RAND  */
    BUILTIN_GET_ENDIAN = 308,      /* BUILTIN_GET_ENDIAN  */
    BUILTIN_SET_ENDIAN = 309,      /* BUILTIN_SET_ENDIAN  */
    BUILTIN_GET_IOS = 310,         /* BUILTIN_GET_IOS  */
    BUILTIN_SET_IOS = 311,         /* BUILTIN_SET_IOS  */
    BUILTIN_OPEN = 312,            /* BUILTIN_OPEN  */
    BUILTIN_CLOSE = 313,           /* BUILTIN_CLOSE  */
    BUILTIN_IOSIZE = 314,          /* BUILTIN_IOSIZE  */
    BUILTIN_IOFLAGS = 315,         /* BUILTIN_IOFLAGS  */
    BUILTIN_IOGETB = 316,          /* BUILTIN_IOGETB  */
    BUILTIN_IOSETB = 317,          /* BUILTIN_IOSETB  */
    BUILTIN_GETENV = 318,          /* BUILTIN_GETENV  */
    BUILTIN_FORGET = 319,          /* BUILTIN_FORGET  */
    BUILTIN_GET_TIME = 320,        /* BUILTIN_GET_TIME  */
    BUILTIN_STRACE = 321,          /* BUILTIN_STRACE  */
    BUILTIN_TERM_RGB_TO_COLOR = 322, /* BUILTIN_TERM_RGB_TO_COLOR  */
    BUILTIN_SLEEP = 323,           /* BUILTIN_SLEEP  */
    BUILTIN_TERM_GET_COLOR = 324,  /* BUILTIN_TERM_GET_COLOR  */
    BUILTIN_TERM_SET_COLOR = 325,  /* BUILTIN_TERM_SET_COLOR  */
    BUILTIN_TERM_GET_BGCOLOR = 326, /* BUILTIN_TERM_GET_BGCOLOR  */
    BUILTIN_TERM_SET_BGCOLOR = 327, /* BUILTIN_TERM_SET_BGCOLOR  */
    BUILTIN_TERM_BEGIN_CLASS = 328, /* BUILTIN_TERM_BEGIN_CLASS  */
    BUILTIN_TERM_END_CLASS = 329,  /* BUILTIN_TERM_END_CLASS  */
    BUILTIN_TERM_BEGIN_HYPERLINK = 330, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
    BUILTIN_TERM_END_HYPERLINK = 331, /* BUILTIN_TERM_END_HYPERLINK  */
    BUILTIN_VM_OBASE = 332,        /* BUILTIN_VM_OBASE  */
    BUILTIN_VM_SET_OBASE = 333,    /* BUILTIN_VM_SET_OBASE  */
    BUILTIN_VM_OACUTOFF = 334,     /* BUILTIN_VM_OACUTOFF  */
    BUILTIN_VM_SET_OACUTOFF = 335, /* BUILTIN_VM_SET_OACUTOFF  */
    BUILTIN_VM_ODEPTH = 336,       /* BUILTIN_VM_ODEPTH  */
    BUILTIN_VM_SET_ODEPTH = 337,   /* BUILTIN_VM_SET_ODEPTH  */
    BUILTIN_VM_OINDENT = 338,      /* BUILTIN_VM_OINDENT  */
    BUILTIN_VM_SET_OINDENT = 339,  /* BUILTIN_VM_SET_OINDENT  */
    BUILTIN_VM_OMAPS = 340,        /* BUILTIN_VM_OMAPS  */
    BUILTIN_VM_SET_OMAPS = 341,    /* BUILTIN_VM_SET_OMAPS  */
    BUILTIN_VM_OMODE = 342,        /* BUILTIN_VM_OMODE  */
    BUILTIN_VM_SET_OMODE = 343,    /* BUILTIN_VM_SET_OMODE  */
    BUILTIN_VM_OPPRINT = 344,      /* BUILTIN_VM_OPPRINT  */
    BUILTIN_VM_SET_OPPRINT = 345,  /* BUILTIN_VM_SET_OPPRINT  */
    BUILTIN_UNSAFE_STRING_SET = 346, /* BUILTIN_UNSAFE_STRING_SET  */
    POWA = 347,                    /* "power-and-assign operator"  */
    MULA = 348,                    /* "multiply-and-assign operator"  */
    DIVA = 349,                    /* "divide-and-assing operator"  */
    MODA = 350,                    /* "modulus-and-assign operator"  */
    ADDA = 351,                    /* "add-and-assing operator"  */
    SUBA = 352,                    /* "subtract-and-assign operator"  */
    SLA = 353,                     /* "shift-left-and-assign operator"  */
    SRA = 354,                     /* "shift-right-and-assign operator"  */
    BANDA = 355,                   /* "bit-and-and-assign operator"  */
    XORA = 356,                    /* "bit-xor-and-assign operator"  */
    IORA = 357,                    /* "bit-or-and-assign operator"  */
    RANGEA = 358,                  /* "range separator"  */
    OR = 359,                      /* "logical or operator"  */
    AND = 360,                     /* "logical and operator"  */
    IMPL = 361,                    /* "logical implication operator"  */
    EQ = 362,                      /* "equality operator"  */
    NE = 363,                      /* "inequality operator"  */
    LE = 364,                      /* "less-or-equal operator"  */
    GE = 365,                      /* "bigger-or-equal-than operator"  */
    SL = 366,                      /* "left shift operator"  */
    SR = 367,                      /* "right shift operator"  */
    CEILDIV = 368,                 /* "ceiling division operator"  */
    POW = 369,                     /* "power operator"  */
    BCONC = 370,                   /* "bit-concatenation operator"  */
    NSMAP = 371,                   /* "non-strict map operator"  */
    INC = 372,                     /* "increment operator"  */
    DEC = 373,                     /* "decrement operator"  */
    AS = 374,                      /* "cast operator"  */
    ISA = 375,                     /* "type identification operator"  */
    ATTR = 376,                    /* "attribute"  */
    UNMAP = 377,                   /* "unmap operator"  */
    EXCOND = 378,                  /* "conditional on exception operator"  */
    BIG = 379,                     /* "keyword `big'"  */
    LITTLE = 380,                  /* "keyword `little'"  */
    SIGNED = 381,                  /* "keyword `signed'"  */
    UNSIGNED = 382,                /* "keyword `unsigned'"  */
    THREEDOTS = 383,               /* "varargs indicator"  */
    THEN = 384,                    /* THEN  */
    UNARY = 385,                   /* UNARY  */
    HYPERUNARY = 386,              /* HYPERUNARY  */
    START_EXP = 387,               /* START_EXP  */
    START_DECL = 388,              /* START_DECL  */
    START_STMT = 389,              /* START_STMT  */
    START_PROGRAM = 390            /* START_PROGRAM  */
  };
  typedef enum pkl_tab_tokentype pkl_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PKL_TAB_STYPE && ! defined PKL_TAB_STYPE_IS_DECLARED
union PKL_TAB_STYPE
{
#line 341 "pkl-tab.y"

  pkl_ast_node ast;
  struct
  {
    pkl_ast_node constraint;
    pkl_ast_node initializer;
    int impl_constraint_p;
  } field_const_init;
  enum pkl_ast_op opcode;
  int integer;

#line 589 "pkl-tab.c"

};
typedef union PKL_TAB_STYPE PKL_TAB_STYPE;
# define PKL_TAB_STYPE_IS_TRIVIAL 1
# define PKL_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PKL_TAB_LTYPE && ! defined PKL_TAB_LTYPE_IS_DECLARED
typedef struct PKL_TAB_LTYPE PKL_TAB_LTYPE;
struct PKL_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PKL_TAB_LTYPE_IS_DECLARED 1
# define PKL_TAB_LTYPE_IS_TRIVIAL 1
#endif



int pkl_tab_parse (struct pkl_parser *pkl_parser);

#endif /* !YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_INTEGER = 3,                    /* "integer literal"  */
  YYSYMBOL_INTEGER_OVERFLOW = 4,           /* INTEGER_OVERFLOW  */
  YYSYMBOL_CHAR = 5,                       /* "character literal"  */
  YYSYMBOL_STR = 6,                        /* "string"  */
  YYSYMBOL_IDENTIFIER = 7,                 /* "identifier"  */
  YYSYMBOL_TYPENAME = 8,                   /* "type name"  */
  YYSYMBOL_UNIT = 9,                       /* "offset unit"  */
  YYSYMBOL_OFFSET = 10,                    /* "offset"  */
  YYSYMBOL_ENUM = 11,                      /* "keyword `enum'"  */
  YYSYMBOL_PINNED = 12,                    /* "keyword `pinned'"  */
  YYSYMBOL_STRUCT = 13,                    /* "keyword `struct'"  */
  YYSYMBOL_token = 14,                     /* token  */
  YYSYMBOL_UNION = 15,                     /* "keyword `union'"  */
  YYSYMBOL_CONST = 16,                     /* "keyword `const'"  */
  YYSYMBOL_CONTINUE = 17,                  /* "keyword `continue'"  */
  YYSYMBOL_ELSE = 18,                      /* "keyword `else'"  */
  YYSYMBOL_IF = 19,                        /* "keyword `if'"  */
  YYSYMBOL_WHILE = 20,                     /* "keyword `while"  */
  YYSYMBOL_UNTIL = 21,                     /* "keyword `until'"  */
  YYSYMBOL_FOR = 22,                       /* "keyword `for'"  */
  YYSYMBOL_IN = 23,                        /* "keyword `in'"  */
  YYSYMBOL_WHERE = 24,                     /* "keyword `where'"  */
  YYSYMBOL_SIZEOF = 25,                    /* "keyword `sizeof'"  */
  YYSYMBOL_TYPEOF = 26,                    /* "keyword `typeof'"  */
  YYSYMBOL_ASSERT = 27,                    /* "keyword `assert'"  */
  YYSYMBOL_ERR = 28,                       /* "token"  */
  YYSYMBOL_ALIEN = 29,                     /* ALIEN  */
  YYSYMBOL_INTCONSTR = 30,                 /* "int type constructor"  */
  YYSYMBOL_UINTCONSTR = 31,                /* "uint type constructor"  */
  YYSYMBOL_OFFSETCONSTR = 32,              /* "offset type constructor"  */
  YYSYMBOL_DEFUN = 33,                     /* "keyword `fun'"  */
  YYSYMBOL_DEFSET = 34,                    /* "keyword `defset'"  */
  YYSYMBOL_DEFTYPE = 35,                   /* "keyword `type'"  */
  YYSYMBOL_DEFVAR = 36,                    /* "keyword `var'"  */
  YYSYMBOL_DEFUNIT = 37,                   /* "keyword `unit'"  */
  YYSYMBOL_METHOD = 38,                    /* "keyword `method'"  */
  YYSYMBOL_RETURN = 39,                    /* "keyword `return'"  */
  YYSYMBOL_BREAK = 40,                     /* "keyword `break'"  */
  YYSYMBOL_STRING = 41,                    /* "string type specifier"  */
  YYSYMBOL_TRY = 42,                       /* "keyword `try'"  */
  YYSYMBOL_CATCH = 43,                     /* "keyword `catch'"  */
  YYSYMBOL_RAISE = 44,                     /* "keyword `raise'"  */
  YYSYMBOL_VOID = 45,                      /* "void type specifier"  */
  YYSYMBOL_ANY = 46,                       /* "any type specifier"  */
  YYSYMBOL_PRINT = 47,                     /* "keyword `print'"  */
  YYSYMBOL_PRINTF = 48,                    /* "keyword `printf'"  */
  YYSYMBOL_LOAD = 49,                      /* "keyword `load'"  */
  YYSYMBOL_LAMBDA = 50,                    /* "keyword `lambda'"  */
  YYSYMBOL_FORMAT = 51,                    /* "keyword `format'"  */
  YYSYMBOL_BUILTIN_RAND = 52,              /* BUILTIN_RAND  */
  YYSYMBOL_BUILTIN_GET_ENDIAN = 53,        /* BUILTIN_GET_ENDIAN  */
  YYSYMBOL_BUILTIN_SET_ENDIAN = 54,        /* BUILTIN_SET_ENDIAN  */
  YYSYMBOL_BUILTIN_GET_IOS = 55,           /* BUILTIN_GET_IOS  */
  YYSYMBOL_BUILTIN_SET_IOS = 56,           /* BUILTIN_SET_IOS  */
  YYSYMBOL_BUILTIN_OPEN = 57,              /* BUILTIN_OPEN  */
  YYSYMBOL_BUILTIN_CLOSE = 58,             /* BUILTIN_CLOSE  */
  YYSYMBOL_BUILTIN_IOSIZE = 59,            /* BUILTIN_IOSIZE  */
  YYSYMBOL_BUILTIN_IOFLAGS = 60,           /* BUILTIN_IOFLAGS  */
  YYSYMBOL_BUILTIN_IOGETB = 61,            /* BUILTIN_IOGETB  */
  YYSYMBOL_BUILTIN_IOSETB = 62,            /* BUILTIN_IOSETB  */
  YYSYMBOL_BUILTIN_GETENV = 63,            /* BUILTIN_GETENV  */
  YYSYMBOL_BUILTIN_FORGET = 64,            /* BUILTIN_FORGET  */
  YYSYMBOL_BUILTIN_GET_TIME = 65,          /* BUILTIN_GET_TIME  */
  YYSYMBOL_BUILTIN_STRACE = 66,            /* BUILTIN_STRACE  */
  YYSYMBOL_BUILTIN_TERM_RGB_TO_COLOR = 67, /* BUILTIN_TERM_RGB_TO_COLOR  */
  YYSYMBOL_BUILTIN_SLEEP = 68,             /* BUILTIN_SLEEP  */
  YYSYMBOL_BUILTIN_TERM_GET_COLOR = 69,    /* BUILTIN_TERM_GET_COLOR  */
  YYSYMBOL_BUILTIN_TERM_SET_COLOR = 70,    /* BUILTIN_TERM_SET_COLOR  */
  YYSYMBOL_BUILTIN_TERM_GET_BGCOLOR = 71,  /* BUILTIN_TERM_GET_BGCOLOR  */
  YYSYMBOL_BUILTIN_TERM_SET_BGCOLOR = 72,  /* BUILTIN_TERM_SET_BGCOLOR  */
  YYSYMBOL_BUILTIN_TERM_BEGIN_CLASS = 73,  /* BUILTIN_TERM_BEGIN_CLASS  */
  YYSYMBOL_BUILTIN_TERM_END_CLASS = 74,    /* BUILTIN_TERM_END_CLASS  */
  YYSYMBOL_BUILTIN_TERM_BEGIN_HYPERLINK = 75, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
  YYSYMBOL_BUILTIN_TERM_END_HYPERLINK = 76, /* BUILTIN_TERM_END_HYPERLINK  */
  YYSYMBOL_BUILTIN_VM_OBASE = 77,          /* BUILTIN_VM_OBASE  */
  YYSYMBOL_BUILTIN_VM_SET_OBASE = 78,      /* BUILTIN_VM_SET_OBASE  */
  YYSYMBOL_BUILTIN_VM_OACUTOFF = 79,       /* BUILTIN_VM_OACUTOFF  */
  YYSYMBOL_BUILTIN_VM_SET_OACUTOFF = 80,   /* BUILTIN_VM_SET_OACUTOFF  */
  YYSYMBOL_BUILTIN_VM_ODEPTH = 81,         /* BUILTIN_VM_ODEPTH  */
  YYSYMBOL_BUILTIN_VM_SET_ODEPTH = 82,     /* BUILTIN_VM_SET_ODEPTH  */
  YYSYMBOL_BUILTIN_VM_OINDENT = 83,        /* BUILTIN_VM_OINDENT  */
  YYSYMBOL_BUILTIN_VM_SET_OINDENT = 84,    /* BUILTIN_VM_SET_OINDENT  */
  YYSYMBOL_BUILTIN_VM_OMAPS = 85,          /* BUILTIN_VM_OMAPS  */
  YYSYMBOL_BUILTIN_VM_SET_OMAPS = 86,      /* BUILTIN_VM_SET_OMAPS  */
  YYSYMBOL_BUILTIN_VM_OMODE = 87,          /* BUILTIN_VM_OMODE  */
  YYSYMBOL_BUILTIN_VM_SET_OMODE = 88,      /* BUILTIN_VM_SET_OMODE  */
  YYSYMBOL_BUILTIN_VM_OPPRINT = 89,        /* BUILTIN_VM_OPPRINT  */
  YYSYMBOL_BUILTIN_VM_SET_OPPRINT = 90,    /* BUILTIN_VM_SET_OPPRINT  */
  YYSYMBOL_BUILTIN_UNSAFE_STRING_SET = 91, /* BUILTIN_UNSAFE_STRING_SET  */
  YYSYMBOL_POWA = 92,                      /* "power-and-assign operator"  */
  YYSYMBOL_MULA = 93,                      /* "multiply-and-assign operator"  */
  YYSYMBOL_DIVA = 94,                      /* "divide-and-assing operator"  */
  YYSYMBOL_MODA = 95,                      /* "modulus-and-assign operator"  */
  YYSYMBOL_ADDA = 96,                      /* "add-and-assing operator"  */
  YYSYMBOL_SUBA = 97,                      /* "subtract-and-assign operator"  */
  YYSYMBOL_SLA = 98,                       /* "shift-left-and-assign operator"  */
  YYSYMBOL_SRA = 99,                       /* "shift-right-and-assign operator"  */
  YYSYMBOL_BANDA = 100,                    /* "bit-and-and-assign operator"  */
  YYSYMBOL_XORA = 101,                     /* "bit-xor-and-assign operator"  */
  YYSYMBOL_IORA = 102,                     /* "bit-or-and-assign operator"  */
  YYSYMBOL_RANGEA = 103,                   /* "range separator"  */
  YYSYMBOL_OR = 104,                       /* "logical or operator"  */
  YYSYMBOL_AND = 105,                      /* "logical and operator"  */
  YYSYMBOL_IMPL = 106,                     /* "logical implication operator"  */
  YYSYMBOL_107_bit_wise_or_operator_ = 107, /* "bit-wise or operator"  */
  YYSYMBOL_108_bit_wise_xor_operator_ = 108, /* "bit-wise xor operator"  */
  YYSYMBOL_109_bit_wise_and_operator_ = 109, /* "bit-wise and operator"  */
  YYSYMBOL_EQ = 110,                       /* "equality operator"  */
  YYSYMBOL_NE = 111,                       /* "inequality operator"  */
  YYSYMBOL_LE = 112,                       /* "less-or-equal operator"  */
  YYSYMBOL_GE = 113,                       /* "bigger-or-equal-than operator"  */
  YYSYMBOL_114_less_than_operator_ = 114,  /* "less-than operator"  */
  YYSYMBOL_115_bigger_than_operator_ = 115, /* "bigger-than operator"  */
  YYSYMBOL_SL = 116,                       /* "left shift operator"  */
  YYSYMBOL_SR = 117,                       /* "right shift operator"  */
  YYSYMBOL_118_addition_operator_ = 118,   /* "addition operator"  */
  YYSYMBOL_119_subtraction_operator_ = 119, /* "subtraction operator"  */
  YYSYMBOL_120_multiplication_operator_ = 120, /* "multiplication operator"  */
  YYSYMBOL_121_division_operator_ = 121,   /* "division operator"  */
  YYSYMBOL_CEILDIV = 122,                  /* "ceiling division operator"  */
  YYSYMBOL_123_modulus_operator_ = 123,    /* "modulus operator"  */
  YYSYMBOL_POW = 124,                      /* "power operator"  */
  YYSYMBOL_BCONC = 125,                    /* "bit-concatenation operator"  */
  YYSYMBOL_126_map_operator_ = 126,        /* "map operator"  */
  YYSYMBOL_NSMAP = 127,                    /* "non-strict map operator"  */
  YYSYMBOL_INC = 128,                      /* "increment operator"  */
  YYSYMBOL_DEC = 129,                      /* "decrement operator"  */
  YYSYMBOL_AS = 130,                       /* "cast operator"  */
  YYSYMBOL_ISA = 131,                      /* "type identification operator"  */
  YYSYMBOL_132_dot_operator_ = 132,        /* "dot operator"  */
  YYSYMBOL_ATTR = 133,                     /* "attribute"  */
  YYSYMBOL_UNMAP = 134,                    /* "unmap operator"  */
  YYSYMBOL_EXCOND = 135,                   /* "conditional on exception operator"  */
  YYSYMBOL_BIG = 136,                      /* "keyword `big'"  */
  YYSYMBOL_LITTLE = 137,                   /* "keyword `little'"  */
  YYSYMBOL_SIGNED = 138,                   /* "keyword `signed'"  */
  YYSYMBOL_UNSIGNED = 139,                 /* "keyword `unsigned'"  */
  YYSYMBOL_THREEDOTS = 140,                /* "varargs indicator"  */
  YYSYMBOL_THEN = 141,                     /* THEN  */
  YYSYMBOL_142_ = 142,                     /* '?'  */
  YYSYMBOL_143_ = 143,                     /* ':'  */
  YYSYMBOL_UNARY = 144,                    /* UNARY  */
  YYSYMBOL_HYPERUNARY = 145,               /* HYPERUNARY  */
  YYSYMBOL_START_EXP = 146,                /* START_EXP  */
  YYSYMBOL_START_DECL = 147,               /* START_DECL  */
  YYSYMBOL_START_STMT = 148,               /* START_STMT  */
  YYSYMBOL_START_PROGRAM = 149,            /* START_PROGRAM  */
  YYSYMBOL_150_ = 150,                     /* ','  */
  YYSYMBOL_151_ = 151,                     /* ';'  */
  YYSYMBOL_152_ = 152,                     /* '~'  */
  YYSYMBOL_153_ = 153,                     /* '!'  */
  YYSYMBOL_154_ = 154,                     /* '('  */
  YYSYMBOL_155_ = 155,                     /* ')'  */
  YYSYMBOL_156_ = 156,                     /* ".>"  */
  YYSYMBOL_157_ = 157,                     /* '['  */
  YYSYMBOL_158_ = 158,                     /* ']'  */
  YYSYMBOL_159_ = 159,                     /* '{'  */
  YYSYMBOL_160_ = 160,                     /* '}'  */
  YYSYMBOL_161_ = 161,                     /* '='  */
  YYSYMBOL_YYACCEPT = 162,                 /* $accept  */
  YYSYMBOL_pushlevel = 163,                /* pushlevel  */
  YYSYMBOL_start = 164,                    /* start  */
  YYSYMBOL_program = 165,                  /* program  */
  YYSYMBOL_program_elem_list = 166,        /* program_elem_list  */
  YYSYMBOL_program_elem = 167,             /* program_elem  */
  YYSYMBOL_load = 168,                     /* load  */
  YYSYMBOL_identifier = 169,               /* identifier  */
  YYSYMBOL_expression_list = 170,          /* expression_list  */
  YYSYMBOL_expression_opt = 171,           /* expression_opt  */
  YYSYMBOL_expression = 172,               /* expression  */
  YYSYMBOL_bconc = 173,                    /* bconc  */
  YYSYMBOL_mapop = 174,                    /* mapop  */
  YYSYMBOL_map = 175,                      /* map  */
  YYSYMBOL_unary_operator = 176,           /* unary_operator  */
  YYSYMBOL_primary = 177,                  /* primary  */
  YYSYMBOL_178_1 = 178,                    /* $@1  */
  YYSYMBOL_funcall = 179,                  /* funcall  */
  YYSYMBOL_funcall_arg_list = 180,         /* funcall_arg_list  */
  YYSYMBOL_funcall_arg = 181,              /* funcall_arg  */
  YYSYMBOL_format_arg_list = 182,          /* format_arg_list  */
  YYSYMBOL_format_arg = 183,               /* format_arg  */
  YYSYMBOL_opt_comma = 184,                /* opt_comma  */
  YYSYMBOL_struct_field_list = 185,        /* struct_field_list  */
  YYSYMBOL_struct_field = 186,             /* struct_field  */
  YYSYMBOL_array = 187,                    /* array  */
  YYSYMBOL_array_initializer_list = 188,   /* array_initializer_list  */
  YYSYMBOL_array_initializer = 189,        /* array_initializer  */
  YYSYMBOL_pushlevel_args = 190,           /* pushlevel_args  */
  YYSYMBOL_function_specifier = 191,       /* function_specifier  */
  YYSYMBOL_function_arg_list = 192,        /* function_arg_list  */
  YYSYMBOL_function_arg = 193,             /* function_arg  */
  YYSYMBOL_function_arg_initial = 194,     /* function_arg_initial  */
  YYSYMBOL_type_specifier = 195,           /* type_specifier  */
  YYSYMBOL_typename = 196,                 /* typename  */
  YYSYMBOL_string_type_specifier = 197,    /* string_type_specifier  */
  YYSYMBOL_simple_type_specifier = 198,    /* simple_type_specifier  */
  YYSYMBOL_cons_type_specifier = 199,      /* cons_type_specifier  */
  YYSYMBOL_integral_type_specifier = 200,  /* integral_type_specifier  */
  YYSYMBOL_integral_type_sign = 201,       /* integral_type_sign  */
  YYSYMBOL_offset_type_specifier = 202,    /* offset_type_specifier  */
  YYSYMBOL_array_type_specifier = 203,     /* array_type_specifier  */
  YYSYMBOL_function_type_specifier = 204,  /* function_type_specifier  */
  YYSYMBOL_function_type_arg_list = 205,   /* function_type_arg_list  */
  YYSYMBOL_function_type_arg = 206,        /* function_type_arg  */
  YYSYMBOL_struct_type_specifier = 207,    /* struct_type_specifier  */
  YYSYMBOL_208_2 = 208,                    /* $@2  */
  YYSYMBOL_struct_or_union = 209,          /* struct_or_union  */
  YYSYMBOL_struct_type_pinned = 210,       /* struct_type_pinned  */
  YYSYMBOL_integral_struct = 211,          /* integral_struct  */
  YYSYMBOL_struct_type_elem_list = 212,    /* struct_type_elem_list  */
  YYSYMBOL_endianness = 213,               /* endianness  */
  YYSYMBOL_struct_type_field = 214,        /* struct_type_field  */
  YYSYMBOL_215_3 = 215,                    /* $@3  */
  YYSYMBOL_struct_type_field_identifier = 216, /* struct_type_field_identifier  */
  YYSYMBOL_struct_type_field_label = 217,  /* struct_type_field_label  */
  YYSYMBOL_struct_type_field_constraint_and_init = 218, /* struct_type_field_constraint_and_init  */
  YYSYMBOL_struct_type_field_optcond = 219, /* struct_type_field_optcond  */
  YYSYMBOL_simple_declaration = 220,       /* simple_declaration  */
  YYSYMBOL_declaration = 221,              /* declaration  */
  YYSYMBOL_222_4 = 222,                    /* @4  */
  YYSYMBOL_defun_or_method = 223,          /* defun_or_method  */
  YYSYMBOL_defvar_list = 224,              /* defvar_list  */
  YYSYMBOL_defvar = 225,                   /* defvar  */
  YYSYMBOL_deftype_list = 226,             /* deftype_list  */
  YYSYMBOL_deftype = 227,                  /* deftype  */
  YYSYMBOL_defunit_list = 228,             /* defunit_list  */
  YYSYMBOL_defunit = 229,                  /* defunit  */
  YYSYMBOL_comp_stmt = 230,                /* comp_stmt  */
  YYSYMBOL_builtin = 231,                  /* builtin  */
  YYSYMBOL_stmt_decl_list = 232,           /* stmt_decl_list  */
  YYSYMBOL_ass_exp_op = 233,               /* ass_exp_op  */
  YYSYMBOL_simple_stmt_list = 234,         /* simple_stmt_list  */
  YYSYMBOL_simple_stmt = 235,              /* simple_stmt  */
  YYSYMBOL_stmt = 236,                     /* stmt  */
  YYSYMBOL_237_5 = 237,                    /* @5  */
  YYSYMBOL_238_6 = 238,                    /* @6  */
  YYSYMBOL_funcall_stmt = 239,             /* funcall_stmt  */
  YYSYMBOL_funcall_stmt_arg_list = 240,    /* funcall_stmt_arg_list  */
  YYSYMBOL_funcall_stmt_arg = 241          /* funcall_stmt_arg  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef N_
# define N_(Msgid) Msgid
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
# define YYCOPY_NEEDED 1
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL \
             && defined PKL_TAB_STYPE_IS_TRIVIAL && PKL_TAB_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE) \
             + YYSIZEOF (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  86
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   4177

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  162
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  80
/* YYNRULES -- Number of rules.  */
#define YYNRULES  304
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  522

#define YYMAXUTOK   391


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   153,     2,     2,     2,   123,   109,     2,
     154,   155,   120,   118,   150,   119,   132,   121,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   143,   151,
     114,   161,   115,   142,   126,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   157,     2,   158,   108,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   159,   107,   160,   152,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   110,   111,   112,   113,   116,   117,   122,   124,
     125,   127,   128,   129,   130,   131,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   144,   145,   146,   147,   148,
     149,   156
};

#if PKL_TAB_DEBUG
  /* YYRLINEYYN -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   574,   574,   589,   595,   602,   608,   614,   620,   630,
     636,   642,   651,   655,   659,   660,   670,   671,   672,   676,
     710,   750,   751,   759,   761,   762,   769,   770,   774,   775,
     781,   788,   794,   800,   806,   812,   817,   822,   828,   834,
     840,   846,   852,   858,   864,   870,   876,   882,   888,   894,
     900,   906,   912,   918,   923,   928,   934,   940,   946,   961,
     966,   981,   987,   993,   994,   998,  1007,  1008,  1012,  1018,
    1027,  1028,  1029,  1030,  1031,  1035,  1064,  1070,  1077,  1083,
    1089,  1097,  1098,  1104,  1110,  1115,  1121,  1127,  1133,  1139,
    1145,  1146,  1151,  1150,  1166,  1178,  1184,  1190,  1195,  1200,
    1205,  1219,  1242,  1251,  1253,  1254,  1261,  1270,  1272,  1273,
    1284,  1292,  1293,  1297,  1299,  1300,  1307,  1314,  1325,  1336,
    1337,  1344,  1350,  1363,  1377,  1387,  1400,  1401,  1408,  1418,
    1444,  1445,  1453,  1454,  1455,  1459,  1474,  1482,  1487,  1492,
    1493,  1494,  1495,  1496,  1500,  1501,  1502,  1506,  1517,  1518,
    1522,  1553,  1564,  1570,  1578,  1585,  1595,  1596,  1603,  1609,
    1616,  1635,  1655,  1653,  1715,  1716,  1720,  1721,  1725,  1726,
    1730,  1731,  1732,  1734,  1739,  1740,  1741,  1746,  1745,  1855,
    1856,  1860,  1864,  1872,  1878,  1885,  1892,  1900,  1908,  1918,
    1922,  1934,  1935,  1936,  1941,  1940,  2003,  2007,  2008,  2012,
    2013,  2018,  2040,  2041,  2046,  2070,  2071,  2076,  2115,  2123,
    2131,  2142,  2143,  2144,  2145,  2146,  2147,  2148,  2149,  2150,
    2151,  2152,  2153,  2154,  2155,  2156,  2157,  2158,  2159,  2160,
    2161,  2162,  2163,  2164,  2165,  2166,  2167,  2168,  2169,  2170,
    2171,  2172,  2173,  2174,  2175,  2176,  2177,  2178,  2179,  2180,
    2184,  2185,  2187,  2188,  2193,  2194,  2195,  2196,  2197,  2198,
    2199,  2200,  2201,  2202,  2203,  2207,  2208,  2209,  2214,  2220,
    2231,  2237,  2243,  2249,  2262,  2269,  2276,  2285,  2286,  2291,
    2295,  2301,  2307,  2323,  2343,  2361,  2360,  2414,  2413,  2466,
    2471,  2476,  2482,  2488,  2494,  2500,  2510,  2521,  2527,  2533,
    2539,  2555,  2564,  2565,  2572
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  N_("end of file"), N_("error"), N_("invalid token"),
  N_("integer literal"), "INTEGER_OVERFLOW", N_("character literal"),
  N_("string"), N_("identifier"), N_("type name"), N_("offset unit"),
  N_("offset"), N_("keyword `enum'"), N_("keyword `pinned'"),
  N_("keyword `struct'"), "token", N_("keyword `union'"),
  N_("keyword `const'"), N_("keyword `continue'"), N_("keyword `else'"),
  N_("keyword `if'"), N_("keyword `while"), N_("keyword `until'"),
  N_("keyword `for'"), N_("keyword `in'"), N_("keyword `where'"),
  N_("keyword `sizeof'"), N_("keyword `typeof'"), N_("keyword `assert'"),
  N_("token"), "ALIEN", N_("int type constructor"),
  N_("uint type constructor"), N_("offset type constructor"),
  N_("keyword `fun'"), N_("keyword `defset'"), N_("keyword `type'"),
  N_("keyword `var'"), N_("keyword `unit'"), N_("keyword `method'"),
  N_("keyword `return'"), N_("keyword `break'"),
  N_("string type specifier"), N_("keyword `try'"), N_("keyword `catch'"),
  N_("keyword `raise'"), N_("void type specifier"),
  N_("any type specifier"), N_("keyword `print'"), N_("keyword `printf'"),
  N_("keyword `load'"), N_("keyword `lambda'"), N_("keyword `format'"),
  "BUILTIN_RAND", "BUILTIN_GET_ENDIAN", "BUILTIN_SET_ENDIAN",
  "BUILTIN_GET_IOS", "BUILTIN_SET_IOS", "BUILTIN_OPEN", "BUILTIN_CLOSE",
  "BUILTIN_IOSIZE", "BUILTIN_IOFLAGS", "BUILTIN_IOGETB", "BUILTIN_IOSETB",
  "BUILTIN_GETENV", "BUILTIN_FORGET", "BUILTIN_GET_TIME", "BUILTIN_STRACE",
  "BUILTIN_TERM_RGB_TO_COLOR", "BUILTIN_SLEEP", "BUILTIN_TERM_GET_COLOR",
  "BUILTIN_TERM_SET_COLOR", "BUILTIN_TERM_GET_BGCOLOR",
  "BUILTIN_TERM_SET_BGCOLOR", "BUILTIN_TERM_BEGIN_CLASS",
  "BUILTIN_TERM_END_CLASS", "BUILTIN_TERM_BEGIN_HYPERLINK",
  "BUILTIN_TERM_END_HYPERLINK", "BUILTIN_VM_OBASE", "BUILTIN_VM_SET_OBASE",
  "BUILTIN_VM_OACUTOFF", "BUILTIN_VM_SET_OACUTOFF", "BUILTIN_VM_ODEPTH",
  "BUILTIN_VM_SET_ODEPTH", "BUILTIN_VM_OINDENT", "BUILTIN_VM_SET_OINDENT",
  "BUILTIN_VM_OMAPS", "BUILTIN_VM_SET_OMAPS", "BUILTIN_VM_OMODE",
  "BUILTIN_VM_SET_OMODE", "BUILTIN_VM_OPPRINT", "BUILTIN_VM_SET_OPPRINT",
  "BUILTIN_UNSAFE_STRING_SET", N_("power-and-assign operator"),
  N_("multiply-and-assign operator"), N_("divide-and-assing operator"),
  N_("modulus-and-assign operator"), N_("add-and-assing operator"),
  N_("subtract-and-assign operator"), N_("shift-left-and-assign operator"),
  N_("shift-right-and-assign operator"), N_("bit-and-and-assign operator"),
  N_("bit-xor-and-assign operator"), N_("bit-or-and-assign operator"),
  N_("range separator"), N_("logical or operator"),
  N_("logical and operator"), N_("logical implication operator"),
  N_("bit-wise or operator"), N_("bit-wise xor operator"),
  N_("bit-wise and operator"), N_("equality operator"),
  N_("inequality operator"), N_("less-or-equal operator"),
  N_("bigger-or-equal-than operator"), N_("less-than operator"),
  N_("bigger-than operator"), N_("left shift operator"),
  N_("right shift operator"), N_("addition operator"),
  N_("subtraction operator"), N_("multiplication operator"),
  N_("division operator"), N_("ceiling division operator"),
  N_("modulus operator"), N_("power operator"),
  N_("bit-concatenation operator"), N_("map operator"),
  N_("non-strict map operator"), N_("increment operator"),
  N_("decrement operator"), N_("cast operator"),
  N_("type identification operator"), N_("dot operator"), N_("attribute"),
  N_("unmap operator"), N_("conditional on exception operator"),
  N_("keyword `big'"), N_("keyword `little'"), N_("keyword `signed'"),
  N_("keyword `unsigned'"), N_("varargs indicator"), "THEN", "'?'", "':'",
  "UNARY", "HYPERUNARY", "START_EXP", "START_DECL", "START_STMT",
  "START_PROGRAM", "','", "';'", "'~'", "'!'", "'('", "')'", ".>", "'['",
  "']'", "'{'", "'}'", "'='", "$accept", "pushlevel", "start", "program",
  "program_elem_list", "program_elem", "load", "identifier",
  "expression_list", "expression_opt", "expression", "bconc", "mapop",
  "map", "unary_operator", "primary", "$@1", "funcall", "funcall_arg_list",
  "funcall_arg", "format_arg_list", "format_arg", "opt_comma",
  "struct_field_list", "struct_field", "array", "array_initializer_list",
  "array_initializer", "pushlevel_args", "function_specifier",
  "function_arg_list", "function_arg", "function_arg_initial",
  "type_specifier", "typename", "string_type_specifier",
  "simple_type_specifier", "cons_type_specifier",
  "integral_type_specifier", "integral_type_sign", "offset_type_specifier",
  "array_type_specifier", "function_type_specifier",
  "function_type_arg_list", "function_type_arg", "struct_type_specifier",
  "$@2", "struct_or_union", "struct_type_pinned", "integral_struct",
  "struct_type_elem_list", "endianness", "struct_type_field", "$@3",
  "struct_type_field_identifier", "struct_type_field_label",
  "struct_type_field_constraint_and_init", "struct_type_field_optcond",
  "simple_declaration", "declaration", "@4", "defun_or_method",
  "defvar_list", "defvar", "deftype_list", "deftype", "defunit_list",
  "defunit", "comp_stmt", "builtin", "stmt_decl_list", "ass_exp_op",
  "simple_stmt_list", "simple_stmt", "stmt", "@5", "@6", "funcall_stmt",
  "funcall_stmt_arg_list", "funcall_stmt_arg", YY_NULLPTR
  };
  /* YYTRANSLATABLE[SYMBOL-NUM] -- Whether YY_SNAME[SYMBOL-NUM] is
     internationalizable.  */
  static yytype_int8 yytranslatable[] =
  {
       1,     1,     1,     1,     0,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0
  };
  return (yysymbol < YYNTOKENS && yytranslatable[yysymbol]
          ? _(yy_sname[yysymbol])
          : yy_sname[yysymbol]);
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   124,    94,    38,
     362,   363,   364,   365,    60,    62,   366,   367,    43,    45,
      42,    47,   368,    37,   369,   370,    64,   371,   372,   373,
     374,   375,    46,   376,   377,   378,   379,   380,   381,   382,
     383,   384,    63,    58,   385,   386,   387,   388,   389,   390,
      44,    59,   126,    33,    40,    41,   391,    91,    93,   123,
     125,    61
};
#endif

#define YYPACT_NINF (-421)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-288)

#define yytable_value_is_error(Yyn) \
  ((Yyn) == YYTABLE_NINF)

  /* YYPACTSTATE-NUM -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     135,  2440,    91,  1548,   738,     8,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,  -421,  -121,  -101,  -421,  -421,   436,  -421,
    -421,  -421,  -421,   -65,  -421,  -421,  2440,  2440,  -421,  -421,
    -421,  2440,  2084,  4018,  3466,  -421,  -421,  2440,   -58,  -421,
    -421,  -127,   -38,  -105,   -31,  -421,   132,  -421,   -17,     9,
    -421,    73,    73,    73,  -421,    28,    32,    73,    39,    47,
      56,    60,    65,  2133,    46,  1597,  2189,  2440,    17,    94,
    -421,    55,  3700,    37,    64,  1563,     9,    72,    79,  -421,
    -421,   790,  -421,  -421,  -421,  -421,  -421,   436,  2440,  -421,
    -421,   -96,  -421,    74,   210,   108,   108,  2881,   123,    85,
      86,  3700,    99,  -421,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,   929,  -421,  -421,  2440,  2440,  2440,  2440,
    2440,  2440,  2440,  2440,  2440,  2440,  2440,  2440,  2440,  2440,
    2440,  2440,  2440,  2440,  2440,  2440,  2440,  2440,  2440,  -421,
    -421,   436,   436,  -421,  2440,  2440,  -421,   129,    73,  1731,
      73,  2250,  1068,  -421,  -421,  1256,  2440,  1816,   139,  2440,
    -421,  -421,    83,   100,  -421,   102,   106,  -421,   103,   121,
    -421,  -421,  -421,  -421,  -421,  2440,  2440,    21,  2440,  -421,
    3232,  -421,    -4,  -421,  3288,  3349,  1865,   252,   117,   124,
    -421,  2440,  2440,  -421,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -421,  -421,   265,  2440,  2440,   131,  -421,  -421,
    -421,  -421,   -86,  2937,  -120,    52,  -421,  -421,  -123,  1950,
    -421,  -421,  2440,  1207,   120,  -421,  -421,   978,  -421,  3902,
    3817,  3873,  3700,  3934,   921,   565,   730,   730,   286,   286,
     286,   286,   431,   431,   586,   586,   199,   199,   199,   199,
     215,   261,   119,   119,   730,  3522,  -421,  3700,   -87,  -421,
    -421,  1312,  2413,   125,  -137,   127,  3700,   141,  -421,  -421,
    2530,  3583,   142,  3700,  -421,   730,   181,    73,  2440,    73,
    2440,    73,   128,  2998,  3054,   262,  2306,    95,  2820,  -421,
    2440,   -15,  -421,  -421,  3700,    43,  -421,  1950,  -421,  -421,
    3700,  3700,  2440,  3700,  3700,  -421,  -421,  -421,  -421,   179,
     182,   419,  -421,   -83,  2586,  -421,  -421,  -421,  -421,  -421,
    2440,  2440,  -421,  -421,  2647,  2440,  1392,  -421,  2440,  1117,
     138,  -421,  2440,  2355,   144,    62,   288,  -421,   119,  -421,
    -421,  -421,  3700,  -421,  3700,  -421,    74,  1597,  1597,  2440,
     151,  3700,   152,  2440,  -421,  3405,  2440,  -421,  -421,  2440,
    -421,   -67,  3700,  -421,  -421,   165,   156,   162,    11,  -421,
    -421,   145,  3756,  -421,  -421,  2703,  -421,  2764,  3700,  -421,
    -421,  3700,  3700,  -421,  -421,   436,  -128,   158,   166,  -421,
      97,  -421,   300,  -421,  3700,  1682,  2306,  3115,  -421,  3700,
     419,  3700,  -421,  -421,   436,   419,   160,  -421,  2440,  -421,
    -421,   119,  -421,   436,   201,  -421,  -421,   436,  1597,   298,
     171,   -60,  -421,   183,  -421,  -421,   178,   -99,  -421,  2440,
    -421,  3700,   119,  -421,   119,   177,  -421,   187,   313,  1999,
    1597,  1682,  -421,  -421,  3700,   191,  1597,  2440,  -421,  -421,
     -53,  -421,  -421,  -421,     5,  -421,  3171,  1597,  -421,  -421,
      40,   181,  -421,  -421,  1597,  -421,  -421,  -421,  -421,    73,
    -421,  -421,  -421,   -97,  2440,  2440,  2440,   212,  3700,  1370,
    3639,  2440,   328,  2440,  2440,  3700,  2440,   202,  3700,  3700,
    3700,  -421
};

  /* YYDEFACTSTATE-NUM -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       0,     2,     0,     2,     2,     0,    76,    77,    78,    79,
      75,   135,    58,    59,     0,     0,   148,   149,     0,   136,
     138,   137,    92,     0,    71,    70,     2,     2,    74,    72,
      73,     2,     2,     0,     3,    63,    64,     2,    28,    90,
      81,   139,   143,     0,     0,   140,     0,   141,   142,     0,
     197,     0,     0,     0,   198,     0,     5,     0,     0,     0,
       0,     0,     0,     2,     0,     2,     2,     2,     0,     0,
     278,     9,   272,    63,    64,    28,   277,     0,     7,   276,
      11,     2,    14,    18,    16,    17,     1,     0,     2,   139,
     143,     0,   142,     0,     0,    61,    62,     0,    28,     0,
       0,   121,   111,   119,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   226,   225,
     227,   228,   229,   230,   231,   232,   233,   234,   235,   236,
     237,   238,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,     2,   210,    60,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    95,
      96,     0,     0,    30,     2,     2,     4,    29,     0,     2,
       0,     2,     2,    66,    67,     2,     2,     2,     0,     2,
      22,    21,     0,   192,   202,     0,   191,   199,     0,   193,
     205,   196,     6,   194,   290,     2,     2,     2,     2,   291,
       0,   289,     0,   297,     0,     0,     2,     0,     0,     0,
      10,     2,     2,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   264,   263,     0,     2,     2,   301,   302,   279,
       8,    15,     0,     0,     0,     0,   123,    93,     0,     2,
      80,    91,     2,     2,     0,   208,   252,     2,   250,    52,
      50,    49,    51,    46,    47,    48,    40,    41,    44,    45,
      42,    43,    38,    39,    31,    32,    33,    34,    35,    37,
      36,    65,    53,    54,    57,     0,    83,   106,     0,   104,
      82,     2,     0,    75,   135,     0,   116,   111,   114,   152,
       0,    68,   111,    24,   147,    56,     2,     0,     2,     0,
       2,     0,     0,     0,     0,     0,     2,     0,     0,   292,
       2,     2,   298,   299,   110,     0,   108,     2,    20,    19,
     270,   271,     2,   268,   269,   303,    99,    97,    98,     0,
       0,     0,   123,     0,     0,   120,   118,   209,   253,   251,
       2,     2,   102,    87,     0,     2,     2,    84,     2,     2,
       0,   153,     2,     2,     0,     0,   166,   204,   132,   134,
     133,   203,   201,   200,   207,   206,     0,     2,     2,     2,
       0,    27,     0,     2,   274,     0,     2,     2,   293,     2,
     300,     0,   304,   151,   150,     0,     0,   126,     0,     2,
      94,     0,    55,   105,    88,     0,    89,     0,   117,   115,
     101,    69,    25,   100,   160,     0,   158,     0,   156,   167,
       0,   195,   280,   282,     2,     2,     2,     0,   296,     2,
       0,   109,   273,   129,     0,     0,   130,   125,     2,    85,
      86,   155,   159,     0,     0,   164,   165,   168,     2,   285,
       0,     0,   266,     0,   275,   294,     0,     0,   127,     2,
     128,   122,   154,   157,   169,     0,   281,     0,     0,     2,
       2,     2,     2,     2,   131,   162,     2,     2,   267,   284,
       0,   295,   124,   161,   174,   286,     0,     2,   176,   175,
     174,     2,   170,   171,     2,   283,   163,   173,   172,   179,
     288,   180,   177,   183,     2,     2,     2,   181,   188,   184,
     185,     2,   189,     2,     2,   182,     2,     0,   187,   186,
     190,   178
};

  /* YYPGOTONTERM-NUM.  */
static const yytype_int16 yypgoto[] =
{
    -421,  -195,  -421,  -421,  -421,   271,   356,   -42,  -421,   -66,
      -1,   110,  -421,   118,  -421,    53,  -421,  -421,  -421,    13,
    -248,  -421,  -252,  -421,     7,  -421,  -421,   115,    27,    -6,
     -62,   -54,  -421,  -112,   200,   227,    -2,  -421,  -421,  -421,
    -421,   290,  -421,   -64,  -421,  -421,  -421,  -421,  -421,  -421,
    -421,  -421,  -109,  -421,  -421,  -421,  -421,  -421,    67,     0,
    -421,  -421,  -421,    77,  -421,    88,  -421,    82,    44,  -421,
    -421,  -421,   -75,  -420,   130,  -421,  -421,   366,  -421,   163
};

  /* YYDEFGOTONTERM-NUM.  */
static const yytype_int16 yydefgoto[] =
{
      -1,    33,     5,    80,    81,    82,    83,   395,   302,   380,
      72,    35,   186,    36,    37,    38,    93,    39,   288,   289,
     325,   326,   254,   297,   298,    40,   102,   103,   341,   247,
     396,   397,   460,   367,    41,    42,    43,    44,    45,    46,
      47,    48,   369,   417,   418,   370,   484,   447,   420,   465,
     490,   491,   492,   503,   502,   512,   507,   517,    55,    84,
     312,    57,   196,   197,   193,   194,   199,   200,    49,   144,
     257,   236,   451,    77,    85,   467,   468,    79,   237,   238
};

  /* YYTABLEYYPACT[STATE-NUM] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      34,   343,    56,   -21,   386,   452,   183,   184,    86,   192,
     195,   198,   317,   504,   442,   203,    91,   320,   190,   191,
     342,   183,   184,   216,   -21,    95,    96,  -144,   315,   185,
      97,   101,   182,    87,   185,   338,   177,   185,    50,   321,
      51,    52,    53,    54,   473,   360,   505,    76,    76,   478,
     364,   452,   185,    88,   245,   339,    75,    75,   185,   190,
     191,   185,   210,   351,   506,   214,   215,   389,   352,   336,
      11,   185,   400,    50,   178,    51,    52,    53,    54,   391,
     190,   191,    11,   389,    98,   242,   244,   243,   432,    94,
     469,   248,    16,    17,    18,   470,   179,   469,   180,   181,
     218,   219,   487,    19,    16,    17,    18,    20,    21,    76,
     445,   366,   446,    73,    73,    19,  -146,  -288,    75,    20,
      21,    74,    74,   187,    50,    76,    51,    52,    53,    54,
      51,    52,    53,    78,    75,   188,   286,  -145,   290,   387,
     295,   488,   489,   256,   189,   259,   260,   261,   262,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,   281,   185,   282,
     283,   217,   316,   284,   285,    73,   488,   489,   287,   201,
     292,   296,   202,    74,   300,   301,   303,    76,   305,    11,
     204,    73,   430,   389,   390,   212,    75,   211,   221,    74,
     496,   205,   414,   340,   313,   314,   220,   318,   145,    11,
     206,    16,    17,    18,   207,   324,   249,   415,    89,   208,
     330,   331,    19,   239,   145,   222,    20,    21,   246,   449,
     240,    16,    17,    18,   333,   334,  -288,  -288,   171,   172,
     251,   173,    19,   252,   306,    90,    20,    21,   324,   253,
     307,   344,   101,    73,   304,   178,   309,   348,   327,   171,
     172,    74,   173,   308,   310,   192,   234,   195,   328,   198,
     145,   311,   332,   258,   234,   329,   185,   179,   346,   180,
     181,     1,     2,     3,     4,   379,   -22,    89,   358,   376,
     354,   359,   363,    89,   393,   145,   366,   394,   410,   413,
     419,    76,   425,   426,   368,   433,   438,   372,    92,   374,
      75,   434,   435,   443,    90,   381,   444,   295,   448,   385,
      90,   459,  -287,   167,   168,   217,   324,   169,   170,   171,
     172,   392,   173,   472,   471,   365,   475,   477,   511,   398,
     168,   414,   476,   169,   170,   171,   172,   516,   173,   402,
     287,   483,   241,   521,   405,   407,   436,   408,   296,    71,
     453,   411,   412,   416,   403,   388,   409,    73,   345,   399,
     421,    89,    89,   458,   248,    74,   456,    92,   424,   499,
     463,   497,   427,    92,   382,   429,   373,   349,   431,   169,
     170,   171,   172,   375,   173,   371,   480,    99,    90,    90,
     335,     0,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,     0,   441,   169,   170,   171,   172,     0,   173,
       0,    76,    76,     0,     0,   381,   190,   294,   398,     0,
      75,    75,   457,   398,     0,     0,     0,   461,     0,     0,
     145,   462,   416,   437,    11,   464,     0,     0,     0,    16,
      17,    18,     0,     0,     0,     0,     0,   501,   474,     0,
      19,    92,    92,     0,    20,    21,    16,    17,    18,     0,
       0,     0,     0,   455,     0,     0,   486,    19,    75,     0,
       0,    20,    21,     0,   493,     0,     0,    73,    73,   368,
     498,     0,    76,     0,     0,    74,    74,     0,     0,     0,
       0,    75,     0,   508,   509,   510,    89,   422,   423,     0,
     515,     0,   518,   519,    76,   520,   481,   482,     0,     0,
      76,     0,    75,    75,    75,     0,     0,     0,     0,    75,
       0,    76,     0,    90,     0,    73,     0,     0,    76,     0,
      75,    89,     0,    74,     0,     0,     0,    75,     0,   161,
     162,   163,   164,   165,   166,   167,   168,     0,    73,   169,
     170,   171,   172,     0,   173,    89,    74,     0,    90,     0,
       0,     0,     0,     0,   145,     0,    89,     0,   466,    73,
      73,    73,     0,     0,     0,     0,    73,    74,    74,    74,
       0,     0,    90,     0,    74,   145,    92,    73,     0,     0,
     479,     0,     0,    90,    73,    74,   485,     0,     0,     0,
       0,     0,    74,     0,     0,    89,     0,   495,     0,     0,
       0,     0,     0,     0,   500,     0,     0,     0,     0,     0,
      89,    92,     0,     0,    89,    89,     0,     0,     0,     0,
       0,     0,    90,    89,    89,     0,     0,    89,     0,     0,
       0,     0,     0,     0,     0,    92,     0,    90,     0,     0,
       0,    90,    90,     0,     0,     0,    92,     0,     0,     0,
      90,    90,     0,     0,    90,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,    89,     0,   169,   170,   171,   172,     0,   173,     0,
     174,     0,     0,     0,     0,    92,   163,   164,   165,   166,
     167,   168,     0,     0,   169,   170,   171,   172,    90,   173,
      92,     0,     0,     0,    92,    92,     0,     0,     0,     0,
       0,     0,     0,    92,    92,     0,     0,    92,   -12,   145,
       0,     6,     7,     8,     9,    10,    11,    12,    13,     0,
       0,     0,     0,     0,     0,    58,     0,    59,    60,     0,
      61,     0,     0,    14,    15,    62,     0,     0,    16,    17,
      18,    50,     0,    51,    52,    53,    54,    63,    64,    19,
      65,    92,    66,    20,    21,    67,    68,    69,    22,    23,
     -13,     0,     0,     6,     7,     8,     9,    10,    11,    12,
      13,     0,     0,     0,     0,     0,     0,    58,     0,    59,
      60,     0,    61,     0,     0,    14,    15,    62,     0,     0,
      16,    17,    18,    50,     0,    51,    52,    53,    54,    63,
      64,    19,    65,     0,    66,    20,    21,    67,    68,    69,
      22,    23,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,    24,    25,   169,   170,
     171,   172,     0,   173,     0,     0,    26,    27,     0,     0,
       0,     0,    28,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    70,
      29,    30,    31,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    24,    25,
       0,     0,     0,     0,     0,     0,     0,     0,    26,    27,
       0,     0,     0,     0,    28,     0,     0,     0,     0,     0,
     145,     0,     6,     7,     8,     9,    10,    11,    12,    13,
       0,    70,    29,    30,    31,     0,    58,    32,    59,    60,
       0,    61,     0,     0,    14,    15,    62,     0,     0,    16,
      17,    18,    50,     0,    51,    52,    53,    54,    63,    64,
      19,    65,     0,    66,    20,    21,    67,    68,     0,    22,
      23,     6,     7,     8,     9,    10,    11,    12,    13,     0,
       0,     0,     0,     0,     0,    58,     0,    59,    60,     0,
      61,     0,     0,    14,    15,    62,     0,     0,    16,    17,
      18,    50,     0,    51,    52,    53,    54,    63,    64,    19,
      65,     0,    66,    20,    21,    67,    68,     0,    22,    23,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,    24,    25,   169,
     170,   171,   172,     0,   173,     0,   174,    26,    27,     0,
       0,     0,     0,    28,     0,     0,     0,     0,     0,     0,
       0,     6,     7,     8,     9,   293,   294,    12,    13,     0,
      70,    29,    30,    31,     0,     0,    32,     0,     0,   255,
       0,     0,     0,    14,    15,     0,    24,    25,    16,    17,
      18,     0,     0,     0,     0,     0,    26,    27,     0,    19,
       0,     0,    28,    20,    21,     0,     0,     0,    22,    23,
       6,     7,     8,     9,   293,   294,    12,    13,     0,    70,
      29,    30,    31,     0,     0,    32,     0,     0,   347,     0,
       0,     0,    14,    15,     0,     0,     0,    16,    17,    18,
       0,     0,     0,     0,     0,     0,     0,     0,    19,     0,
       0,     0,    20,    21,     0,     0,     0,    22,    23,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    24,    25,     0,     0,
       0,     0,     0,     0,     0,     0,    26,    27,     0,     0,
       0,     0,    28,     0,     0,     0,     0,     0,     0,     0,
       6,     7,     8,     9,    10,    11,    12,    13,  -113,     0,
      29,    30,    31,     0,     0,    32,     0,     0,  -113,     0,
       0,     0,    14,    15,     0,    24,    25,    16,    17,    18,
       0,     0,     0,     0,     0,    26,    27,     0,    19,     0,
       0,    28,    20,    21,     0,     0,     0,    22,    23,     6,
       7,     8,     9,    10,    11,    12,    13,     0,     0,    29,
      30,    31,     0,     0,    32,     0,     0,  -112,     0,     0,
       0,    14,    15,     0,     0,     0,    16,    17,    18,     0,
       0,     0,     0,     0,     0,     0,     0,    19,     0,     0,
       0,    20,    21,     0,     0,     0,    22,    23,     0,     0,
       0,     0,     0,     0,     0,     6,     7,     8,     9,    10,
      11,    12,    13,     0,     0,    24,    25,     0,     0,     0,
       0,     0,     0,     0,     0,    26,    27,    14,    15,   100,
       0,    28,    16,    17,    18,     0,     0,     0,     0,     0,
       0,     0,     0,    19,     0,     0,     0,    20,    21,    29,
      30,    31,    22,    23,    32,  -112,     0,     0,     0,     0,
       0,     0,     0,     0,    24,    25,     0,     0,     0,   145,
       0,     0,     0,     0,    26,    27,     0,     0,     0,     0,
      28,     0,     0,   146,     0,     6,     7,     8,     9,    10,
      11,    12,    13,     0,     0,     0,     0,     0,    29,    30,
      31,     0,     0,    32,   299,     0,     0,    14,    15,     0,
       0,     0,    16,    17,    18,     0,     0,     0,     0,     0,
      24,    25,     0,    19,     0,     0,     0,    20,    21,     0,
      26,    27,    22,    23,     0,     0,    28,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    29,    30,    31,     0,     0,    32,
     353,     0,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,     0,     0,   169,   170,
     171,   172,     0,   173,     0,   174,     0,     0,     0,     0,
      24,    25,   175,     0,     0,     0,     0,     0,     0,     0,
      26,    27,     0,     0,     0,     0,    28,     0,     0,     0,
       0,   513,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    29,    30,    31,     0,     0,    32,
     406,     6,     7,     8,     9,    10,    11,    12,    13,     0,
       0,     0,     0,     0,     0,    58,     0,    59,    60,     0,
      61,     0,     0,    14,    15,    62,     0,     0,    16,    17,
      18,     0,     0,     0,     0,     0,     0,    63,    64,    19,
      65,     0,    66,    20,    21,    67,    68,    69,    22,    23,
       6,     7,     8,     9,    10,    11,    12,    13,     0,     0,
       0,     0,     0,     0,    58,     0,    59,    60,     0,    61,
       0,     0,    14,    15,    62,     0,     0,    16,    17,    18,
       0,     0,     0,     0,     0,     0,    63,    64,    19,    65,
       0,    66,    20,    21,    67,    68,     0,    22,    23,     0,
       0,     0,     0,     0,     0,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,    24,    25,     0,     0,
       0,     0,     0,     0,     0,     0,    26,    27,     0,     0,
       0,     0,    28,     0,     0,     6,     7,     8,     9,    10,
      11,    12,    13,     0,     0,   178,     0,     0,     0,    70,
      29,    30,    31,     0,     0,    32,   234,    14,    15,    62,
       0,     0,    16,    17,    18,    24,    25,   179,     0,   180,
     181,     0,     0,    19,   235,    26,    27,    20,    21,     0,
     450,    28,    22,    23,     6,     7,     8,     9,    10,    11,
      12,    13,     0,     0,     0,     0,     0,     0,    70,    29,
      30,    31,     0,     0,    32,     0,    14,    15,     0,     0,
       0,    16,    17,    18,     0,     0,     0,     0,     0,     0,
       0,     0,    19,     0,     0,     0,    20,    21,     0,     0,
       0,    22,    23,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      24,    25,     0,     0,     0,     0,     0,     0,     0,     0,
      26,    27,     0,     0,     0,     0,    28,     0,     0,     6,
       7,     8,     9,    10,    11,    12,    13,     0,     0,     0,
       0,     0,  -265,     0,    29,    30,    31,  -265,     0,    32,
       0,    14,    15,     0,     0,     0,    16,    17,    18,    24,
      25,     0,     0,     0,     0,     0,     0,    19,     0,    26,
      27,    20,    21,     0,     0,    28,    22,    23,     6,     7,
       8,     9,    10,    11,    12,    13,     0,     0,     0,     0,
       0,  -103,     0,    29,    30,    31,  -103,     0,    32,     0,
      14,    15,     0,     0,     0,    16,    17,    18,     0,     0,
       0,     0,     0,     0,     0,     0,    19,     0,     0,     0,
      20,    21,     0,     0,     0,    22,    23,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    24,    25,     0,     0,     0,     0,
       0,     0,     0,     0,    26,    27,     0,     0,     0,     0,
      28,     0,     0,     6,     7,     8,     9,    10,    11,    12,
      13,     0,     0,     0,     0,     0,   -23,     0,    29,    30,
      31,   -23,     0,    32,     0,    14,    15,     0,     0,     0,
      16,    17,    18,    24,    25,     0,     0,     0,     0,     0,
       0,    19,     0,    26,    27,    20,    21,     0,     0,    28,
      22,    23,     6,     7,     8,     9,    10,    11,    12,    13,
       0,     0,     0,     0,     0,  -107,  -107,    29,    30,    31,
       0,     0,    32,     0,    14,    15,    62,     0,     0,    16,
      17,    18,     0,     0,     0,     0,     0,     0,     0,     0,
      19,     0,     0,     0,    20,    21,     0,   450,     0,    22,
      23,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    24,    25,
       0,     0,     0,     0,     0,     0,     0,     0,    26,    27,
       0,     0,     0,     0,    28,     0,     0,     6,     7,     8,
       9,    10,    11,    12,    13,     0,     0,     0,     0,     0,
    -107,     0,    29,    30,    31,  -107,     0,    32,     0,    14,
      15,     0,     0,     0,    16,    17,    18,    24,    25,     0,
       0,     0,     0,     0,     0,    19,     0,    26,    27,    20,
      21,     0,     0,    28,    22,    23,     6,     7,     8,     9,
      10,    11,    12,    13,     0,     0,     0,     0,     0,     0,
       0,    29,    30,    31,     0,     0,    32,     0,    14,    15,
       0,     0,     0,    16,    17,    18,     0,     0,     0,     0,
       0,     0,     0,     0,    19,     0,     0,     0,    20,    21,
       0,     0,     0,    22,    23,     0,     0,     0,     0,     0,
       0,     0,     6,     7,     8,     9,    10,    11,    12,    13,
       0,     0,    24,    25,     0,     0,     0,     0,     0,     0,
       0,     0,    26,    27,    14,    15,   100,     0,    28,    16,
      17,    18,     0,     0,     0,     0,     0,     0,     0,     0,
      19,     0,     0,     0,    20,    21,    29,    30,    31,    22,
      23,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    24,    25,     6,     7,     8,     9,    10,    11,    12,
      13,    26,    27,     0,     0,     0,     0,    28,     0,     0,
       0,     0,     0,     0,     0,    14,    15,     0,     0,     0,
      16,    17,    18,     0,   209,    29,    30,    31,     0,     0,
      32,    19,     0,     0,     0,    20,    21,     0,     0,     0,
      22,    23,     0,     0,     0,     0,     0,    24,    25,     6,
       7,     8,     9,    10,    11,    12,    13,    26,    27,     0,
       0,     0,     0,    28,     0,     0,     0,     0,     0,     0,
       0,    14,    15,     0,     0,     0,    16,    17,    18,     0,
     213,    29,    30,    31,     0,     0,    32,    19,     0,     0,
       0,    20,    21,     0,     0,     0,    22,    23,     6,     7,
       8,     9,    10,    11,    12,    13,     0,     0,    24,    25,
       0,     0,     0,     0,     0,     0,     0,     0,    26,    27,
      14,    15,     0,     0,    28,    16,    17,    18,     0,     0,
       0,     0,     0,   291,     0,     0,    19,     0,     0,     0,
      20,    21,    29,    30,    31,    22,    23,    32,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   145,     0,    24,    25,     0,     0,     0,     0,
       0,     0,     0,     0,    26,    27,   146,     0,     0,     0,
      28,     0,     0,     6,     7,     8,     9,    10,    11,    12,
      13,     0,     0,     0,     0,     0,     0,   -26,    29,    30,
      31,     0,     0,    32,     0,    14,    15,     0,     0,     0,
      16,    17,    18,    24,    25,     0,     0,     0,     0,     0,
       0,    19,     0,    26,    27,    20,    21,     0,     0,    28,
      22,    23,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    29,    30,    31,
    -112,     0,    32,     0,     0,     0,   355,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   145,
       0,   169,   170,   171,   172,     0,   173,     0,   174,     0,
       0,     0,     0,   146,     0,   175,   356,     0,    24,    25,
       0,     0,     0,     0,     0,     0,     0,     0,    26,    27,
       0,   357,     0,     0,    28,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    29,    30,    31,   145,     0,    32,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   146,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   145,     0,   169,   170,
     171,   172,     0,   173,     0,   174,     0,     0,     0,     0,
     146,     0,   175,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   361,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   145,     0,   169,   170,   171,   172,     0,   173,
       0,   174,     0,     0,     0,     0,   146,     0,   175,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   401,     0,     0,     0,     0,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   145,     0,   169,   170,   171,   172,     0,
     173,     0,   174,     0,     0,     0,     0,   146,     0,   175,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   404,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   145,
       0,   169,   170,   171,   172,     0,   173,     0,   174,     0,
       0,     0,     0,   146,     0,   175,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   439,     0,     0,     0,     0,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     145,     0,   169,   170,   171,   172,     0,   173,     0,   174,
       0,     0,     0,     0,   146,     0,   175,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   440,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   145,     0,   169,   170,
     171,   172,     0,   173,     0,   174,     0,     0,     0,     0,
     146,     0,   175,     0,     0,     0,     0,     0,     0,     0,
     383,     0,     0,     0,     0,   384,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   145,     0,   169,
     170,   171,   172,     0,   173,     0,   174,     0,     0,     0,
       0,   146,     0,   175,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   250,     0,     0,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   145,     0,   169,   170,   171,   172,     0,
     173,     0,   174,     0,     0,     0,     0,   146,     0,   175,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   337,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   145,     0,   169,   170,   171,   172,
       0,   173,     0,   174,     0,     0,     0,     0,   146,     0,
     175,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   377,     0,     0,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     145,     0,   169,   170,   171,   172,     0,   173,     0,   174,
       0,     0,     0,     0,   146,     0,   175,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   378,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   145,     0,   169,   170,   171,   172,     0,   173,     0,
     174,     0,     0,     0,     0,   146,     0,   175,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     454,     0,     0,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   145,     0,   169,
     170,   171,   172,     0,   173,     0,   174,     0,     0,     0,
       0,   146,     0,   175,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   494,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   145,     0,
     169,   170,   171,   172,     0,   173,     0,   174,     0,     0,
       0,     0,   146,     0,   175,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,     0,     0,     0,     0,     0,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   145,     0,   169,   170,   171,   172,
       0,   173,     0,   174,     0,     0,     0,     0,   146,     0,
     175,     0,     0,     0,     0,     0,     0,     0,     0,   322,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   145,     0,   169,   170,   171,
     172,     0,   173,     0,   174,     0,     0,     0,     0,   146,
       0,   175,     0,     0,     0,     0,     0,     0,     0,     0,
     323,     0,     0,     0,     0,     0,     0,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   145,     0,   169,   170,   171,   172,     0,   173,     0,
     174,     0,     0,     0,     0,   146,     0,   175,     0,     0,
       0,     0,     0,     0,     0,     0,   428,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   145,     0,   169,   170,   171,   172,     0,   173,
       0,   174,     0,     0,     0,     0,   146,     0,   175,     0,
       0,     0,     0,     0,     0,     0,   176,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   145,     0,
     169,   170,   171,   172,     0,   173,     0,   174,     0,     0,
       0,     0,   146,     0,   175,   350,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   145,
       0,   169,   170,   171,   172,     0,   173,     0,   174,     0,
       0,     0,     0,   146,     0,   175,   362,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   145,     0,   169,   170,   171,
     172,     0,   173,     0,   174,     0,     0,     0,     0,   146,
       0,   175,   514,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   145,     0,   169,   170,
     171,   172,     0,   173,     0,   174,     0,     0,     0,     0,
     146,     0,   175,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     147,   148,     0,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   145,     0,   169,   170,   171,   172,     0,   173,
       0,   174,     0,     0,     0,     0,   146,     0,   175,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   145,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   148,     0,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   145,     0,   169,   170,   171,   172,     0,
     173,     0,   174,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,     0,
       0,   169,   170,   171,   172,     0,   173,     0,   174,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,     0,     0,
     169,   170,   171,   172,     0,   173,     0,   174,     0,     0,
       0,     0,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
       0,     0,   169,   170,   171,   172,     0,   173,     0,   174,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,     0,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   143
};

static const yytype_int16 yycheck[] =
{
       1,   249,     2,   140,    19,   425,   126,   127,     0,    51,
      52,    53,   207,   110,   142,    57,    18,    21,     7,     8,
     143,   126,   127,     6,   161,    26,    27,   154,     7,   157,
      31,    32,   159,   154,   157,   155,    37,   157,    33,    43,
      35,    36,    37,    38,   143,   297,   143,     3,     4,   469,
     302,   471,   157,   154,   150,     3,     3,     4,   157,     7,
       8,   157,    63,   150,   161,    66,    67,   150,   155,   155,
       8,   157,   155,    33,   132,    35,    36,    37,    38,   327,
       7,     8,     8,   150,    31,    87,    88,    88,   155,   154,
     150,    93,    30,    31,    32,   155,   154,   150,   156,   157,
       6,     7,   155,    41,    30,    31,    32,    45,    46,    65,
      13,   306,    15,     3,     4,    41,   154,     9,    65,    45,
      46,     3,     4,   154,    33,    81,    35,    36,    37,    38,
      35,    36,    37,     3,    81,     3,   178,   154,   180,   154,
     182,   136,   137,   143,   135,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   157,   171,
     172,   154,   151,   174,   175,    65,   136,   137,   179,   151,
     181,   182,   150,    65,   185,   186,   187,   143,   189,     8,
     151,    81,   387,   150,   151,    65,   143,   151,   161,    81,
     160,   154,   140,   245,   205,   206,   151,   208,     9,     8,
     154,    30,    31,    32,   154,   216,     6,   155,    18,   154,
     221,   222,    41,   151,     9,   161,    45,    46,   154,   424,
     151,    30,    31,    32,   235,   236,   128,   129,   130,   131,
     155,   133,    41,   157,   161,    18,    45,    46,   249,   150,
     150,   252,   253,   143,   115,   132,   150,   257,     6,   130,
     131,   143,   133,   161,   161,   307,   143,   309,   151,   311,
       9,   150,     7,   143,   143,   151,   157,   154,   158,   156,
     157,   146,   147,   148,   149,    23,   161,    87,   161,   161,
     291,   150,   150,    93,   115,     9,   491,   115,   160,   155,
      12,   257,   151,   151,   306,   140,   161,   308,    18,   310,
     257,   155,   150,   155,    87,   316,   150,   359,    18,   320,
      93,   161,    24,   124,   125,   154,   327,   128,   129,   130,
     131,   332,   133,   155,   151,   154,   159,    24,   126,   341,
     125,   140,   155,   128,   129,   130,   131,    19,   133,   350,
     351,   160,    81,   151,   355,   356,   398,   358,   359,     3,
     426,   362,   363,   365,   351,   321,   359,   257,   253,   342,
     376,   171,   172,   435,   376,   257,   430,    87,   379,   491,
     444,   490,   383,    93,   317,   386,   309,   257,   389,   128,
     129,   130,   131,   311,   133,   307,   471,    31,   171,   172,
     237,    -1,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,    -1,   415,   128,   129,   130,   131,    -1,   133,
      -1,   377,   378,    -1,    -1,   426,     7,     8,   430,    -1,
     377,   378,   434,   435,    -1,    -1,    -1,   438,    -1,    -1,
       9,   443,   444,   399,     8,   447,    -1,    -1,    -1,    30,
      31,    32,    -1,    -1,    -1,    -1,    -1,   499,   459,    -1,
      41,   171,   172,    -1,    45,    46,    30,    31,    32,    -1,
      -1,    -1,    -1,   429,    -1,    -1,   477,    41,   425,    -1,
      -1,    45,    46,    -1,   484,    -1,    -1,   377,   378,   491,
     490,    -1,   448,    -1,    -1,   377,   378,    -1,    -1,    -1,
      -1,   448,    -1,   504,   505,   506,   306,   377,   378,    -1,
     511,    -1,   513,   514,   470,   516,   472,   473,    -1,    -1,
     476,    -1,   469,   470,   471,    -1,    -1,    -1,    -1,   476,
      -1,   487,    -1,   306,    -1,   425,    -1,    -1,   494,    -1,
     487,   341,    -1,   425,    -1,    -1,    -1,   494,    -1,   118,
     119,   120,   121,   122,   123,   124,   125,    -1,   448,   128,
     129,   130,   131,    -1,   133,   365,   448,    -1,   341,    -1,
      -1,    -1,    -1,    -1,     9,    -1,   376,    -1,   448,   469,
     470,   471,    -1,    -1,    -1,    -1,   476,   469,   470,   471,
      -1,    -1,   365,    -1,   476,     9,   306,   487,    -1,    -1,
     470,    -1,    -1,   376,   494,   487,   476,    -1,    -1,    -1,
      -1,    -1,   494,    -1,    -1,   415,    -1,   487,    -1,    -1,
      -1,    -1,    -1,    -1,   494,    -1,    -1,    -1,    -1,    -1,
     430,   341,    -1,    -1,   434,   435,    -1,    -1,    -1,    -1,
      -1,    -1,   415,   443,   444,    -1,    -1,   447,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   365,    -1,   430,    -1,    -1,
      -1,   434,   435,    -1,    -1,    -1,   376,    -1,    -1,    -1,
     443,   444,    -1,    -1,   447,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   491,    -1,   128,   129,   130,   131,    -1,   133,    -1,
     135,    -1,    -1,    -1,    -1,   415,   120,   121,   122,   123,
     124,   125,    -1,    -1,   128,   129,   130,   131,   491,   133,
     430,    -1,    -1,    -1,   434,   435,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   443,   444,    -1,    -1,   447,     0,     9,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    17,    -1,    19,    20,    -1,
      22,    -1,    -1,    25,    26,    27,    -1,    -1,    30,    31,
      32,    33,    -1,    35,    36,    37,    38,    39,    40,    41,
      42,   491,    44,    45,    46,    47,    48,    49,    50,    51,
       0,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,    19,
      20,    -1,    22,    -1,    -1,    25,    26,    27,    -1,    -1,
      30,    31,    32,    33,    -1,    35,    36,    37,    38,    39,
      40,    41,    42,    -1,    44,    45,    46,    47,    48,    49,
      50,    51,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   118,   119,   128,   129,
     130,   131,    -1,   133,    -1,    -1,   128,   129,    -1,    -1,
      -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   151,
     152,   153,   154,    -1,    -1,   157,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,   129,
      -1,    -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,
       9,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,   151,   152,   153,   154,    -1,    17,   157,    19,    20,
      -1,    22,    -1,    -1,    25,    26,    27,    -1,    -1,    30,
      31,    32,    33,    -1,    35,    36,    37,    38,    39,    40,
      41,    42,    -1,    44,    45,    46,    47,    48,    -1,    50,
      51,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    17,    -1,    19,    20,    -1,
      22,    -1,    -1,    25,    26,    27,    -1,    -1,    30,    31,
      32,    33,    -1,    35,    36,    37,    38,    39,    40,    41,
      42,    -1,    44,    45,    46,    47,    48,    -1,    50,    51,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   118,   119,   128,
     129,   130,   131,    -1,   133,    -1,   135,   128,   129,    -1,
      -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
     151,   152,   153,   154,    -1,    -1,   157,    -1,    -1,   160,
      -1,    -1,    -1,    25,    26,    -1,   118,   119,    30,    31,
      32,    -1,    -1,    -1,    -1,    -1,   128,   129,    -1,    41,
      -1,    -1,   134,    45,    46,    -1,    -1,    -1,    50,    51,
       3,     4,     5,     6,     7,     8,     9,    10,    -1,   151,
     152,   153,   154,    -1,    -1,   157,    -1,    -1,   160,    -1,
      -1,    -1,    25,    26,    -1,    -1,    -1,    30,    31,    32,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,
      -1,    -1,    45,    46,    -1,    -1,    -1,    50,    51,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   118,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   128,   129,    -1,    -1,
      -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,     4,     5,     6,     7,     8,     9,    10,   150,    -1,
     152,   153,   154,    -1,    -1,   157,    -1,    -1,   160,    -1,
      -1,    -1,    25,    26,    -1,   118,   119,    30,    31,    32,
      -1,    -1,    -1,    -1,    -1,   128,   129,    -1,    41,    -1,
      -1,   134,    45,    46,    -1,    -1,    -1,    50,    51,     3,
       4,     5,     6,     7,     8,     9,    10,    -1,    -1,   152,
     153,   154,    -1,    -1,   157,    -1,    -1,   160,    -1,    -1,
      -1,    25,    26,    -1,    -1,    -1,    30,    31,    32,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,    -1,
      -1,    45,    46,    -1,    -1,    -1,    50,    51,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    -1,    -1,   118,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   128,   129,    25,    26,   132,
      -1,   134,    30,    31,    32,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    41,    -1,    -1,    -1,    45,    46,   152,
     153,   154,    50,    51,   157,   158,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,   119,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    -1,   128,   129,    -1,    -1,    -1,    -1,
     134,    -1,    -1,    23,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    -1,    -1,    -1,    -1,    -1,   152,   153,
     154,    -1,    -1,   157,   158,    -1,    -1,    25,    26,    -1,
      -1,    -1,    30,    31,    32,    -1,    -1,    -1,    -1,    -1,
     118,   119,    -1,    41,    -1,    -1,    -1,    45,    46,    -1,
     128,   129,    50,    51,    -1,    -1,   134,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   152,   153,   154,    -1,    -1,   157,
     158,    -1,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,    -1,    -1,   128,   129,
     130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,
     118,   119,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     128,   129,    -1,    -1,    -1,    -1,   134,    -1,    -1,    -1,
      -1,   161,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   152,   153,   154,    -1,    -1,   157,
     158,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    -1,    17,    -1,    19,    20,    -1,
      22,    -1,    -1,    25,    26,    27,    -1,    -1,    30,    31,
      32,    -1,    -1,    -1,    -1,    -1,    -1,    39,    40,    41,
      42,    -1,    44,    45,    46,    47,    48,    49,    50,    51,
       3,     4,     5,     6,     7,     8,     9,    10,    -1,    -1,
      -1,    -1,    -1,    -1,    17,    -1,    19,    20,    -1,    22,
      -1,    -1,    25,    26,    27,    -1,    -1,    30,    31,    32,
      -1,    -1,    -1,    -1,    -1,    -1,    39,    40,    41,    42,
      -1,    44,    45,    46,    47,    48,    -1,    50,    51,    -1,
      -1,    -1,    -1,    -1,    -1,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   118,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   128,   129,    -1,    -1,
      -1,    -1,   134,    -1,    -1,     3,     4,     5,     6,     7,
       8,     9,    10,    -1,    -1,   132,    -1,    -1,    -1,   151,
     152,   153,   154,    -1,    -1,   157,   143,    25,    26,    27,
      -1,    -1,    30,    31,    32,   118,   119,   154,    -1,   156,
     157,    -1,    -1,    41,   161,   128,   129,    45,    46,    -1,
      48,   134,    50,    51,     3,     4,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    -1,    -1,   151,   152,
     153,   154,    -1,    -1,   157,    -1,    25,    26,    -1,    -1,
      -1,    30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    41,    -1,    -1,    -1,    45,    46,    -1,    -1,
      -1,    50,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     118,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     128,   129,    -1,    -1,    -1,    -1,   134,    -1,    -1,     3,
       4,     5,     6,     7,     8,     9,    10,    -1,    -1,    -1,
      -1,    -1,   150,    -1,   152,   153,   154,   155,    -1,   157,
      -1,    25,    26,    -1,    -1,    -1,    30,    31,    32,   118,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,   128,
     129,    45,    46,    -1,    -1,   134,    50,    51,     3,     4,
       5,     6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,
      -1,   150,    -1,   152,   153,   154,   155,    -1,   157,    -1,
      25,    26,    -1,    -1,    -1,    30,    31,    32,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,
      45,    46,    -1,    -1,    -1,    50,    51,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   118,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   128,   129,    -1,    -1,    -1,    -1,
     134,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,   150,    -1,   152,   153,
     154,   155,    -1,   157,    -1,    25,    26,    -1,    -1,    -1,
      30,    31,    32,   118,   119,    -1,    -1,    -1,    -1,    -1,
      -1,    41,    -1,   128,   129,    45,    46,    -1,    -1,   134,
      50,    51,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,   157,    -1,    25,    26,    27,    -1,    -1,    30,
      31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    -1,    -1,    45,    46,    -1,    48,    -1,    50,
      51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,   129,
      -1,    -1,    -1,    -1,   134,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,
     150,    -1,   152,   153,   154,   155,    -1,   157,    -1,    25,
      26,    -1,    -1,    -1,    30,    31,    32,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    41,    -1,   128,   129,    45,
      46,    -1,    -1,   134,    50,    51,     3,     4,     5,     6,
       7,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   152,   153,   154,    -1,    -1,   157,    -1,    25,    26,
      -1,    -1,    -1,    30,    31,    32,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,    45,    46,
      -1,    -1,    -1,    50,    51,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    -1,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   128,   129,    25,    26,   132,    -1,   134,    30,
      31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    -1,    -1,    45,    46,   152,   153,   154,    50,
      51,   157,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   118,   119,     3,     4,     5,     6,     7,     8,     9,
      10,   128,   129,    -1,    -1,    -1,    -1,   134,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    25,    26,    -1,    -1,    -1,
      30,    31,    32,    -1,   151,   152,   153,   154,    -1,    -1,
     157,    41,    -1,    -1,    -1,    45,    46,    -1,    -1,    -1,
      50,    51,    -1,    -1,    -1,    -1,    -1,   118,   119,     3,
       4,     5,     6,     7,     8,     9,    10,   128,   129,    -1,
      -1,    -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    25,    26,    -1,    -1,    -1,    30,    31,    32,    -1,
     151,   152,   153,   154,    -1,    -1,   157,    41,    -1,    -1,
      -1,    45,    46,    -1,    -1,    -1,    50,    51,     3,     4,
       5,     6,     7,     8,     9,    10,    -1,    -1,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,   129,
      25,    26,    -1,    -1,   134,    30,    31,    32,    -1,    -1,
      -1,    -1,    -1,   143,    -1,    -1,    41,    -1,    -1,    -1,
      45,    46,   152,   153,   154,    50,    51,   157,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     9,    -1,   118,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   128,   129,    23,    -1,    -1,    -1,
     134,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,    -1,   151,   152,   153,
     154,    -1,    -1,   157,    -1,    25,    26,    -1,    -1,    -1,
      30,    31,    32,   118,   119,    -1,    -1,    -1,    -1,    -1,
      -1,    41,    -1,   128,   129,    45,    46,    -1,    -1,   134,
      50,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   152,   153,   154,
     155,    -1,   157,    -1,    -1,    -1,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,     9,
      -1,   128,   129,   130,   131,    -1,   133,    -1,   135,    -1,
      -1,    -1,    -1,    23,    -1,   142,   143,    -1,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,   129,
      -1,   158,    -1,    -1,   134,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   152,   153,   154,     9,    -1,   157,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    23,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,     9,    -1,   128,   129,
     130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,
      23,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   158,    -1,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,     9,    -1,   128,   129,   130,   131,    -1,   133,
      -1,   135,    -1,    -1,    -1,    -1,    23,    -1,   142,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   158,    -1,    -1,    -1,    -1,    -1,
      -1,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,     9,    -1,   128,   129,   130,   131,    -1,
     133,    -1,   135,    -1,    -1,    -1,    -1,    23,    -1,   142,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   158,    -1,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,     9,
      -1,   128,   129,   130,   131,    -1,   133,    -1,   135,    -1,
      -1,    -1,    -1,    23,    -1,   142,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   158,    -1,    -1,    -1,    -1,    -1,    -1,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
       9,    -1,   128,   129,   130,   131,    -1,   133,    -1,   135,
      -1,    -1,    -1,    -1,    23,    -1,   142,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   158,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,     9,    -1,   128,   129,
     130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,
      23,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     150,    -1,    -1,    -1,    -1,   155,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,     9,    -1,   128,
     129,   130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,
      -1,    23,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   155,    -1,    -1,    -1,
      -1,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,     9,    -1,   128,   129,   130,   131,    -1,
     133,    -1,   135,    -1,    -1,    -1,    -1,    23,    -1,   142,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   155,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,     9,    -1,   128,   129,   130,   131,
      -1,   133,    -1,   135,    -1,    -1,    -1,    -1,    23,    -1,
     142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   155,    -1,    -1,    -1,    -1,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
       9,    -1,   128,   129,   130,   131,    -1,   133,    -1,   135,
      -1,    -1,    -1,    -1,    23,    -1,   142,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   155,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,     9,    -1,   128,   129,   130,   131,    -1,   133,    -1,
     135,    -1,    -1,    -1,    -1,    23,    -1,   142,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     155,    -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,     9,    -1,   128,
     129,   130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,
      -1,    23,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   155,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,     9,    -1,
     128,   129,   130,   131,    -1,   133,    -1,   135,    -1,    -1,
      -1,    -1,    23,    -1,   142,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   151,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,     9,    -1,   128,   129,   130,   131,
      -1,   133,    -1,   135,    -1,    -1,    -1,    -1,    23,    -1,
     142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   151,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,     9,    -1,   128,   129,   130,
     131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,    23,
      -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     151,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,     9,    -1,   128,   129,   130,   131,    -1,   133,    -1,
     135,    -1,    -1,    -1,    -1,    23,    -1,   142,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   151,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,     9,    -1,   128,   129,   130,   131,    -1,   133,
      -1,   135,    -1,    -1,    -1,    -1,    23,    -1,   142,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   150,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,     9,    -1,
     128,   129,   130,   131,    -1,   133,    -1,   135,    -1,    -1,
      -1,    -1,    23,    -1,   142,   143,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,     9,
      -1,   128,   129,   130,   131,    -1,   133,    -1,   135,    -1,
      -1,    -1,    -1,    23,    -1,   142,   143,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,     9,    -1,   128,   129,   130,
     131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,    23,
      -1,   142,   143,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,     9,    -1,   128,   129,
     130,   131,    -1,   133,    -1,   135,    -1,    -1,    -1,    -1,
      23,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     104,   105,    -1,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,     9,    -1,   128,   129,   130,   131,    -1,   133,
      -1,   135,    -1,    -1,    -1,    -1,    23,    -1,   142,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   105,    -1,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,     9,    -1,   128,   129,   130,   131,    -1,
     133,    -1,   135,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,    -1,
      -1,   128,   129,   130,   131,    -1,   133,    -1,   135,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,    -1,    -1,
     128,   129,   130,   131,    -1,   133,    -1,   135,    -1,    -1,
      -1,    -1,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
      -1,    -1,   128,   129,   130,   131,    -1,   133,    -1,   135,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    -1,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   159
};

  /* YYSTOSSTATE-NUM -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,   146,   147,   148,   149,   164,     3,     4,     5,     6,
       7,     8,     9,    10,    25,    26,    30,    31,    32,    41,
      45,    46,    50,    51,   118,   119,   128,   129,   134,   152,
     153,   154,   157,   163,   172,   173,   175,   176,   177,   179,
     187,   196,   197,   198,   199,   200,   201,   202,   203,   230,
      33,    35,    36,    37,    38,   220,   221,   223,    17,    19,
      20,    22,    27,    39,    40,    42,    44,    47,    48,    49,
     151,   168,   172,   173,   175,   177,   230,   235,   236,   239,
     165,   166,   167,   168,   221,   236,     0,   154,   154,   196,
     197,   198,   203,   178,   154,   172,   172,   172,   177,   239,
     132,   172,   188,   189,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,   159,   231,     9,    23,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   128,
     129,   130,   131,   133,   135,   142,   150,   172,   132,   154,
     156,   157,   159,   126,   127,   157,   174,   154,     3,   135,
       7,     8,   169,   226,   227,   169,   224,   225,   169,   228,
     229,   151,   150,   169,   151,   154,   154,   154,   154,   151,
     172,   151,   236,   151,   172,   172,     6,   154,     6,     7,
     151,   161,   161,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   143,   161,   233,   240,   241,   151,
     151,   167,   198,   172,   198,   150,   154,   191,   198,     6,
     155,   155,   157,   150,   184,   160,   221,   232,   236,   172,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   198,   198,   172,   172,   169,   172,   180,   181,
     169,   143,   172,     7,     8,   169,   172,   185,   186,   158,
     172,   172,   170,   172,   115,   172,   161,   150,   161,   150,
     161,   150,   222,   172,   172,     7,   151,   163,   172,   151,
      21,    43,   151,   151,   172,   182,   183,     6,   151,   151,
     172,   172,     7,   172,   172,   241,   155,   155,   155,     3,
     169,   190,   143,   182,   172,   189,   158,   160,   221,   236,
     143,   150,   155,   158,   172,   103,   143,   158,   161,   150,
     184,   158,   143,   150,   184,   154,   163,   195,   198,   204,
     207,   227,   172,   225,   172,   229,   161,   155,   155,    23,
     171,   172,   220,   150,   155,   172,    19,   154,   230,   150,
     151,   182,   172,   115,   115,   169,   192,   193,   198,   190,
     155,   158,   172,   181,   158,   172,   158,   172,   172,   186,
     160,   172,   172,   155,   140,   155,   198,   205,   206,    12,
     210,   191,   236,   236,   172,   151,   151,   172,   151,   172,
     163,   172,   155,   140,   155,   150,   169,   230,   161,   158,
     158,   198,   142,   155,   150,    13,    15,   209,    18,   163,
      48,   234,   235,   171,   155,   230,   193,   198,   192,   161,
     194,   172,   198,   205,   198,   211,   236,   237,   238,   150,
     155,   151,   155,   143,   172,   159,   155,    24,   235,   236,
     234,   230,   230,   160,   208,   236,   172,   155,   136,   137,
     212,   213,   214,   221,   155,   236,   160,   214,   221,   195,
     236,   169,   216,   215,   110,   143,   161,   218,   172,   172,
     172,   126,   217,   161,   143,   172,    19,   219,   172,   172,
     172,   151
};

  /* YYR1YYN -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   162,   163,   164,   164,   164,   164,   164,   164,   164,
     164,   164,   165,   165,   166,   166,   167,   167,   167,   168,
     168,   169,   169,   170,   170,   170,   171,   171,   172,   172,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   172,   172,   172,   172,   172,   172,   172,   172,
     172,   172,   172,   172,   172,   173,   174,   174,   175,   175,
     176,   176,   176,   176,   176,   177,   177,   177,   177,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   177,   178,   177,   177,   177,   177,   177,   177,   177,
     177,   177,   179,   180,   180,   180,   181,   182,   182,   182,
     183,   184,   184,   185,   185,   185,   186,   186,   187,   188,
     188,   189,   189,   190,   191,   191,   192,   192,   193,   193,
     194,   194,   195,   195,   195,   196,   197,   198,   198,   198,
     198,   198,   198,   198,   199,   199,   199,   200,   201,   201,
     202,   202,   203,   203,   204,   204,   205,   205,   206,   206,
     206,   207,   208,   207,   209,   209,   210,   210,   211,   211,
     212,   212,   212,   212,   213,   213,   213,   215,   214,   216,
     216,   217,   217,   218,   218,   218,   218,   218,   218,   219,
     219,   220,   220,   220,   222,   221,   221,   223,   223,   224,
     224,   225,   226,   226,   227,   228,   228,   229,   230,   230,
     230,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     232,   232,   232,   232,   233,   233,   233,   233,   233,   233,
     233,   233,   233,   233,   233,   234,   234,   234,   235,   235,
     235,   235,   235,   235,   235,   235,   235,   236,   236,   236,
     236,   236,   236,   236,   236,   237,   236,   238,   236,   236,
     236,   236,   236,   236,   236,   236,   236,   236,   236,   236,
     236,   239,   240,   240,   241
};

  /* YYR2YYN -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     3,     2,     3,     2,     3,     2,
       3,     2,     0,     1,     1,     2,     1,     1,     1,     3,
       3,     1,     1,     0,     1,     3,     0,     1,     1,     2,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     5,     3,     3,     1,     1,
       2,     2,     2,     1,     1,     3,     1,     1,     3,     5,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     3,     3,     4,     6,     6,     4,     5,     5,
       1,     3,     0,     3,     5,     2,     2,     4,     4,     4,
       5,     5,     4,     0,     1,     3,     1,     0,     1,     3,
       1,     0,     1,     0,     1,     3,     1,     3,     4,     1,
       3,     1,     6,     0,     7,     4,     1,     3,     3,     2,
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     1,     1,
       5,     5,     3,     4,     4,     3,     1,     3,     1,     2,
       1,     6,     0,     8,     1,     1,     0,     1,     0,     1,
       1,     1,     2,     2,     0,     1,     1,     0,     8,     0,
       1,     0,     2,     0,     2,     2,     4,     4,     2,     0,
       2,     2,     2,     2,     0,     5,     2,     1,     1,     1,
       3,     3,     1,     3,     3,     1,     3,     3,     3,     4,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     3,     3,     3,
       3,     3,     1,     5,     4,     6,     1,     1,     1,     2,
       5,     7,     5,    10,     8,     0,     9,     0,    11,     2,
       2,     2,     3,     4,     6,     8,     5,     2,     3,     3,
       4,     2,     1,     2,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = PKL_TAB_EMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == PKL_TAB_EMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        YY_LAC_DISCARD ("YYBACKUP");                              \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (&yylloc, pkl_parser, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use PKL_TAB_error or PKL_TAB_UNDEF. */
#define YYERRCODE PKL_TAB_UNDEF

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if PKL_TAB_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# ifndef YY_LOCATION_PRINT
#  if defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static int
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  int res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#   define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

#  else
#   define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#  endif
# endif /* !defined YY_LOCATION_PRINT */


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, Location, pkl_parser); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (yylocationp);
  YYUSE (pkl_parser);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, struct pkl_parser *pkl_parser)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  YY_LOCATION_PRINT (yyo, *yylocationp);
  YYFPRINTF (yyo, ": ");
  yy_symbol_value_print (yyo, yykind, yyvaluep, yylocationp, pkl_parser);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp,
                 int yyrule, struct pkl_parser *pkl_parser)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)],
                       &(yylsp[(yyi + 1) - (yynrhs)]), pkl_parser);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, pkl_parser); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !PKL_TAB_DEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !PKL_TAB_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Given a state stack such that *YYBOTTOM is its bottom, such that
   *YYTOP is either its top or is YYTOP_EMPTY to indicate an empty
   stack, and such that *YYCAPACITY is the maximum number of elements it
   can hold without a reallocation, make sure there is enough room to
   store YYADD more elements.  If not, allocate a new stack using
   YYSTACK_ALLOC, copy the existing elements, and adjust *YYBOTTOM,
   *YYTOP, and *YYCAPACITY to reflect the new capacity and memory
   location.  If *YYBOTTOM != YYBOTTOM_NO_FREE, then free the old stack
   using YYSTACK_FREE.  Return 0 if successful or if no reallocation is
   required.  Return YYENOMEM if memory is exhausted.  */
static int
yy_lac_stack_realloc (YYPTRDIFF_T *yycapacity, YYPTRDIFF_T yyadd,
#if PKL_TAB_DEBUG
                      char const *yydebug_prefix,
                      char const *yydebug_suffix,
#endif
                      yy_state_t **yybottom,
                      yy_state_t *yybottom_no_free,
                      yy_state_t **yytop, yy_state_t *yytop_empty)
{
  YYPTRDIFF_T yysize_old =
    *yytop == yytop_empty ? 0 : *yytop - *yybottom + 1;
  YYPTRDIFF_T yysize_new = yysize_old + yyadd;
  if (*yycapacity < yysize_new)
    {
      YYPTRDIFF_T yyalloc = 2 * yysize_new;
      yy_state_t *yybottom_new;
      /* Use YYMAXDEPTH for maximum stack size given that the stack
         should never need to grow larger than the main state stack
         needs to grow without LAC.  */
      if (YYMAXDEPTH < yysize_new)
        {
          YYDPRINTF ((stderr, "%smax size exceeded%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (YYMAXDEPTH < yyalloc)
        yyalloc = YYMAXDEPTH;
      yybottom_new =
        YY_CAST (yy_state_t *,
                 YYSTACK_ALLOC (YY_CAST (YYSIZE_T,
                                         yyalloc * YYSIZEOF (*yybottom_new))));
      if (!yybottom_new)
        {
          YYDPRINTF ((stderr, "%srealloc failed%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (*yytop != yytop_empty)
        {
          YYCOPY (yybottom_new, *yybottom, yysize_old);
          *yytop = yybottom_new + (yysize_old - 1);
        }
      if (*yybottom != yybottom_no_free)
        YYSTACK_FREE (*yybottom);
      *yybottom = yybottom_new;
      *yycapacity = yyalloc;
    }
  return 0;
}

/* Establish the initial context for the current lookahead if no initial
   context is currently established.

   We define a context as a snapshot of the parser stacks.  We define
   the initial context for a lookahead as the context in which the
   parser initially examines that lookahead in order to select a
   syntactic action.  Thus, if the lookahead eventually proves
   syntactically unacceptable (possibly in a later context reached via a
   series of reductions), the initial context can be used to determine
   the exact set of tokens that would be syntactically acceptable in the
   lookahead's place.  Moreover, it is the context after which any
   further semantic actions would be erroneous because they would be
   determined by a syntactically unacceptable token.

   YY_LAC_ESTABLISH should be invoked when a reduction is about to be
   performed in an inconsistent state (which, for the purposes of LAC,
   includes consistent states that don't know they're consistent because
   their default reductions have been disabled).  Iff there is a
   lookahead token, it should also be invoked before reporting a syntax
   error.  This latter case is for the sake of the debugging output.

   For parse.lac=full, the implementation of YY_LAC_ESTABLISH is as
   follows.  If no initial context is currently established for the
   current lookahead, then check if that lookahead can eventually be
   shifted if syntactic actions continue from the current context.
   Report a syntax error if it cannot.  */
#define YY_LAC_ESTABLISH                                                \
do {                                                                    \
  if (!yy_lac_established)                                              \
    {                                                                   \
      YYDPRINTF ((stderr,                                               \
                  "LAC: initial context established for %s\n",          \
                  yysymbol_name (yytoken)));                            \
      yy_lac_established = 1;                                           \
      switch (yy_lac (yyesa, &yyes, &yyes_capacity, yyssp, yytoken))    \
        {                                                               \
        case YYENOMEM:                                                  \
          goto yyexhaustedlab;                                          \
        case 1:                                                         \
          goto yyerrlab;                                                \
        }                                                               \
    }                                                                   \
} while (0)

/* Discard any previous initial lookahead context because of Event,
   which may be a lookahead change or an invalidation of the currently
   established initial context for the current lookahead.

   The most common example of a lookahead change is a shift.  An example
   of both cases is syntax error recovery.  That is, a syntax error
   occurs when the lookahead is syntactically erroneous for the
   currently established initial context, so error recovery manipulates
   the parser stacks to try to find a new initial context in which the
   current lookahead is syntactically acceptable.  If it fails to find
   such a context, it discards the lookahead.  */
#if PKL_TAB_DEBUG
# define YY_LAC_DISCARD(Event)                                           \
do {                                                                     \
  if (yy_lac_established)                                                \
    {                                                                    \
      YYDPRINTF ((stderr, "LAC: initial context discarded due to "       \
                  Event "\n"));                                          \
      yy_lac_established = 0;                                            \
    }                                                                    \
} while (0)
#else
# define YY_LAC_DISCARD(Event) yy_lac_established = 0
#endif

/* Given the stack whose top is *YYSSP, return 0 iff YYTOKEN can
   eventually (after perhaps some reductions) be shifted, return 1 if
   not, or return YYENOMEM if memory is exhausted.  As preconditions and
   postconditions: *YYES_CAPACITY is the allocated size of the array to
   which *YYES points, and either *YYES = YYESA or *YYES points to an
   array allocated with YYSTACK_ALLOC.  yy_lac may overwrite the
   contents of either array, alter *YYES and *YYES_CAPACITY, and free
   any old *YYES other than YYESA.  */
static int
yy_lac (yy_state_t *yyesa, yy_state_t **yyes,
        YYPTRDIFF_T *yyes_capacity, yy_state_t *yyssp, yysymbol_kind_t yytoken)
{
  yy_state_t *yyes_prev = yyssp;
  yy_state_t *yyesp = yyes_prev;
  /* Reduce until we encounter a shift and thereby accept the token.  */
  YYDPRINTF ((stderr, "LAC: checking lookahead %s:", yysymbol_name (yytoken)));
  if (yytoken == YYSYMBOL_YYUNDEF)
    {
      YYDPRINTF ((stderr, " Always Err\n"));
      return 1;
    }
  while (1)
    {
      int yyrule = yypact[+*yyesp];
      if (yypact_value_is_default (yyrule)
          || (yyrule += yytoken) < 0 || YYLAST < yyrule
          || yycheck[yyrule] != yytoken)
        {
          /* Use the default action.  */
          yyrule = yydefact[+*yyesp];
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
        }
      else
        {
          /* Use the action from yytable.  */
          yyrule = yytable[yyrule];
          if (yytable_value_is_error (yyrule))
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
          if (0 < yyrule)
            {
              YYDPRINTF ((stderr, " S%d\n", yyrule));
              return 0;
            }
          yyrule = -yyrule;
        }
      /* By now we know we have to simulate a reduce.  */
      YYDPRINTF ((stderr, " R%d", yyrule - 1));
      {
        /* Pop the corresponding number of values from the stack.  */
        YYPTRDIFF_T yylen = yyr2[yyrule];
        /* First pop from the LAC stack as many tokens as possible.  */
        if (yyesp != yyes_prev)
          {
            YYPTRDIFF_T yysize = yyesp - *yyes + 1;
            if (yylen < yysize)
              {
                yyesp -= yylen;
                yylen = 0;
              }
            else
              {
                yyesp = yyes_prev;
                yylen -= yysize;
              }
          }
        /* Only afterwards look at the main stack.  */
        if (yylen)
          yyesp = yyes_prev -= yylen;
      }
      /* Push the resulting state of the reduction.  */
      {
        yy_state_fast_t yystate;
        {
          const int yylhs = yyr1[yyrule] - YYNTOKENS;
          const int yyi = yypgoto[yylhs] + *yyesp;
          yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyesp
                     ? yytable[yyi]
                     : yydefgoto[yylhs]);
        }
        if (yyesp == yyes_prev)
          {
            yyesp = *yyes;
            YY_IGNORE_USELESS_CAST_BEGIN
            *yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        else
          {
            if (yy_lac_stack_realloc (yyes_capacity, 1,
#if PKL_TAB_DEBUG
                                      " (", ")",
#endif
                                      yyes, yyesa, &yyesp, yyes_prev))
              {
                YYDPRINTF ((stderr, "\n"));
                return YYENOMEM;
              }
            YY_IGNORE_USELESS_CAST_BEGIN
            *++yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        YYDPRINTF ((stderr, " G%d", yystate));
      }
    }
}

/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yy_state_t *yyesa;
  yy_state_t **yyes;
  YYPTRDIFF_T *yyes_capacity;
  yysymbol_kind_t yytoken;
  YYLTYPE *yylloc;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;

  int yyx;
  for (yyx = 0; yyx < YYNTOKENS; ++yyx)
    {
      yysymbol_kind_t yysym = YY_CAST (yysymbol_kind_t, yyx);
      if (yysym != YYSYMBOL_YYerror && yysym != YYSYMBOL_YYUNDEF)
        switch (yy_lac (yyctx->yyesa, yyctx->yyes, yyctx->yyes_capacity, yyctx->yyssp, yysym))
          {
          case YYENOMEM:
            return YYENOMEM;
          case 1:
            continue;
          default:
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




/* The kind of the lookahead of this context.  */
static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static yysymbol_kind_t
yypcontext_token (const yypcontext_t *yyctx)
{
  return yyctx->yytoken;
}

/* The location of the lookahead of this context.  */
static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx) YY_ATTRIBUTE_UNUSED;

static YYLTYPE *
yypcontext_location (const yypcontext_t *yyctx)
{
  return yyctx->yylloc;
}

/* User defined function to report a syntax error.  */
static int
yyreport_syntax_error (const yypcontext_t *yyctx, struct pkl_parser *pkl_parser);

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, struct pkl_parser *pkl_parser)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (pkl_parser);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case 3: /* "integer literal"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3158 "pkl-tab.c"
        break;

    case 5: /* "character literal"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3186 "pkl-tab.c"
        break;

    case 6: /* "string"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3214 "pkl-tab.c"
        break;

    case 7: /* "identifier"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3242 "pkl-tab.c"
        break;

    case 8: /* "type name"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3270 "pkl-tab.c"
        break;

    case 9: /* "offset unit"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3298 "pkl-tab.c"
        break;

    case 10: /* "offset"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3326 "pkl-tab.c"
        break;

    case 133: /* "attribute"  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3354 "pkl-tab.c"
        break;

    case 164: /* start  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3382 "pkl-tab.c"
        break;

    case 165: /* program  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3410 "pkl-tab.c"
        break;

    case 166: /* program_elem_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3438 "pkl-tab.c"
        break;

    case 167: /* program_elem  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3466 "pkl-tab.c"
        break;

    case 168: /* load  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3494 "pkl-tab.c"
        break;

    case 169: /* identifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3522 "pkl-tab.c"
        break;

    case 170: /* expression_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3550 "pkl-tab.c"
        break;

    case 171: /* expression_opt  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3578 "pkl-tab.c"
        break;

    case 172: /* expression  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3606 "pkl-tab.c"
        break;

    case 173: /* bconc  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3634 "pkl-tab.c"
        break;

    case 175: /* map  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3662 "pkl-tab.c"
        break;

    case 177: /* primary  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3690 "pkl-tab.c"
        break;

    case 179: /* funcall  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3718 "pkl-tab.c"
        break;

    case 180: /* funcall_arg_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3746 "pkl-tab.c"
        break;

    case 181: /* funcall_arg  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3774 "pkl-tab.c"
        break;

    case 182: /* format_arg_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3802 "pkl-tab.c"
        break;

    case 183: /* format_arg  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3830 "pkl-tab.c"
        break;

    case 185: /* struct_field_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3858 "pkl-tab.c"
        break;

    case 186: /* struct_field  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3886 "pkl-tab.c"
        break;

    case 187: /* array  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3914 "pkl-tab.c"
        break;

    case 188: /* array_initializer_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3942 "pkl-tab.c"
        break;

    case 189: /* array_initializer  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3970 "pkl-tab.c"
        break;

    case 191: /* function_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 3998 "pkl-tab.c"
        break;

    case 192: /* function_arg_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4026 "pkl-tab.c"
        break;

    case 193: /* function_arg  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4054 "pkl-tab.c"
        break;

    case 194: /* function_arg_initial  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4082 "pkl-tab.c"
        break;

    case 195: /* type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4110 "pkl-tab.c"
        break;

    case 196: /* typename  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4138 "pkl-tab.c"
        break;

    case 197: /* string_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4166 "pkl-tab.c"
        break;

    case 198: /* simple_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4194 "pkl-tab.c"
        break;

    case 199: /* cons_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4222 "pkl-tab.c"
        break;

    case 200: /* integral_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4250 "pkl-tab.c"
        break;

    case 202: /* offset_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4278 "pkl-tab.c"
        break;

    case 203: /* array_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4306 "pkl-tab.c"
        break;

    case 204: /* function_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4334 "pkl-tab.c"
        break;

    case 205: /* function_type_arg_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4362 "pkl-tab.c"
        break;

    case 206: /* function_type_arg  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4390 "pkl-tab.c"
        break;

    case 207: /* struct_type_specifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4418 "pkl-tab.c"
        break;

    case 211: /* integral_struct  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4446 "pkl-tab.c"
        break;

    case 212: /* struct_type_elem_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4474 "pkl-tab.c"
        break;

    case 214: /* struct_type_field  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4502 "pkl-tab.c"
        break;

    case 216: /* struct_type_field_identifier  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4530 "pkl-tab.c"
        break;

    case 217: /* struct_type_field_label  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4558 "pkl-tab.c"
        break;

    case 219: /* struct_type_field_optcond  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4586 "pkl-tab.c"
        break;

    case 220: /* simple_declaration  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4614 "pkl-tab.c"
        break;

    case 221: /* declaration  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4642 "pkl-tab.c"
        break;

    case 224: /* defvar_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4670 "pkl-tab.c"
        break;

    case 225: /* defvar  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4698 "pkl-tab.c"
        break;

    case 226: /* deftype_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4726 "pkl-tab.c"
        break;

    case 227: /* deftype  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4754 "pkl-tab.c"
        break;

    case 228: /* defunit_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4782 "pkl-tab.c"
        break;

    case 229: /* defunit  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4810 "pkl-tab.c"
        break;

    case 230: /* comp_stmt  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4838 "pkl-tab.c"
        break;

    case 232: /* stmt_decl_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4866 "pkl-tab.c"
        break;

    case 234: /* simple_stmt_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4894 "pkl-tab.c"
        break;

    case 235: /* simple_stmt  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4922 "pkl-tab.c"
        break;

    case 236: /* stmt  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4950 "pkl-tab.c"
        break;

    case 239: /* funcall_stmt  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 4978 "pkl-tab.c"
        break;

    case 240: /* funcall_stmt_arg_list  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5006 "pkl-tab.c"
        break;

    case 241: /* funcall_stmt_arg  */
#line 353 "pkl-tab.y"
            {
  if (((*yyvaluep).ast))
    {
      switch (PKL_AST_CODE (((*yyvaluep).ast)))
        {
        case PKL_AST_COMP_STMT:
            /*          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        case PKL_AST_TYPE:
          /*          if (PKL_AST_TYPE_CODE ($$) == PKL_TYPE_STRUCT)
                      pkl_parser->env = pkl_env_pop_frame (pkl_parser->env); */
          break;
        case PKL_AST_FUNC:
            /*          if (PKL_AST_FUNC_ARGS ($$))
                        pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);*/
          break;
        default:
          break;
        }
    }

  ((*yyvaluep).ast) = ASTREF (((*yyvaluep).ast)); pkl_ast_node_free (((*yyvaluep).ast));
 }
#line 5034 "pkl-tab.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct pkl_parser *pkl_parser)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined PKL_TAB_LTYPE_IS_TRIVIAL && PKL_TAB_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize;

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    yy_state_t yyesa[20];
    yy_state_t *yyes;
    YYPTRDIFF_T yyes_capacity;

  /* Whether LAC context is established.  A Boolean.  */
  int yy_lac_established = 0;
  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* The locations where the error started and ended.  */
  YYLTYPE yyerror_range[3];



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yynerrs = 0;
  yystate = 0;
  yyerrstatus = 0;

  yystacksize = YYINITDEPTH;
  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;

  yyes = yyesa;
  yyes_capacity = 20;
  if (YYMAXDEPTH < yyes_capacity)
    yyes_capacity = YYMAXDEPTH;


  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = PKL_TAB_EMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 30 "pkl-tab.y"
{
    yylloc.first_line = yylloc.last_line = 1;
    yylloc.first_column = yylloc.last_column = 1;
}

#line 5159 "pkl-tab.c"

  yylsp[0] = yylloc;
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yyls1, yysize * YYSIZEOF (*yylsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
        yyls = yyls1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == PKL_TAB_EMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= PKL_TAB_EOF)
    {
      yychar = PKL_TAB_EOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == PKL_TAB_error)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = PKL_TAB_UNDEF;
      yytoken = YYSYMBOL_YYerror;
      yyerror_range[1] = yylloc;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    {
      YY_LAC_ESTABLISH;
      goto yydefault;
    }
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      YY_LAC_ESTABLISH;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;

  /* Discard the shifted token.  */
  yychar = PKL_TAB_EMPTY;
  YY_LAC_DISCARD ("shift");
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  {
    int yychar_backup = yychar;
    switch (yyn)
      {
  case 2:
#line 575 "pkl-tab.y"
                {
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);
                }
#line 5380 "pkl-tab.c"
    break;

  case 3:
#line 590 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5390 "pkl-tab.c"
    break;

  case 4:
#line 596 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                  YYACCEPT;
                }
#line 5401 "pkl-tab.c"
    break;

  case 5:
#line 603 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5411 "pkl-tab.c"
    break;

  case 6:
#line 609 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5421 "pkl-tab.c"
    break;

  case 7:
#line 615 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5431 "pkl-tab.c"
    break;

  case 8:
#line 621 "pkl-tab.y"
                {
                  /* This rule is to allow the presence of an extra
                     ';' after the sentence.  This to allow the poke
                     command manager to ease the handling of
                     semicolons in the command line.  */
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5445 "pkl-tab.c"
    break;

  case 9:
#line 631 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5455 "pkl-tab.c"
    break;

  case 10:
#line 637 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5465 "pkl-tab.c"
    break;

  case 11:
#line 643 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_program (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  pkl_parser->ast->ast = ASTREF ((yyval.ast));
                }
#line 5475 "pkl-tab.c"
    break;

  case 12:
#line 652 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 5483 "pkl-tab.c"
    break;

  case 15:
#line 661 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) != NULL)
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                  else
                    (yyval.ast) = (yyvsp[-1].ast);
                }
#line 5494 "pkl-tab.c"
    break;

  case 19:
#line 677 "pkl-tab.y"
                {
                  char *filename = NULL;
                  int ret = load_module (pkl_parser,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                         &(yyval.ast), 0 /* filename_p */, &filename);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load `%s'",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)));
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                  free (filename);
                }
#line 5532 "pkl-tab.c"
    break;

  case 20:
#line 711 "pkl-tab.y"
                {
                  char *filename = PKL_AST_STRING_POINTER ((yyvsp[-1].ast));
                  int ret = load_module (pkl_parser,
                                         filename,
                                         &(yyval.ast), 1 /* filename_p */, NULL);
                  if (ret == 2)
                    /* The sub-parser should have emitted proper error
                       messages.  No need to be verbose here.  */
                    YYERROR;
                  else if (ret == 1)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                 "cannot load module from file `%s'",
                                 filename);
                      YYERROR;
                    }

                  /* Prepend and append SRC nodes to handle the change of
                     source files.  */
                  {
                      pkl_ast_node src1 = pkl_ast_make_src (pkl_parser->ast,
                                                            filename);
                      pkl_ast_node src2 = pkl_ast_make_src (pkl_parser->ast,
                                                            pkl_parser->filename);

                      (yyval.ast) = pkl_ast_chainon (src1, (yyval.ast));
                      (yyval.ast) = pkl_ast_chainon ((yyval.ast), src2);
                  }

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast));
                  pkl_ast_node_free ((yyvsp[-1].ast));
                }
#line 5569 "pkl-tab.c"
    break;

  case 23:
#line 760 "pkl-tab.y"
                  { (yyval.ast) = NULL; }
#line 5575 "pkl-tab.c"
    break;

  case 25:
#line 763 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                  }
#line 5583 "pkl-tab.c"
    break;

  case 26:
#line 769 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 5589 "pkl-tab.c"
    break;

  case 29:
#line 776 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast,
                                               (yyvsp[-1].opcode), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-1]);
                }
#line 5599 "pkl-tab.c"
    break;

  case 30:
#line 782 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ATTR,
                                                (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5610 "pkl-tab.c"
    break;

  case 31:
#line 789 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_ADD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5620 "pkl-tab.c"
    break;

  case 32:
#line 795 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SUB,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5630 "pkl-tab.c"
    break;

  case 33:
#line 801 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MUL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5640 "pkl-tab.c"
    break;

  case 34:
#line 807 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_DIV,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5650 "pkl-tab.c"
    break;

  case 35:
#line 813 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_CEILDIV, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5659 "pkl-tab.c"
    break;

  case 36:
#line 818 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_POW, (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5668 "pkl-tab.c"
    break;

  case 37:
#line 823 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_MOD,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5678 "pkl-tab.c"
    break;

  case 38:
#line 829 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5688 "pkl-tab.c"
    break;

  case 39:
#line 835 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_SR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5698 "pkl-tab.c"
    break;

  case 40:
#line 841 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EQ,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5708 "pkl-tab.c"
    break;

  case 41:
#line 847 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_NE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5718 "pkl-tab.c"
    break;

  case 42:
#line 853 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5728 "pkl-tab.c"
    break;

  case 43:
#line 859 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GT,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5738 "pkl-tab.c"
    break;

  case 44:
#line 865 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_LE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5748 "pkl-tab.c"
    break;

  case 45:
#line 871 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_GE,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5758 "pkl-tab.c"
    break;

  case 46:
#line 877 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5768 "pkl-tab.c"
    break;

  case 47:
#line 883 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_XOR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5778 "pkl-tab.c"
    break;

  case 48:
#line 889 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BAND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5788 "pkl-tab.c"
    break;

  case 49:
#line 895 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_AND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5798 "pkl-tab.c"
    break;

  case 50:
#line 901 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_OR,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5808 "pkl-tab.c"
    break;

  case 51:
#line 907 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IMPL,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5818 "pkl-tab.c"
    break;

  case 52:
#line 913 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_IN,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5828 "pkl-tab.c"
    break;

  case 53:
#line 919 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cast (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5837 "pkl-tab.c"
    break;

  case 54:
#line 924 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_isa (pkl_parser->ast, (yyvsp[0].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5846 "pkl-tab.c"
    break;

  case 55:
#line 929 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_cond_exp (pkl_parser->ast,
                                              (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5856 "pkl-tab.c"
    break;

  case 56:
#line 935 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EXCOND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5866 "pkl-tab.c"
    break;

  case 57:
#line 941 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_EXCOND,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5876 "pkl-tab.c"
    break;

  case 58:
#line 947 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5895 "pkl-tab.c"
    break;

  case 59:
#line 962 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5904 "pkl-tab.c"
    break;

  case 60:
#line 967 "pkl-tab.y"
                {
                  if ((yyvsp[0].ast) == NULL)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "invalid unit in offset");
                      YYERROR;
                    }

                    (yyval.ast) = pkl_ast_make_offset (pkl_parser->ast, (yyvsp[-1].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    if (PKL_AST_TYPE ((yyvsp[0].ast)))
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[0].ast))) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5923 "pkl-tab.c"
    break;

  case 61:
#line 982 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5933 "pkl-tab.c"
    break;

  case 62:
#line 988 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[0].ast),
                                              PKL_AST_ORDER_PRE, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5943 "pkl-tab.c"
    break;

  case 65:
#line 999 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_binary_exp (pkl_parser->ast, PKL_AST_OP_BCONC,
                                                (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5953 "pkl-tab.c"
    break;

  case 66:
#line 1007 "pkl-tab.y"
             { (yyval.integer) = 1; }
#line 5959 "pkl-tab.c"
    break;

  case 67:
#line 1008 "pkl-tab.y"
                { (yyval.integer) = 0; }
#line 5965 "pkl-tab.c"
    break;

  case 68:
#line 1013 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-1].integer),
                                         (yyvsp[-2].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5975 "pkl-tab.c"
    break;

  case 69:
#line 1019 "pkl-tab.y"
                 {
                   (yyval.ast) = pkl_ast_make_map (pkl_parser->ast, (yyvsp[-3].integer),
                                          (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                   PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 5985 "pkl-tab.c"
    break;

  case 70:
#line 1027 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NEG; }
#line 5991 "pkl-tab.c"
    break;

  case 71:
#line 1028 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_POS; }
#line 5997 "pkl-tab.c"
    break;

  case 72:
#line 1029 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_BNOT; }
#line 6003 "pkl-tab.c"
    break;

  case 73:
#line 1030 "pkl-tab.y"
                             { (yyval.opcode) = PKL_AST_OP_NOT; }
#line 6009 "pkl-tab.c"
    break;

  case 74:
#line 1031 "pkl-tab.y"
                               { (yyval.opcode) = PKL_AST_OP_UNMAP; }
#line 6015 "pkl-tab.c"
    break;

  case 75:
#line 1036 "pkl-tab.y"
                  {
                  /* Search for a variable definition in the
                     compile-time environment, and create a
                     PKL_AST_VAR node with it's lexical environment,
                     annotated with its initialization.  */

                  int back, over;
                  const char *name = PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast));

                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_MAIN,
                                      name, &back, &over);
                  if (!decl
                      || (PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_VAR
                          && PKL_AST_DECL_KIND (decl) != PKL_AST_DECL_KIND_FUNC))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "undefined variable '%s'", name);
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_var (pkl_parser->ast,
                                         (yyvsp[0].ast), /* name.  */
                                         decl,
                                         back, over);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 6048 "pkl-tab.c"
    break;

  case 76:
#line 1065 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6058 "pkl-tab.c"
    break;

  case 77:
#line 1071 "pkl-tab.y"
                {
                  (yyval.ast) = NULL; /* To avoid bison warning.  */
                  pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                             "integer literal is too big");
                  YYERROR;
                }
#line 6069 "pkl-tab.c"
    break;

  case 78:
#line 1078 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6079 "pkl-tab.c"
    break;

  case 79:
#line 1084 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6089 "pkl-tab.c"
    break;

  case 80:
#line 1090 "pkl-tab.y"
                {
                  if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_VAR)
                    PKL_AST_VAR_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  else if (PKL_AST_CODE ((yyvsp[-1].ast)) == PKL_AST_STRUCT_REF)
                    PKL_AST_STRUCT_REF_IS_PARENTHESIZED ((yyvsp[-1].ast)) = 1;
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 6101 "pkl-tab.c"
    break;

  case 82:
#line 1099 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6111 "pkl-tab.c"
    break;

  case 83:
#line 1105 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_ref (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6121 "pkl-tab.c"
    break;

  case 84:
#line 1111 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_indexer (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6130 "pkl-tab.c"
    break;

  case 85:
#line 1116 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6140 "pkl-tab.c"
    break;

  case 86:
#line 1122 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-5].ast), (yyvsp[-3].ast), (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6150 "pkl-tab.c"
    break;

  case 87:
#line 1128 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-3].ast), NULL, NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6160 "pkl-tab.c"
    break;

  case 88:
#line 1134 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), NULL, (yyvsp[-1].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6170 "pkl-tab.c"
    break;

  case 89:
#line 1140 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_trimmer (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6180 "pkl-tab.c"
    break;

  case 91:
#line 1147 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 6188 "pkl-tab.c"
    break;

  case 92:
#line 1151 "pkl-tab.y"
                {
                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = 0;
                }
#line 6199 "pkl-tab.c"
    break;

  case 93:
#line 1158 "pkl-tab.y"
                {
                  /* Annotate the contained RETURN statements with
                     their function and their lexical nest level
                     within the function.  */
                  pkl_ast_finish_returns ((yyvsp[0].ast));
                  (yyval.ast) = pkl_ast_make_lambda (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6212 "pkl-tab.c"
    break;

  case 94:
#line 1167 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                            0 /* printf_p */);
                  PKL_AST_TYPE ((yyval.ast))
                      = ASTREF (pkl_ast_make_string_type (pkl_parser->ast));
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_LOC (PKL_AST_TYPE ((yyval.ast))) = (yyloc);
                }
#line 6228 "pkl-tab.c"
    break;

  case 95:
#line 1179 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_INCR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6238 "pkl-tab.c"
    break;

  case 96:
#line 1185 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_incrdecr (pkl_parser->ast, (yyvsp[-1].ast),
                                              PKL_AST_ORDER_POST, PKL_AST_SIGN_DECR);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6248 "pkl-tab.c"
    break;

  case 97:
#line 1191 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_TYPEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6257 "pkl-tab.c"
    break;

  case 98:
#line 1196 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_TYPEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6266 "pkl-tab.c"
    break;

  case 99:
#line 1201 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_unary_exp (pkl_parser->ast, PKL_AST_OP_SIZEOF, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);
                }
#line 6275 "pkl-tab.c"
    break;

  case 100:
#line 1206 "pkl-tab.y"
                {
                  /* This syntax is only used for array
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_ARRAY)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected array type in constructor");
                      YYERROR;
                    }

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6293 "pkl-tab.c"
    break;

  case 101:
#line 1220 "pkl-tab.y"
                {
                  pkl_ast_node astruct;

                  /* This syntax is only used for struct
                     constructors.  */
                  if (PKL_AST_TYPE_CODE ((yyvsp[-4].ast)) != PKL_TYPE_STRUCT)
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-4]),
                                 "expected struct type in constructor");
                      YYERROR;
                    }

                  astruct = pkl_ast_make_struct (pkl_parser->ast,
                                           0 /* nelem */, (yyvsp[-2].ast));
                  PKL_AST_LOC (astruct) = (yyloc);

                  (yyval.ast) = pkl_ast_make_cons (pkl_parser->ast, (yyvsp[-4].ast), astruct);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6317 "pkl-tab.c"
    break;

  case 102:
#line 1243 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6327 "pkl-tab.c"
    break;

  case 103:
#line 1252 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6333 "pkl-tab.c"
    break;

  case 105:
#line 1255 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6341 "pkl-tab.c"
    break;

  case 106:
#line 1262 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6351 "pkl-tab.c"
    break;

  case 107:
#line 1271 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6357 "pkl-tab.c"
    break;

  case 109:
#line 1274 "pkl-tab.y"
                {
                  pkl_ast_node arg
                    = pkl_ast_make_format_arg (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC (arg) = (yylsp[0]);

                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), arg);
                }
#line 6369 "pkl-tab.c"
    break;

  case 110:
#line 1285 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_format_arg (pkl_parser->ast, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6378 "pkl-tab.c"
    break;

  case 113:
#line 1298 "pkl-tab.y"
                { (yyval.ast) = NULL; }
#line 6384 "pkl-tab.c"
    break;

  case 115:
#line 1301 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6392 "pkl-tab.c"
    break;

  case 116:
#line 1308 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    NULL /* name */,
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6403 "pkl-tab.c"
    break;

  case 117:
#line 1315 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_field (pkl_parser->ast,
                                                    (yyvsp[-2].ast),
                                                    (yyvsp[0].ast));
                    PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6415 "pkl-tab.c"
    break;

  case 118:
#line 1326 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array (pkl_parser->ast,
                                             0 /* nelem */,
                                             0 /* ninitializer */,
                                             (yyvsp[-2].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6427 "pkl-tab.c"
    break;

  case 120:
#line 1338 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6435 "pkl-tab.c"
    break;

  case 121:
#line 1345 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         NULL, (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6445 "pkl-tab.c"
    break;

  case 122:
#line 1351 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_array_initializer (pkl_parser->ast,
                                                         (yyvsp[-3].ast), (yyvsp[0].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6455 "pkl-tab.c"
    break;

  case 123:
#line 1364 "pkl-tab.y"
                {
                  /* Push the lexical frame for the function's
                     arguments.  */
                  pkl_parser->env = pkl_env_push_frame (pkl_parser->env);

                  /* If in a method, register a dummy for the initial
                     implicit argument.  */
                  if (pkl_parser->in_method_decl_p)
                    pkl_register_dummies (pkl_parser, 1);
                }
#line 6470 "pkl-tab.c"
    break;

  case 124:
#line 1378 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-2].ast), (yyvsp[-4].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6484 "pkl-tab.c"
    break;

  case 125:
#line 1388 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func (pkl_parser->ast,
                                          (yyvsp[-3].ast), NULL, (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6498 "pkl-tab.c"
    break;

  case 127:
#line 1402 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6506 "pkl-tab.c"
    break;

  case 128:
#line 1409 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6520 "pkl-tab.c"
    break;

  case 129:
#line 1419 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type,
                                               NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[-1]);
                  PKL_AST_LOC (array_type) = (yylsp[-1]);

                  (yyval.ast) = pkl_ast_make_func_arg (pkl_parser->ast,
                                              array_type,
                                              (yyvsp[-1].ast),
                                              NULL /* initial */);
                  PKL_AST_FUNC_ARG_VARARG ((yyval.ast)) = 1;
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_register_arg (pkl_parser, (yyval.ast)))
                      YYERROR;
                }
#line 6547 "pkl-tab.c"
    break;

  case 130:
#line 1444 "pkl-tab.y"
                                      { (yyval.ast) = NULL; }
#line 6553 "pkl-tab.c"
    break;

  case 131:
#line 1445 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 6559 "pkl-tab.c"
    break;

  case 135:
#line 1460 "pkl-tab.y"
                  {
                  pkl_ast_node decl = pkl_env_lookup (pkl_parser->env,
                                                      PKL_ENV_NS_MAIN,
                                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                                      NULL, NULL);
                  assert (decl != NULL
                          && PKL_AST_DECL_KIND (decl) == PKL_AST_DECL_KIND_TYPE);
                  (yyval.ast) = PKL_AST_DECL_INITIAL (decl);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  (yyvsp[0].ast) = ASTREF ((yyvsp[0].ast)); pkl_ast_node_free ((yyvsp[0].ast));
                }
#line 6575 "pkl-tab.c"
    break;

  case 136:
#line 1475 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_string_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6584 "pkl-tab.c"
    break;

  case 137:
#line 1483 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_any_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6593 "pkl-tab.c"
    break;

  case 138:
#line 1488 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_void_type (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6602 "pkl-tab.c"
    break;

  case 147:
#line 1507 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_integral_type (pkl_parser->ast,
                                                     PKL_AST_INTEGER_VALUE ((yyvsp[-1].ast)),
                                                     (yyvsp[-2].integer));
                    (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast)); pkl_ast_node_free ((yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6614 "pkl-tab.c"
    break;

  case 148:
#line 1517 "pkl-tab.y"
                           { (yyval.integer) = 1; }
#line 6620 "pkl-tab.c"
    break;

  case 149:
#line 1518 "pkl-tab.y"
                            { (yyval.integer) = 0; }
#line 6626 "pkl-tab.c"
    break;

  case 150:
#line 1523 "pkl-tab.y"
                {
                  pkl_ast_node decl
                    = pkl_env_lookup (pkl_parser->env,
                                      PKL_ENV_NS_UNITS,
                                      PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                      NULL, NULL);

                  if (!decl)
                    {
                      /* This could be the name of a type.  Try it out.  */
                      decl = pkl_env_lookup (pkl_parser->env,
                                             PKL_ENV_NS_MAIN,
                                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-1].ast)),
                                             NULL, NULL);

                      if (!decl)
                        {
                          pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-1]),
                                     "invalid unit in offset type");
                          YYERROR;
                        }
                    }

                  (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                 (yyvsp[-3].ast),
                                                 PKL_AST_DECL_INITIAL (decl));

                  (yyvsp[-1].ast) = ASTREF ((yyvsp[-1].ast)); pkl_ast_node_free ((yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6661 "pkl-tab.c"
    break;

  case 151:
#line 1554 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_offset_type (pkl_parser->ast,
                                                   (yyvsp[-3].ast), (yyvsp[-1].ast));
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-1].ast))) = (yylsp[-1]);
                    PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6673 "pkl-tab.c"
    break;

  case 152:
#line 1565 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-2].ast),
                                                NULL /* bound */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6683 "pkl-tab.c"
    break;

  case 153:
#line 1571 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_array_type (pkl_parser->ast, (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6692 "pkl-tab.c"
    break;

  case 154:
#line 1579 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   (yyvsp[-2].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6703 "pkl-tab.c"
    break;

  case 155:
#line 1586 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_function_type (pkl_parser->ast,
                                                   (yyvsp[0].ast), 0 /* narg */,
                                                   NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6714 "pkl-tab.c"
    break;

  case 157:
#line 1597 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast));
                }
#line 6722 "pkl-tab.c"
    break;

  case 158:
#line 1604 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[0].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 6732 "pkl-tab.c"
    break;

  case 159:
#line 1610 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   (yyvsp[-1].ast), NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_OPTIONAL ((yyval.ast)) = 1;
                }
#line 6743 "pkl-tab.c"
    break;

  case 160:
#line 1617 "pkl-tab.y"
                {
                  pkl_ast_node type
                    = pkl_ast_make_any_type (pkl_parser->ast);
                  pkl_ast_node array_type
                    = pkl_ast_make_array_type (pkl_parser->ast,
                                               type, NULL /* bound */);

                  PKL_AST_LOC (type) = (yylsp[0]);
                  PKL_AST_LOC (array_type) = (yylsp[0]);

                  (yyval.ast) = pkl_ast_make_func_type_arg (pkl_parser->ast,
                                                   array_type, NULL /* name */);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                  PKL_AST_FUNC_TYPE_ARG_VARARG ((yyval.ast)) = 1;
                }
#line 6763 "pkl-tab.c"
    break;

  case 161:
#line 1637 "pkl-tab.y"
                  {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-2].ast),
                                                   NULL /* elems */,
                                                   (yyvsp[-4].integer), (yyvsp[-3].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* The pushlevel in this rule and the subsequent
                       pop_frame, while not strictly needed, is to
                       avoid shift/reduce conflicts with the next
                       rule.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6784 "pkl-tab.c"
    break;

  case 162:
#line 1655 "pkl-tab.y"
                {
                  /* Register dummies for the locals used in
                     pkl-gen.pks:struct_mapper (not counting
                     OFFSET).  */
                  pkl_register_dummies (pkl_parser, 5);

                  /* Now register OFFSET with a type of
                     offset<uint<64>,1> */
                  {
                    pkl_ast_node decl, type;
                    pkl_ast_node offset_identifier
                      = pkl_ast_make_identifier (pkl_parser->ast, "OFFSET");
                    pkl_ast_node offset_magnitude
                      = pkl_ast_make_integer (pkl_parser->ast, 0);
                    pkl_ast_node offset_unit
                      = pkl_ast_make_integer (pkl_parser->ast, 1);
                    pkl_ast_node offset;

                    type = pkl_ast_make_integral_type (pkl_parser->ast, 64, 0);
                    PKL_AST_TYPE (offset_magnitude) = ASTREF (type);
                    PKL_AST_TYPE (offset_unit) = ASTREF (type);

                    offset = pkl_ast_make_offset (pkl_parser->ast,
                                                  offset_magnitude,
                                                  offset_unit);
                    type = pkl_ast_make_offset_type (pkl_parser->ast,
                                                     type,
                                                     offset_unit);
                    PKL_AST_TYPE (offset) = ASTREF (type);

                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              offset_identifier,
                                              offset,
                                              NULL /* source */);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (offset_identifier),
                                           decl))
                      assert (0);
                  }
                }
#line 6832 "pkl-tab.c"
    break;

  case 163:
#line 1699 "pkl-tab.y"
                {
                    (yyval.ast) = pkl_ast_make_struct_type (pkl_parser->ast,
                                                   0 /* nelem */,
                                                   0 /* nfield */,
                                                   0 /* ndecl */,
                                                   (yyvsp[-4].ast),
                                                   (yyvsp[-1].ast),
                                                   (yyvsp[-6].integer), (yyvsp[-5].integer));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* Pop the frame pushed in the `pushlevel' above.  */
                    pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 6850 "pkl-tab.c"
    break;

  case 164:
#line 1715 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 6856 "pkl-tab.c"
    break;

  case 165:
#line 1716 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 6862 "pkl-tab.c"
    break;

  case 166:
#line 1720 "pkl-tab.y"
                        { (yyval.integer) = 0; }
#line 6868 "pkl-tab.c"
    break;

  case 167:
#line 1721 "pkl-tab.y"
                        { (yyval.integer) = 1; }
#line 6874 "pkl-tab.c"
    break;

  case 168:
#line 1725 "pkl-tab.y"
                         { (yyval.ast) = NULL; }
#line 6880 "pkl-tab.c"
    break;

  case 169:
#line 1726 "pkl-tab.y"
                                { (yyval.ast) = (yyvsp[0].ast); }
#line 6886 "pkl-tab.c"
    break;

  case 172:
#line 1733 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 6892 "pkl-tab.c"
    break;

  case 173:
#line 1735 "pkl-tab.y"
                { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 6898 "pkl-tab.c"
    break;

  case 174:
#line 1739 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_DFL; }
#line 6904 "pkl-tab.c"
    break;

  case 175:
#line 1740 "pkl-tab.y"
                        { (yyval.integer) = PKL_AST_ENDIAN_LSB; }
#line 6910 "pkl-tab.c"
    break;

  case 176:
#line 1741 "pkl-tab.y"
                             { (yyval.integer) = PKL_AST_ENDIAN_MSB; }
#line 6916 "pkl-tab.c"
    break;

  case 177:
#line 1746 "pkl-tab.y"
                  {
                    /* Register a variable in the current environment
                       for the field.  We do it in this mid-rule so
                       the element can be used in the constraint.  */

                    pkl_ast_node dummy, decl;
                    pkl_ast_node identifier
                      = ((yyvsp[0].ast) != NULL
                         ? (yyvsp[0].ast)
                         : pkl_ast_make_identifier (pkl_parser->ast, ""));


                    dummy = pkl_ast_make_integer (pkl_parser->ast, 0);
                    PKL_AST_TYPE (dummy) = ASTREF ((yyvsp[-1].ast));
                    decl = pkl_ast_make_decl (pkl_parser->ast,
                                              PKL_AST_DECL_KIND_VAR,
                                              identifier, dummy,
                                              NULL /* source */);
                    PKL_AST_DECL_STRUCT_FIELD_P (decl) = 1;
                    PKL_AST_LOC (decl) = (yyloc);

                    if (!pkl_env_register (pkl_parser->env,
                                           PKL_ENV_NS_MAIN,
                                           PKL_AST_IDENTIFIER_POINTER (identifier),
                                           decl))
                      {
                        pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                   "duplicated struct element '%s'",
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                        YYERROR;
                      }

                    if (identifier)
                      {
                        identifier = ASTREF (identifier);
                        pkl_ast_node_free (identifier);
                      }
                  }
#line 6959 "pkl-tab.c"
    break;

  case 178:
#line 1786 "pkl-tab.y"
                  {
                    pkl_ast_node constraint = (yyvsp[-3].field_const_init).constraint;
                    pkl_ast_node initializer = (yyvsp[-3].field_const_init).initializer;
                    int impl_constraint_p = (yyvsp[-3].field_const_init).impl_constraint_p;

                    if (initializer)
                      {
                        pkl_ast_node field_decl, field_var;
                        int back, over;

                        /* We need a field name.  */
                        if ((yyvsp[-5].ast) == NULL)
                          {
                            pkl_error (pkl_parser->compiler, pkl_parser->ast, (yyloc),
                                       "no initializer allowed in anonymous field");
                            YYERROR;
                          }

                        /* Build a constraint derived from the
                           initializer if a constraint has not been
                           specified.  */
                        if (impl_constraint_p)
                          {
                            field_decl = pkl_env_lookup (pkl_parser->env,
                                                         PKL_ENV_NS_MAIN,
                                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-5].ast)),
                                                         &back, &over);
                            assert (field_decl);

                            field_var = pkl_ast_make_var (pkl_parser->ast,
                                                          (yyvsp[-5].ast),
                                                          field_decl,
                                                          back, over);
                            PKL_AST_LOC (field_var) = PKL_AST_LOC (initializer);

                            constraint = pkl_ast_make_binary_exp (pkl_parser->ast,
                                                                  PKL_AST_OP_EQ,
                                                                  field_var,
                                                                  initializer);
                            PKL_AST_LOC (constraint) = PKL_AST_LOC (initializer);
                          }
                      }

                    (yyval.ast) = pkl_ast_make_struct_type_field (pkl_parser->ast, (yyvsp[-5].ast), (yyvsp[-6].ast),
                                                         constraint, initializer,
                                                         (yyvsp[-2].ast), (yyvsp[-7].integer), (yyvsp[-1].ast));
                    PKL_AST_LOC ((yyval.ast)) = (yyloc);

                    /* If endianness is empty, bison includes the
                       blank characters before the type field as if
                       they were part of this rule.  Therefore the
                       location should be adjusted here.  */
                    if ((yyvsp[-7].integer) == PKL_AST_ENDIAN_DFL)
                      {
                        PKL_AST_LOC ((yyval.ast)).first_line = (yylsp[-6]).first_line;
                        PKL_AST_LOC ((yyval.ast)).first_column = (yylsp[-6]).first_column;
                      }

                    if ((yyvsp[-5].ast) != NULL)
                      {
                        PKL_AST_LOC ((yyvsp[-5].ast)) = (yylsp[-5]);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = pkl_ast_make_string_type (pkl_parser->ast);
                        PKL_AST_TYPE ((yyvsp[-5].ast)) = ASTREF (PKL_AST_TYPE ((yyvsp[-5].ast)));
                        PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-5].ast))) = (yylsp[-5]);
                      }
                  }
#line 7030 "pkl-tab.c"
    break;

  case 179:
#line 1855 "pkl-tab.y"
                        { (yyval.ast) = NULL; }
#line 7036 "pkl-tab.c"
    break;

  case 180:
#line 1856 "pkl-tab.y"
                            { (yyval.ast) = (yyvsp[0].ast); }
#line 7042 "pkl-tab.c"
    break;

  case 181:
#line 1861 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7050 "pkl-tab.c"
    break;

  case 182:
#line 1865 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 7059 "pkl-tab.c"
    break;

  case 183:
#line 1873 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = NULL;
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7069 "pkl-tab.c"
    break;

  case 184:
#line 1879 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[0]);
                  (yyval.field_const_init).initializer = NULL;
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7080 "pkl-tab.c"
    break;

  case 185:
#line 1886 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7091 "pkl-tab.c"
    break;

  case 186:
#line 1893 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[0]);
                  (yyval.field_const_init).initializer = (yyvsp[-2].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[-2]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7103 "pkl-tab.c"
    break;

  case 187:
#line 1901 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = (yyvsp[-2].ast);
                  PKL_AST_LOC ((yyval.field_const_init).constraint) = (yylsp[-2]);
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 0;
                }
#line 7115 "pkl-tab.c"
    break;

  case 188:
#line 1909 "pkl-tab.y"
                {
                  (yyval.field_const_init).constraint = NULL;
                  (yyval.field_const_init).initializer = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.field_const_init).initializer) = (yylsp[0]);
                  (yyval.field_const_init).impl_constraint_p = 1;
                }
#line 7126 "pkl-tab.c"
    break;

  case 189:
#line 1919 "pkl-tab.y"
                {
                  (yyval.ast) = NULL;
                }
#line 7134 "pkl-tab.c"
    break;

  case 190:
#line 1923 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[0].ast);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[0]);
                }
#line 7143 "pkl-tab.c"
    break;

  case 191:
#line 1934 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7149 "pkl-tab.c"
    break;

  case 192:
#line 1935 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7155 "pkl-tab.c"
    break;

  case 193:
#line 1936 "pkl-tab.y"
                               { (yyval.ast) = (yyvsp[0].ast); }
#line 7161 "pkl-tab.c"
    break;

  case 194:
#line 1941 "pkl-tab.y"
                {
                  /* In order to allow for the function to be called
                     from within itself (recursive calls) we should
                     register a partial declaration in the
                     compile-time environment before processing the
                     `function_specifier' below.  */

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_FUNC, (yyvsp[0].ast),
                                               NULL /* initial */,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyvsp[0].ast)) = (yylsp[0]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)),
                                         (yyval.ast)))
                    {
                      pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[0]),
                                 "function or variable `%s' already defined",
                                 PKL_AST_IDENTIFIER_POINTER ((yyvsp[0].ast)));
                      YYERROR;
                    }

                  /* function_specifier needs to know whether we are
                     in a function declaration or a method
                     declaration.  */
                  pkl_parser->in_method_decl_p = ((yyvsp[-1].integer) == IS_METHOD);
                }
#line 7196 "pkl-tab.c"
    break;

  case 195:
#line 1972 "pkl-tab.y"
                {
                  /* Complete the declaration registered above with
                     it's initial value, which is the specifier of the
                     function being defined.  */
                  PKL_AST_DECL_INITIAL ((yyvsp[-2].ast))
                    = ASTREF ((yyvsp[0].ast));
                  (yyval.ast) = (yyvsp[-2].ast);

                  /* If the reference counting of the declaration is
                     bigger than 1, this means there are recursive
                     calls in the function body.  Reset the refcount
                     to 1, since these references are weak.  */
                  if (PKL_AST_REFCOUNT ((yyvsp[-2].ast)) > 1)
                    PKL_AST_REFCOUNT ((yyvsp[-2].ast)) = 1;

                  /* Annotate the contained RETURN statements with
                     their function and their lexical nest level
                     within the function.  */
                  pkl_ast_finish_returns ((yyvsp[0].ast));

                  /* Annotate the function to be a method whenever
                     appropriate.  */
                  if ((yyvsp[-4].integer) == IS_METHOD)
                    PKL_AST_FUNC_METHOD_P ((yyvsp[0].ast)) = 1;

                  /* XXX: move to trans1.  */
                  PKL_AST_FUNC_NAME ((yyvsp[0].ast))
                    = xstrdup (PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)));

                  pkl_parser->in_method_decl_p = 0;
                }
#line 7232 "pkl-tab.c"
    break;

  case 196:
#line 2003 "pkl-tab.y"
                                 { (yyval.ast) = (yyvsp[-1].ast); }
#line 7238 "pkl-tab.c"
    break;

  case 197:
#line 2007 "pkl-tab.y"
                               { (yyval.integer) = IS_DEFUN; }
#line 7244 "pkl-tab.c"
    break;

  case 198:
#line 2008 "pkl-tab.y"
                        { (yyval.integer) = IS_METHOD; }
#line 7250 "pkl-tab.c"
    break;

  case 200:
#line 2014 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7256 "pkl-tab.c"
    break;

  case 201:
#line 2019 "pkl-tab.y"
            {
                (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                        PKL_AST_DECL_KIND_VAR, (yyvsp[-2].ast), (yyvsp[0].ast),
                                        pkl_parser->filename);
                PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                PKL_AST_LOC ((yyval.ast)) = (yyloc);

                if (!pkl_env_register (pkl_parser->env,
                                       PKL_ENV_NS_MAIN,
                                       PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                       (yyval.ast)))
                  {
                    pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                               "the variable `%s' is already defined",
                               PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                    YYERROR;
                  }
          }
#line 7279 "pkl-tab.c"
    break;

  case 203:
#line 2042 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7285 "pkl-tab.c"
    break;

  case 204:
#line 2047 "pkl-tab.y"
          {
            (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                    PKL_AST_DECL_KIND_TYPE, (yyvsp[-2].ast), (yyvsp[0].ast),
                                    pkl_parser->filename);
            PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
            PKL_AST_LOC ((yyval.ast)) = (yyloc);

            PKL_AST_TYPE_NAME ((yyvsp[0].ast)) = ASTREF ((yyvsp[-2].ast));

            if (!pkl_env_register (pkl_parser->env,
                                   PKL_ENV_NS_MAIN,
                                   PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                   (yyval.ast)))
              {
                pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                           "the type `%s' is already defined",
                           PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                YYERROR;
              }
          }
#line 7310 "pkl-tab.c"
    break;

  case 206:
#line 2072 "pkl-tab.y"
          { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7316 "pkl-tab.c"
    break;

  case 207:
#line 2077 "pkl-tab.y"
            {
              /* We need to cast the expression to uint<64> here,
                 instead of pkl-promo, because the installed
                 initializer is used as earlier as in the lexer.  Not
                 pretty.  */
              pkl_ast_node type
                = pkl_ast_make_integral_type (pkl_parser->ast,
                                              64, 0);
              pkl_ast_node cast
                = pkl_ast_make_cast (pkl_parser->ast,
                                     type, (yyvsp[0].ast));

              (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                      PKL_AST_DECL_KIND_UNIT, (yyvsp[-2].ast), cast,
                                      pkl_parser->filename);

              PKL_AST_LOC (type) = (yylsp[0]);
              PKL_AST_LOC (cast) = (yylsp[0]);
              PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              if (!pkl_env_register (pkl_parser->env,
                                     PKL_ENV_NS_UNITS,
                                     PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)),
                                     (yyval.ast)))
                {
                  pkl_error (pkl_parser->compiler, pkl_parser->ast, (yylsp[-2]),
                             "the unit `%s' is already defined",
                             PKL_AST_IDENTIFIER_POINTER ((yyvsp[-2].ast)));
                  YYERROR;
                }
            }
#line 7353 "pkl-tab.c"
    break;

  case 208:
#line 2116 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, NULL);
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 7365 "pkl-tab.c"
    break;

  case 209:
#line 2124 "pkl-tab.y"
            {
              (yyval.ast) = pkl_ast_make_comp_stmt (pkl_parser->ast, (yyvsp[-1].ast));
              PKL_AST_LOC ((yyval.ast)) = (yyloc);

              /* Pop the frame pushed by the `pushlevel' above.  */
              pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
            }
#line 7377 "pkl-tab.c"
    break;

  case 210:
#line 2132 "pkl-tab.y"
        {
          (yyval.ast) = pkl_ast_make_builtin (pkl_parser->ast, (yyvsp[0].integer));
          PKL_AST_LOC ((yyval.ast)) = (yyloc);

          /* Pop the frame pushed by the `pushlevel' above.  */
          pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
        }
#line 7389 "pkl-tab.c"
    break;

  case 211:
#line 2142 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_RAND; }
#line 7395 "pkl-tab.c"
    break;

  case 212:
#line 2143 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_ENDIAN; }
#line 7401 "pkl-tab.c"
    break;

  case 213:
#line 2144 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_SET_ENDIAN; }
#line 7407 "pkl-tab.c"
    break;

  case 214:
#line 2145 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_IOS; }
#line 7413 "pkl-tab.c"
    break;

  case 215:
#line 2146 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_SET_IOS; }
#line 7419 "pkl-tab.c"
    break;

  case 216:
#line 2147 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_OPEN; }
#line 7425 "pkl-tab.c"
    break;

  case 217:
#line 2148 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_CLOSE; }
#line 7431 "pkl-tab.c"
    break;

  case 218:
#line 2149 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_IOSIZE; }
#line 7437 "pkl-tab.c"
    break;

  case 219:
#line 2150 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_IOFLAGS; }
#line 7443 "pkl-tab.c"
    break;

  case 220:
#line 2151 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_IOGETB; }
#line 7449 "pkl-tab.c"
    break;

  case 221:
#line 2152 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_IOSETB; }
#line 7455 "pkl-tab.c"
    break;

  case 222:
#line 2153 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GETENV; }
#line 7461 "pkl-tab.c"
    break;

  case 223:
#line 2154 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_FORGET; }
#line 7467 "pkl-tab.c"
    break;

  case 224:
#line 2155 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_GET_TIME; }
#line 7473 "pkl-tab.c"
    break;

  case 225:
#line 2156 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_SLEEP; }
#line 7479 "pkl-tab.c"
    break;

  case 226:
#line 2157 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_STRACE; }
#line 7485 "pkl-tab.c"
    break;

  case 227:
#line 2158 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_GET_COLOR; }
#line 7491 "pkl-tab.c"
    break;

  case 228:
#line 2159 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_SET_COLOR; }
#line 7497 "pkl-tab.c"
    break;

  case 229:
#line 2160 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_GET_BGCOLOR; }
#line 7503 "pkl-tab.c"
    break;

  case 230:
#line 2161 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_SET_BGCOLOR; }
#line 7509 "pkl-tab.c"
    break;

  case 231:
#line 2162 "pkl-tab.y"
                                   { (yyval.integer) = PKL_AST_BUILTIN_TERM_BEGIN_CLASS; }
#line 7515 "pkl-tab.c"
    break;

  case 232:
#line 2163 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_TERM_END_CLASS; }
#line 7521 "pkl-tab.c"
    break;

  case 233:
#line 2164 "pkl-tab.y"
                                       { (yyval.integer) = PKL_AST_BUILTIN_TERM_BEGIN_HYPERLINK; }
#line 7527 "pkl-tab.c"
    break;

  case 234:
#line 2165 "pkl-tab.y"
                                     { (yyval.integer) = PKL_AST_BUILTIN_TERM_END_HYPERLINK; }
#line 7533 "pkl-tab.c"
    break;

  case 235:
#line 2166 "pkl-tab.y"
                           { (yyval.integer) = PKL_AST_BUILTIN_VM_OBASE; }
#line 7539 "pkl-tab.c"
    break;

  case 236:
#line 2167 "pkl-tab.y"
                               { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OBASE; }
#line 7545 "pkl-tab.c"
    break;

  case 237:
#line 2168 "pkl-tab.y"
                              { (yyval.integer) = PKL_AST_BUILTIN_VM_OACUTOFF; }
#line 7551 "pkl-tab.c"
    break;

  case 238:
#line 2169 "pkl-tab.y"
                                  { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OACUTOFF; }
#line 7557 "pkl-tab.c"
    break;

  case 239:
#line 2170 "pkl-tab.y"
                           { (yyval.integer) = PKL_AST_BUILTIN_VM_ODEPTH; }
#line 7563 "pkl-tab.c"
    break;

  case 240:
#line 2171 "pkl-tab.y"
                                { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_ODEPTH; }
#line 7569 "pkl-tab.c"
    break;

  case 241:
#line 2172 "pkl-tab.y"
                             { (yyval.integer) = PKL_AST_BUILTIN_VM_OINDENT; }
#line 7575 "pkl-tab.c"
    break;

  case 242:
#line 2173 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OINDENT; }
#line 7581 "pkl-tab.c"
    break;

  case 243:
#line 2174 "pkl-tab.y"
                           { (yyval.integer) = PKL_AST_BUILTIN_VM_OMAPS; }
#line 7587 "pkl-tab.c"
    break;

  case 244:
#line 2175 "pkl-tab.y"
                               { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OMAPS; }
#line 7593 "pkl-tab.c"
    break;

  case 245:
#line 2176 "pkl-tab.y"
                           { (yyval.integer) = PKL_AST_BUILTIN_VM_OMODE; }
#line 7599 "pkl-tab.c"
    break;

  case 246:
#line 2177 "pkl-tab.y"
                               { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OMODE; }
#line 7605 "pkl-tab.c"
    break;

  case 247:
#line 2178 "pkl-tab.y"
                             { (yyval.integer) = PKL_AST_BUILTIN_VM_OPPRINT; }
#line 7611 "pkl-tab.c"
    break;

  case 248:
#line 2179 "pkl-tab.y"
                                 { (yyval.integer) = PKL_AST_BUILTIN_VM_SET_OPPRINT; }
#line 7617 "pkl-tab.c"
    break;

  case 249:
#line 2180 "pkl-tab.y"
                                    { (yyval.integer) = PKL_AST_BUILTIN_UNSAFE_STRING_SET; }
#line 7623 "pkl-tab.c"
    break;

  case 251:
#line 2186 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7629 "pkl-tab.c"
    break;

  case 253:
#line 2189 "pkl-tab.y"
                  { (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast)); }
#line 7635 "pkl-tab.c"
    break;

  case 254:
#line 2193 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_POW; }
#line 7641 "pkl-tab.c"
    break;

  case 255:
#line 2194 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MUL; }
#line 7647 "pkl-tab.c"
    break;

  case 256:
#line 2195 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_DIV; }
#line 7653 "pkl-tab.c"
    break;

  case 257:
#line 2196 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_MOD; }
#line 7659 "pkl-tab.c"
    break;

  case 258:
#line 2197 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_ADD; }
#line 7665 "pkl-tab.c"
    break;

  case 259:
#line 2198 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SUB; }
#line 7671 "pkl-tab.c"
    break;

  case 260:
#line 2199 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SL; }
#line 7677 "pkl-tab.c"
    break;

  case 261:
#line 2200 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_SR; }
#line 7683 "pkl-tab.c"
    break;

  case 262:
#line 2201 "pkl-tab.y"
                { (yyval.integer) = PKL_AST_OP_BAND; }
#line 7689 "pkl-tab.c"
    break;

  case 263:
#line 2202 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_IOR; }
#line 7695 "pkl-tab.c"
    break;

  case 264:
#line 2203 "pkl-tab.y"
               { (yyval.integer) = PKL_AST_OP_XOR; }
#line 7701 "pkl-tab.c"
    break;

  case 265:
#line 2207 "pkl-tab.y"
                 { (yyval.ast) = NULL; }
#line 7707 "pkl-tab.c"
    break;

  case 267:
#line 2210 "pkl-tab.y"
                 { (yyval.ast) = pkl_ast_chainon ((yyvsp[-2].ast), (yyvsp[0].ast)); }
#line 7713 "pkl-tab.c"
    break;

  case 268:
#line 2215 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7723 "pkl-tab.c"
    break;

  case 269:
#line 2221 "pkl-tab.y"
                {
                  pkl_ast_node exp
                    = pkl_ast_make_binary_exp (pkl_parser->ast,
                                               (yyvsp[-1].integer), (yyvsp[-2].ast), (yyvsp[0].ast));

                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), exp);
                  PKL_AST_LOC (exp) = (yyloc);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7738 "pkl-tab.c"
    break;

  case 270:
#line 2232 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7748 "pkl-tab.c"
    break;

  case 271:
#line 2238 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_ass_stmt (pkl_parser->ast,
                                              (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7758 "pkl-tab.c"
    break;

  case 272:
#line 2244 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7768 "pkl-tab.c"
    break;

  case 273:
#line 2250 "pkl-tab.y"
                {
                  pkl_ast_node format =
                    pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                         1 /* printf_p */);
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                1 /* printf_p */, format);
                  PKL_AST_LOC (format) = (yyloc);
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7785 "pkl-tab.c"
    break;

  case 274:
#line 2263 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-1].ast), NULL, (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7796 "pkl-tab.c"
    break;

  case 275:
#line 2270 "pkl-tab.y"
                {
                  if (((yyval.ast) = pkl_make_assertion (pkl_parser, (yyvsp[-3].ast), (yyvsp[-1].ast), (yyloc)))
                      == NULL)
                    YYERROR;
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7807 "pkl-tab.c"
    break;

  case 276:
#line 2277 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_exp_stmt (pkl_parser->ast,
                                              (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7817 "pkl-tab.c"
    break;

  case 278:
#line 2287 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_null_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7826 "pkl-tab.c"
    break;

  case 279:
#line 2292 "pkl-tab.y"
                {
                  (yyval.ast) = (yyvsp[-1].ast);
                }
#line 7834 "pkl-tab.c"
    break;

  case 280:
#line 2296 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-2].ast), (yyvsp[0].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7844 "pkl-tab.c"
    break;

  case 281:
#line 2302 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_if_stmt (pkl_parser->ast,
                                             (yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7854 "pkl-tab.c"
    break;

  case 282:
#line 2308 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_WHILE,
                                               NULL, /* iterator */
                                               (yyvsp[-2].ast),   /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));
                }
#line 7874 "pkl-tab.c"
    break;

  case 283:
#line 2324 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               (yyvsp[-6].ast),   /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7898 "pkl-tab.c"
    break;

  case 284:
#line 2344 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR,
                                               NULL, /* iterator */
                                               (yyvsp[-4].ast),   /* condition */
                                               NULL, /* head */
                                               (yyvsp[-2].ast),   /* tail */
                                               (yyvsp[0].ast));  /* body */

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 7919 "pkl-tab.c"
    break;

  case 285:
#line 2361 "pkl-tab.y"
                {
                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    assert (0);
                }
#line 7947 "pkl-tab.c"
    break;

  case 286:
#line 2385 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-2].ast), /* decl */
                                                       (yyvsp[-4].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               NULL, /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast));  /* body */
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Free the identifier.  */
                  (yyvsp[-6].ast) = ASTREF ((yyvsp[-6].ast)); pkl_ast_node_free ((yyvsp[-6].ast));

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 7980 "pkl-tab.c"
    break;

  case 287:
#line 2414 "pkl-tab.y"
                {
                  /* XXX: avoid code replication here.  */

                  /* Push a new lexical level and register a variable
                     with name IDENTIFIER.  Note that the variable is
                     created with a dummy INITIAL, as there is none.  */

                  pkl_ast_node dummy = pkl_ast_make_integer (pkl_parser->ast,
                                                             0);
                  PKL_AST_LOC (dummy) = (yylsp[-3]);

                  (yyval.ast) = pkl_ast_make_decl (pkl_parser->ast,
                                               PKL_AST_DECL_KIND_VAR,
                                               (yyvsp[-3].ast),
                                               dummy,
                                               pkl_parser->filename);
                  PKL_AST_LOC ((yyval.ast)) = (yylsp[-3]);

                  if (!pkl_env_register (pkl_parser->env,
                                         PKL_ENV_NS_MAIN,
                                         PKL_AST_IDENTIFIER_POINTER ((yyvsp[-3].ast)),
                                         (yyval.ast)))
                    /* This should never happen.  */
                    assert (0);
                }
#line 8010 "pkl-tab.c"
    break;

  case 288:
#line 2440 "pkl-tab.y"
                {
                  pkl_ast_node iterator
                    = pkl_ast_make_loop_stmt_iterator (pkl_parser->ast,
                                                       (yyvsp[-4].ast), /* decl */
                                                       (yyvsp[-6].ast)); /* container */
                  PKL_AST_LOC (iterator) = (yyloc);

                  (yyval.ast) = pkl_ast_make_loop_stmt (pkl_parser->ast,
                                               PKL_AST_LOOP_STMT_KIND_FOR_IN,
                                               iterator,
                                               (yyvsp[-2].ast), /* condition */
                                               NULL, /* head */
                                               NULL, /* tail */
                                               (yyvsp[0].ast)); /* body */
                  PKL_AST_LOC ((yyvsp[-8].ast)) = (yylsp[-8]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[0].ast));

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 8041 "pkl-tab.c"
    break;

  case 289:
#line 2467 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_break_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8050 "pkl-tab.c"
    break;

  case 290:
#line 2472 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_continue_stmt (pkl_parser->ast);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8059 "pkl-tab.c"
    break;

  case 291:
#line 2477 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8069 "pkl-tab.c"
    break;

  case 292:
#line 2483 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_return_stmt (pkl_parser->ast,
                                                 (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8079 "pkl-tab.c"
    break;

  case 293:
#line 2489 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-2].ast), (yyvsp[0].ast), NULL, NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8089 "pkl-tab.c"
    break;

  case 294:
#line 2495 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-4].ast), (yyvsp[0].ast), NULL, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8099 "pkl-tab.c"
    break;

  case 295:
#line 2501 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_catch_stmt (pkl_parser->ast,
                                                    (yyvsp[-6].ast), (yyvsp[0].ast), (yyvsp[-2].ast), NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Pop the frame introduced by `pushlevel'
                     above.  */
                  pkl_parser->env = pkl_env_pop_frame (pkl_parser->env);
                }
#line 8113 "pkl-tab.c"
    break;

  case 296:
#line 2511 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_try_until_stmt (pkl_parser->ast,
                                                    (yyvsp[-3].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);

                  /* Annotate the contained BREAK and CONTINUE
                     statements with their lexical level within this
                     loop.  */
                  pkl_ast_finish_breaks ((yyval.ast), (yyvsp[-3].ast));
                }
#line 8128 "pkl-tab.c"
    break;

  case 297:
#line 2522 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                NULL);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8138 "pkl-tab.c"
    break;

  case 298:
#line 2528 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_raise_stmt (pkl_parser->ast,
                                                (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8148 "pkl-tab.c"
    break;

  case 299:
#line 2534 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                0 /* printf_p */, (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8158 "pkl-tab.c"
    break;

  case 300:
#line 2540 "pkl-tab.y"
                {
                  pkl_ast_node format =
                    pkl_ast_make_format (pkl_parser->ast, (yyvsp[-2].ast), (yyvsp[-1].ast),
                                         1 /* printf_p */);
                  (yyval.ast) = pkl_ast_make_print_stmt (pkl_parser->ast,
                                                1 /* printf_p */, format);
                  PKL_AST_LOC (format) = (yyloc);
                  PKL_AST_LOC ((yyvsp[-2].ast)) = (yylsp[-2]);
                  if (PKL_AST_TYPE ((yyvsp[-2].ast)))
                    PKL_AST_LOC (PKL_AST_TYPE ((yyvsp[-2].ast))) = (yylsp[-2]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8175 "pkl-tab.c"
    break;

  case 301:
#line 2556 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_make_funcall (pkl_parser->ast,
                                             (yyvsp[-1].ast), (yyvsp[0].ast));
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8185 "pkl-tab.c"
    break;

  case 303:
#line 2566 "pkl-tab.y"
                {
                  (yyval.ast) = pkl_ast_chainon ((yyvsp[-1].ast), (yyvsp[0].ast));
                }
#line 8193 "pkl-tab.c"
    break;

  case 304:
#line 2573 "pkl-tab.y"
                  {
                  (yyval.ast) = pkl_ast_make_funcall_arg (pkl_parser->ast,
                                                 (yyvsp[0].ast), (yyvsp[-1].ast));
                  PKL_AST_LOC ((yyvsp[-1].ast)) = (yylsp[-1]);
                  PKL_AST_LOC ((yyval.ast)) = (yyloc);
                }
#line 8204 "pkl-tab.c"
    break;


#line 8208 "pkl-tab.c"

        default: break;
      }
    if (yychar_backup != yychar)
      YY_LAC_DISCARD ("yychar change");
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == PKL_TAB_EMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yyesa, &yyes, &yyes_capacity, yytoken, &yylloc};
        if (yychar != PKL_TAB_EMPTY)
          YY_LAC_ESTABLISH;
        if (yyreport_syntax_error (&yyctx, pkl_parser) == 2)
          goto yyexhaustedlab;
      }
    }

  yyerror_range[1] = yylloc;
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= PKL_TAB_EOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == PKL_TAB_EOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, pkl_parser);
          yychar = PKL_TAB_EMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  /* If the stack popping above didn't lose the initial context for the
     current lookahead token, the shift below will for sure.  */
  YY_LAC_DISCARD ("error recovery");

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  ++yylsp;
  YYLLOC_DEFAULT (*yylsp, yyerror_range, 2);

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if 1
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, pkl_parser, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != PKL_TAB_EMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, pkl_parser);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, yylsp, pkl_parser);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yyes != yyesa)
    YYSTACK_FREE (yyes);

  return yyresult;
}

#line 2612 "pkl-tab.y"


/* Handle syntax errors.  */

int
yyreport_syntax_error (const yypcontext_t *ctx,
                       struct pkl_parser *pkl_parser)
{
  int res = 0;
  yysymbol_kind_t lookahead = yypcontext_token (ctx);

  /* if the unexpected token is alien, then report
     pkl_parser->alien_err_msg.  */
  if (lookahead == YYSYMBOL_ALIEN)
    {
      pkl_tab_error (yypcontext_location (ctx),
                     pkl_parser,
                     pkl_parser->alien_errmsg);
      free (pkl_parser->alien_errmsg);
      pkl_parser->alien_errmsg = NULL;
    }
  else
    {
      /* report tokens expected at this point.  */
      yysymbol_kind_t expected[YYNTOKENS];
      int nexpected = yypcontext_expected_tokens (ctx, expected, YYNTOKENS);

      if (nexpected < 0)
        /* forward errors to yyparse.  */
        res = nexpected;
      else
        {
          char *errmsg = strdup ("syntax error");

          if (!errmsg)
            return YYENOMEM;

          if (lookahead != YYSYMBOL_YYEMPTY)
            {
              char *tmp = pk_str_concat (errmsg,
                                         ": unexpected ",
                                         yysymbol_name (lookahead),
                                         NULL);
              free (errmsg);
              if (!tmp)
                return YYENOMEM;
              errmsg = tmp;
            }

          pkl_tab_error (yypcontext_location (ctx), pkl_parser, errmsg);
          free (errmsg);
        }
    }

  return res;
}
