/* A Bison parser, made by GNU Bison 3.6.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
# define YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PKL_TAB_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PKL_TAB_DEBUG 1
#  else
#   define PKL_TAB_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PKL_TAB_DEBUG 1
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PKL_TAB_DEBUG */
#if PKL_TAB_DEBUG
extern int pkl_tab_debug;
#endif

/* Token kinds.  */
#ifndef PKL_TAB_TOKENTYPE
# define PKL_TAB_TOKENTYPE
  enum pkl_tab_tokentype
  {
    PKL_TAB_EMPTY = -2,
    PKL_TAB_EOF = 0,               /* "end of file"  */
    PKL_TAB_error = 256,           /* error  */
    PKL_TAB_UNDEF = 257,           /* "invalid token"  */
    INTEGER = 258,                 /* "integer literal"  */
    INTEGER_OVERFLOW = 259,        /* INTEGER_OVERFLOW  */
    CHAR = 260,                    /* "character literal"  */
    STR = 261,                     /* "string"  */
    IDENTIFIER = 262,              /* "identifier"  */
    TYPENAME = 263,                /* "type name"  */
    UNIT = 264,                    /* "offset unit"  */
    OFFSET = 265,                  /* "offset"  */
    ENUM = 266,                    /* "keyword `enum'"  */
    PINNED = 267,                  /* "keyword `pinned'"  */
    STRUCT = 268,                  /* "keyword `struct'"  */
    token = 269,                   /* token  */
    UNION = 270,                   /* "keyword `union'"  */
    CONST = 271,                   /* "keyword `const'"  */
    CONTINUE = 272,                /* "keyword `continue'"  */
    ELSE = 273,                    /* "keyword `else'"  */
    IF = 274,                      /* "keyword `if'"  */
    WHILE = 275,                   /* "keyword `while"  */
    UNTIL = 276,                   /* "keyword `until'"  */
    FOR = 277,                     /* "keyword `for'"  */
    IN = 278,                      /* "keyword `in'"  */
    WHERE = 279,                   /* "keyword `where'"  */
    SIZEOF = 280,                  /* "keyword `sizeof'"  */
    TYPEOF = 281,                  /* "keyword `typeof'"  */
    ASSERT = 282,                  /* "keyword `assert'"  */
    ERR = 283,                     /* "token"  */
    ALIEN = 284,                   /* ALIEN  */
    INTCONSTR = 285,               /* "int type constructor"  */
    UINTCONSTR = 286,              /* "uint type constructor"  */
    OFFSETCONSTR = 287,            /* "offset type constructor"  */
    DEFUN = 288,                   /* "keyword `fun'"  */
    DEFSET = 289,                  /* "keyword `defset'"  */
    DEFTYPE = 290,                 /* "keyword `type'"  */
    DEFVAR = 291,                  /* "keyword `var'"  */
    DEFUNIT = 292,                 /* "keyword `unit'"  */
    METHOD = 293,                  /* "keyword `method'"  */
    RETURN = 294,                  /* "keyword `return'"  */
    BREAK = 295,                   /* "keyword `break'"  */
    STRING = 296,                  /* "string type specifier"  */
    TRY = 297,                     /* "keyword `try'"  */
    CATCH = 298,                   /* "keyword `catch'"  */
    RAISE = 299,                   /* "keyword `raise'"  */
    VOID = 300,                    /* "void type specifier"  */
    ANY = 301,                     /* "any type specifier"  */
    PRINT = 302,                   /* "keyword `print'"  */
    PRINTF = 303,                  /* "keyword `printf'"  */
    LOAD = 304,                    /* "keyword `load'"  */
    LAMBDA = 305,                  /* "keyword `lambda'"  */
    FORMAT = 306,                  /* "keyword `format'"  */
    BUILTIN_RAND = 307,            /* BUILTIN_RAND  */
    BUILTIN_GET_ENDIAN = 308,      /* BUILTIN_GET_ENDIAN  */
    BUILTIN_SET_ENDIAN = 309,      /* BUILTIN_SET_ENDIAN  */
    BUILTIN_GET_IOS = 310,         /* BUILTIN_GET_IOS  */
    BUILTIN_SET_IOS = 311,         /* BUILTIN_SET_IOS  */
    BUILTIN_OPEN = 312,            /* BUILTIN_OPEN  */
    BUILTIN_CLOSE = 313,           /* BUILTIN_CLOSE  */
    BUILTIN_IOSIZE = 314,          /* BUILTIN_IOSIZE  */
    BUILTIN_IOFLAGS = 315,         /* BUILTIN_IOFLAGS  */
    BUILTIN_IOGETB = 316,          /* BUILTIN_IOGETB  */
    BUILTIN_IOSETB = 317,          /* BUILTIN_IOSETB  */
    BUILTIN_GETENV = 318,          /* BUILTIN_GETENV  */
    BUILTIN_FORGET = 319,          /* BUILTIN_FORGET  */
    BUILTIN_GET_TIME = 320,        /* BUILTIN_GET_TIME  */
    BUILTIN_STRACE = 321,          /* BUILTIN_STRACE  */
    BUILTIN_TERM_RGB_TO_COLOR = 322, /* BUILTIN_TERM_RGB_TO_COLOR  */
    BUILTIN_SLEEP = 323,           /* BUILTIN_SLEEP  */
    BUILTIN_TERM_GET_COLOR = 324,  /* BUILTIN_TERM_GET_COLOR  */
    BUILTIN_TERM_SET_COLOR = 325,  /* BUILTIN_TERM_SET_COLOR  */
    BUILTIN_TERM_GET_BGCOLOR = 326, /* BUILTIN_TERM_GET_BGCOLOR  */
    BUILTIN_TERM_SET_BGCOLOR = 327, /* BUILTIN_TERM_SET_BGCOLOR  */
    BUILTIN_TERM_BEGIN_CLASS = 328, /* BUILTIN_TERM_BEGIN_CLASS  */
    BUILTIN_TERM_END_CLASS = 329,  /* BUILTIN_TERM_END_CLASS  */
    BUILTIN_TERM_BEGIN_HYPERLINK = 330, /* BUILTIN_TERM_BEGIN_HYPERLINK  */
    BUILTIN_TERM_END_HYPERLINK = 331, /* BUILTIN_TERM_END_HYPERLINK  */
    BUILTIN_VM_OBASE = 332,        /* BUILTIN_VM_OBASE  */
    BUILTIN_VM_SET_OBASE = 333,    /* BUILTIN_VM_SET_OBASE  */
    BUILTIN_VM_OACUTOFF = 334,     /* BUILTIN_VM_OACUTOFF  */
    BUILTIN_VM_SET_OACUTOFF = 335, /* BUILTIN_VM_SET_OACUTOFF  */
    BUILTIN_VM_ODEPTH = 336,       /* BUILTIN_VM_ODEPTH  */
    BUILTIN_VM_SET_ODEPTH = 337,   /* BUILTIN_VM_SET_ODEPTH  */
    BUILTIN_VM_OINDENT = 338,      /* BUILTIN_VM_OINDENT  */
    BUILTIN_VM_SET_OINDENT = 339,  /* BUILTIN_VM_SET_OINDENT  */
    BUILTIN_VM_OMAPS = 340,        /* BUILTIN_VM_OMAPS  */
    BUILTIN_VM_SET_OMAPS = 341,    /* BUILTIN_VM_SET_OMAPS  */
    BUILTIN_VM_OMODE = 342,        /* BUILTIN_VM_OMODE  */
    BUILTIN_VM_SET_OMODE = 343,    /* BUILTIN_VM_SET_OMODE  */
    BUILTIN_VM_OPPRINT = 344,      /* BUILTIN_VM_OPPRINT  */
    BUILTIN_VM_SET_OPPRINT = 345,  /* BUILTIN_VM_SET_OPPRINT  */
    BUILTIN_UNSAFE_STRING_SET = 346, /* BUILTIN_UNSAFE_STRING_SET  */
    POWA = 347,                    /* "power-and-assign operator"  */
    MULA = 348,                    /* "multiply-and-assign operator"  */
    DIVA = 349,                    /* "divide-and-assing operator"  */
    MODA = 350,                    /* "modulus-and-assign operator"  */
    ADDA = 351,                    /* "add-and-assing operator"  */
    SUBA = 352,                    /* "subtract-and-assign operator"  */
    SLA = 353,                     /* "shift-left-and-assign operator"  */
    SRA = 354,                     /* "shift-right-and-assign operator"  */
    BANDA = 355,                   /* "bit-and-and-assign operator"  */
    XORA = 356,                    /* "bit-xor-and-assign operator"  */
    IORA = 357,                    /* "bit-or-and-assign operator"  */
    RANGEA = 358,                  /* "range separator"  */
    OR = 359,                      /* "logical or operator"  */
    AND = 360,                     /* "logical and operator"  */
    IMPL = 361,                    /* "logical implication operator"  */
    EQ = 362,                      /* "equality operator"  */
    NE = 363,                      /* "inequality operator"  */
    LE = 364,                      /* "less-or-equal operator"  */
    GE = 365,                      /* "bigger-or-equal-than operator"  */
    SL = 366,                      /* "left shift operator"  */
    SR = 367,                      /* "right shift operator"  */
    CEILDIV = 368,                 /* "ceiling division operator"  */
    POW = 369,                     /* "power operator"  */
    BCONC = 370,                   /* "bit-concatenation operator"  */
    NSMAP = 371,                   /* "non-strict map operator"  */
    INC = 372,                     /* "increment operator"  */
    DEC = 373,                     /* "decrement operator"  */
    AS = 374,                      /* "cast operator"  */
    ISA = 375,                     /* "type identification operator"  */
    ATTR = 376,                    /* "attribute"  */
    UNMAP = 377,                   /* "unmap operator"  */
    EXCOND = 378,                  /* "conditional on exception operator"  */
    BIG = 379,                     /* "keyword `big'"  */
    LITTLE = 380,                  /* "keyword `little'"  */
    SIGNED = 381,                  /* "keyword `signed'"  */
    UNSIGNED = 382,                /* "keyword `unsigned'"  */
    THREEDOTS = 383,               /* "varargs indicator"  */
    THEN = 384,                    /* THEN  */
    UNARY = 385,                   /* UNARY  */
    HYPERUNARY = 386,              /* HYPERUNARY  */
    START_EXP = 387,               /* START_EXP  */
    START_DECL = 388,              /* START_DECL  */
    START_STMT = 389,              /* START_STMT  */
    START_PROGRAM = 390            /* START_PROGRAM  */
  };
  typedef enum pkl_tab_tokentype pkl_tab_token_kind_t;
#endif

/* Value type.  */
#if ! defined PKL_TAB_STYPE && ! defined PKL_TAB_STYPE_IS_DECLARED
union PKL_TAB_STYPE
{
#line 341 "pkl-tab.y"

  pkl_ast_node ast;
  struct
  {
    pkl_ast_node constraint;
    pkl_ast_node initializer;
    int impl_constraint_p;
  } field_const_init;
  enum pkl_ast_op opcode;
  int integer;

#line 219 "pkl-tab.h"

};
typedef union PKL_TAB_STYPE PKL_TAB_STYPE;
# define PKL_TAB_STYPE_IS_TRIVIAL 1
# define PKL_TAB_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined PKL_TAB_LTYPE && ! defined PKL_TAB_LTYPE_IS_DECLARED
typedef struct PKL_TAB_LTYPE PKL_TAB_LTYPE;
struct PKL_TAB_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define PKL_TAB_LTYPE_IS_DECLARED 1
# define PKL_TAB_LTYPE_IS_TRIVIAL 1
#endif



int pkl_tab_parse (struct pkl_parser *pkl_parser);

#endif /* !YY_PKL_TAB_PKL_TAB_TAB_H_INCLUDED  */
