/* This machine-generated file includes source code from GNU Jitter.

   Copyright (C) 2016-2021 Luca Saiu
   Written by Luca Saiu

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>.

This generated code also is also derived from a user VM specification.

*/

/* User-specified code, initial vm1 part: beginning. */
#line 212 "../../libpoke/pvm.jitter"
#line 212 "../../libpoke/pvm.jitter"

#   include <config.h>
  
/* User-specified code, initial vm1 part: end */

/* VM library: main VM C file template.

   Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021 Luca Saiu
   Written by Luca Saiu

   This file is part of GNU Jitter.

   GNU Jitter is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNU Jitter is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Jitter.  If not, see <http://www.gnu.org/licenses/>. */


/* Generated file warning.
 * ************************************************************************** */

/* Unless this file is named exactly "vm1.c" , without any prefix, you are
   looking at a machine-generated derived file.  The original source is the vm.c
   template from Jitter, with added code implementing the pvm VM. */




/* When we are using Gnulib the standard header files included below will in
   fact be Gnulib replacements; make sure that the Gnulib macros are
   recognised.
   It is in fact possible that  HAVE_CONFIG_H  is defined even in other
   contexts; it should be harmless to include config.h anyway. */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif // #ifdef HAVE_CONFIG_H

#include <assert.h>
#include <string.h>

#include <jitter/jitter.h>

#if defined (PVM_PROFILE_SAMPLE)
#include <sys/time.h>
#endif // #if defined (PVM_PROFILE_SAMPLE)

#include <jitter/jitter-hash.h>
#include <jitter/jitter-instruction.h>
#include <jitter/jitter-mmap.h>
#include <jitter/jitter-mutable-routine.h>
#include <jitter/jitter-print.h>
#include <jitter/jitter-rewrite.h>
#include <jitter/jitter-routine.h>
#include <jitter/jitter-routine-parser.h>
#include <jitter/jitter-specialize.h>
#include <jitter/jitter-defect.h>
#include <jitter/jitter-patch-in.h>

/* I don't need to include <jitter/jitter-executor.h> here, nor to define
   JITTER_THIS_CAN_INCLUDE_JITTER_EXECUTOR_H ; doing so carelessly might
   lead to subtle bugs, that it is better to prevent.
   Of course I can reconsider this decision in the future. */

#include <jitter/jitter-data-locations.h>

#include "pvm-vm.h"
//#include "pvm-specialized-instructions.h"
//#include "pvm-meta-instructions.h"
#include <jitter/jitter-fatal.h>




/* Check requirements for particular features.
 * ************************************************************************** */

/* VM sample-profiling is only supported with GCC.  Do not bother activating it
   with other compilers, when the numbers would turn out to be unreliable in the
   end. */
#if  defined (PVM_PROFILE_SAMPLE)        \
     && ! defined (JITTER_HAVE_ACTUAL_GCC)
# error "Sample-profiling is only supported with GCC: it requires (machine-independent)"
# error "GNU C extended asm.  It is not worth supporting other compilers if"
# error "the numbers turn out to be unreliable in the end."
#endif

/* Warn about the unreliability of sample-profiling with simple dispatches
   unless one of the complex dispatches is in use. */
#if  defined (PVM_PROFILE_SAMPLE)                 \
     && ! defined (JITTER_DISPATCH_MINIMAL_THREADING)  \
     && ! defined (JITTER_DISPATCH_NO_THREADING)
# warning "Sample-profiling is unreliable with simple dispatches: the sample"
# warning "incrementation code can be executed at any point in the VM"
# warning "instruction, not necessarily at the same point (the beginning) for"
# warning "every VM instruction."
#endif




/* Machine-generated data structures.
 * ************************************************************************** */

/* Machine-generated data structures defining this VM.  Initializing a static
   struct is problematic, as it requires constant expressions for each field --
   and const pointers don't qualify.  This is why we initialize the struct
   fields below in pvm_initialize. */
static struct jitter_vm
the_pvm_vm;

struct jitter_vm * const
pvm_vm = & the_pvm_vm;

struct jitter_list_header * const
pvm_states = & the_pvm_vm.states;

/* It is convenient to have this initialised at start up, even before calling
   any initialisation function.  This makes it reliable to read this when, for
   example, handling --version . */
static const struct jitter_vm_configuration
pvm_vm_the_configuration
  = {
      PVM_LOWER_CASE_PREFIX /* lower_case_prefix */,
      PVM_UPPER_CASE_PREFIX /* upper_case_prefix */,
      PVM_HASH_PREFIX /* hash_prefix */,
      PVM_MAX_FAST_REGISTER_NO_PER_CLASS
        /* max_fast_register_no_per_class */,
      PVM_MAX_NONRESIDUAL_LITERAL_NO /* max_nonresidual_literal_no */,
      PVM_DISPATCH_HUMAN_READABLE /* dispatch_human_readable */,
      /* The instrumentation field can be seen as a bit map.  See the comment
         in jitter/jitter-vm.h . */
      (jitter_vm_instrumentation_none
#if defined (PVM_PROFILE_COUNT)
       | jitter_vm_instrumentation_count
#endif
#if defined (PVM_PROFILE_SAMPLE)
       | jitter_vm_instrumentation_sample
#endif
       ) /* instrumentation */
    };

const struct jitter_vm_configuration * const
pvm_vm_configuration
  = & pvm_vm_the_configuration;




/* Initialization and finalization: internal functions, not for the user.
 * ************************************************************************** */

/* Initialize threads.  This only needs to be called once at initialization, and
   the user doesn't need to bother with it.  Defined along with the executor. */
void
pvm_initialize_threads (void);

/* Check that the encodings in enum jitter_specialized_instruction_opcode (as
   used in the specializer) are coherent with machine-generated code.  Making a
   mistake here would introduce subtle bugs, so it's better to be defensive. */
static void
pvm_check_specialized_instruction_opcode_once (void)
{
  static bool already_checked = false;
  if (already_checked)
    return;

  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eINVALID)
          == jitter_specialized_instruction_opcode_INVALID);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eBEGINBASICBLOCK)
          == jitter_specialized_instruction_opcode_BEGINBASICBLOCK);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eEXITVM)
          == jitter_specialized_instruction_opcode_EXITVM);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eDATALOCATIONS)
          == jitter_specialized_instruction_opcode_DATALOCATIONS);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eNOP)
          == jitter_specialized_instruction_opcode_NOP);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eUNREACHABLE0)
          == jitter_specialized_instruction_opcode_UNREACHABLE0);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__eUNREACHABLE1)
          == jitter_specialized_instruction_opcode_UNREACHABLE1);
  assert (((enum jitter_specialized_instruction_opcode)
           pvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE)
          == jitter_specialized_instruction_opcode_PRETENDTOJUMPANYWHERE);

  already_checked = true;
}

/* A prototype for a machine-generated function not needing a public
   declaration, only called thru a pointer within struct jitter_vm . */
int
pvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins);

/* Initialize the pointed special-purpose data structure. */
static void
pvm_initialize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  d->pending_notifications = 0;
  jitter_initialize_pending_signal_notifications
     (& d->pending_signal_notifications);

  /* Initialise profiling fields. */
  jitter_profile_runtime_initialize (pvm_vm,
                                     (struct jitter_profile_runtime *)
                                     & d->profile_runtime);
}

/* Finalize the pointed special-purpose data structure. */
static void
pvm_finalize_special_purpose_data
   (volatile struct jitter_special_purpose_state_data *d)
{
  jitter_finalize_pending_signal_notifications
     (d->pending_signal_notifications);

  jitter_profile_runtime_finalize (pvm_vm,
                                   (struct jitter_profile_runtime *)
                                   & d->profile_runtime);
}




/* Check that we link with the correct Jitter library.
 * ************************************************************************** */

/* It is possible to make a mistake at link time, and link a VM compiled with
   some dispatch with the Jitter runtime for a different dispatch.  That
   would cause crashes that is better to prevent.  This is a way to detect such
   mistakes very early, by causing a link-time failure in case of mismatch. */
extern volatile const bool
JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME;




/* Low-level debugging features relying on assembly: data locations.
 * ************************************************************************** */

#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT) && ! defined (JITTER_DISPATCH_SWITCH)
/* A declaration for data locations, as visible from C.  The global is defined in
   assembly in its own separate section thru the machinery in
   jitter/jitter-sections.h . */
extern const char
JITTER_DATA_LOCATION_NAME(pvm) [];
#endif // #if ...

void
pvm_dump_data_locations (jitter_print_context output)
{
#ifndef JITTER_DISPATCH_SWITCH
  jitter_dump_data_locations (output, & the_pvm_vm);
#else
  jitter_print_char_star (output,
                          "VM data location information unavailable\n");
#endif // #ifndef JITTER_DISPATCH_SWITCH
}




/* Initialization and finalization.
 * ************************************************************************** */

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
JITTER_DEFECT_DESCRIPTOR_DECLARATIONS_(pvm)

/* This global is defined from C: there is no particular need of doing it in
   assembly.  It is initialised in pvm_execute_or_initialize , where C
   labels are visible. */ // FIXME: unless it turns out to be simpler in assembly ...
jitter_int
pvm_defect_descriptors_correct_displacement;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
JITTER_PATCH_IN_DESCRIPTOR_DECLARATIONS_(pvm)
#endif // #if defined (JITTER_HAVE_PATCH_IN)

#ifndef JITTER_DISPATCH_SWITCH
/* True iff thread sizes are all non-negative and non-huge.  We refuse to
   disassemble otherwise, and when replication is enabled we refuse to run
   altogether.  See the comment right below. */
static bool
pvm_threads_validated = false;
#endif // #ifndef JITTER_DISPATCH_SWITCH

/* Omit pvm_validate_thread_sizes_once for switch-dispatching, as threads
   don't exist at all in that case.*/
#ifndef JITTER_DISPATCH_SWITCH
/* Check that VM instruction sizes are all non-negative, and that no thread
   starts before the end of the previous one.  Even one violation of such
   conditions is a symptom that the code has not been compiled with
   -fno-reorder-blocks , which would have disastrous effects with replication.
   It's better to validate threads at startup and fail immediately than to crash
   at run time.

   If even one thread appears to be wrong then refuse to disassemble when
   replication is disabled, and refuse to run altogether if replication is
   enabled. */
static void
pvm_validate_threads_once (void)
{
  /* Return if this is not the first time we got here. */
  static bool already_validated = false;
  if (already_validated)
    return;

#ifdef JITTER_REPLICATE
# define JITTER_FAIL(error_text)                                             \
    do                                                                       \
      {                                                                      \
        fprintf (stderr,                                                     \
                 "About specialized instruction %i (%s) at %p, size %liB\n", \
                 i, pvm_specialized_instruction_names [i],              \
                 pvm_threads [i],                                       \
                 pvm_thread_sizes [i]);                                 \
        jitter_fatal ("%s: you are not compiling with -fno-reorder-blocks",  \
                      error_text);                                           \
      }                                                                      \
    while (false)
#else
# define JITTER_FAIL(ignored_error_text)  \
    do                                    \
      {                                   \
        everything_valid = false;         \
        goto out;                         \
      }                                   \
    while (false)
#endif // #ifdef JITTER_REPLICATE

  /* The minimum address the next instruction code has to start at.

     This relies on NULL being zero, or in general lower in magnitude than any
     valid pointer.  It is not worth the trouble to be pedantic, as this will be
     true on every architecture where I can afford low-level tricks. */
  jitter_thread lower_bound = NULL;

  /* Check every thread.  We rely on the order here, following specialized
     instruction opcodes. */
  int i;
  bool everything_valid = true;
  for (i = 0; i < PVM_SPECIALIZED_INSTRUCTION_NO; i ++)
    {
      jitter_thread thread = pvm_threads [i];
      long size = pvm_thread_sizes [i];

      /* Check that the current thread has non-negative non-huge size and
         doesn't start before the end of the previous one.  If this is true for
         all threads we can conclude that they are non-overlapping as well. */
      if (__builtin_expect (size < 0, false))
        JITTER_FAIL("a specialized instruction has negative code size");
      if (__builtin_expect (size > (1 << 24), false))
        JITTER_FAIL("a specialized instruction has huge code size");
      if (__builtin_expect (lower_bound > thread, false))
        JITTER_FAIL("non-sequential thread");

      /* The next thread cannot start before the end of the current one. */
      lower_bound = ((char*) thread) + size;
    }

#undef JITTER_FAIL

#ifndef JITTER_REPLICATE
 out:
#endif // #ifndef JITTER_REPLICATE

  /* If we have validated every thread size then disassembling appears safe. */
  if (everything_valid)
    pvm_threads_validated = true;

  /* We have checked the thread sizes, once and for all.  If this function gets
     called again, thru a second pvm initialization, it will immediately
     return. */
  already_validated = true;
}
#endif // #ifndef JITTER_DISPATCH_SWITCH

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
/* The actual replacement table.  We only need it when defect replacement is in
   use. */
jitter_uint
pvm_replacement_table [PVM_SPECIALIZED_INSTRUCTION_NO];

/* The defective-instruction array for this VM.  The first
   defective_specialized_instruction_no elements of the array contain the
   specialized_instruction_ids of defective instructions; the remaining elements
   are set to -1.  This is initialised by jitter_fill_replacement_table . */
jitter_int
pvm_defective_specialized_instructions [PVM_SPECIALIZED_INSTRUCTION_NO];
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

void
pvm_initialize (void)
{
  /* Check that the Jitter library we linked is the right one.  This check
     actually only useful to force the global to be used.  I prefer not to use
     an assert, because assertions can be disabled. */
  if (! JITTER_DISPATCH_DEPENDENT_GLOBAL_NAME)
    jitter_fatal ("impossible to reach: the thing should fail at link time");

#ifdef JITTER_REPLICATE
  /* Initialize the executable-memory subsystem. */
  jitter_initialize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Initialise the print-context machinery. */
  jitter_print_initialize ();

  /* Perform some sanity checks which only need to be run once. */
  pvm_check_specialized_instruction_opcode_once ();

  /* We have to initialize threads before pvm_threads , since the struct
     needs threads. */
  pvm_initialize_threads ();

#ifndef JITTER_DISPATCH_SWITCH
  /* Validate threads, to make sure the generated code was not compiled with
     incorrect options.  This only needs to be done once. */
  pvm_validate_threads_once ();
#endif // ifndef JITTER_DISPATCH_SWITCH

  /* Initialize the object pointed by pvm_vm (see the comment above as to
     why we do it here).  Before actually setting the fields to valid data, fill
     the whole struct with a -- hopefully -- invalid pattern, just to catch
     bugs. */
  static bool vm_struct_initialized = false;
  if (! vm_struct_initialized)
    {
      memset (& the_pvm_vm, 0xff, sizeof (struct jitter_vm));

      /* Make the configuration struct reachable from the VM struct. */
      the_pvm_vm.configuration = pvm_vm_configuration;
      //pvm_print_vm_configuration (stdout, & the_pvm_vm.configuration);

      /* Initialize meta-instruction pointers for implicit instructions.
         VM-independent program specialization relies on those, so they have to
         be accessible to the Jitter library, out of generated code.  Since
         meta-instructions are sorted alphabetically in the array, the index
         is not fixed. */
      the_pvm_vm.exitvm_meta_instruction
        = (pvm_meta_instructions + pvm_meta_instruction_id_exitvm);
      the_pvm_vm.unreachable_meta_instruction
        = (pvm_meta_instructions
           + pvm_meta_instruction_id_unreachable);

      /* Threads or pointers to native code blocks of course don't exist with
         switch-dispatching. */
#ifndef JITTER_DISPATCH_SWITCH
      the_pvm_vm.threads = (jitter_thread *)pvm_threads;
      the_pvm_vm.thread_sizes = (long *) pvm_thread_sizes;
      the_pvm_vm.threads_validated = pvm_threads_validated;
#if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
      the_pvm_vm.data_locations = JITTER_DATA_LOCATION_NAME(pvm);
#else
      the_pvm_vm.data_locations = NULL;
#endif // #if defined (JITTER_HAVE_KNOWN_BINARY_FORMAT)
#endif // #ifndef JITTER_DISPATCH_SWITCH

      the_pvm_vm.specialized_instruction_residual_arities
        = pvm_specialized_instruction_residual_arities;
      the_pvm_vm.specialized_instruction_label_bitmasks
        = pvm_specialized_instruction_label_bitmasks;

      /* FIXME: I might want to conditionalize this. */
      the_pvm_vm.specialized_instruction_relocatables
        = pvm_specialized_instruction_relocatables;

      the_pvm_vm.specialized_instruction_callers
        = pvm_specialized_instruction_callers;
      the_pvm_vm.specialized_instruction_callees
        = pvm_specialized_instruction_callees;

      the_pvm_vm.specialized_instruction_names
        = pvm_specialized_instruction_names;
      the_pvm_vm.specialized_instruction_no
        = PVM_SPECIALIZED_INSTRUCTION_NO;

      the_pvm_vm.meta_instruction_string_hash
        = & pvm_meta_instruction_hash;
      the_pvm_vm.meta_instructions
        = (struct jitter_meta_instruction *) pvm_meta_instructions;
      the_pvm_vm.meta_instruction_no = PVM_META_INSTRUCTION_NO;
      the_pvm_vm.max_meta_instruction_name_length
        = PVM_MAX_META_INSTRUCTION_NAME_LENGTH;
      the_pvm_vm.specialized_instruction_to_unspecialized_instruction
        = pvm_specialized_instruction_to_unspecialized_instruction;
      the_pvm_vm.register_class_character_to_register_class
        = pvm_register_class_character_to_register_class;
      the_pvm_vm.specialize_instruction = pvm_specialize_instruction;
      the_pvm_vm.rewrite = pvm_rewrite;

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
      /* Fill the replacement table.  Since the array in question is a global
         with a fixed size, this needs to be done only once. */
      jitter_fill_replacement_table
         (pvm_replacement_table,
          pvm_defective_specialized_instructions,
          & the_pvm_vm,
          pvm_worst_case_replacement_table,
          pvm_call_related_specialized_instruction_ids,
          pvm_call_related_specialized_instruction_id_no,
          pvm_specialized_instruction_call_relateds,
          JITTER_DEFECT_DESCRIPTORS_NAME (pvm),
          (JITTER_DEFECT_DESCRIPTORS_SIZE_IN_BYTES_NAME (pvm)
           / sizeof (struct jitter_defect_descriptor)),
          JITTER_DEFECT_CORRECT_DISPLACEMENT_NAME (pvm));
      the_pvm_vm.replacement_table = pvm_replacement_table;
      the_pvm_vm.defective_specialized_instructions
        = pvm_defective_specialized_instructions;
      the_pvm_vm.specialized_instruction_call_relateds
        = pvm_specialized_instruction_call_relateds;
#else /* no defect replacement */
      /* In this configuration it is impossible to have defects: set every
         defect count to zero. */
      the_pvm_vm.defect_no = 0;
      the_pvm_vm.defective_specialized_instruction_no = 0;
      the_pvm_vm.defective_call_related_specialized_instruction_no = 0;
      the_pvm_vm.replacement_specialized_instruction_no = 0;
      the_pvm_vm.replacement_table = NULL;
      the_pvm_vm.defective_specialized_instructions = NULL;
      the_pvm_vm.specialized_instruction_call_relateds = NULL;
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)

      /* Initialize the empty list of states. */
      JITTER_LIST_INITIALIZE_HEADER (& the_pvm_vm.states);

      vm_struct_initialized = true;
    }

#ifdef JITTER_HAVE_PATCH_IN
    /* Since the patch-in table is destroyed at finalisation time we have to
       rebuild it at every initialisation, out of the previous conditional. */
    the_pvm_vm.specialized_instruction_fast_label_bitmasks
      = pvm_specialized_instruction_fast_label_bitmasks;
    the_pvm_vm.patch_in_descriptors =
      JITTER_PATCH_IN_DESCRIPTORS_NAME(pvm);
    const size_t patch_in_descriptor_size
      = sizeof (struct jitter_patch_in_descriptor);
    the_pvm_vm.patch_in_descriptor_no
      = (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(pvm)
         / patch_in_descriptor_size);
    /* Cheap sanity check: if the size in bytes is not a multiple of
       the element size, we are doing something very wrong. */
    if (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME(pvm)
        % patch_in_descriptor_size != 0)
      jitter_fatal ("patch-in descriptors total size %li not a multiple "
                    "of the element size %li",
                    (long) (JITTER_PATCH_IN_DESCRIPTORS_SIZE_IN_BYTES_NAME
                            (pvm)),
                    (long) patch_in_descriptor_size);
    /* Initialize the patch-in table for this VM. */
    the_pvm_vm.patch_in_table
      = jitter_make_patch_in_table (the_pvm_vm.patch_in_descriptors,
                                    the_pvm_vm.patch_in_descriptor_no,
                                    PVM_SPECIALIZED_INSTRUCTION_NO);
#else
    the_pvm_vm.specialized_instruction_fast_label_bitmasks = NULL;
#endif // #ifdef JITTER_HAVE_PATCH_IN

  jitter_initialize_meta_instructions (& pvm_meta_instruction_hash,
                                         pvm_meta_instructions,
                                         PVM_META_INSTRUCTION_NO);

#if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
# if 0
  jitter_dump_replacement_table (stderr, pvm_replacement_table,
                                 & the_pvm_vm);
  jitter_dump_defects (stderr, pvm_defective_specialized_instructions,
                       & the_pvm_vm,
                       pvm_specialized_instruction_call_relateds);
# endif
#endif // #if defined (JITTER_HAVE_DEFECT_REPLACEMENT)
#if defined (JITTER_HAVE_PATCH_IN)
  //printf ("======================= Patch-in descriptors: BEGIN\n");
  //JITTER_DUMP_PATCH_IN_DESCRIPTORS(pvm);
  //printf ("======================= Patch-in descriptors: END\n");
#endif // #if defined (JITTER_HAVE_PATCH_IN)
}

void
pvm_finalize (void)
{
  /* There's no need to touch the_pvm_vm ; we can keep it as it is, as it
     contains no dynamically-allocated fields. */
  /* Threads need no finalization. */
  jitter_finalize_meta_instructions (& pvm_meta_instruction_hash);

#ifdef JITTER_HAVE_PATCH_IN
  /* Destroy the patch-in table for this VM. */
  jitter_destroy_patch_in_table (the_pvm_vm.patch_in_table,
                                 PVM_SPECIALIZED_INSTRUCTION_NO);
#endif // #ifdef JITTER_HAVE_PATCH_IN

#ifdef JITTER_REPLICATE
  /* Finalize the executable-memory subsystem. */
  jitter_finalize_executable ();
#endif // #ifdef JITTER_REPLICATE

  /* Finalize the state list.  If it is not empty then something has gone
     wrong earlier. */
  if (the_pvm_vm.states.first != NULL
      || the_pvm_vm.states.last != NULL)
    jitter_fatal ("not every state structure was destroyed before PVM "
                  "finalisation.");
}




/* VM-dependant mutable routine initialization.
 * ************************************************************************** */

struct jitter_mutable_routine*
pvm_make_mutable_routine (void)
{
  return jitter_make_mutable_routine (pvm_vm);
}




/* Sample profiling: internal API.
 * ************************************************************************** */

#if defined (PVM_PROFILE_SAMPLE)

/* Sample profiling depends on some system features: fail immediately if they
   are not available */
#if ! defined (JITTER_HAVE_SIGACTION) || ! defined (JITTER_HAVE_SETITIMER)
# jitter_fatal "sample-profiling depends on sigaction and setitimer"
#endif

static struct itimerval
pvm_timer_interval;

static struct itimerval
pvm_timer_disabled_interval;

/* The sampling data, currently global.  The current implementation does not
   play well with threads, but it can be changed later keeping the same user
   API. */
struct pvm_sample_profile_state
{
  /* The state currently sample-profiling.  Since such a state can be only one
     right now this field is useful for printing error messages in case the user
     sets up sample-profiling from two states at the same time by mistake.
     This field is also useful for temporarily suspending and then reenabling
     sampling, when The Array is being resized: if the signal handler sees that
     this field is NULL it will not touch the fields. */
  struct pvm_state *state_p;

  /* A pointer to the counts field within the sample_profile_runtime struct. */
  uint32_t *counts;

  /* A pointer to the current specialised instruction opcode within the
     sample_profile_runtime struct. */
  volatile jitter_int * specialized_opcode_p;

  /* A pointer to the field counting the number of samples, again within the
     sample_profile_runtime struct. */
  unsigned int *sample_no_p;
};

/* The (currently) one and only global state for sample-profiling. */
static struct pvm_sample_profile_state
pvm_sample_profile_state;

static void
pvm_sigprof_handler (int signal)
{
#if 0
  assert (pvm_sample_profile_state.state_p != NULL);
#endif

  jitter_int specialized_opcode
    = * pvm_sample_profile_state.specialized_opcode_p;
  if (__builtin_expect ((specialized_opcode >= 0
                         && (specialized_opcode
                             < PVM_SPECIALIZED_INSTRUCTION_NO)),
                        true))
    pvm_sample_profile_state.counts [specialized_opcode] ++;

  (* pvm_sample_profile_state.sample_no_p) ++;
}

void
pvm_profile_sample_initialize (void)
{
  /* Perform a sanity check over the sampling period. */
  if (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS <= 0 ||
      JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS >= 1000)
    jitter_fatal ("invalid JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS: %f",
                  (double) JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS);
  struct sigaction action;
  sigaction (SIGPROF, NULL, & action);
  action.sa_handler = pvm_sigprof_handler;
  sigaction (SIGPROF, & action, NULL);

  long microseconds
    = (long) (JITTER_PROFILE_SAMPLE_PERIOD_IN_MILLISECONDS * 1000);
  pvm_timer_interval.it_interval.tv_sec = 0;
  pvm_timer_interval.it_interval.tv_usec = microseconds;
  pvm_timer_interval.it_value = pvm_timer_interval.it_interval;

  pvm_sample_profile_state.state_p = NULL;
  pvm_timer_disabled_interval.it_interval.tv_sec = 0;
  pvm_timer_disabled_interval.it_interval.tv_usec = 0;
  pvm_timer_disabled_interval.it_value
    = pvm_timer_disabled_interval.it_interval;
}

void
pvm_profile_sample_start (struct pvm_state *state_p)
{
  struct jitter_sample_profile_runtime *spr
    = ((struct jitter_sample_profile_runtime *)
       & PVM_STATE_TO_SPECIAL_PURPOSE_STATE_DATA (state_p)
           ->profile_runtime.sample_profile_runtime);

  if (pvm_sample_profile_state.state_p != NULL)
    {
      if (state_p != pvm_sample_profile_state.state_p)
        jitter_fatal ("currently it is only possible to sample-profile from "
                      "one state at the time: trying to sample-profile from "
                      "the state %p when already sample-profiling from the "
                      "state %p",
                      state_p, pvm_sample_profile_state.state_p);
      else
        {
          /* This situation is a symptom of a bug, but does not need to lead
             to a fatal error. */
          printf ("WARNING: starting profile on the state %p when profiling "
                  "was already active in the same state.\n"
                  "Did you call longjmp from VM code?", state_p);
          fflush (stdout);
        }
    }
  pvm_sample_profile_state.state_p = state_p;
  pvm_sample_profile_state.sample_no_p = & spr->sample_no;
  pvm_sample_profile_state.counts = spr->counts;
  pvm_sample_profile_state.specialized_opcode_p
    = & spr->current_specialized_instruction_opcode;
  //fprintf (stderr, "SAMPLE START\n"); fflush (NULL);
  if (setitimer (ITIMER_PROF, & pvm_timer_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when establishing a timer");
}

void
pvm_profile_sample_stop (void)
{
  if (setitimer (ITIMER_PROF, & pvm_timer_disabled_interval, NULL) != 0)
    jitter_fatal ("setitimer failed when disabling a timer");

  pvm_sample_profile_state.state_p = NULL;

  /* The rest is just for defenisveness' sake. */
  * pvm_sample_profile_state.specialized_opcode_p = -1;
  pvm_sample_profile_state.sample_no_p = NULL;
  pvm_sample_profile_state.counts = NULL;
  pvm_sample_profile_state.specialized_opcode_p = NULL;
}
#endif // #if defined (PVM_PROFILE_SAMPLE)




/* Slow register initialisation.
 * ************************************************************************** */

/* Initialise slow registers (for register classes defining an initial value) in
   a given Array, starting from a given rank up to another given rank.  The
   argument old_slow_register_no_per_class indicates the number of already
   initialised ranks, which this functions does not touch;
   new_slow_register_no_per_class indicates the new number of ranks.  Every rank
   from old_slow_register_no_per_class + 1 to new_slow_register_no_per_class,
   both included, will be initialised. */
static void
pvm_initialize_slow_registers (char *initial_array_pointer,
                                    jitter_int old_slow_register_no_per_class,
                                    jitter_int new_slow_register_no_per_class)
{
  /* Compute the address of the first slow registers, which is the beginning
     of the first rank. */
  union pvm_any_register *first_slow_register
    = ((union pvm_any_register *)
       ((char *) initial_array_pointer
        + PVM_FIRST_SLOW_REGISTER_UNBIASED_OFFSET));

  /* Initialise every *new* rank, without touching the old ones. */
  jitter_int i;
  for (i = old_slow_register_no_per_class;
       i < new_slow_register_no_per_class;
       i ++)
    {
      /* A pointer to the i-th rank of slow registers.  Every register
         in the rank is new and in general (according to its class) may
         need initialisation. */
      union pvm_any_register *rank
        = first_slow_register + (i * PVM_REGISTER_CLASS_NO);
      PVM_INITIALIZE_SLOW_REGISTER_RANK (rank);
    }
#if 0
      fprintf (stderr, "initialised %li (up from %li) slow registers per class, array at %p\n",
               (long) new_slow_register_no_per_class,
               (long) old_slow_register_no_per_class,
               initial_array_pointer);
#endif
}




/* Array re-allocation.
 * ************************************************************************** */

char *
pvm_make_place_for_slow_registers (struct pvm_state *s,
                                        jitter_int new_slow_register_no_per_class)
{
  if (new_slow_register_no_per_class < 0)
    jitter_fatal ("pvm_make_place_for_slow_registers: negative slow "
                  "register number");
  jitter_int old_slow_register_no_per_class
    = s->pvm_state_backing.jitter_slow_register_no_per_class;
  /* Change nothing if we already have enough space for the required number of
     slow registers.  The no-change case will be the most common one, and
     this function might be worth optimizing. */
  if (__builtin_expect (new_slow_register_no_per_class
                        > old_slow_register_no_per_class,
                        false))
    {
#if defined (PVM_PROFILE_SAMPLE)
      /* If sample-profiling is currently in progress on this state suspend it
         temporarily. */
      bool suspending_sample_profiling
        = (pvm_sample_profile_state.state_p == s);
      if (suspending_sample_profiling)
        pvm_profile_sample_stop ();
#endif // #if defined (PVM_PROFILE_SAMPLE)

#if 0
      printf ("Increasing slow register-no (per class) from %li to %li\n", (long) old_slow_register_no_per_class, (long)new_slow_register_no_per_class);
      printf ("Array size %li -> %li\n", (long) PVM_ARRAY_SIZE(old_slow_register_no_per_class), (long) PVM_ARRAY_SIZE(new_slow_register_no_per_class));
#endif
      /* Save the new value for new_slow_register_no_per_class in the state
         structure; reallocate The Array. */
      s->pvm_state_backing.jitter_slow_register_no_per_class
        = new_slow_register_no_per_class;
      s->pvm_state_backing.jitter_array
        = jitter_xrealloc ((void *) s->pvm_state_backing.jitter_array,
                           PVM_ARRAY_SIZE(new_slow_register_no_per_class));

     /* Initialise the slow registers we have just added, for every class. */
     pvm_initialize_slow_registers (s->pvm_state_backing.jitter_array,
                                         old_slow_register_no_per_class,
                                         new_slow_register_no_per_class);

#if defined (PVM_PROFILE_SAMPLE)
      /* Now we can resume sample-profiling on this state if we suspended it. */
      if (suspending_sample_profiling)
        pvm_profile_sample_start (s);
#endif // #if defined (PVM_PROFILE_SAMPLE)
#if 0
      fprintf (stderr, "slow registers are now %li per class, Array at %p (biased %p)\n",
               ((long)
                s->pvm_state_backing.jitter_slow_register_no_per_class),
               s->pvm_state_backing.jitter_array,
               s->pvm_state_backing.jitter_array + JITTER_ARRAY_BIAS);
#endif
    }

  /* Return the new (or unchanged) base, by simply adding the bias to the
     Array as it is now. */
  return s->pvm_state_backing.jitter_array + JITTER_ARRAY_BIAS;
}

void
pvm_ensure_enough_slow_registers_for_executable_routine
   (const struct jitter_executable_routine *er, struct pvm_state *s)
{
  pvm_make_place_for_slow_registers (s, er->slow_register_per_class_no);
}




/* Program text frontend.
 * ************************************************************************** */

void
pvm_parse_mutable_routine_from_file_star (FILE *input_file,
                                               struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file_star (input_file, p, pvm_vm);
}

void
pvm_parse_mutable_routine_from_file (const char *input_file_name,
                                          struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_file (input_file_name, p, pvm_vm);
}

void
pvm_parse_mutable_routine_from_string (const char *string,
                                            struct jitter_mutable_routine *p)
{
  jitter_parse_mutable_routine_from_string (string, p, pvm_vm);
}




/* State making and destroying.
 * ************************************************************************** */

/* State initialisation (with a given number of slow registers), reset and
   finalisation are machine-generated. */

void
pvm_state_initialize (struct pvm_state *sp)
{
  pvm_state_initialize_with_slow_registers (sp, 0);
}

struct pvm_state *
pvm_state_make_with_slow_registers (jitter_uint slow_register_no_per_class)
{
  struct pvm_state *res = jitter_xmalloc (sizeof (struct pvm_state));
  pvm_state_initialize_with_slow_registers (res,
                                                 slow_register_no_per_class);
  return res;
}

struct pvm_state *
pvm_state_make (void)
{
  return pvm_state_make_with_slow_registers (0);
}

void
pvm_state_destroy (struct pvm_state *state)
{
  pvm_state_finalize (state);
  free (state);
}




/* Executing code: unified routine API.
 * ************************************************************************** */

void
pvm_ensure_enough_slow_registers_for_routine
   (jitter_routine r, struct pvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  pvm_ensure_enough_slow_registers_for_executable_routine (e, s);
}

enum pvm_exit_status
pvm_execute_routine (jitter_routine r,
                          struct pvm_state *s)
{
  struct jitter_executable_routine *e
    = jitter_routine_make_executable_if_needed (r);
  return pvm_execute_executable_routine (e, s);
}





/* Defects and replacements: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-defect.h, hiding the VM pointer. */

void
pvm_defect_print_summary (jitter_print_context cx)
{
  jitter_defect_print_summary (cx, pvm_vm);
}

void
pvm_defect_print (jitter_print_context cx,
                       unsigned indentation_column_no)
{
  jitter_defect_print (cx, pvm_vm, indentation_column_no);
}

void
pvm_defect_print_replacement_table (jitter_print_context cx,
                                         unsigned indentation_column_no)
{
  jitter_defect_print_replacement_table (cx, pvm_vm, indentation_column_no);
}




/* Profiling: user API.
 * ************************************************************************** */

/* These functions are all trivial wrappers around the functionality declared
   in jitter/jitter-profile.h, hiding the VM pointer. */

struct pvm_profile_runtime *
pvm_state_profile_runtime (struct pvm_state *s)
{
  volatile struct jitter_special_purpose_state_data *spd
    = PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA
        (s->pvm_state_backing.jitter_array);
  return (struct pvm_profile_runtime *) & spd->profile_runtime;
}

struct pvm_profile_runtime *
pvm_profile_runtime_make (void)
{
  return jitter_profile_runtime_make (pvm_vm);
}

void
pvm_profile_runtime_destroy (struct pvm_profile_runtime *p)
{
  jitter_profile_runtime_destroy (pvm_vm, p);
}

void
pvm_profile_runtime_clear (struct pvm_profile_runtime * p)
{
  jitter_profile_runtime_clear (pvm_vm, p);
}

void
pvm_profile_runtime_merge_from (struct pvm_profile_runtime *to,
                                     const struct pvm_profile_runtime *from)
{
  jitter_profile_runtime_merge_from (pvm_vm, to, from);
}

void
pvm_profile_runtime_merge_from_state (struct pvm_profile_runtime *to,
                                           const struct pvm_state *from_state)
{
  const struct pvm_profile_runtime* from
    = pvm_state_profile_runtime ((struct pvm_state *) from_state);
  jitter_profile_runtime_merge_from (pvm_vm, to, from);
}

void
pvm_profile_runtime_print_unspecialized
   (jitter_print_context ct,
    const struct pvm_profile_runtime *p)
{
  jitter_profile_runtime_print_unspecialized (ct, pvm_vm, p);
}

void
pvm_profile_runtime_print_specialized (jitter_print_context ct,
                                            const struct pvm_profile_runtime
                                            *p)
{
  jitter_profile_runtime_print_specialized (ct, pvm_vm, p);
}

struct pvm_profile *
pvm_profile_unspecialized_from_runtime
   (const struct pvm_profile_runtime *p)
{
  return jitter_profile_unspecialized_from_runtime (pvm_vm, p);
}

struct pvm_profile *
pvm_profile_specialized_from_runtime (const struct pvm_profile_runtime
                                           *p)
{
  return jitter_profile_specialized_from_runtime (pvm_vm, p);
}




/* Evrything following this point is machine-generated.
 * ************************************************************************** */

/* What follows could be conceptually split into several generated C files, but
   having too many of them would be inconvenient for the user to compile and
   link.  For this reason we currently generate just three files: one is this,
   which also contains the specializer, another is for the executor, and then a
   header -- a main module is optional.  The executor will be potentially very
   large, so it is best compiled separately.  The specializer might be large as
   well at this stage, even if its compilation is usually much less
   expensive. */
/* These two macros are convenient for making VM-specific identifiers
   using VM-independent macros from a public header, without polluting
   the global namespace. */
#define JITTER_VM_PREFIX_LOWER_CASE pvm
#define JITTER_VM_PREFIX_UPPER_CASE PVM

/* User-specified code, printer part: beginning. */
#line 958 "../../libpoke/pvm.jitter"
#line 958 "../../libpoke/pvm.jitter"

    static jitter_uint printer_hi;

    static void
    pvm_literal_printer_cast (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%" JITTER_PRIu, val);
      pk_term_flush ();
    }

    static void
    pvm_literal_printer (jitter_print_context out, jitter_uint val)
    {
      pvm_print_val_with_params (NULL /* not used since no
                                         pretty-print */,
                                 (pvm_val) val,
                                 1 /* depth */,
                                 PVM_PRINT_FLAT,
                                 16 /* base */,
                                 0 /* indent */,
                                 2 /* acutoff */,
                                 0 /* flags */,
                                 NULL /* exit_exception */);
      pk_term_flush ();
    }

    static void
    pvm_literal_printer_hi (jitter_print_context out, jitter_uint hi)
    {
      pk_printf ("%%hi(0x%" JITTER_PRIx ")", hi);
      pk_term_flush ();
      printer_hi = hi; /* This sucks */
    }

    static void
    pvm_literal_printer_lo (jitter_print_context out, jitter_uint lo)
    {
      pk_printf ("%%lo(0x%" JITTER_PRIx") (", lo);

      pvm_print_val_with_params (NULL /* not used since no
                                         pretty-print */,
                                 ((pvm_val) printer_hi << 32) | lo,
                                 1 /* depth */,
                                 PVM_PRINT_FLAT,
                                 16 /* base */,
                                 0 /* indent */,
                                 2 /* acutoff */,
                                 0 /* flags */,
                                 NULL /* exit_exception */);
      pk_puts (")");
      pk_term_flush ();
      printer_hi = 0;
    }

    static void
    popf_printer (jitter_print_context out, jitter_uint nframes)
    {
      pk_printf ("%" JITTER_PRIu, nframes);
      pk_term_flush ();
    }

    static void
    bits_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%" JITTER_PRIu, val);
      pk_term_flush ();
    }

    static void
    endian_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%s", val == IOS_ENDIAN_MSB ? "big" : "little");
      pk_term_flush ();
    }

    static void
    nenc_printer (jitter_print_context out, jitter_uint val)
    {
      pk_printf ("%s", val == IOS_NENC_1 ? "1c" : "2c");
      pk_term_flush ();
    }
  
/* User-specified code, printer part: end */

//#include <stdbool.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>

//#include "pvm-meta-instructions.h"

// FIXME: comment.
struct jitter_hash_table
pvm_meta_instruction_hash;


static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ba_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnziu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bnzlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bziu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_bzlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_formati_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_formatiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_formatl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_formatlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_itolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_iutolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_ltolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutoi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutoiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutol_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_lutolu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer_cast } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_note_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekdlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peeki_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekiu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peekl_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_peeklu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokediu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokedlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokei_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokeiu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokel_meta_instruction_parameter_types [3] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, nenc_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pokelu_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, endian_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, bits_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popf_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_popvar_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printiu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printl_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_printlu_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_push_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_push32_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushe_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_label, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushf_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushhi_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer_hi } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushlo_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum_or_literal_label, NULL, pvm_literal_printer_lo } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushtopvar_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_pushvar_meta_instruction_parameter_types [2] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer }, { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_restorer_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_revn_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, popf_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_saver_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_setr_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_register, & pvm_register_class_r, jitter_default_literal_parameter_printer } };

static const /*FIXME: use enum jitterc_instruction_argument_kind instead*/struct jitter_meta_instruction_parameter_type pvm_strace_meta_instruction_parameter_types [1] =
  { { jitter_meta_instruction_parameter_kind_literal_fixnum, NULL, jitter_default_literal_parameter_printer } };


const struct jitter_meta_instruction
pvm_meta_instructions [PVM_META_INSTRUCTION_NO]
  = {
      { 0, "addi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 1, "addiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 2, "addl", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 3, "addlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 4, "ains", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 5, "and", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 6, "aref", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 7, "arefo", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 8, "arem", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 9, "aset", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 10, "asettb", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 11, "atr", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 12, "ba", 1, true, false, false, true /* this ignores replacements */, pvm_ba_meta_instruction_parameter_types },
      { 13, "bandi", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 14, "bandiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 15, "bandl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 16, "bandlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 17, "beghl", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 18, "begsc", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 19, "bn", 1, true, false, false, true /* this ignores replacements */, pvm_bn_meta_instruction_parameter_types },
      { 20, "bnn", 1, true, false, false, true /* this ignores replacements */, pvm_bnn_meta_instruction_parameter_types },
      { 21, "bnoti", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 22, "bnotiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 23, "bnotl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 24, "bnotlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 25, "bnzi", 1, true, false, false, true /* this ignores replacements */, pvm_bnzi_meta_instruction_parameter_types },
      { 26, "bnziu", 1, true, false, false, true /* this ignores replacements */, pvm_bnziu_meta_instruction_parameter_types },
      { 27, "bnzl", 1, true, false, false, true /* this ignores replacements */, pvm_bnzl_meta_instruction_parameter_types },
      { 28, "bnzlu", 1, true, false, false, true /* this ignores replacements */, pvm_bnzlu_meta_instruction_parameter_types },
      { 29, "bori", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 30, "boriu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 31, "borl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 32, "borlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 33, "bsli", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 34, "bsliu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 35, "bsll", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 36, "bsllu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 37, "bsri", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 38, "bsriu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 39, "bsrl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 40, "bsrlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 41, "bxori", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 42, "bxoriu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 43, "bxorl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 44, "bxorlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 45, "bzi", 1, true, false, false, true /* this ignores replacements */, pvm_bzi_meta_instruction_parameter_types },
      { 46, "bziu", 1, true, false, false, true /* this ignores replacements */, pvm_bziu_meta_instruction_parameter_types },
      { 47, "bzl", 1, true, false, false, true /* this ignores replacements */, pvm_bzl_meta_instruction_parameter_types },
      { 48, "bzlu", 1, true, false, false, true /* this ignores replacements */, pvm_bzlu_meta_instruction_parameter_types },
      { 49, "call", 0, true, true, false, true /* this ignores replacements */, NULL },
      { 50, "canary", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 51, "close", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 52, "ctos", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 53, "disas", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 54, "divi", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 55, "diviu", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 56, "divl", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 57, "divlu", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 58, "drop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 59, "drop2", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 60, "drop3", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 61, "drop4", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 62, "duc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 63, "dup", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 64, "endhl", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 65, "endsc", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 66, "eqc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 67, "eqi", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 68, "eqiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 69, "eql", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 70, "eqlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 71, "eqs", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 72, "exit", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 73, "exitvm", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 74, "flush", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 75, "formati", 1, false, false, false, false /* this ignores replacements */, pvm_formati_meta_instruction_parameter_types },
      { 76, "formatiu", 1, false, false, false, false /* this ignores replacements */, pvm_formatiu_meta_instruction_parameter_types },
      { 77, "formatl", 1, false, false, false, false /* this ignores replacements */, pvm_formatl_meta_instruction_parameter_types },
      { 78, "formatlu", 1, false, false, false, false /* this ignores replacements */, pvm_formatlu_meta_instruction_parameter_types },
      { 79, "fromr", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 80, "gei", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 81, "geiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 82, "gel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 83, "gelu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 84, "ges", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 85, "getenv", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 86, "gti", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 87, "gtiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 88, "gtl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 89, "gtlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 90, "gts", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 91, "indent", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 92, "ioflags", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 93, "iogetb", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 94, "iosetb", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 95, "iosize", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 96, "isa", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 97, "itoi", 1, false, false, false, true /* this ignores replacements */, pvm_itoi_meta_instruction_parameter_types },
      { 98, "itoiu", 1, false, false, false, true /* this ignores replacements */, pvm_itoiu_meta_instruction_parameter_types },
      { 99, "itol", 1, false, false, false, true /* this ignores replacements */, pvm_itol_meta_instruction_parameter_types },
      { 100, "itolu", 1, false, false, false, true /* this ignores replacements */, pvm_itolu_meta_instruction_parameter_types },
      { 101, "iutoi", 1, false, false, false, true /* this ignores replacements */, pvm_iutoi_meta_instruction_parameter_types },
      { 102, "iutoiu", 1, false, false, false, true /* this ignores replacements */, pvm_iutoiu_meta_instruction_parameter_types },
      { 103, "iutol", 1, false, false, false, true /* this ignores replacements */, pvm_iutol_meta_instruction_parameter_types },
      { 104, "iutolu", 1, false, false, false, true /* this ignores replacements */, pvm_iutolu_meta_instruction_parameter_types },
      { 105, "lei", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 106, "leiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 107, "lel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 108, "lelu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 109, "les", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 110, "lti", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 111, "ltiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 112, "ltl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 113, "ltlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 114, "ltoi", 1, false, false, false, true /* this ignores replacements */, pvm_ltoi_meta_instruction_parameter_types },
      { 115, "ltoiu", 1, false, false, false, true /* this ignores replacements */, pvm_ltoiu_meta_instruction_parameter_types },
      { 116, "ltol", 1, false, false, false, true /* this ignores replacements */, pvm_ltol_meta_instruction_parameter_types },
      { 117, "ltolu", 1, false, false, false, true /* this ignores replacements */, pvm_ltolu_meta_instruction_parameter_types },
      { 118, "lts", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 119, "lutoi", 1, false, false, false, true /* this ignores replacements */, pvm_lutoi_meta_instruction_parameter_types },
      { 120, "lutoiu", 1, false, false, false, true /* this ignores replacements */, pvm_lutoiu_meta_instruction_parameter_types },
      { 121, "lutol", 1, false, false, false, true /* this ignores replacements */, pvm_lutol_meta_instruction_parameter_types },
      { 122, "lutolu", 1, false, false, false, true /* this ignores replacements */, pvm_lutolu_meta_instruction_parameter_types },
      { 123, "map", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 124, "mgetios", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 125, "mgetm", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 126, "mgeto", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 127, "mgets", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 128, "mgetsel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 129, "mgetsiz", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 130, "mgetw", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 131, "mka", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 132, "mko", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 133, "mksct", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 134, "mktya", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 135, "mktyany", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 136, "mktyc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 137, "mktyi", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 138, "mktyo", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 139, "mktys", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 140, "mktysct", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 141, "mktyv", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 142, "mm", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 143, "modi", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 144, "modiu", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 145, "modl", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 146, "modlu", 0, true, false, false, false /* this ignores replacements */, NULL },
      { 147, "msetios", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 148, "msetm", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 149, "mseto", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 150, "msets", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 151, "msetsel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 152, "msetsiz", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 153, "msetw", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 154, "muli", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 155, "muliu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 156, "mull", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 157, "mullu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 158, "muls", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 159, "nec", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 160, "negi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 161, "negiu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 162, "negl", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 163, "neglu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 164, "nei", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 165, "neiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 166, "nel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 167, "nelu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 168, "nes", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 169, "nip", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 170, "nip2", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 171, "nip3", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 172, "nn", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 173, "nnn", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 174, "nop", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 175, "not", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 176, "note", 1, false, false, false, true /* this ignores replacements */, pvm_note_meta_instruction_parameter_types },
      { 177, "nrot", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 178, "ogetbt", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 179, "ogetm", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 180, "ogetu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 181, "open", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 182, "or", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 183, "osetm", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 184, "over", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 185, "pec", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 186, "peekdi", 1, true, false, false, true /* this ignores replacements */, pvm_peekdi_meta_instruction_parameter_types },
      { 187, "peekdiu", 1, true, false, false, true /* this ignores replacements */, pvm_peekdiu_meta_instruction_parameter_types },
      { 188, "peekdl", 1, true, false, false, true /* this ignores replacements */, pvm_peekdl_meta_instruction_parameter_types },
      { 189, "peekdlu", 1, true, false, false, true /* this ignores replacements */, pvm_peekdlu_meta_instruction_parameter_types },
      { 190, "peeki", 3, true, false, false, true /* this ignores replacements */, pvm_peeki_meta_instruction_parameter_types },
      { 191, "peekiu", 2, true, false, false, true /* this ignores replacements */, pvm_peekiu_meta_instruction_parameter_types },
      { 192, "peekl", 3, true, false, false, true /* this ignores replacements */, pvm_peekl_meta_instruction_parameter_types },
      { 193, "peeklu", 2, true, false, false, true /* this ignores replacements */, pvm_peeklu_meta_instruction_parameter_types },
      { 194, "peeks", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 195, "pokedi", 1, true, false, false, true /* this ignores replacements */, pvm_pokedi_meta_instruction_parameter_types },
      { 196, "pokediu", 1, true, false, false, true /* this ignores replacements */, pvm_pokediu_meta_instruction_parameter_types },
      { 197, "pokedl", 1, true, false, false, true /* this ignores replacements */, pvm_pokedl_meta_instruction_parameter_types },
      { 198, "pokedlu", 1, true, false, false, true /* this ignores replacements */, pvm_pokedlu_meta_instruction_parameter_types },
      { 199, "pokei", 3, true, false, false, true /* this ignores replacements */, pvm_pokei_meta_instruction_parameter_types },
      { 200, "pokeiu", 2, true, false, false, true /* this ignores replacements */, pvm_pokeiu_meta_instruction_parameter_types },
      { 201, "pokel", 3, true, false, false, true /* this ignores replacements */, pvm_pokel_meta_instruction_parameter_types },
      { 202, "pokelu", 2, true, false, false, true /* this ignores replacements */, pvm_pokelu_meta_instruction_parameter_types },
      { 203, "pokes", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 204, "pope", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 205, "popend", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 206, "popexite", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 207, "popf", 1, false, false, false, true /* this ignores replacements */, pvm_popf_meta_instruction_parameter_types },
      { 208, "popios", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 209, "popoac", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 210, "popob", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 211, "popobc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 212, "popoc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 213, "popod", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 214, "popoi", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 215, "popom", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 216, "popoo", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 217, "popopp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 218, "popr", 1, false, false, false, true /* this ignores replacements */, pvm_popr_meta_instruction_parameter_types },
      { 219, "popvar", 2, false, false, false, true /* this ignores replacements */, pvm_popvar_meta_instruction_parameter_types },
      { 220, "powi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 221, "powiu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 222, "powl", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 223, "powlu", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 224, "printi", 1, false, false, false, false /* this ignores replacements */, pvm_printi_meta_instruction_parameter_types },
      { 225, "printiu", 1, false, false, false, false /* this ignores replacements */, pvm_printiu_meta_instruction_parameter_types },
      { 226, "printl", 1, false, false, false, false /* this ignores replacements */, pvm_printl_meta_instruction_parameter_types },
      { 227, "printlu", 1, false, false, false, false /* this ignores replacements */, pvm_printlu_meta_instruction_parameter_types },
      { 228, "prints", 0, false, false, false, false /* this ignores replacements */, NULL },
      { 229, "prolog", 0, true, false, true, true /* this ignores replacements */, NULL },
      { 230, "push", 1, false, false, false, true /* this ignores replacements */, pvm_push_meta_instruction_parameter_types },
      { 231, "push32", 1, false, false, false, true /* this ignores replacements */, pvm_push32_meta_instruction_parameter_types },
      { 232, "pushe", 1, false, false, false, true /* this ignores replacements */, pvm_pushe_meta_instruction_parameter_types },
      { 233, "pushend", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 234, "pushf", 1, false, false, false, true /* this ignores replacements */, pvm_pushf_meta_instruction_parameter_types },
      { 235, "pushhi", 1, false, false, false, true /* this ignores replacements */, pvm_pushhi_meta_instruction_parameter_types },
      { 236, "pushios", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 237, "pushlo", 1, false, false, false, true /* this ignores replacements */, pvm_pushlo_meta_instruction_parameter_types },
      { 238, "pushoac", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 239, "pushob", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 240, "pushobc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 241, "pushoc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 242, "pushod", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 243, "pushoi", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 244, "pushom", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 245, "pushoo", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 246, "pushopp", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 247, "pushr", 1, false, false, false, true /* this ignores replacements */, pvm_pushr_meta_instruction_parameter_types },
      { 248, "pushtopvar", 1, true, false, false, true /* this ignores replacements */, pvm_pushtopvar_meta_instruction_parameter_types },
      { 249, "pushvar", 2, false, false, false, true /* this ignores replacements */, pvm_pushvar_meta_instruction_parameter_types },
      { 250, "quake", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 251, "raise", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 252, "rand", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 253, "regvar", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 254, "reloc", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 255, "restorer", 1, false, false, false, true /* this ignores replacements */, pvm_restorer_meta_instruction_parameter_types },
      { 256, "return", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 257, "revn", 1, false, false, false, true /* this ignores replacements */, pvm_revn_meta_instruction_parameter_types },
      { 258, "rot", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 259, "saver", 1, false, false, false, true /* this ignores replacements */, pvm_saver_meta_instruction_parameter_types },
      { 260, "sconc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 261, "sel", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 262, "setr", 1, false, false, false, true /* this ignores replacements */, pvm_setr_meta_instruction_parameter_types },
      { 263, "siz", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 264, "sleep", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 265, "smodi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 266, "spropc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 267, "sproph", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 268, "sprops", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 269, "sref", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 270, "srefi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 271, "srefia", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 272, "srefio", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 273, "srefmnt", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 274, "srefnt", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 275, "srefo", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 276, "sset", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 277, "sseti", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 278, "strace", 1, false, false, false, false /* this ignores replacements */, pvm_strace_meta_instruction_parameter_types },
      { 279, "strref", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 280, "strset", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 281, "subi", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 282, "subiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 283, "subl", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 284, "sublu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 285, "substr", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 286, "swap", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 287, "swapgti", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 288, "swapgtiu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 289, "swapgtl", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 290, "swapgtlu", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 291, "sync", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 292, "time", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 293, "tor", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 294, "tuck", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 295, "tyagetb", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 296, "tyagett", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 297, "tyisc", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 298, "tyissct", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 299, "typof", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 300, "tysctn", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 301, "unmap", 0, false, false, false, true /* this ignores replacements */, NULL },
      { 302, "unreachable", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 303, "ureloc", 0, true, false, false, true /* this ignores replacements */, NULL },
      { 304, "write", 0, true, true, false, true /* this ignores replacements */, NULL }
    };

/* The register class descriptor for r registers. */
const struct jitter_register_class
pvm_register_class_r
  = {
      pvm_register_class_id_r,
      'r',
      "register_class_r",
      "REGISTER_CLASS_R",
      PVM_REGISTER_r_FAST_REGISTER_NO,
      1 /* Use slow registers */
    };


/* A pointer to every existing register class descriptor. */
const struct jitter_register_class * const
pvm_regiter_classes []
  = {
      & pvm_register_class_r
    };

const struct jitter_register_class *
pvm_register_class_character_to_register_class (char c)
{
  switch (c)
    {
    case 'r': return & pvm_register_class_r;
    default:  return NULL;
    }
}

//#include "pvm-specialized-instructions.h"

const char * const
pvm_specialized_instruction_names [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      "!INVALID",
      "!BEGINBASICBLOCK",
      "!EXITVM",
      "!DATALOCATIONS",
      "!NOP",
      "!UNREACHABLE0",
      "!UNREACHABLE1",
      "!PRETENDTOJUMPANYWHERE",
      "addi",
      "addiu",
      "addl",
      "addlu",
      "ains",
      "and",
      "aref",
      "arefo",
      "arem",
      "aset",
      "asettb",
      "atr",
      "ba/fR",
      "bandi",
      "bandiu",
      "bandl",
      "bandlu",
      "beghl/retR",
      "begsc/retR",
      "bn/fR",
      "bnn/fR",
      "bnoti",
      "bnotiu",
      "bnotl",
      "bnotlu",
      "bnzi/fR",
      "bnziu/fR",
      "bnzl/fR",
      "bnzlu/fR",
      "bori",
      "boriu",
      "borl",
      "borlu",
      "bsli",
      "bsliu",
      "bsll",
      "bsllu",
      "bsri",
      "bsriu",
      "bsrl",
      "bsrlu",
      "bxori",
      "bxoriu",
      "bxorl",
      "bxorlu",
      "bzi/fR",
      "bziu/fR",
      "bzl/fR",
      "bzlu/fR",
      "call/retR",
      "canary",
      "close",
      "ctos",
      "disas/retR",
      "divi/retR",
      "diviu/retR",
      "divl/retR",
      "divlu/retR",
      "drop",
      "drop2",
      "drop3",
      "drop4",
      "duc",
      "dup",
      "endhl/retR",
      "endsc/retR",
      "eqc",
      "eqi",
      "eqiu",
      "eql",
      "eqlu",
      "eqs",
      "exit",
      "exitvm",
      "flush",
      "formati/nR/retR",
      "formatiu/nR/retR",
      "formatl/nR/retR",
      "formatlu/nR/retR",
      "fromr",
      "gei",
      "geiu",
      "gel",
      "gelu",
      "ges",
      "getenv",
      "gti",
      "gtiu",
      "gtl",
      "gtlu",
      "gts",
      "indent/retR",
      "ioflags",
      "iogetb/retR",
      "iosetb",
      "iosize",
      "isa",
      "itoi/nR",
      "itoiu/nR",
      "itol/nR",
      "itolu/nR",
      "iutoi/nR",
      "iutoiu/nR",
      "iutol/nR",
      "iutolu/nR",
      "lei",
      "leiu",
      "lel",
      "lelu",
      "les",
      "lti",
      "ltiu",
      "ltl",
      "ltlu",
      "ltoi/nR",
      "ltoiu/nR",
      "ltol/nR",
      "ltolu/nR",
      "lts",
      "lutoi/nR",
      "lutoiu/nR",
      "lutol/nR",
      "lutolu/nR",
      "map",
      "mgetios",
      "mgetm",
      "mgeto",
      "mgets",
      "mgetsel",
      "mgetsiz",
      "mgetw",
      "mka",
      "mko",
      "mksct",
      "mktya",
      "mktyany",
      "mktyc",
      "mktyi",
      "mktyo",
      "mktys",
      "mktysct",
      "mktyv",
      "mm",
      "modi/retR",
      "modiu/retR",
      "modl/retR",
      "modlu/retR",
      "msetios",
      "msetm",
      "mseto",
      "msets",
      "msetsel",
      "msetsiz",
      "msetw",
      "muli",
      "muliu",
      "mull",
      "mullu",
      "muls",
      "nec",
      "negi",
      "negiu",
      "negl",
      "neglu",
      "nei",
      "neiu",
      "nel",
      "nelu",
      "nes",
      "nip",
      "nip2",
      "nip3",
      "nn",
      "nnn",
      "nop",
      "not",
      "note/nR",
      "nrot",
      "ogetbt",
      "ogetm",
      "ogetu",
      "open",
      "or",
      "osetm",
      "over",
      "pec",
      "peekdi/nR",
      "peekdiu/nR",
      "peekdl/nR",
      "peekdlu/nR",
      "peeki/nR/nR/nR",
      "peekiu/nR/nR",
      "peekl/nR/nR/nR",
      "peeklu/nR/nR",
      "peeks",
      "pokedi/nR",
      "pokediu/nR",
      "pokedl/nR",
      "pokedlu/nR",
      "pokei/nR/nR/nR",
      "pokeiu/nR/nR",
      "pokel/nR/nR/nR",
      "pokelu/nR/nR",
      "pokes",
      "pope",
      "popend",
      "popexite",
      "popf/nR",
      "popios",
      "popoac",
      "popob",
      "popobc",
      "popoc",
      "popod",
      "popoi",
      "popom",
      "popoo",
      "popopp",
      "popr/%rR",
      "popvar/nR/nR",
      "powi",
      "powiu",
      "powl",
      "powlu",
      "printi/nR/retR",
      "printiu/nR/retR",
      "printl/nR/retR",
      "printlu/nR/retR",
      "prints/retR",
      "prolog",
      "push/nR",
      "push/lR",
      "push32/nR",
      "push32/lR",
      "pushe/lR",
      "pushend",
      "pushf/nR",
      "pushhi/nR",
      "pushhi/lR",
      "pushios",
      "pushlo/nR",
      "pushlo/lR",
      "pushoac",
      "pushob",
      "pushobc",
      "pushoc",
      "pushod",
      "pushoi",
      "pushom",
      "pushoo",
      "pushopp",
      "pushr/%rR",
      "pushtopvar/nR",
      "pushvar/n0/n0",
      "pushvar/n0/n1",
      "pushvar/n0/n2",
      "pushvar/n0/n3",
      "pushvar/n0/n4",
      "pushvar/n0/n5",
      "pushvar/n0/nR",
      "pushvar/nR/n0",
      "pushvar/nR/n1",
      "pushvar/nR/n2",
      "pushvar/nR/n3",
      "pushvar/nR/n4",
      "pushvar/nR/n5",
      "pushvar/nR/nR",
      "quake",
      "raise",
      "rand",
      "regvar",
      "reloc",
      "restorer/%rR",
      "return",
      "revn/n3",
      "revn/n4",
      "revn/nR",
      "rot",
      "saver/%rR",
      "sconc",
      "sel",
      "setr/%rR",
      "siz",
      "sleep",
      "smodi",
      "spropc",
      "sproph",
      "sprops",
      "sref",
      "srefi",
      "srefia",
      "srefio",
      "srefmnt",
      "srefnt",
      "srefo",
      "sset",
      "sseti",
      "strace/nR/retR",
      "strref",
      "strset",
      "subi",
      "subiu",
      "subl",
      "sublu",
      "substr",
      "swap",
      "swapgti",
      "swapgtiu",
      "swapgtl",
      "swapgtlu",
      "sync",
      "time",
      "tor",
      "tuck",
      "tyagetb",
      "tyagett",
      "tyisc",
      "tyissct",
      "typof",
      "tysctn",
      "unmap",
      "unreachable",
      "ureloc",
      "write/retR",
      "!REPLACEMENT-addi/retR",
      "!REPLACEMENT-addl/retR",
      "!REPLACEMENT-ains/retR",
      "!REPLACEMENT-aref/retR",
      "!REPLACEMENT-arefo/retR",
      "!REPLACEMENT-arem/retR",
      "!REPLACEMENT-aset/retR",
      "!REPLACEMENT-ba/fR/retR",
      "!REPLACEMENT-bn/fR/retR",
      "!REPLACEMENT-bnn/fR/retR",
      "!REPLACEMENT-bnzi/fR/retR",
      "!REPLACEMENT-bnziu/fR/retR",
      "!REPLACEMENT-bnzl/fR/retR",
      "!REPLACEMENT-bnzlu/fR/retR",
      "!REPLACEMENT-bsli/retR",
      "!REPLACEMENT-bsliu/retR",
      "!REPLACEMENT-bsll/retR",
      "!REPLACEMENT-bsllu/retR",
      "!REPLACEMENT-bzi/fR/retR",
      "!REPLACEMENT-bziu/fR/retR",
      "!REPLACEMENT-bzl/fR/retR",
      "!REPLACEMENT-bzlu/fR/retR",
      "!REPLACEMENT-call/retR",
      "!REPLACEMENT-close/retR",
      "!REPLACEMENT-divi/retR",
      "!REPLACEMENT-diviu/retR",
      "!REPLACEMENT-divl/retR",
      "!REPLACEMENT-divlu/retR",
      "!REPLACEMENT-endhl/retR",
      "!REPLACEMENT-endsc/retR",
      "!REPLACEMENT-exit/retR",
      "!REPLACEMENT-exitvm/retR",
      "!REPLACEMENT-flush/retR",
      "!REPLACEMENT-ioflags/retR",
      "!REPLACEMENT-iogetb/retR",
      "!REPLACEMENT-iosetb/retR",
      "!REPLACEMENT-iosize/retR",
      "!REPLACEMENT-map/retR",
      "!REPLACEMENT-modi/retR",
      "!REPLACEMENT-modiu/retR",
      "!REPLACEMENT-modl/retR",
      "!REPLACEMENT-modlu/retR",
      "!REPLACEMENT-muli/retR",
      "!REPLACEMENT-mull/retR",
      "!REPLACEMENT-negi/retR",
      "!REPLACEMENT-negiu/retR",
      "!REPLACEMENT-negl/retR",
      "!REPLACEMENT-neglu/retR",
      "!REPLACEMENT-open/retR",
      "!REPLACEMENT-peekdi/nR/retR",
      "!REPLACEMENT-peekdiu/nR/retR",
      "!REPLACEMENT-peekdl/nR/retR",
      "!REPLACEMENT-peekdlu/nR/retR",
      "!REPLACEMENT-peeki/nR/nR/nR/retR",
      "!REPLACEMENT-peekiu/nR/nR/retR",
      "!REPLACEMENT-peekl/nR/nR/nR/retR",
      "!REPLACEMENT-peeklu/nR/nR/retR",
      "!REPLACEMENT-peeks/retR",
      "!REPLACEMENT-pokedi/nR/retR",
      "!REPLACEMENT-pokediu/nR/retR",
      "!REPLACEMENT-pokedl/nR/retR",
      "!REPLACEMENT-pokedlu/nR/retR",
      "!REPLACEMENT-pokei/nR/nR/nR/retR",
      "!REPLACEMENT-pokeiu/nR/nR/retR",
      "!REPLACEMENT-pokel/nR/nR/nR/retR",
      "!REPLACEMENT-pokelu/nR/nR/retR",
      "!REPLACEMENT-pokes/retR",
      "!REPLACEMENT-popios/retR",
      "!REPLACEMENT-popob/retR",
      "!REPLACEMENT-popom/retR",
      "!REPLACEMENT-powi/retR",
      "!REPLACEMENT-powiu/retR",
      "!REPLACEMENT-powl/retR",
      "!REPLACEMENT-powlu/retR",
      "!REPLACEMENT-prolog/retR",
      "!REPLACEMENT-pushios/retR",
      "!REPLACEMENT-pushtopvar/nR/retR",
      "!REPLACEMENT-raise/retR",
      "!REPLACEMENT-reloc/retR",
      "!REPLACEMENT-return/retR",
      "!REPLACEMENT-sleep/retR",
      "!REPLACEMENT-smodi/retR",
      "!REPLACEMENT-sref/retR",
      "!REPLACEMENT-srefi/retR",
      "!REPLACEMENT-srefia/retR",
      "!REPLACEMENT-srefio/retR",
      "!REPLACEMENT-srefo/retR",
      "!REPLACEMENT-sset/retR",
      "!REPLACEMENT-sseti/retR",
      "!REPLACEMENT-strref/retR",
      "!REPLACEMENT-strset/retR",
      "!REPLACEMENT-subi/retR",
      "!REPLACEMENT-subl/retR",
      "!REPLACEMENT-substr/retR",
      "!REPLACEMENT-sync/retR",
      "!REPLACEMENT-unreachable/retR",
      "!REPLACEMENT-ureloc/retR",
      "!REPLACEMENT-write/retR"
    };
// #include <stdlib.h>

// #include "pvm-specialized-instructions.h"
const size_t
pvm_specialized_instruction_residual_arities [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      0, /* !INVALID */
      1, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      1, /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      1, /* beghl/retR */
      1, /* begsc/retR */
      1, /* bn/fR */
      1, /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      1, /* bnzi/fR */
      1, /* bnziu/fR */
      1, /* bnzl/fR */
      1, /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      1, /* bzi/fR */
      1, /* bziu/fR */
      1, /* bzl/fR */
      1, /* bzlu/fR */
      1, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      1, /* disas/retR */
      1, /* divi/retR */
      1, /* diviu/retR */
      1, /* divl/retR */
      1, /* divlu/retR */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      1, /* endhl/retR */
      1, /* endsc/retR */
      0, /* eqc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      2, /* formati/nR/retR */
      2, /* formatiu/nR/retR */
      2, /* formatl/nR/retR */
      2, /* formatlu/nR/retR */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      1, /* indent/retR */
      0, /* ioflags */
      1, /* iogetb/retR */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      1, /* itoi/nR */
      1, /* itoiu/nR */
      1, /* itol/nR */
      1, /* itolu/nR */
      1, /* iutoi/nR */
      1, /* iutoiu/nR */
      1, /* iutol/nR */
      1, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      1, /* ltoi/nR */
      1, /* ltoiu/nR */
      1, /* ltol/nR */
      1, /* ltolu/nR */
      0, /* lts */
      1, /* lutoi/nR */
      1, /* lutoiu/nR */
      1, /* lutol/nR */
      1, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      1, /* modi/retR */
      1, /* modiu/retR */
      1, /* modl/retR */
      1, /* modlu/retR */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* nec */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      1, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      1, /* peekdi/nR */
      1, /* peekdiu/nR */
      1, /* peekdl/nR */
      1, /* peekdlu/nR */
      3, /* peeki/nR/nR/nR */
      2, /* peekiu/nR/nR */
      3, /* peekl/nR/nR/nR */
      2, /* peeklu/nR/nR */
      0, /* peeks */
      1, /* pokedi/nR */
      1, /* pokediu/nR */
      1, /* pokedl/nR */
      1, /* pokedlu/nR */
      3, /* pokei/nR/nR/nR */
      2, /* pokeiu/nR/nR */
      3, /* pokel/nR/nR/nR */
      2, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      0, /* popexite */
      1, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      1, /* popr/%rR */
      2, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      2, /* printi/nR/retR */
      2, /* printiu/nR/retR */
      2, /* printl/nR/retR */
      2, /* printlu/nR/retR */
      1, /* prints/retR */
      0, /* prolog */
      1, /* push/nR */
      1, /* push/lR */
      1, /* push32/nR */
      1, /* push32/lR */
      1, /* pushe/lR */
      0, /* pushend */
      1, /* pushf/nR */
      1, /* pushhi/nR */
      1, /* pushhi/lR */
      0, /* pushios */
      1, /* pushlo/nR */
      1, /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      1, /* pushr/%rR */
      1, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      1, /* pushvar/n0/nR */
      1, /* pushvar/nR/n0 */
      1, /* pushvar/nR/n1 */
      1, /* pushvar/nR/n2 */
      1, /* pushvar/nR/n3 */
      1, /* pushvar/nR/n4 */
      1, /* pushvar/nR/n5 */
      2, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      1, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      1, /* revn/nR */
      0, /* rot */
      1, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      1, /* setr/%rR */
      0, /* siz */
      0, /* sleep */
      0, /* smodi */
      0, /* spropc */
      0, /* sproph */
      0, /* sprops */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      0, /* sseti */
      2, /* strace/nR/retR */
      0, /* strref */
      0, /* strset */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      1, /* write/retR */
      1, /* !REPLACEMENT-addi/retR */
      1, /* !REPLACEMENT-addl/retR */
      1, /* !REPLACEMENT-ains/retR */
      1, /* !REPLACEMENT-aref/retR */
      1, /* !REPLACEMENT-arefo/retR */
      1, /* !REPLACEMENT-arem/retR */
      1, /* !REPLACEMENT-aset/retR */
      2, /* !REPLACEMENT-ba/fR/retR */
      2, /* !REPLACEMENT-bn/fR/retR */
      2, /* !REPLACEMENT-bnn/fR/retR */
      2, /* !REPLACEMENT-bnzi/fR/retR */
      2, /* !REPLACEMENT-bnziu/fR/retR */
      2, /* !REPLACEMENT-bnzl/fR/retR */
      2, /* !REPLACEMENT-bnzlu/fR/retR */
      1, /* !REPLACEMENT-bsli/retR */
      1, /* !REPLACEMENT-bsliu/retR */
      1, /* !REPLACEMENT-bsll/retR */
      1, /* !REPLACEMENT-bsllu/retR */
      2, /* !REPLACEMENT-bzi/fR/retR */
      2, /* !REPLACEMENT-bziu/fR/retR */
      2, /* !REPLACEMENT-bzl/fR/retR */
      2, /* !REPLACEMENT-bzlu/fR/retR */
      1, /* !REPLACEMENT-call/retR */
      1, /* !REPLACEMENT-close/retR */
      1, /* !REPLACEMENT-divi/retR */
      1, /* !REPLACEMENT-diviu/retR */
      1, /* !REPLACEMENT-divl/retR */
      1, /* !REPLACEMENT-divlu/retR */
      1, /* !REPLACEMENT-endhl/retR */
      1, /* !REPLACEMENT-endsc/retR */
      1, /* !REPLACEMENT-exit/retR */
      1, /* !REPLACEMENT-exitvm/retR */
      1, /* !REPLACEMENT-flush/retR */
      1, /* !REPLACEMENT-ioflags/retR */
      1, /* !REPLACEMENT-iogetb/retR */
      1, /* !REPLACEMENT-iosetb/retR */
      1, /* !REPLACEMENT-iosize/retR */
      1, /* !REPLACEMENT-map/retR */
      1, /* !REPLACEMENT-modi/retR */
      1, /* !REPLACEMENT-modiu/retR */
      1, /* !REPLACEMENT-modl/retR */
      1, /* !REPLACEMENT-modlu/retR */
      1, /* !REPLACEMENT-muli/retR */
      1, /* !REPLACEMENT-mull/retR */
      1, /* !REPLACEMENT-negi/retR */
      1, /* !REPLACEMENT-negiu/retR */
      1, /* !REPLACEMENT-negl/retR */
      1, /* !REPLACEMENT-neglu/retR */
      1, /* !REPLACEMENT-open/retR */
      2, /* !REPLACEMENT-peekdi/nR/retR */
      2, /* !REPLACEMENT-peekdiu/nR/retR */
      2, /* !REPLACEMENT-peekdl/nR/retR */
      2, /* !REPLACEMENT-peekdlu/nR/retR */
      4, /* !REPLACEMENT-peeki/nR/nR/nR/retR */
      3, /* !REPLACEMENT-peekiu/nR/nR/retR */
      4, /* !REPLACEMENT-peekl/nR/nR/nR/retR */
      3, /* !REPLACEMENT-peeklu/nR/nR/retR */
      1, /* !REPLACEMENT-peeks/retR */
      2, /* !REPLACEMENT-pokedi/nR/retR */
      2, /* !REPLACEMENT-pokediu/nR/retR */
      2, /* !REPLACEMENT-pokedl/nR/retR */
      2, /* !REPLACEMENT-pokedlu/nR/retR */
      4, /* !REPLACEMENT-pokei/nR/nR/nR/retR */
      3, /* !REPLACEMENT-pokeiu/nR/nR/retR */
      4, /* !REPLACEMENT-pokel/nR/nR/nR/retR */
      3, /* !REPLACEMENT-pokelu/nR/nR/retR */
      1, /* !REPLACEMENT-pokes/retR */
      1, /* !REPLACEMENT-popios/retR */
      1, /* !REPLACEMENT-popob/retR */
      1, /* !REPLACEMENT-popom/retR */
      1, /* !REPLACEMENT-powi/retR */
      1, /* !REPLACEMENT-powiu/retR */
      1, /* !REPLACEMENT-powl/retR */
      1, /* !REPLACEMENT-powlu/retR */
      1, /* !REPLACEMENT-prolog/retR */
      1, /* !REPLACEMENT-pushios/retR */
      2, /* !REPLACEMENT-pushtopvar/nR/retR */
      1, /* !REPLACEMENT-raise/retR */
      1, /* !REPLACEMENT-reloc/retR */
      1, /* !REPLACEMENT-return/retR */
      1, /* !REPLACEMENT-sleep/retR */
      1, /* !REPLACEMENT-smodi/retR */
      1, /* !REPLACEMENT-sref/retR */
      1, /* !REPLACEMENT-srefi/retR */
      1, /* !REPLACEMENT-srefia/retR */
      1, /* !REPLACEMENT-srefio/retR */
      1, /* !REPLACEMENT-srefo/retR */
      1, /* !REPLACEMENT-sset/retR */
      1, /* !REPLACEMENT-sseti/retR */
      1, /* !REPLACEMENT-strref/retR */
      1, /* !REPLACEMENT-strset/retR */
      1, /* !REPLACEMENT-subi/retR */
      1, /* !REPLACEMENT-subl/retR */
      1, /* !REPLACEMENT-substr/retR */
      1, /* !REPLACEMENT-sync/retR */
      1, /* !REPLACEMENT-unreachable/retR */
      1, /* !REPLACEMENT-ureloc/retR */
      1 /* !REPLACEMENT-write/retR */
    };
const unsigned long // FIXME: shall I use a shorter type when possible?
pvm_specialized_instruction_label_bitmasks [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      0 | (1UL << 0), /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      0, /* beghl/retR */
      0, /* begsc/retR */
      0 | (1UL << 0), /* bn/fR */
      0 | (1UL << 0), /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      0 | (1UL << 0), /* bnzi/fR */
      0 | (1UL << 0), /* bnziu/fR */
      0 | (1UL << 0), /* bnzl/fR */
      0 | (1UL << 0), /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      0 | (1UL << 0), /* bzi/fR */
      0 | (1UL << 0), /* bziu/fR */
      0 | (1UL << 0), /* bzl/fR */
      0 | (1UL << 0), /* bzlu/fR */
      0, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      0, /* disas/retR */
      0, /* divi/retR */
      0, /* diviu/retR */
      0, /* divl/retR */
      0, /* divlu/retR */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      0, /* endhl/retR */
      0, /* endsc/retR */
      0, /* eqc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      0, /* formati/nR/retR */
      0, /* formatiu/nR/retR */
      0, /* formatl/nR/retR */
      0, /* formatlu/nR/retR */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      0, /* indent/retR */
      0, /* ioflags */
      0, /* iogetb/retR */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      0, /* itoi/nR */
      0, /* itoiu/nR */
      0, /* itol/nR */
      0, /* itolu/nR */
      0, /* iutoi/nR */
      0, /* iutoiu/nR */
      0, /* iutol/nR */
      0, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      0, /* ltoi/nR */
      0, /* ltoiu/nR */
      0, /* ltol/nR */
      0, /* ltolu/nR */
      0, /* lts */
      0, /* lutoi/nR */
      0, /* lutoiu/nR */
      0, /* lutol/nR */
      0, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      0, /* modi/retR */
      0, /* modiu/retR */
      0, /* modl/retR */
      0, /* modlu/retR */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* nec */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      0, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      0, /* peekdi/nR */
      0, /* peekdiu/nR */
      0, /* peekdl/nR */
      0, /* peekdlu/nR */
      0, /* peeki/nR/nR/nR */
      0, /* peekiu/nR/nR */
      0, /* peekl/nR/nR/nR */
      0, /* peeklu/nR/nR */
      0, /* peeks */
      0, /* pokedi/nR */
      0, /* pokediu/nR */
      0, /* pokedl/nR */
      0, /* pokedlu/nR */
      0, /* pokei/nR/nR/nR */
      0, /* pokeiu/nR/nR */
      0, /* pokel/nR/nR/nR */
      0, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      0, /* popexite */
      0, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      0, /* popr/%rR */
      0, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      0, /* printi/nR/retR */
      0, /* printiu/nR/retR */
      0, /* printl/nR/retR */
      0, /* printlu/nR/retR */
      0, /* prints/retR */
      0, /* prolog */
      0, /* push/nR */
      0 | (1UL << 0), /* push/lR */
      0, /* push32/nR */
      0 | (1UL << 0), /* push32/lR */
      0 | (1UL << 0), /* pushe/lR */
      0, /* pushend */
      0, /* pushf/nR */
      0, /* pushhi/nR */
      0 | (1UL << 0), /* pushhi/lR */
      0, /* pushios */
      0, /* pushlo/nR */
      0 | (1UL << 0), /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      0, /* pushr/%rR */
      0, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      0, /* pushvar/n0/nR */
      0, /* pushvar/nR/n0 */
      0, /* pushvar/nR/n1 */
      0, /* pushvar/nR/n2 */
      0, /* pushvar/nR/n3 */
      0, /* pushvar/nR/n4 */
      0, /* pushvar/nR/n5 */
      0, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      0, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      0, /* revn/nR */
      0, /* rot */
      0, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      0, /* setr/%rR */
      0, /* siz */
      0, /* sleep */
      0, /* smodi */
      0, /* spropc */
      0, /* sproph */
      0, /* sprops */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      0, /* sseti */
      0, /* strace/nR/retR */
      0, /* strref */
      0, /* strset */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      0, /* write/retR */
      0, /* !REPLACEMENT-addi/retR */
      0, /* !REPLACEMENT-addl/retR */
      0, /* !REPLACEMENT-ains/retR */
      0, /* !REPLACEMENT-aref/retR */
      0, /* !REPLACEMENT-arefo/retR */
      0, /* !REPLACEMENT-arem/retR */
      0, /* !REPLACEMENT-aset/retR */
      0 | (1UL << 0), /* !REPLACEMENT-ba/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bn/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bnn/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bnzi/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bnziu/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bnzl/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bnzlu/fR/retR */
      0, /* !REPLACEMENT-bsli/retR */
      0, /* !REPLACEMENT-bsliu/retR */
      0, /* !REPLACEMENT-bsll/retR */
      0, /* !REPLACEMENT-bsllu/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bzi/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bziu/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bzl/fR/retR */
      0 | (1UL << 0), /* !REPLACEMENT-bzlu/fR/retR */
      0, /* !REPLACEMENT-call/retR */
      0, /* !REPLACEMENT-close/retR */
      0, /* !REPLACEMENT-divi/retR */
      0, /* !REPLACEMENT-diviu/retR */
      0, /* !REPLACEMENT-divl/retR */
      0, /* !REPLACEMENT-divlu/retR */
      0, /* !REPLACEMENT-endhl/retR */
      0, /* !REPLACEMENT-endsc/retR */
      0, /* !REPLACEMENT-exit/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0, /* !REPLACEMENT-flush/retR */
      0, /* !REPLACEMENT-ioflags/retR */
      0, /* !REPLACEMENT-iogetb/retR */
      0, /* !REPLACEMENT-iosetb/retR */
      0, /* !REPLACEMENT-iosize/retR */
      0, /* !REPLACEMENT-map/retR */
      0, /* !REPLACEMENT-modi/retR */
      0, /* !REPLACEMENT-modiu/retR */
      0, /* !REPLACEMENT-modl/retR */
      0, /* !REPLACEMENT-modlu/retR */
      0, /* !REPLACEMENT-muli/retR */
      0, /* !REPLACEMENT-mull/retR */
      0, /* !REPLACEMENT-negi/retR */
      0, /* !REPLACEMENT-negiu/retR */
      0, /* !REPLACEMENT-negl/retR */
      0, /* !REPLACEMENT-neglu/retR */
      0, /* !REPLACEMENT-open/retR */
      0, /* !REPLACEMENT-peekdi/nR/retR */
      0, /* !REPLACEMENT-peekdiu/nR/retR */
      0, /* !REPLACEMENT-peekdl/nR/retR */
      0, /* !REPLACEMENT-peekdlu/nR/retR */
      0, /* !REPLACEMENT-peeki/nR/nR/nR/retR */
      0, /* !REPLACEMENT-peekiu/nR/nR/retR */
      0, /* !REPLACEMENT-peekl/nR/nR/nR/retR */
      0, /* !REPLACEMENT-peeklu/nR/nR/retR */
      0, /* !REPLACEMENT-peeks/retR */
      0, /* !REPLACEMENT-pokedi/nR/retR */
      0, /* !REPLACEMENT-pokediu/nR/retR */
      0, /* !REPLACEMENT-pokedl/nR/retR */
      0, /* !REPLACEMENT-pokedlu/nR/retR */
      0, /* !REPLACEMENT-pokei/nR/nR/nR/retR */
      0, /* !REPLACEMENT-pokeiu/nR/nR/retR */
      0, /* !REPLACEMENT-pokel/nR/nR/nR/retR */
      0, /* !REPLACEMENT-pokelu/nR/nR/retR */
      0, /* !REPLACEMENT-pokes/retR */
      0, /* !REPLACEMENT-popios/retR */
      0, /* !REPLACEMENT-popob/retR */
      0, /* !REPLACEMENT-popom/retR */
      0, /* !REPLACEMENT-powi/retR */
      0, /* !REPLACEMENT-powiu/retR */
      0, /* !REPLACEMENT-powl/retR */
      0, /* !REPLACEMENT-powlu/retR */
      0, /* !REPLACEMENT-prolog/retR */
      0, /* !REPLACEMENT-pushios/retR */
      0, /* !REPLACEMENT-pushtopvar/nR/retR */
      0, /* !REPLACEMENT-raise/retR */
      0, /* !REPLACEMENT-reloc/retR */
      0, /* !REPLACEMENT-return/retR */
      0, /* !REPLACEMENT-sleep/retR */
      0, /* !REPLACEMENT-smodi/retR */
      0, /* !REPLACEMENT-sref/retR */
      0, /* !REPLACEMENT-srefi/retR */
      0, /* !REPLACEMENT-srefia/retR */
      0, /* !REPLACEMENT-srefio/retR */
      0, /* !REPLACEMENT-srefo/retR */
      0, /* !REPLACEMENT-sset/retR */
      0, /* !REPLACEMENT-sseti/retR */
      0, /* !REPLACEMENT-strref/retR */
      0, /* !REPLACEMENT-strset/retR */
      0, /* !REPLACEMENT-subi/retR */
      0, /* !REPLACEMENT-subl/retR */
      0, /* !REPLACEMENT-substr/retR */
      0, /* !REPLACEMENT-sync/retR */
      0, /* !REPLACEMENT-unreachable/retR */
      0, /* !REPLACEMENT-ureloc/retR */
      0 /* !REPLACEMENT-write/retR */
    };
#ifdef JITTER_HAVE_PATCH_IN
const unsigned long // FIXME: shall I use a shorter type when possible?
pvm_specialized_instruction_fast_label_bitmasks [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      /* It's important that !BEGINBASICBLOCK has a zero here: it does not need residual patching. */
      0, /* !INVALID */
      0, /* !BEGINBASICBLOCK */
      0, /* !EXITVM */
      0, /* !DATALOCATIONS */
      0, /* !NOP */
      0, /* !UNREACHABLE0 */
      0, /* !UNREACHABLE1 */
      0, /* !PRETENDTOJUMPANYWHERE */
      0, /* addi */
      0, /* addiu */
      0, /* addl */
      0, /* addlu */
      0, /* ains */
      0, /* and */
      0, /* aref */
      0, /* arefo */
      0, /* arem */
      0, /* aset */
      0, /* asettb */
      0, /* atr */
      0 | (1UL << 0), /* ba/fR */
      0, /* bandi */
      0, /* bandiu */
      0, /* bandl */
      0, /* bandlu */
      0, /* beghl/retR */
      0, /* begsc/retR */
      0 | (1UL << 0), /* bn/fR */
      0 | (1UL << 0), /* bnn/fR */
      0, /* bnoti */
      0, /* bnotiu */
      0, /* bnotl */
      0, /* bnotlu */
      0 | (1UL << 0), /* bnzi/fR */
      0 | (1UL << 0), /* bnziu/fR */
      0 | (1UL << 0), /* bnzl/fR */
      0 | (1UL << 0), /* bnzlu/fR */
      0, /* bori */
      0, /* boriu */
      0, /* borl */
      0, /* borlu */
      0, /* bsli */
      0, /* bsliu */
      0, /* bsll */
      0, /* bsllu */
      0, /* bsri */
      0, /* bsriu */
      0, /* bsrl */
      0, /* bsrlu */
      0, /* bxori */
      0, /* bxoriu */
      0, /* bxorl */
      0, /* bxorlu */
      0 | (1UL << 0), /* bzi/fR */
      0 | (1UL << 0), /* bziu/fR */
      0 | (1UL << 0), /* bzl/fR */
      0 | (1UL << 0), /* bzlu/fR */
      0, /* call/retR */
      0, /* canary */
      0, /* close */
      0, /* ctos */
      0, /* disas/retR */
      0, /* divi/retR */
      0, /* diviu/retR */
      0, /* divl/retR */
      0, /* divlu/retR */
      0, /* drop */
      0, /* drop2 */
      0, /* drop3 */
      0, /* drop4 */
      0, /* duc */
      0, /* dup */
      0, /* endhl/retR */
      0, /* endsc/retR */
      0, /* eqc */
      0, /* eqi */
      0, /* eqiu */
      0, /* eql */
      0, /* eqlu */
      0, /* eqs */
      0, /* exit */
      0, /* exitvm */
      0, /* flush */
      0, /* formati/nR/retR */
      0, /* formatiu/nR/retR */
      0, /* formatl/nR/retR */
      0, /* formatlu/nR/retR */
      0, /* fromr */
      0, /* gei */
      0, /* geiu */
      0, /* gel */
      0, /* gelu */
      0, /* ges */
      0, /* getenv */
      0, /* gti */
      0, /* gtiu */
      0, /* gtl */
      0, /* gtlu */
      0, /* gts */
      0, /* indent/retR */
      0, /* ioflags */
      0, /* iogetb/retR */
      0, /* iosetb */
      0, /* iosize */
      0, /* isa */
      0, /* itoi/nR */
      0, /* itoiu/nR */
      0, /* itol/nR */
      0, /* itolu/nR */
      0, /* iutoi/nR */
      0, /* iutoiu/nR */
      0, /* iutol/nR */
      0, /* iutolu/nR */
      0, /* lei */
      0, /* leiu */
      0, /* lel */
      0, /* lelu */
      0, /* les */
      0, /* lti */
      0, /* ltiu */
      0, /* ltl */
      0, /* ltlu */
      0, /* ltoi/nR */
      0, /* ltoiu/nR */
      0, /* ltol/nR */
      0, /* ltolu/nR */
      0, /* lts */
      0, /* lutoi/nR */
      0, /* lutoiu/nR */
      0, /* lutol/nR */
      0, /* lutolu/nR */
      0, /* map */
      0, /* mgetios */
      0, /* mgetm */
      0, /* mgeto */
      0, /* mgets */
      0, /* mgetsel */
      0, /* mgetsiz */
      0, /* mgetw */
      0, /* mka */
      0, /* mko */
      0, /* mksct */
      0, /* mktya */
      0, /* mktyany */
      0, /* mktyc */
      0, /* mktyi */
      0, /* mktyo */
      0, /* mktys */
      0, /* mktysct */
      0, /* mktyv */
      0, /* mm */
      0, /* modi/retR */
      0, /* modiu/retR */
      0, /* modl/retR */
      0, /* modlu/retR */
      0, /* msetios */
      0, /* msetm */
      0, /* mseto */
      0, /* msets */
      0, /* msetsel */
      0, /* msetsiz */
      0, /* msetw */
      0, /* muli */
      0, /* muliu */
      0, /* mull */
      0, /* mullu */
      0, /* muls */
      0, /* nec */
      0, /* negi */
      0, /* negiu */
      0, /* negl */
      0, /* neglu */
      0, /* nei */
      0, /* neiu */
      0, /* nel */
      0, /* nelu */
      0, /* nes */
      0, /* nip */
      0, /* nip2 */
      0, /* nip3 */
      0, /* nn */
      0, /* nnn */
      0, /* nop */
      0, /* not */
      0, /* note/nR */
      0, /* nrot */
      0, /* ogetbt */
      0, /* ogetm */
      0, /* ogetu */
      0, /* open */
      0, /* or */
      0, /* osetm */
      0, /* over */
      0, /* pec */
      0, /* peekdi/nR */
      0, /* peekdiu/nR */
      0, /* peekdl/nR */
      0, /* peekdlu/nR */
      0, /* peeki/nR/nR/nR */
      0, /* peekiu/nR/nR */
      0, /* peekl/nR/nR/nR */
      0, /* peeklu/nR/nR */
      0, /* peeks */
      0, /* pokedi/nR */
      0, /* pokediu/nR */
      0, /* pokedl/nR */
      0, /* pokedlu/nR */
      0, /* pokei/nR/nR/nR */
      0, /* pokeiu/nR/nR */
      0, /* pokel/nR/nR/nR */
      0, /* pokelu/nR/nR */
      0, /* pokes */
      0, /* pope */
      0, /* popend */
      0, /* popexite */
      0, /* popf/nR */
      0, /* popios */
      0, /* popoac */
      0, /* popob */
      0, /* popobc */
      0, /* popoc */
      0, /* popod */
      0, /* popoi */
      0, /* popom */
      0, /* popoo */
      0, /* popopp */
      0, /* popr/%rR */
      0, /* popvar/nR/nR */
      0, /* powi */
      0, /* powiu */
      0, /* powl */
      0, /* powlu */
      0, /* printi/nR/retR */
      0, /* printiu/nR/retR */
      0, /* printl/nR/retR */
      0, /* printlu/nR/retR */
      0, /* prints/retR */
      0, /* prolog */
      0, /* push/nR */
      0, /* push/lR */
      0, /* push32/nR */
      0, /* push32/lR */
      0, /* pushe/lR */
      0, /* pushend */
      0, /* pushf/nR */
      0, /* pushhi/nR */
      0, /* pushhi/lR */
      0, /* pushios */
      0, /* pushlo/nR */
      0, /* pushlo/lR */
      0, /* pushoac */
      0, /* pushob */
      0, /* pushobc */
      0, /* pushoc */
      0, /* pushod */
      0, /* pushoi */
      0, /* pushom */
      0, /* pushoo */
      0, /* pushopp */
      0, /* pushr/%rR */
      0, /* pushtopvar/nR */
      0, /* pushvar/n0/n0 */
      0, /* pushvar/n0/n1 */
      0, /* pushvar/n0/n2 */
      0, /* pushvar/n0/n3 */
      0, /* pushvar/n0/n4 */
      0, /* pushvar/n0/n5 */
      0, /* pushvar/n0/nR */
      0, /* pushvar/nR/n0 */
      0, /* pushvar/nR/n1 */
      0, /* pushvar/nR/n2 */
      0, /* pushvar/nR/n3 */
      0, /* pushvar/nR/n4 */
      0, /* pushvar/nR/n5 */
      0, /* pushvar/nR/nR */
      0, /* quake */
      0, /* raise */
      0, /* rand */
      0, /* regvar */
      0, /* reloc */
      0, /* restorer/%rR */
      0, /* return */
      0, /* revn/n3 */
      0, /* revn/n4 */
      0, /* revn/nR */
      0, /* rot */
      0, /* saver/%rR */
      0, /* sconc */
      0, /* sel */
      0, /* setr/%rR */
      0, /* siz */
      0, /* sleep */
      0, /* smodi */
      0, /* spropc */
      0, /* sproph */
      0, /* sprops */
      0, /* sref */
      0, /* srefi */
      0, /* srefia */
      0, /* srefio */
      0, /* srefmnt */
      0, /* srefnt */
      0, /* srefo */
      0, /* sset */
      0, /* sseti */
      0, /* strace/nR/retR */
      0, /* strref */
      0, /* strset */
      0, /* subi */
      0, /* subiu */
      0, /* subl */
      0, /* sublu */
      0, /* substr */
      0, /* swap */
      0, /* swapgti */
      0, /* swapgtiu */
      0, /* swapgtl */
      0, /* swapgtlu */
      0, /* sync */
      0, /* time */
      0, /* tor */
      0, /* tuck */
      0, /* tyagetb */
      0, /* tyagett */
      0, /* tyisc */
      0, /* tyissct */
      0, /* typof */
      0, /* tysctn */
      0, /* unmap */
      0, /* unreachable */
      0, /* ureloc */
      0, /* write/retR */
      0, /* !REPLACEMENT-addi/retR */
      0, /* !REPLACEMENT-addl/retR */
      0, /* !REPLACEMENT-ains/retR */
      0, /* !REPLACEMENT-aref/retR */
      0, /* !REPLACEMENT-arefo/retR */
      0, /* !REPLACEMENT-arem/retR */
      0, /* !REPLACEMENT-aset/retR */
      0, /* !REPLACEMENT-ba/fR/retR */
      0, /* !REPLACEMENT-bn/fR/retR */
      0, /* !REPLACEMENT-bnn/fR/retR */
      0, /* !REPLACEMENT-bnzi/fR/retR */
      0, /* !REPLACEMENT-bnziu/fR/retR */
      0, /* !REPLACEMENT-bnzl/fR/retR */
      0, /* !REPLACEMENT-bnzlu/fR/retR */
      0, /* !REPLACEMENT-bsli/retR */
      0, /* !REPLACEMENT-bsliu/retR */
      0, /* !REPLACEMENT-bsll/retR */
      0, /* !REPLACEMENT-bsllu/retR */
      0, /* !REPLACEMENT-bzi/fR/retR */
      0, /* !REPLACEMENT-bziu/fR/retR */
      0, /* !REPLACEMENT-bzl/fR/retR */
      0, /* !REPLACEMENT-bzlu/fR/retR */
      0, /* !REPLACEMENT-call/retR */
      0, /* !REPLACEMENT-close/retR */
      0, /* !REPLACEMENT-divi/retR */
      0, /* !REPLACEMENT-diviu/retR */
      0, /* !REPLACEMENT-divl/retR */
      0, /* !REPLACEMENT-divlu/retR */
      0, /* !REPLACEMENT-endhl/retR */
      0, /* !REPLACEMENT-endsc/retR */
      0, /* !REPLACEMENT-exit/retR */
      0, /* !REPLACEMENT-exitvm/retR */
      0, /* !REPLACEMENT-flush/retR */
      0, /* !REPLACEMENT-ioflags/retR */
      0, /* !REPLACEMENT-iogetb/retR */
      0, /* !REPLACEMENT-iosetb/retR */
      0, /* !REPLACEMENT-iosize/retR */
      0, /* !REPLACEMENT-map/retR */
      0, /* !REPLACEMENT-modi/retR */
      0, /* !REPLACEMENT-modiu/retR */
      0, /* !REPLACEMENT-modl/retR */
      0, /* !REPLACEMENT-modlu/retR */
      0, /* !REPLACEMENT-muli/retR */
      0, /* !REPLACEMENT-mull/retR */
      0, /* !REPLACEMENT-negi/retR */
      0, /* !REPLACEMENT-negiu/retR */
      0, /* !REPLACEMENT-negl/retR */
      0, /* !REPLACEMENT-neglu/retR */
      0, /* !REPLACEMENT-open/retR */
      0, /* !REPLACEMENT-peekdi/nR/retR */
      0, /* !REPLACEMENT-peekdiu/nR/retR */
      0, /* !REPLACEMENT-peekdl/nR/retR */
      0, /* !REPLACEMENT-peekdlu/nR/retR */
      0, /* !REPLACEMENT-peeki/nR/nR/nR/retR */
      0, /* !REPLACEMENT-peekiu/nR/nR/retR */
      0, /* !REPLACEMENT-peekl/nR/nR/nR/retR */
      0, /* !REPLACEMENT-peeklu/nR/nR/retR */
      0, /* !REPLACEMENT-peeks/retR */
      0, /* !REPLACEMENT-pokedi/nR/retR */
      0, /* !REPLACEMENT-pokediu/nR/retR */
      0, /* !REPLACEMENT-pokedl/nR/retR */
      0, /* !REPLACEMENT-pokedlu/nR/retR */
      0, /* !REPLACEMENT-pokei/nR/nR/nR/retR */
      0, /* !REPLACEMENT-pokeiu/nR/nR/retR */
      0, /* !REPLACEMENT-pokel/nR/nR/nR/retR */
      0, /* !REPLACEMENT-pokelu/nR/nR/retR */
      0, /* !REPLACEMENT-pokes/retR */
      0, /* !REPLACEMENT-popios/retR */
      0, /* !REPLACEMENT-popob/retR */
      0, /* !REPLACEMENT-popom/retR */
      0, /* !REPLACEMENT-powi/retR */
      0, /* !REPLACEMENT-powiu/retR */
      0, /* !REPLACEMENT-powl/retR */
      0, /* !REPLACEMENT-powlu/retR */
      0, /* !REPLACEMENT-prolog/retR */
      0, /* !REPLACEMENT-pushios/retR */
      0, /* !REPLACEMENT-pushtopvar/nR/retR */
      0, /* !REPLACEMENT-raise/retR */
      0, /* !REPLACEMENT-reloc/retR */
      0, /* !REPLACEMENT-return/retR */
      0, /* !REPLACEMENT-sleep/retR */
      0, /* !REPLACEMENT-smodi/retR */
      0, /* !REPLACEMENT-sref/retR */
      0, /* !REPLACEMENT-srefi/retR */
      0, /* !REPLACEMENT-srefia/retR */
      0, /* !REPLACEMENT-srefio/retR */
      0, /* !REPLACEMENT-srefo/retR */
      0, /* !REPLACEMENT-sset/retR */
      0, /* !REPLACEMENT-sseti/retR */
      0, /* !REPLACEMENT-strref/retR */
      0, /* !REPLACEMENT-strset/retR */
      0, /* !REPLACEMENT-subi/retR */
      0, /* !REPLACEMENT-subl/retR */
      0, /* !REPLACEMENT-substr/retR */
      0, /* !REPLACEMENT-sync/retR */
      0, /* !REPLACEMENT-unreachable/retR */
      0, /* !REPLACEMENT-ureloc/retR */
      0 /* !REPLACEMENT-write/retR */
    };
#endif // #ifdef JITTER_HAVE_PATCH_IN

// FIXME: I may want to conditionalize this.
const bool
pvm_specialized_instruction_relocatables [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      true, // !INVALID
      true, // !BEGINBASICBLOCK
      true, // !EXITVM
      true, // !DATALOCATIONS
      true, // !NOP
      true, // !UNREACHABLE0
      true, // !UNREACHABLE1
      true, // !PRETENDTOJUMPANYWHERE
      true, // addi
      true, // addiu
      true, // addl
      true, // addlu
      true, // ains
      true, // and
      true, // aref
      true, // arefo
      true, // arem
      true, // aset
      true, // asettb
      true, // atr
      true, // ba/fR
      true, // bandi
      true, // bandiu
      true, // bandl
      true, // bandlu
      false, // beghl/retR
      false, // begsc/retR
      true, // bn/fR
      true, // bnn/fR
      true, // bnoti
      true, // bnotiu
      true, // bnotl
      true, // bnotlu
      true, // bnzi/fR
      true, // bnziu/fR
      true, // bnzl/fR
      true, // bnzlu/fR
      true, // bori
      true, // boriu
      true, // borl
      true, // borlu
      true, // bsli
      true, // bsliu
      true, // bsll
      true, // bsllu
      true, // bsri
      true, // bsriu
      true, // bsrl
      true, // bsrlu
      true, // bxori
      true, // bxoriu
      true, // bxorl
      true, // bxorlu
      true, // bzi/fR
      true, // bziu/fR
      true, // bzl/fR
      true, // bzlu/fR
      true, // call/retR
      true, // canary
      true, // close
      true, // ctos
      false, // disas/retR
      false, // divi/retR
      false, // diviu/retR
      false, // divl/retR
      false, // divlu/retR
      true, // drop
      true, // drop2
      true, // drop3
      true, // drop4
      true, // duc
      true, // dup
      false, // endhl/retR
      false, // endsc/retR
      true, // eqc
      true, // eqi
      true, // eqiu
      true, // eql
      true, // eqlu
      true, // eqs
      true, // exit
      true, // exitvm
      true, // flush
      false, // formati/nR/retR
      false, // formatiu/nR/retR
      false, // formatl/nR/retR
      false, // formatlu/nR/retR
      true, // fromr
      true, // gei
      true, // geiu
      true, // gel
      true, // gelu
      true, // ges
      true, // getenv
      true, // gti
      true, // gtiu
      true, // gtl
      true, // gtlu
      true, // gts
      false, // indent/retR
      true, // ioflags
      false, // iogetb/retR
      true, // iosetb
      true, // iosize
      true, // isa
      true, // itoi/nR
      true, // itoiu/nR
      true, // itol/nR
      true, // itolu/nR
      true, // iutoi/nR
      true, // iutoiu/nR
      true, // iutol/nR
      true, // iutolu/nR
      true, // lei
      true, // leiu
      true, // lel
      true, // lelu
      true, // les
      true, // lti
      true, // ltiu
      true, // ltl
      true, // ltlu
      true, // ltoi/nR
      true, // ltoiu/nR
      true, // ltol/nR
      true, // ltolu/nR
      true, // lts
      true, // lutoi/nR
      true, // lutoiu/nR
      true, // lutol/nR
      true, // lutolu/nR
      true, // map
      true, // mgetios
      true, // mgetm
      true, // mgeto
      true, // mgets
      true, // mgetsel
      true, // mgetsiz
      true, // mgetw
      true, // mka
      true, // mko
      true, // mksct
      true, // mktya
      true, // mktyany
      true, // mktyc
      true, // mktyi
      true, // mktyo
      true, // mktys
      true, // mktysct
      true, // mktyv
      true, // mm
      false, // modi/retR
      false, // modiu/retR
      false, // modl/retR
      false, // modlu/retR
      true, // msetios
      true, // msetm
      true, // mseto
      true, // msets
      true, // msetsel
      true, // msetsiz
      true, // msetw
      true, // muli
      true, // muliu
      true, // mull
      true, // mullu
      true, // muls
      true, // nec
      true, // negi
      true, // negiu
      true, // negl
      true, // neglu
      true, // nei
      true, // neiu
      true, // nel
      true, // nelu
      true, // nes
      true, // nip
      true, // nip2
      true, // nip3
      true, // nn
      true, // nnn
      true, // nop
      true, // not
      true, // note/nR
      true, // nrot
      true, // ogetbt
      true, // ogetm
      true, // ogetu
      true, // open
      true, // or
      true, // osetm
      true, // over
      true, // pec
      true, // peekdi/nR
      true, // peekdiu/nR
      true, // peekdl/nR
      true, // peekdlu/nR
      true, // peeki/nR/nR/nR
      true, // peekiu/nR/nR
      true, // peekl/nR/nR/nR
      true, // peeklu/nR/nR
      true, // peeks
      true, // pokedi/nR
      true, // pokediu/nR
      true, // pokedl/nR
      true, // pokedlu/nR
      true, // pokei/nR/nR/nR
      true, // pokeiu/nR/nR
      true, // pokel/nR/nR/nR
      true, // pokelu/nR/nR
      true, // pokes
      true, // pope
      true, // popend
      true, // popexite
      true, // popf/nR
      true, // popios
      true, // popoac
      true, // popob
      true, // popobc
      true, // popoc
      true, // popod
      true, // popoi
      true, // popom
      true, // popoo
      true, // popopp
      true, // popr/%rR
      true, // popvar/nR/nR
      true, // powi
      true, // powiu
      true, // powl
      true, // powlu
      false, // printi/nR/retR
      false, // printiu/nR/retR
      false, // printl/nR/retR
      false, // printlu/nR/retR
      false, // prints/retR
      true, // prolog
      true, // push/nR
      true, // push/lR
      true, // push32/nR
      true, // push32/lR
      true, // pushe/lR
      true, // pushend
      true, // pushf/nR
      true, // pushhi/nR
      true, // pushhi/lR
      true, // pushios
      true, // pushlo/nR
      true, // pushlo/lR
      true, // pushoac
      true, // pushob
      true, // pushobc
      true, // pushoc
      true, // pushod
      true, // pushoi
      true, // pushom
      true, // pushoo
      true, // pushopp
      true, // pushr/%rR
      true, // pushtopvar/nR
      true, // pushvar/n0/n0
      true, // pushvar/n0/n1
      true, // pushvar/n0/n2
      true, // pushvar/n0/n3
      true, // pushvar/n0/n4
      true, // pushvar/n0/n5
      true, // pushvar/n0/nR
      true, // pushvar/nR/n0
      true, // pushvar/nR/n1
      true, // pushvar/nR/n2
      true, // pushvar/nR/n3
      true, // pushvar/nR/n4
      true, // pushvar/nR/n5
      true, // pushvar/nR/nR
      true, // quake
      true, // raise
      true, // rand
      true, // regvar
      true, // reloc
      true, // restorer/%rR
      true, // return
      true, // revn/n3
      true, // revn/n4
      true, // revn/nR
      true, // rot
      true, // saver/%rR
      true, // sconc
      true, // sel
      true, // setr/%rR
      true, // siz
      true, // sleep
      true, // smodi
      true, // spropc
      true, // sproph
      true, // sprops
      true, // sref
      true, // srefi
      true, // srefia
      true, // srefio
      true, // srefmnt
      true, // srefnt
      true, // srefo
      true, // sset
      true, // sseti
      false, // strace/nR/retR
      true, // strref
      true, // strset
      true, // subi
      true, // subiu
      true, // subl
      true, // sublu
      true, // substr
      true, // swap
      true, // swapgti
      true, // swapgtiu
      true, // swapgtl
      true, // swapgtlu
      true, // sync
      true, // time
      true, // tor
      true, // tuck
      true, // tyagetb
      true, // tyagett
      true, // tyisc
      true, // tyissct
      true, // typof
      true, // tysctn
      true, // unmap
      true, // unreachable
      true, // ureloc
      true, // write/retR
      false, // !REPLACEMENT-addi/retR
      false, // !REPLACEMENT-addl/retR
      false, // !REPLACEMENT-ains/retR
      false, // !REPLACEMENT-aref/retR
      false, // !REPLACEMENT-arefo/retR
      false, // !REPLACEMENT-arem/retR
      false, // !REPLACEMENT-aset/retR
      false, // !REPLACEMENT-ba/fR/retR
      false, // !REPLACEMENT-bn/fR/retR
      false, // !REPLACEMENT-bnn/fR/retR
      false, // !REPLACEMENT-bnzi/fR/retR
      false, // !REPLACEMENT-bnziu/fR/retR
      false, // !REPLACEMENT-bnzl/fR/retR
      false, // !REPLACEMENT-bnzlu/fR/retR
      false, // !REPLACEMENT-bsli/retR
      false, // !REPLACEMENT-bsliu/retR
      false, // !REPLACEMENT-bsll/retR
      false, // !REPLACEMENT-bsllu/retR
      false, // !REPLACEMENT-bzi/fR/retR
      false, // !REPLACEMENT-bziu/fR/retR
      false, // !REPLACEMENT-bzl/fR/retR
      false, // !REPLACEMENT-bzlu/fR/retR
      false, // !REPLACEMENT-call/retR
      false, // !REPLACEMENT-close/retR
      false, // !REPLACEMENT-divi/retR
      false, // !REPLACEMENT-diviu/retR
      false, // !REPLACEMENT-divl/retR
      false, // !REPLACEMENT-divlu/retR
      false, // !REPLACEMENT-endhl/retR
      false, // !REPLACEMENT-endsc/retR
      false, // !REPLACEMENT-exit/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-flush/retR
      false, // !REPLACEMENT-ioflags/retR
      false, // !REPLACEMENT-iogetb/retR
      false, // !REPLACEMENT-iosetb/retR
      false, // !REPLACEMENT-iosize/retR
      false, // !REPLACEMENT-map/retR
      false, // !REPLACEMENT-modi/retR
      false, // !REPLACEMENT-modiu/retR
      false, // !REPLACEMENT-modl/retR
      false, // !REPLACEMENT-modlu/retR
      false, // !REPLACEMENT-muli/retR
      false, // !REPLACEMENT-mull/retR
      false, // !REPLACEMENT-negi/retR
      false, // !REPLACEMENT-negiu/retR
      false, // !REPLACEMENT-negl/retR
      false, // !REPLACEMENT-neglu/retR
      false, // !REPLACEMENT-open/retR
      false, // !REPLACEMENT-peekdi/nR/retR
      false, // !REPLACEMENT-peekdiu/nR/retR
      false, // !REPLACEMENT-peekdl/nR/retR
      false, // !REPLACEMENT-peekdlu/nR/retR
      false, // !REPLACEMENT-peeki/nR/nR/nR/retR
      false, // !REPLACEMENT-peekiu/nR/nR/retR
      false, // !REPLACEMENT-peekl/nR/nR/nR/retR
      false, // !REPLACEMENT-peeklu/nR/nR/retR
      false, // !REPLACEMENT-peeks/retR
      false, // !REPLACEMENT-pokedi/nR/retR
      false, // !REPLACEMENT-pokediu/nR/retR
      false, // !REPLACEMENT-pokedl/nR/retR
      false, // !REPLACEMENT-pokedlu/nR/retR
      false, // !REPLACEMENT-pokei/nR/nR/nR/retR
      false, // !REPLACEMENT-pokeiu/nR/nR/retR
      false, // !REPLACEMENT-pokel/nR/nR/nR/retR
      false, // !REPLACEMENT-pokelu/nR/nR/retR
      false, // !REPLACEMENT-pokes/retR
      false, // !REPLACEMENT-popios/retR
      false, // !REPLACEMENT-popob/retR
      false, // !REPLACEMENT-popom/retR
      false, // !REPLACEMENT-powi/retR
      false, // !REPLACEMENT-powiu/retR
      false, // !REPLACEMENT-powl/retR
      false, // !REPLACEMENT-powlu/retR
      false, // !REPLACEMENT-prolog/retR
      false, // !REPLACEMENT-pushios/retR
      false, // !REPLACEMENT-pushtopvar/nR/retR
      false, // !REPLACEMENT-raise/retR
      false, // !REPLACEMENT-reloc/retR
      false, // !REPLACEMENT-return/retR
      false, // !REPLACEMENT-sleep/retR
      false, // !REPLACEMENT-smodi/retR
      false, // !REPLACEMENT-sref/retR
      false, // !REPLACEMENT-srefi/retR
      false, // !REPLACEMENT-srefia/retR
      false, // !REPLACEMENT-srefio/retR
      false, // !REPLACEMENT-srefo/retR
      false, // !REPLACEMENT-sset/retR
      false, // !REPLACEMENT-sseti/retR
      false, // !REPLACEMENT-strref/retR
      false, // !REPLACEMENT-strset/retR
      false, // !REPLACEMENT-subi/retR
      false, // !REPLACEMENT-subl/retR
      false, // !REPLACEMENT-substr/retR
      false, // !REPLACEMENT-sync/retR
      false, // !REPLACEMENT-unreachable/retR
      false, // !REPLACEMENT-ureloc/retR
      false // !REPLACEMENT-write/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
pvm_specialized_instruction_callers [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // addi
      false, // addiu
      false, // addl
      false, // addlu
      false, // ains
      false, // and
      false, // aref
      false, // arefo
      false, // arem
      false, // aset
      false, // asettb
      false, // atr
      false, // ba/fR
      false, // bandi
      false, // bandiu
      false, // bandl
      false, // bandlu
      false, // beghl/retR
      false, // begsc/retR
      false, // bn/fR
      false, // bnn/fR
      false, // bnoti
      false, // bnotiu
      false, // bnotl
      false, // bnotlu
      false, // bnzi/fR
      false, // bnziu/fR
      false, // bnzl/fR
      false, // bnzlu/fR
      false, // bori
      false, // boriu
      false, // borl
      false, // borlu
      false, // bsli
      false, // bsliu
      false, // bsll
      false, // bsllu
      false, // bsri
      false, // bsriu
      false, // bsrl
      false, // bsrlu
      false, // bxori
      false, // bxoriu
      false, // bxorl
      false, // bxorlu
      false, // bzi/fR
      false, // bziu/fR
      false, // bzl/fR
      false, // bzlu/fR
      true, // call/retR
      false, // canary
      false, // close
      false, // ctos
      false, // disas/retR
      false, // divi/retR
      false, // diviu/retR
      false, // divl/retR
      false, // divlu/retR
      false, // drop
      false, // drop2
      false, // drop3
      false, // drop4
      false, // duc
      false, // dup
      false, // endhl/retR
      false, // endsc/retR
      false, // eqc
      false, // eqi
      false, // eqiu
      false, // eql
      false, // eqlu
      false, // eqs
      false, // exit
      false, // exitvm
      false, // flush
      false, // formati/nR/retR
      false, // formatiu/nR/retR
      false, // formatl/nR/retR
      false, // formatlu/nR/retR
      false, // fromr
      false, // gei
      false, // geiu
      false, // gel
      false, // gelu
      false, // ges
      false, // getenv
      false, // gti
      false, // gtiu
      false, // gtl
      false, // gtlu
      false, // gts
      false, // indent/retR
      false, // ioflags
      false, // iogetb/retR
      false, // iosetb
      false, // iosize
      false, // isa
      false, // itoi/nR
      false, // itoiu/nR
      false, // itol/nR
      false, // itolu/nR
      false, // iutoi/nR
      false, // iutoiu/nR
      false, // iutol/nR
      false, // iutolu/nR
      false, // lei
      false, // leiu
      false, // lel
      false, // lelu
      false, // les
      false, // lti
      false, // ltiu
      false, // ltl
      false, // ltlu
      false, // ltoi/nR
      false, // ltoiu/nR
      false, // ltol/nR
      false, // ltolu/nR
      false, // lts
      false, // lutoi/nR
      false, // lutoiu/nR
      false, // lutol/nR
      false, // lutolu/nR
      false, // map
      false, // mgetios
      false, // mgetm
      false, // mgeto
      false, // mgets
      false, // mgetsel
      false, // mgetsiz
      false, // mgetw
      false, // mka
      false, // mko
      false, // mksct
      false, // mktya
      false, // mktyany
      false, // mktyc
      false, // mktyi
      false, // mktyo
      false, // mktys
      false, // mktysct
      false, // mktyv
      false, // mm
      false, // modi/retR
      false, // modiu/retR
      false, // modl/retR
      false, // modlu/retR
      false, // msetios
      false, // msetm
      false, // mseto
      false, // msets
      false, // msetsel
      false, // msetsiz
      false, // msetw
      false, // muli
      false, // muliu
      false, // mull
      false, // mullu
      false, // muls
      false, // nec
      false, // negi
      false, // negiu
      false, // negl
      false, // neglu
      false, // nei
      false, // neiu
      false, // nel
      false, // nelu
      false, // nes
      false, // nip
      false, // nip2
      false, // nip3
      false, // nn
      false, // nnn
      false, // nop
      false, // not
      false, // note/nR
      false, // nrot
      false, // ogetbt
      false, // ogetm
      false, // ogetu
      false, // open
      false, // or
      false, // osetm
      false, // over
      false, // pec
      false, // peekdi/nR
      false, // peekdiu/nR
      false, // peekdl/nR
      false, // peekdlu/nR
      false, // peeki/nR/nR/nR
      false, // peekiu/nR/nR
      false, // peekl/nR/nR/nR
      false, // peeklu/nR/nR
      false, // peeks
      false, // pokedi/nR
      false, // pokediu/nR
      false, // pokedl/nR
      false, // pokedlu/nR
      false, // pokei/nR/nR/nR
      false, // pokeiu/nR/nR
      false, // pokel/nR/nR/nR
      false, // pokelu/nR/nR
      false, // pokes
      false, // pope
      false, // popend
      false, // popexite
      false, // popf/nR
      false, // popios
      false, // popoac
      false, // popob
      false, // popobc
      false, // popoc
      false, // popod
      false, // popoi
      false, // popom
      false, // popoo
      false, // popopp
      false, // popr/%rR
      false, // popvar/nR/nR
      false, // powi
      false, // powiu
      false, // powl
      false, // powlu
      false, // printi/nR/retR
      false, // printiu/nR/retR
      false, // printl/nR/retR
      false, // printlu/nR/retR
      false, // prints/retR
      false, // prolog
      false, // push/nR
      false, // push/lR
      false, // push32/nR
      false, // push32/lR
      false, // pushe/lR
      false, // pushend
      false, // pushf/nR
      false, // pushhi/nR
      false, // pushhi/lR
      false, // pushios
      false, // pushlo/nR
      false, // pushlo/lR
      false, // pushoac
      false, // pushob
      false, // pushobc
      false, // pushoc
      false, // pushod
      false, // pushoi
      false, // pushom
      false, // pushoo
      false, // pushopp
      false, // pushr/%rR
      false, // pushtopvar/nR
      false, // pushvar/n0/n0
      false, // pushvar/n0/n1
      false, // pushvar/n0/n2
      false, // pushvar/n0/n3
      false, // pushvar/n0/n4
      false, // pushvar/n0/n5
      false, // pushvar/n0/nR
      false, // pushvar/nR/n0
      false, // pushvar/nR/n1
      false, // pushvar/nR/n2
      false, // pushvar/nR/n3
      false, // pushvar/nR/n4
      false, // pushvar/nR/n5
      false, // pushvar/nR/nR
      false, // quake
      false, // raise
      false, // rand
      false, // regvar
      false, // reloc
      false, // restorer/%rR
      false, // return
      false, // revn/n3
      false, // revn/n4
      false, // revn/nR
      false, // rot
      false, // saver/%rR
      false, // sconc
      false, // sel
      false, // setr/%rR
      false, // siz
      false, // sleep
      false, // smodi
      false, // spropc
      false, // sproph
      false, // sprops
      false, // sref
      false, // srefi
      false, // srefia
      false, // srefio
      false, // srefmnt
      false, // srefnt
      false, // srefo
      false, // sset
      false, // sseti
      false, // strace/nR/retR
      false, // strref
      false, // strset
      false, // subi
      false, // subiu
      false, // subl
      false, // sublu
      false, // substr
      false, // swap
      false, // swapgti
      false, // swapgtiu
      false, // swapgtl
      false, // swapgtlu
      false, // sync
      false, // time
      false, // tor
      false, // tuck
      false, // tyagetb
      false, // tyagett
      false, // tyisc
      false, // tyissct
      false, // typof
      false, // tysctn
      false, // unmap
      false, // unreachable
      false, // ureloc
      true, // write/retR
      false, // !REPLACEMENT-addi/retR
      false, // !REPLACEMENT-addl/retR
      false, // !REPLACEMENT-ains/retR
      false, // !REPLACEMENT-aref/retR
      false, // !REPLACEMENT-arefo/retR
      false, // !REPLACEMENT-arem/retR
      false, // !REPLACEMENT-aset/retR
      false, // !REPLACEMENT-ba/fR/retR
      false, // !REPLACEMENT-bn/fR/retR
      false, // !REPLACEMENT-bnn/fR/retR
      false, // !REPLACEMENT-bnzi/fR/retR
      false, // !REPLACEMENT-bnziu/fR/retR
      false, // !REPLACEMENT-bnzl/fR/retR
      false, // !REPLACEMENT-bnzlu/fR/retR
      false, // !REPLACEMENT-bsli/retR
      false, // !REPLACEMENT-bsliu/retR
      false, // !REPLACEMENT-bsll/retR
      false, // !REPLACEMENT-bsllu/retR
      false, // !REPLACEMENT-bzi/fR/retR
      false, // !REPLACEMENT-bziu/fR/retR
      false, // !REPLACEMENT-bzl/fR/retR
      false, // !REPLACEMENT-bzlu/fR/retR
      true, // !REPLACEMENT-call/retR
      false, // !REPLACEMENT-close/retR
      false, // !REPLACEMENT-divi/retR
      false, // !REPLACEMENT-diviu/retR
      false, // !REPLACEMENT-divl/retR
      false, // !REPLACEMENT-divlu/retR
      false, // !REPLACEMENT-endhl/retR
      false, // !REPLACEMENT-endsc/retR
      false, // !REPLACEMENT-exit/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-flush/retR
      false, // !REPLACEMENT-ioflags/retR
      false, // !REPLACEMENT-iogetb/retR
      false, // !REPLACEMENT-iosetb/retR
      false, // !REPLACEMENT-iosize/retR
      false, // !REPLACEMENT-map/retR
      false, // !REPLACEMENT-modi/retR
      false, // !REPLACEMENT-modiu/retR
      false, // !REPLACEMENT-modl/retR
      false, // !REPLACEMENT-modlu/retR
      false, // !REPLACEMENT-muli/retR
      false, // !REPLACEMENT-mull/retR
      false, // !REPLACEMENT-negi/retR
      false, // !REPLACEMENT-negiu/retR
      false, // !REPLACEMENT-negl/retR
      false, // !REPLACEMENT-neglu/retR
      false, // !REPLACEMENT-open/retR
      false, // !REPLACEMENT-peekdi/nR/retR
      false, // !REPLACEMENT-peekdiu/nR/retR
      false, // !REPLACEMENT-peekdl/nR/retR
      false, // !REPLACEMENT-peekdlu/nR/retR
      false, // !REPLACEMENT-peeki/nR/nR/nR/retR
      false, // !REPLACEMENT-peekiu/nR/nR/retR
      false, // !REPLACEMENT-peekl/nR/nR/nR/retR
      false, // !REPLACEMENT-peeklu/nR/nR/retR
      false, // !REPLACEMENT-peeks/retR
      false, // !REPLACEMENT-pokedi/nR/retR
      false, // !REPLACEMENT-pokediu/nR/retR
      false, // !REPLACEMENT-pokedl/nR/retR
      false, // !REPLACEMENT-pokedlu/nR/retR
      false, // !REPLACEMENT-pokei/nR/nR/nR/retR
      false, // !REPLACEMENT-pokeiu/nR/nR/retR
      false, // !REPLACEMENT-pokel/nR/nR/nR/retR
      false, // !REPLACEMENT-pokelu/nR/nR/retR
      false, // !REPLACEMENT-pokes/retR
      false, // !REPLACEMENT-popios/retR
      false, // !REPLACEMENT-popob/retR
      false, // !REPLACEMENT-popom/retR
      false, // !REPLACEMENT-powi/retR
      false, // !REPLACEMENT-powiu/retR
      false, // !REPLACEMENT-powl/retR
      false, // !REPLACEMENT-powlu/retR
      false, // !REPLACEMENT-prolog/retR
      false, // !REPLACEMENT-pushios/retR
      false, // !REPLACEMENT-pushtopvar/nR/retR
      false, // !REPLACEMENT-raise/retR
      false, // !REPLACEMENT-reloc/retR
      false, // !REPLACEMENT-return/retR
      false, // !REPLACEMENT-sleep/retR
      false, // !REPLACEMENT-smodi/retR
      false, // !REPLACEMENT-sref/retR
      false, // !REPLACEMENT-srefi/retR
      false, // !REPLACEMENT-srefia/retR
      false, // !REPLACEMENT-srefio/retR
      false, // !REPLACEMENT-srefo/retR
      false, // !REPLACEMENT-sset/retR
      false, // !REPLACEMENT-sseti/retR
      false, // !REPLACEMENT-strref/retR
      false, // !REPLACEMENT-strset/retR
      false, // !REPLACEMENT-subi/retR
      false, // !REPLACEMENT-subl/retR
      false, // !REPLACEMENT-substr/retR
      false, // !REPLACEMENT-sync/retR
      false, // !REPLACEMENT-unreachable/retR
      false, // !REPLACEMENT-ureloc/retR
      true // !REPLACEMENT-write/retR
    };

// FIXME: this is not currently accessed, and in fact may be useless.
const bool
pvm_specialized_instruction_callees [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
      false, // !INVALID
      false, // !BEGINBASICBLOCK
      false, // !EXITVM
      false, // !DATALOCATIONS
      false, // !NOP
      false, // !UNREACHABLE0
      false, // !UNREACHABLE1
      false, // !PRETENDTOJUMPANYWHERE
      false, // addi
      false, // addiu
      false, // addl
      false, // addlu
      false, // ains
      false, // and
      false, // aref
      false, // arefo
      false, // arem
      false, // aset
      false, // asettb
      false, // atr
      false, // ba/fR
      false, // bandi
      false, // bandiu
      false, // bandl
      false, // bandlu
      false, // beghl/retR
      false, // begsc/retR
      false, // bn/fR
      false, // bnn/fR
      false, // bnoti
      false, // bnotiu
      false, // bnotl
      false, // bnotlu
      false, // bnzi/fR
      false, // bnziu/fR
      false, // bnzl/fR
      false, // bnzlu/fR
      false, // bori
      false, // boriu
      false, // borl
      false, // borlu
      false, // bsli
      false, // bsliu
      false, // bsll
      false, // bsllu
      false, // bsri
      false, // bsriu
      false, // bsrl
      false, // bsrlu
      false, // bxori
      false, // bxoriu
      false, // bxorl
      false, // bxorlu
      false, // bzi/fR
      false, // bziu/fR
      false, // bzl/fR
      false, // bzlu/fR
      false, // call/retR
      false, // canary
      false, // close
      false, // ctos
      false, // disas/retR
      false, // divi/retR
      false, // diviu/retR
      false, // divl/retR
      false, // divlu/retR
      false, // drop
      false, // drop2
      false, // drop3
      false, // drop4
      false, // duc
      false, // dup
      false, // endhl/retR
      false, // endsc/retR
      false, // eqc
      false, // eqi
      false, // eqiu
      false, // eql
      false, // eqlu
      false, // eqs
      false, // exit
      false, // exitvm
      false, // flush
      false, // formati/nR/retR
      false, // formatiu/nR/retR
      false, // formatl/nR/retR
      false, // formatlu/nR/retR
      false, // fromr
      false, // gei
      false, // geiu
      false, // gel
      false, // gelu
      false, // ges
      false, // getenv
      false, // gti
      false, // gtiu
      false, // gtl
      false, // gtlu
      false, // gts
      false, // indent/retR
      false, // ioflags
      false, // iogetb/retR
      false, // iosetb
      false, // iosize
      false, // isa
      false, // itoi/nR
      false, // itoiu/nR
      false, // itol/nR
      false, // itolu/nR
      false, // iutoi/nR
      false, // iutoiu/nR
      false, // iutol/nR
      false, // iutolu/nR
      false, // lei
      false, // leiu
      false, // lel
      false, // lelu
      false, // les
      false, // lti
      false, // ltiu
      false, // ltl
      false, // ltlu
      false, // ltoi/nR
      false, // ltoiu/nR
      false, // ltol/nR
      false, // ltolu/nR
      false, // lts
      false, // lutoi/nR
      false, // lutoiu/nR
      false, // lutol/nR
      false, // lutolu/nR
      false, // map
      false, // mgetios
      false, // mgetm
      false, // mgeto
      false, // mgets
      false, // mgetsel
      false, // mgetsiz
      false, // mgetw
      false, // mka
      false, // mko
      false, // mksct
      false, // mktya
      false, // mktyany
      false, // mktyc
      false, // mktyi
      false, // mktyo
      false, // mktys
      false, // mktysct
      false, // mktyv
      false, // mm
      false, // modi/retR
      false, // modiu/retR
      false, // modl/retR
      false, // modlu/retR
      false, // msetios
      false, // msetm
      false, // mseto
      false, // msets
      false, // msetsel
      false, // msetsiz
      false, // msetw
      false, // muli
      false, // muliu
      false, // mull
      false, // mullu
      false, // muls
      false, // nec
      false, // negi
      false, // negiu
      false, // negl
      false, // neglu
      false, // nei
      false, // neiu
      false, // nel
      false, // nelu
      false, // nes
      false, // nip
      false, // nip2
      false, // nip3
      false, // nn
      false, // nnn
      false, // nop
      false, // not
      false, // note/nR
      false, // nrot
      false, // ogetbt
      false, // ogetm
      false, // ogetu
      false, // open
      false, // or
      false, // osetm
      false, // over
      false, // pec
      false, // peekdi/nR
      false, // peekdiu/nR
      false, // peekdl/nR
      false, // peekdlu/nR
      false, // peeki/nR/nR/nR
      false, // peekiu/nR/nR
      false, // peekl/nR/nR/nR
      false, // peeklu/nR/nR
      false, // peeks
      false, // pokedi/nR
      false, // pokediu/nR
      false, // pokedl/nR
      false, // pokedlu/nR
      false, // pokei/nR/nR/nR
      false, // pokeiu/nR/nR
      false, // pokel/nR/nR/nR
      false, // pokelu/nR/nR
      false, // pokes
      false, // pope
      false, // popend
      false, // popexite
      false, // popf/nR
      false, // popios
      false, // popoac
      false, // popob
      false, // popobc
      false, // popoc
      false, // popod
      false, // popoi
      false, // popom
      false, // popoo
      false, // popopp
      false, // popr/%rR
      false, // popvar/nR/nR
      false, // powi
      false, // powiu
      false, // powl
      false, // powlu
      false, // printi/nR/retR
      false, // printiu/nR/retR
      false, // printl/nR/retR
      false, // printlu/nR/retR
      false, // prints/retR
      true, // prolog
      false, // push/nR
      false, // push/lR
      false, // push32/nR
      false, // push32/lR
      false, // pushe/lR
      false, // pushend
      false, // pushf/nR
      false, // pushhi/nR
      false, // pushhi/lR
      false, // pushios
      false, // pushlo/nR
      false, // pushlo/lR
      false, // pushoac
      false, // pushob
      false, // pushobc
      false, // pushoc
      false, // pushod
      false, // pushoi
      false, // pushom
      false, // pushoo
      false, // pushopp
      false, // pushr/%rR
      false, // pushtopvar/nR
      false, // pushvar/n0/n0
      false, // pushvar/n0/n1
      false, // pushvar/n0/n2
      false, // pushvar/n0/n3
      false, // pushvar/n0/n4
      false, // pushvar/n0/n5
      false, // pushvar/n0/nR
      false, // pushvar/nR/n0
      false, // pushvar/nR/n1
      false, // pushvar/nR/n2
      false, // pushvar/nR/n3
      false, // pushvar/nR/n4
      false, // pushvar/nR/n5
      false, // pushvar/nR/nR
      false, // quake
      false, // raise
      false, // rand
      false, // regvar
      false, // reloc
      false, // restorer/%rR
      false, // return
      false, // revn/n3
      false, // revn/n4
      false, // revn/nR
      false, // rot
      false, // saver/%rR
      false, // sconc
      false, // sel
      false, // setr/%rR
      false, // siz
      false, // sleep
      false, // smodi
      false, // spropc
      false, // sproph
      false, // sprops
      false, // sref
      false, // srefi
      false, // srefia
      false, // srefio
      false, // srefmnt
      false, // srefnt
      false, // srefo
      false, // sset
      false, // sseti
      false, // strace/nR/retR
      false, // strref
      false, // strset
      false, // subi
      false, // subiu
      false, // subl
      false, // sublu
      false, // substr
      false, // swap
      false, // swapgti
      false, // swapgtiu
      false, // swapgtl
      false, // swapgtlu
      false, // sync
      false, // time
      false, // tor
      false, // tuck
      false, // tyagetb
      false, // tyagett
      false, // tyisc
      false, // tyissct
      false, // typof
      false, // tysctn
      false, // unmap
      false, // unreachable
      false, // ureloc
      false, // write/retR
      false, // !REPLACEMENT-addi/retR
      false, // !REPLACEMENT-addl/retR
      false, // !REPLACEMENT-ains/retR
      false, // !REPLACEMENT-aref/retR
      false, // !REPLACEMENT-arefo/retR
      false, // !REPLACEMENT-arem/retR
      false, // !REPLACEMENT-aset/retR
      false, // !REPLACEMENT-ba/fR/retR
      false, // !REPLACEMENT-bn/fR/retR
      false, // !REPLACEMENT-bnn/fR/retR
      false, // !REPLACEMENT-bnzi/fR/retR
      false, // !REPLACEMENT-bnziu/fR/retR
      false, // !REPLACEMENT-bnzl/fR/retR
      false, // !REPLACEMENT-bnzlu/fR/retR
      false, // !REPLACEMENT-bsli/retR
      false, // !REPLACEMENT-bsliu/retR
      false, // !REPLACEMENT-bsll/retR
      false, // !REPLACEMENT-bsllu/retR
      false, // !REPLACEMENT-bzi/fR/retR
      false, // !REPLACEMENT-bziu/fR/retR
      false, // !REPLACEMENT-bzl/fR/retR
      false, // !REPLACEMENT-bzlu/fR/retR
      false, // !REPLACEMENT-call/retR
      false, // !REPLACEMENT-close/retR
      false, // !REPLACEMENT-divi/retR
      false, // !REPLACEMENT-diviu/retR
      false, // !REPLACEMENT-divl/retR
      false, // !REPLACEMENT-divlu/retR
      false, // !REPLACEMENT-endhl/retR
      false, // !REPLACEMENT-endsc/retR
      false, // !REPLACEMENT-exit/retR
      false, // !REPLACEMENT-exitvm/retR
      false, // !REPLACEMENT-flush/retR
      false, // !REPLACEMENT-ioflags/retR
      false, // !REPLACEMENT-iogetb/retR
      false, // !REPLACEMENT-iosetb/retR
      false, // !REPLACEMENT-iosize/retR
      false, // !REPLACEMENT-map/retR
      false, // !REPLACEMENT-modi/retR
      false, // !REPLACEMENT-modiu/retR
      false, // !REPLACEMENT-modl/retR
      false, // !REPLACEMENT-modlu/retR
      false, // !REPLACEMENT-muli/retR
      false, // !REPLACEMENT-mull/retR
      false, // !REPLACEMENT-negi/retR
      false, // !REPLACEMENT-negiu/retR
      false, // !REPLACEMENT-negl/retR
      false, // !REPLACEMENT-neglu/retR
      false, // !REPLACEMENT-open/retR
      false, // !REPLACEMENT-peekdi/nR/retR
      false, // !REPLACEMENT-peekdiu/nR/retR
      false, // !REPLACEMENT-peekdl/nR/retR
      false, // !REPLACEMENT-peekdlu/nR/retR
      false, // !REPLACEMENT-peeki/nR/nR/nR/retR
      false, // !REPLACEMENT-peekiu/nR/nR/retR
      false, // !REPLACEMENT-peekl/nR/nR/nR/retR
      false, // !REPLACEMENT-peeklu/nR/nR/retR
      false, // !REPLACEMENT-peeks/retR
      false, // !REPLACEMENT-pokedi/nR/retR
      false, // !REPLACEMENT-pokediu/nR/retR
      false, // !REPLACEMENT-pokedl/nR/retR
      false, // !REPLACEMENT-pokedlu/nR/retR
      false, // !REPLACEMENT-pokei/nR/nR/nR/retR
      false, // !REPLACEMENT-pokeiu/nR/nR/retR
      false, // !REPLACEMENT-pokel/nR/nR/nR/retR
      false, // !REPLACEMENT-pokelu/nR/nR/retR
      false, // !REPLACEMENT-pokes/retR
      false, // !REPLACEMENT-popios/retR
      false, // !REPLACEMENT-popob/retR
      false, // !REPLACEMENT-popom/retR
      false, // !REPLACEMENT-powi/retR
      false, // !REPLACEMENT-powiu/retR
      false, // !REPLACEMENT-powl/retR
      false, // !REPLACEMENT-powlu/retR
      true, // !REPLACEMENT-prolog/retR
      false, // !REPLACEMENT-pushios/retR
      false, // !REPLACEMENT-pushtopvar/nR/retR
      false, // !REPLACEMENT-raise/retR
      false, // !REPLACEMENT-reloc/retR
      false, // !REPLACEMENT-return/retR
      false, // !REPLACEMENT-sleep/retR
      false, // !REPLACEMENT-smodi/retR
      false, // !REPLACEMENT-sref/retR
      false, // !REPLACEMENT-srefi/retR
      false, // !REPLACEMENT-srefia/retR
      false, // !REPLACEMENT-srefio/retR
      false, // !REPLACEMENT-srefo/retR
      false, // !REPLACEMENT-sset/retR
      false, // !REPLACEMENT-sseti/retR
      false, // !REPLACEMENT-strref/retR
      false, // !REPLACEMENT-strset/retR
      false, // !REPLACEMENT-subi/retR
      false, // !REPLACEMENT-subl/retR
      false, // !REPLACEMENT-substr/retR
      false, // !REPLACEMENT-sync/retR
      false, // !REPLACEMENT-unreachable/retR
      false, // !REPLACEMENT-ureloc/retR
      false // !REPLACEMENT-write/retR
    };

/* An array whose indices are specialised instruction opcodes, and
   whose elements are the corresponding unspecialised instructions
   opcodes -- or -1 when there is no mapping mapping having */
const int
pvm_specialized_instruction_to_unspecialized_instruction
   [PVM_SPECIALIZED_INSTRUCTION_NO]
  = {
    -1, /* !INVALID */
    -1, /* !BEGINBASICBLOCK */
    -1, /* !EXITVM */
    -1, /* !DATALOCATIONS */
    -1, /* !NOP */
    -1, /* !UNREACHABLE0 */
    -1, /* !UNREACHABLE1 */
    -1, /* !PRETENDTOJUMPANYWHERE */
    pvm_meta_instruction_id_addi, /* addi */
    pvm_meta_instruction_id_addiu, /* addiu */
    pvm_meta_instruction_id_addl, /* addl */
    pvm_meta_instruction_id_addlu, /* addlu */
    pvm_meta_instruction_id_ains, /* ains */
    pvm_meta_instruction_id_and, /* and */
    pvm_meta_instruction_id_aref, /* aref */
    pvm_meta_instruction_id_arefo, /* arefo */
    pvm_meta_instruction_id_arem, /* arem */
    pvm_meta_instruction_id_aset, /* aset */
    pvm_meta_instruction_id_asettb, /* asettb */
    pvm_meta_instruction_id_atr, /* atr */
    pvm_meta_instruction_id_ba, /* ba/fR */
    pvm_meta_instruction_id_bandi, /* bandi */
    pvm_meta_instruction_id_bandiu, /* bandiu */
    pvm_meta_instruction_id_bandl, /* bandl */
    pvm_meta_instruction_id_bandlu, /* bandlu */
    pvm_meta_instruction_id_beghl, /* beghl/retR */
    pvm_meta_instruction_id_begsc, /* begsc/retR */
    pvm_meta_instruction_id_bn, /* bn/fR */
    pvm_meta_instruction_id_bnn, /* bnn/fR */
    pvm_meta_instruction_id_bnoti, /* bnoti */
    pvm_meta_instruction_id_bnotiu, /* bnotiu */
    pvm_meta_instruction_id_bnotl, /* bnotl */
    pvm_meta_instruction_id_bnotlu, /* bnotlu */
    pvm_meta_instruction_id_bnzi, /* bnzi/fR */
    pvm_meta_instruction_id_bnziu, /* bnziu/fR */
    pvm_meta_instruction_id_bnzl, /* bnzl/fR */
    pvm_meta_instruction_id_bnzlu, /* bnzlu/fR */
    pvm_meta_instruction_id_bori, /* bori */
    pvm_meta_instruction_id_boriu, /* boriu */
    pvm_meta_instruction_id_borl, /* borl */
    pvm_meta_instruction_id_borlu, /* borlu */
    pvm_meta_instruction_id_bsli, /* bsli */
    pvm_meta_instruction_id_bsliu, /* bsliu */
    pvm_meta_instruction_id_bsll, /* bsll */
    pvm_meta_instruction_id_bsllu, /* bsllu */
    pvm_meta_instruction_id_bsri, /* bsri */
    pvm_meta_instruction_id_bsriu, /* bsriu */
    pvm_meta_instruction_id_bsrl, /* bsrl */
    pvm_meta_instruction_id_bsrlu, /* bsrlu */
    pvm_meta_instruction_id_bxori, /* bxori */
    pvm_meta_instruction_id_bxoriu, /* bxoriu */
    pvm_meta_instruction_id_bxorl, /* bxorl */
    pvm_meta_instruction_id_bxorlu, /* bxorlu */
    pvm_meta_instruction_id_bzi, /* bzi/fR */
    pvm_meta_instruction_id_bziu, /* bziu/fR */
    pvm_meta_instruction_id_bzl, /* bzl/fR */
    pvm_meta_instruction_id_bzlu, /* bzlu/fR */
    pvm_meta_instruction_id_call, /* call/retR */
    pvm_meta_instruction_id_canary, /* canary */
    pvm_meta_instruction_id_close, /* close */
    pvm_meta_instruction_id_ctos, /* ctos */
    pvm_meta_instruction_id_disas, /* disas/retR */
    pvm_meta_instruction_id_divi, /* divi/retR */
    pvm_meta_instruction_id_diviu, /* diviu/retR */
    pvm_meta_instruction_id_divl, /* divl/retR */
    pvm_meta_instruction_id_divlu, /* divlu/retR */
    pvm_meta_instruction_id_drop, /* drop */
    pvm_meta_instruction_id_drop2, /* drop2 */
    pvm_meta_instruction_id_drop3, /* drop3 */
    pvm_meta_instruction_id_drop4, /* drop4 */
    pvm_meta_instruction_id_duc, /* duc */
    pvm_meta_instruction_id_dup, /* dup */
    pvm_meta_instruction_id_endhl, /* endhl/retR */
    pvm_meta_instruction_id_endsc, /* endsc/retR */
    pvm_meta_instruction_id_eqc, /* eqc */
    pvm_meta_instruction_id_eqi, /* eqi */
    pvm_meta_instruction_id_eqiu, /* eqiu */
    pvm_meta_instruction_id_eql, /* eql */
    pvm_meta_instruction_id_eqlu, /* eqlu */
    pvm_meta_instruction_id_eqs, /* eqs */
    pvm_meta_instruction_id_exit, /* exit */
    pvm_meta_instruction_id_exitvm, /* exitvm */
    pvm_meta_instruction_id_flush, /* flush */
    pvm_meta_instruction_id_formati, /* formati/nR/retR */
    pvm_meta_instruction_id_formatiu, /* formatiu/nR/retR */
    pvm_meta_instruction_id_formatl, /* formatl/nR/retR */
    pvm_meta_instruction_id_formatlu, /* formatlu/nR/retR */
    pvm_meta_instruction_id_fromr, /* fromr */
    pvm_meta_instruction_id_gei, /* gei */
    pvm_meta_instruction_id_geiu, /* geiu */
    pvm_meta_instruction_id_gel, /* gel */
    pvm_meta_instruction_id_gelu, /* gelu */
    pvm_meta_instruction_id_ges, /* ges */
    pvm_meta_instruction_id_getenv, /* getenv */
    pvm_meta_instruction_id_gti, /* gti */
    pvm_meta_instruction_id_gtiu, /* gtiu */
    pvm_meta_instruction_id_gtl, /* gtl */
    pvm_meta_instruction_id_gtlu, /* gtlu */
    pvm_meta_instruction_id_gts, /* gts */
    pvm_meta_instruction_id_indent, /* indent/retR */
    pvm_meta_instruction_id_ioflags, /* ioflags */
    pvm_meta_instruction_id_iogetb, /* iogetb/retR */
    pvm_meta_instruction_id_iosetb, /* iosetb */
    pvm_meta_instruction_id_iosize, /* iosize */
    pvm_meta_instruction_id_isa, /* isa */
    pvm_meta_instruction_id_itoi, /* itoi/nR */
    pvm_meta_instruction_id_itoiu, /* itoiu/nR */
    pvm_meta_instruction_id_itol, /* itol/nR */
    pvm_meta_instruction_id_itolu, /* itolu/nR */
    pvm_meta_instruction_id_iutoi, /* iutoi/nR */
    pvm_meta_instruction_id_iutoiu, /* iutoiu/nR */
    pvm_meta_instruction_id_iutol, /* iutol/nR */
    pvm_meta_instruction_id_iutolu, /* iutolu/nR */
    pvm_meta_instruction_id_lei, /* lei */
    pvm_meta_instruction_id_leiu, /* leiu */
    pvm_meta_instruction_id_lel, /* lel */
    pvm_meta_instruction_id_lelu, /* lelu */
    pvm_meta_instruction_id_les, /* les */
    pvm_meta_instruction_id_lti, /* lti */
    pvm_meta_instruction_id_ltiu, /* ltiu */
    pvm_meta_instruction_id_ltl, /* ltl */
    pvm_meta_instruction_id_ltlu, /* ltlu */
    pvm_meta_instruction_id_ltoi, /* ltoi/nR */
    pvm_meta_instruction_id_ltoiu, /* ltoiu/nR */
    pvm_meta_instruction_id_ltol, /* ltol/nR */
    pvm_meta_instruction_id_ltolu, /* ltolu/nR */
    pvm_meta_instruction_id_lts, /* lts */
    pvm_meta_instruction_id_lutoi, /* lutoi/nR */
    pvm_meta_instruction_id_lutoiu, /* lutoiu/nR */
    pvm_meta_instruction_id_lutol, /* lutol/nR */
    pvm_meta_instruction_id_lutolu, /* lutolu/nR */
    pvm_meta_instruction_id_map, /* map */
    pvm_meta_instruction_id_mgetios, /* mgetios */
    pvm_meta_instruction_id_mgetm, /* mgetm */
    pvm_meta_instruction_id_mgeto, /* mgeto */
    pvm_meta_instruction_id_mgets, /* mgets */
    pvm_meta_instruction_id_mgetsel, /* mgetsel */
    pvm_meta_instruction_id_mgetsiz, /* mgetsiz */
    pvm_meta_instruction_id_mgetw, /* mgetw */
    pvm_meta_instruction_id_mka, /* mka */
    pvm_meta_instruction_id_mko, /* mko */
    pvm_meta_instruction_id_mksct, /* mksct */
    pvm_meta_instruction_id_mktya, /* mktya */
    pvm_meta_instruction_id_mktyany, /* mktyany */
    pvm_meta_instruction_id_mktyc, /* mktyc */
    pvm_meta_instruction_id_mktyi, /* mktyi */
    pvm_meta_instruction_id_mktyo, /* mktyo */
    pvm_meta_instruction_id_mktys, /* mktys */
    pvm_meta_instruction_id_mktysct, /* mktysct */
    pvm_meta_instruction_id_mktyv, /* mktyv */
    pvm_meta_instruction_id_mm, /* mm */
    pvm_meta_instruction_id_modi, /* modi/retR */
    pvm_meta_instruction_id_modiu, /* modiu/retR */
    pvm_meta_instruction_id_modl, /* modl/retR */
    pvm_meta_instruction_id_modlu, /* modlu/retR */
    pvm_meta_instruction_id_msetios, /* msetios */
    pvm_meta_instruction_id_msetm, /* msetm */
    pvm_meta_instruction_id_mseto, /* mseto */
    pvm_meta_instruction_id_msets, /* msets */
    pvm_meta_instruction_id_msetsel, /* msetsel */
    pvm_meta_instruction_id_msetsiz, /* msetsiz */
    pvm_meta_instruction_id_msetw, /* msetw */
    pvm_meta_instruction_id_muli, /* muli */
    pvm_meta_instruction_id_muliu, /* muliu */
    pvm_meta_instruction_id_mull, /* mull */
    pvm_meta_instruction_id_mullu, /* mullu */
    pvm_meta_instruction_id_muls, /* muls */
    pvm_meta_instruction_id_nec, /* nec */
    pvm_meta_instruction_id_negi, /* negi */
    pvm_meta_instruction_id_negiu, /* negiu */
    pvm_meta_instruction_id_negl, /* negl */
    pvm_meta_instruction_id_neglu, /* neglu */
    pvm_meta_instruction_id_nei, /* nei */
    pvm_meta_instruction_id_neiu, /* neiu */
    pvm_meta_instruction_id_nel, /* nel */
    pvm_meta_instruction_id_nelu, /* nelu */
    pvm_meta_instruction_id_nes, /* nes */
    pvm_meta_instruction_id_nip, /* nip */
    pvm_meta_instruction_id_nip2, /* nip2 */
    pvm_meta_instruction_id_nip3, /* nip3 */
    pvm_meta_instruction_id_nn, /* nn */
    pvm_meta_instruction_id_nnn, /* nnn */
    pvm_meta_instruction_id_nop, /* nop */
    pvm_meta_instruction_id_not, /* not */
    pvm_meta_instruction_id_note, /* note/nR */
    pvm_meta_instruction_id_nrot, /* nrot */
    pvm_meta_instruction_id_ogetbt, /* ogetbt */
    pvm_meta_instruction_id_ogetm, /* ogetm */
    pvm_meta_instruction_id_ogetu, /* ogetu */
    pvm_meta_instruction_id_open, /* open */
    pvm_meta_instruction_id_or, /* or */
    pvm_meta_instruction_id_osetm, /* osetm */
    pvm_meta_instruction_id_over, /* over */
    pvm_meta_instruction_id_pec, /* pec */
    pvm_meta_instruction_id_peekdi, /* peekdi/nR */
    pvm_meta_instruction_id_peekdiu, /* peekdiu/nR */
    pvm_meta_instruction_id_peekdl, /* peekdl/nR */
    pvm_meta_instruction_id_peekdlu, /* peekdlu/nR */
    pvm_meta_instruction_id_peeki, /* peeki/nR/nR/nR */
    pvm_meta_instruction_id_peekiu, /* peekiu/nR/nR */
    pvm_meta_instruction_id_peekl, /* peekl/nR/nR/nR */
    pvm_meta_instruction_id_peeklu, /* peeklu/nR/nR */
    pvm_meta_instruction_id_peeks, /* peeks */
    pvm_meta_instruction_id_pokedi, /* pokedi/nR */
    pvm_meta_instruction_id_pokediu, /* pokediu/nR */
    pvm_meta_instruction_id_pokedl, /* pokedl/nR */
    pvm_meta_instruction_id_pokedlu, /* pokedlu/nR */
    pvm_meta_instruction_id_pokei, /* pokei/nR/nR/nR */
    pvm_meta_instruction_id_pokeiu, /* pokeiu/nR/nR */
    pvm_meta_instruction_id_pokel, /* pokel/nR/nR/nR */
    pvm_meta_instruction_id_pokelu, /* pokelu/nR/nR */
    pvm_meta_instruction_id_pokes, /* pokes */
    pvm_meta_instruction_id_pope, /* pope */
    pvm_meta_instruction_id_popend, /* popend */
    pvm_meta_instruction_id_popexite, /* popexite */
    pvm_meta_instruction_id_popf, /* popf/nR */
    pvm_meta_instruction_id_popios, /* popios */
    pvm_meta_instruction_id_popoac, /* popoac */
    pvm_meta_instruction_id_popob, /* popob */
    pvm_meta_instruction_id_popobc, /* popobc */
    pvm_meta_instruction_id_popoc, /* popoc */
    pvm_meta_instruction_id_popod, /* popod */
    pvm_meta_instruction_id_popoi, /* popoi */
    pvm_meta_instruction_id_popom, /* popom */
    pvm_meta_instruction_id_popoo, /* popoo */
    pvm_meta_instruction_id_popopp, /* popopp */
    pvm_meta_instruction_id_popr, /* popr/%rR */
    pvm_meta_instruction_id_popvar, /* popvar/nR/nR */
    pvm_meta_instruction_id_powi, /* powi */
    pvm_meta_instruction_id_powiu, /* powiu */
    pvm_meta_instruction_id_powl, /* powl */
    pvm_meta_instruction_id_powlu, /* powlu */
    pvm_meta_instruction_id_printi, /* printi/nR/retR */
    pvm_meta_instruction_id_printiu, /* printiu/nR/retR */
    pvm_meta_instruction_id_printl, /* printl/nR/retR */
    pvm_meta_instruction_id_printlu, /* printlu/nR/retR */
    pvm_meta_instruction_id_prints, /* prints/retR */
    pvm_meta_instruction_id_prolog, /* prolog */
    pvm_meta_instruction_id_push, /* push/nR */
    pvm_meta_instruction_id_push, /* push/lR */
    pvm_meta_instruction_id_push32, /* push32/nR */
    pvm_meta_instruction_id_push32, /* push32/lR */
    pvm_meta_instruction_id_pushe, /* pushe/lR */
    pvm_meta_instruction_id_pushend, /* pushend */
    pvm_meta_instruction_id_pushf, /* pushf/nR */
    pvm_meta_instruction_id_pushhi, /* pushhi/nR */
    pvm_meta_instruction_id_pushhi, /* pushhi/lR */
    pvm_meta_instruction_id_pushios, /* pushios */
    pvm_meta_instruction_id_pushlo, /* pushlo/nR */
    pvm_meta_instruction_id_pushlo, /* pushlo/lR */
    pvm_meta_instruction_id_pushoac, /* pushoac */
    pvm_meta_instruction_id_pushob, /* pushob */
    pvm_meta_instruction_id_pushobc, /* pushobc */
    pvm_meta_instruction_id_pushoc, /* pushoc */
    pvm_meta_instruction_id_pushod, /* pushod */
    pvm_meta_instruction_id_pushoi, /* pushoi */
    pvm_meta_instruction_id_pushom, /* pushom */
    pvm_meta_instruction_id_pushoo, /* pushoo */
    pvm_meta_instruction_id_pushopp, /* pushopp */
    pvm_meta_instruction_id_pushr, /* pushr/%rR */
    pvm_meta_instruction_id_pushtopvar, /* pushtopvar/nR */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n0 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n1 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n2 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n3 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n4 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/n5 */
    pvm_meta_instruction_id_pushvar, /* pushvar/n0/nR */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n0 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n1 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n2 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n3 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n4 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/n5 */
    pvm_meta_instruction_id_pushvar, /* pushvar/nR/nR */
    pvm_meta_instruction_id_quake, /* quake */
    pvm_meta_instruction_id_raise, /* raise */
    pvm_meta_instruction_id_rand, /* rand */
    pvm_meta_instruction_id_regvar, /* regvar */
    pvm_meta_instruction_id_reloc, /* reloc */
    pvm_meta_instruction_id_restorer, /* restorer/%rR */
    pvm_meta_instruction_id_return, /* return */
    pvm_meta_instruction_id_revn, /* revn/n3 */
    pvm_meta_instruction_id_revn, /* revn/n4 */
    pvm_meta_instruction_id_revn, /* revn/nR */
    pvm_meta_instruction_id_rot, /* rot */
    pvm_meta_instruction_id_saver, /* saver/%rR */
    pvm_meta_instruction_id_sconc, /* sconc */
    pvm_meta_instruction_id_sel, /* sel */
    pvm_meta_instruction_id_setr, /* setr/%rR */
    pvm_meta_instruction_id_siz, /* siz */
    pvm_meta_instruction_id_sleep, /* sleep */
    pvm_meta_instruction_id_smodi, /* smodi */
    pvm_meta_instruction_id_spropc, /* spropc */
    pvm_meta_instruction_id_sproph, /* sproph */
    pvm_meta_instruction_id_sprops, /* sprops */
    pvm_meta_instruction_id_sref, /* sref */
    pvm_meta_instruction_id_srefi, /* srefi */
    pvm_meta_instruction_id_srefia, /* srefia */
    pvm_meta_instruction_id_srefio, /* srefio */
    pvm_meta_instruction_id_srefmnt, /* srefmnt */
    pvm_meta_instruction_id_srefnt, /* srefnt */
    pvm_meta_instruction_id_srefo, /* srefo */
    pvm_meta_instruction_id_sset, /* sset */
    pvm_meta_instruction_id_sseti, /* sseti */
    pvm_meta_instruction_id_strace, /* strace/nR/retR */
    pvm_meta_instruction_id_strref, /* strref */
    pvm_meta_instruction_id_strset, /* strset */
    pvm_meta_instruction_id_subi, /* subi */
    pvm_meta_instruction_id_subiu, /* subiu */
    pvm_meta_instruction_id_subl, /* subl */
    pvm_meta_instruction_id_sublu, /* sublu */
    pvm_meta_instruction_id_substr, /* substr */
    pvm_meta_instruction_id_swap, /* swap */
    pvm_meta_instruction_id_swapgti, /* swapgti */
    pvm_meta_instruction_id_swapgtiu, /* swapgtiu */
    pvm_meta_instruction_id_swapgtl, /* swapgtl */
    pvm_meta_instruction_id_swapgtlu, /* swapgtlu */
    pvm_meta_instruction_id_sync, /* sync */
    pvm_meta_instruction_id_time, /* time */
    pvm_meta_instruction_id_tor, /* tor */
    pvm_meta_instruction_id_tuck, /* tuck */
    pvm_meta_instruction_id_tyagetb, /* tyagetb */
    pvm_meta_instruction_id_tyagett, /* tyagett */
    pvm_meta_instruction_id_tyisc, /* tyisc */
    pvm_meta_instruction_id_tyissct, /* tyissct */
    pvm_meta_instruction_id_typof, /* typof */
    pvm_meta_instruction_id_tysctn, /* tysctn */
    pvm_meta_instruction_id_unmap, /* unmap */
    pvm_meta_instruction_id_unreachable, /* unreachable */
    pvm_meta_instruction_id_ureloc, /* ureloc */
    pvm_meta_instruction_id_write, /* write/retR */
    pvm_meta_instruction_id_addi, /* !REPLACEMENT-addi/retR */
    pvm_meta_instruction_id_addl, /* !REPLACEMENT-addl/retR */
    pvm_meta_instruction_id_ains, /* !REPLACEMENT-ains/retR */
    pvm_meta_instruction_id_aref, /* !REPLACEMENT-aref/retR */
    pvm_meta_instruction_id_arefo, /* !REPLACEMENT-arefo/retR */
    pvm_meta_instruction_id_arem, /* !REPLACEMENT-arem/retR */
    pvm_meta_instruction_id_aset, /* !REPLACEMENT-aset/retR */
    pvm_meta_instruction_id_ba, /* !REPLACEMENT-ba/fR/retR */
    pvm_meta_instruction_id_bn, /* !REPLACEMENT-bn/fR/retR */
    pvm_meta_instruction_id_bnn, /* !REPLACEMENT-bnn/fR/retR */
    pvm_meta_instruction_id_bnzi, /* !REPLACEMENT-bnzi/fR/retR */
    pvm_meta_instruction_id_bnziu, /* !REPLACEMENT-bnziu/fR/retR */
    pvm_meta_instruction_id_bnzl, /* !REPLACEMENT-bnzl/fR/retR */
    pvm_meta_instruction_id_bnzlu, /* !REPLACEMENT-bnzlu/fR/retR */
    pvm_meta_instruction_id_bsli, /* !REPLACEMENT-bsli/retR */
    pvm_meta_instruction_id_bsliu, /* !REPLACEMENT-bsliu/retR */
    pvm_meta_instruction_id_bsll, /* !REPLACEMENT-bsll/retR */
    pvm_meta_instruction_id_bsllu, /* !REPLACEMENT-bsllu/retR */
    pvm_meta_instruction_id_bzi, /* !REPLACEMENT-bzi/fR/retR */
    pvm_meta_instruction_id_bziu, /* !REPLACEMENT-bziu/fR/retR */
    pvm_meta_instruction_id_bzl, /* !REPLACEMENT-bzl/fR/retR */
    pvm_meta_instruction_id_bzlu, /* !REPLACEMENT-bzlu/fR/retR */
    pvm_meta_instruction_id_call, /* !REPLACEMENT-call/retR */
    pvm_meta_instruction_id_close, /* !REPLACEMENT-close/retR */
    pvm_meta_instruction_id_divi, /* !REPLACEMENT-divi/retR */
    pvm_meta_instruction_id_diviu, /* !REPLACEMENT-diviu/retR */
    pvm_meta_instruction_id_divl, /* !REPLACEMENT-divl/retR */
    pvm_meta_instruction_id_divlu, /* !REPLACEMENT-divlu/retR */
    pvm_meta_instruction_id_endhl, /* !REPLACEMENT-endhl/retR */
    pvm_meta_instruction_id_endsc, /* !REPLACEMENT-endsc/retR */
    pvm_meta_instruction_id_exit, /* !REPLACEMENT-exit/retR */
    pvm_meta_instruction_id_exitvm, /* !REPLACEMENT-exitvm/retR */
    pvm_meta_instruction_id_flush, /* !REPLACEMENT-flush/retR */
    pvm_meta_instruction_id_ioflags, /* !REPLACEMENT-ioflags/retR */
    pvm_meta_instruction_id_iogetb, /* !REPLACEMENT-iogetb/retR */
    pvm_meta_instruction_id_iosetb, /* !REPLACEMENT-iosetb/retR */
    pvm_meta_instruction_id_iosize, /* !REPLACEMENT-iosize/retR */
    pvm_meta_instruction_id_map, /* !REPLACEMENT-map/retR */
    pvm_meta_instruction_id_modi, /* !REPLACEMENT-modi/retR */
    pvm_meta_instruction_id_modiu, /* !REPLACEMENT-modiu/retR */
    pvm_meta_instruction_id_modl, /* !REPLACEMENT-modl/retR */
    pvm_meta_instruction_id_modlu, /* !REPLACEMENT-modlu/retR */
    pvm_meta_instruction_id_muli, /* !REPLACEMENT-muli/retR */
    pvm_meta_instruction_id_mull, /* !REPLACEMENT-mull/retR */
    pvm_meta_instruction_id_negi, /* !REPLACEMENT-negi/retR */
    pvm_meta_instruction_id_negiu, /* !REPLACEMENT-negiu/retR */
    pvm_meta_instruction_id_negl, /* !REPLACEMENT-negl/retR */
    pvm_meta_instruction_id_neglu, /* !REPLACEMENT-neglu/retR */
    pvm_meta_instruction_id_open, /* !REPLACEMENT-open/retR */
    pvm_meta_instruction_id_peekdi, /* !REPLACEMENT-peekdi/nR/retR */
    pvm_meta_instruction_id_peekdiu, /* !REPLACEMENT-peekdiu/nR/retR */
    pvm_meta_instruction_id_peekdl, /* !REPLACEMENT-peekdl/nR/retR */
    pvm_meta_instruction_id_peekdlu, /* !REPLACEMENT-peekdlu/nR/retR */
    pvm_meta_instruction_id_peeki, /* !REPLACEMENT-peeki/nR/nR/nR/retR */
    pvm_meta_instruction_id_peekiu, /* !REPLACEMENT-peekiu/nR/nR/retR */
    pvm_meta_instruction_id_peekl, /* !REPLACEMENT-peekl/nR/nR/nR/retR */
    pvm_meta_instruction_id_peeklu, /* !REPLACEMENT-peeklu/nR/nR/retR */
    pvm_meta_instruction_id_peeks, /* !REPLACEMENT-peeks/retR */
    pvm_meta_instruction_id_pokedi, /* !REPLACEMENT-pokedi/nR/retR */
    pvm_meta_instruction_id_pokediu, /* !REPLACEMENT-pokediu/nR/retR */
    pvm_meta_instruction_id_pokedl, /* !REPLACEMENT-pokedl/nR/retR */
    pvm_meta_instruction_id_pokedlu, /* !REPLACEMENT-pokedlu/nR/retR */
    pvm_meta_instruction_id_pokei, /* !REPLACEMENT-pokei/nR/nR/nR/retR */
    pvm_meta_instruction_id_pokeiu, /* !REPLACEMENT-pokeiu/nR/nR/retR */
    pvm_meta_instruction_id_pokel, /* !REPLACEMENT-pokel/nR/nR/nR/retR */
    pvm_meta_instruction_id_pokelu, /* !REPLACEMENT-pokelu/nR/nR/retR */
    pvm_meta_instruction_id_pokes, /* !REPLACEMENT-pokes/retR */
    pvm_meta_instruction_id_popios, /* !REPLACEMENT-popios/retR */
    pvm_meta_instruction_id_popob, /* !REPLACEMENT-popob/retR */
    pvm_meta_instruction_id_popom, /* !REPLACEMENT-popom/retR */
    pvm_meta_instruction_id_powi, /* !REPLACEMENT-powi/retR */
    pvm_meta_instruction_id_powiu, /* !REPLACEMENT-powiu/retR */
    pvm_meta_instruction_id_powl, /* !REPLACEMENT-powl/retR */
    pvm_meta_instruction_id_powlu, /* !REPLACEMENT-powlu/retR */
    pvm_meta_instruction_id_prolog, /* !REPLACEMENT-prolog/retR */
    pvm_meta_instruction_id_pushios, /* !REPLACEMENT-pushios/retR */
    pvm_meta_instruction_id_pushtopvar, /* !REPLACEMENT-pushtopvar/nR/retR */
    pvm_meta_instruction_id_raise, /* !REPLACEMENT-raise/retR */
    pvm_meta_instruction_id_reloc, /* !REPLACEMENT-reloc/retR */
    pvm_meta_instruction_id_return, /* !REPLACEMENT-return/retR */
    pvm_meta_instruction_id_sleep, /* !REPLACEMENT-sleep/retR */
    pvm_meta_instruction_id_smodi, /* !REPLACEMENT-smodi/retR */
    pvm_meta_instruction_id_sref, /* !REPLACEMENT-sref/retR */
    pvm_meta_instruction_id_srefi, /* !REPLACEMENT-srefi/retR */
    pvm_meta_instruction_id_srefia, /* !REPLACEMENT-srefia/retR */
    pvm_meta_instruction_id_srefio, /* !REPLACEMENT-srefio/retR */
    pvm_meta_instruction_id_srefo, /* !REPLACEMENT-srefo/retR */
    pvm_meta_instruction_id_sset, /* !REPLACEMENT-sset/retR */
    pvm_meta_instruction_id_sseti, /* !REPLACEMENT-sseti/retR */
    pvm_meta_instruction_id_strref, /* !REPLACEMENT-strref/retR */
    pvm_meta_instruction_id_strset, /* !REPLACEMENT-strset/retR */
    pvm_meta_instruction_id_subi, /* !REPLACEMENT-subi/retR */
    pvm_meta_instruction_id_subl, /* !REPLACEMENT-subl/retR */
    pvm_meta_instruction_id_substr, /* !REPLACEMENT-substr/retR */
    pvm_meta_instruction_id_sync, /* !REPLACEMENT-sync/retR */
    pvm_meta_instruction_id_unreachable, /* !REPLACEMENT-unreachable/retR */
    pvm_meta_instruction_id_ureloc, /* !REPLACEMENT-ureloc/retR */
    pvm_meta_instruction_id_write /* !REPLACEMENT-write/retR */
    };

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
/* Worst-case replacement table. */
const jitter_uint
pvm_worst_case_replacement_table [] =
  {
    pvm_specialized_instruction_opcode__eINVALID, /* !INVALID is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eBEGINBASICBLOCK, /* !BEGINBASICBLOCK is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eEXITVM, /* !EXITVM is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eDATALOCATIONS, /* !DATALOCATIONS is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eNOP, /* !NOP is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eUNREACHABLE0, /* !UNREACHABLE0 is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eUNREACHABLE1, /* !UNREACHABLE1 is NOT potentially defective. */
    pvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE, /* !PRETENDTOJUMPANYWHERE is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maddi__retR, /* addi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-addi/retR. */
    pvm_specialized_instruction_opcode_addiu, /* addiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maddl__retR, /* addl is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-addl/retR. */
    pvm_specialized_instruction_opcode_addlu, /* addlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mains__retR, /* ains is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ains/retR. */
    pvm_specialized_instruction_opcode_and, /* and is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maref__retR, /* aref is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-aref/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_marefo__retR, /* arefo is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-arefo/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_marem__retR, /* arem is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-arem/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maset__retR, /* aset is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-aset/retR. */
    pvm_specialized_instruction_opcode_asettb, /* asettb is NOT potentially defective. */
    pvm_specialized_instruction_opcode_atr, /* atr is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mba__fR__retR, /* ba/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ba/fR/retR. */
    pvm_specialized_instruction_opcode_bandi, /* bandi is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandiu, /* bandiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandl, /* bandl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bandlu, /* bandlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_beghl__retR, /* beghl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_begsc__retR, /* begsc/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbn__fR__retR, /* bn/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bn/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnn__fR__retR, /* bnn/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bnn/fR/retR. */
    pvm_specialized_instruction_opcode_bnoti, /* bnoti is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotiu, /* bnotiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotl, /* bnotl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bnotlu, /* bnotlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzi__fR__retR, /* bnzi/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bnzi/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnziu__fR__retR, /* bnziu/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bnziu/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzl__fR__retR, /* bnzl/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bnzl/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzlu__fR__retR, /* bnzlu/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bnzlu/fR/retR. */
    pvm_specialized_instruction_opcode_bori, /* bori is NOT potentially defective. */
    pvm_specialized_instruction_opcode_boriu, /* boriu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_borl, /* borl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_borlu, /* borlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsli__retR, /* bsli is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bsli/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsliu__retR, /* bsliu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bsliu/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsll__retR, /* bsll is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bsll/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsllu__retR, /* bsllu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bsllu/retR. */
    pvm_specialized_instruction_opcode_bsri, /* bsri is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsriu, /* bsriu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsrl, /* bsrl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bsrlu, /* bsrlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxori, /* bxori is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxoriu, /* bxoriu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxorl, /* bxorl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_bxorlu, /* bxorlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzi__fR__retR, /* bzi/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bzi/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbziu__fR__retR, /* bziu/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bziu/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzl__fR__retR, /* bzl/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bzl/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzlu__fR__retR, /* bzlu/fR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-bzlu/fR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mcall__retR, /* call/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-call/retR. */
    pvm_specialized_instruction_opcode_canary, /* canary is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mclose__retR, /* close is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-close/retR. */
    pvm_specialized_instruction_opcode_ctos, /* ctos is NOT potentially defective. */
    pvm_specialized_instruction_opcode_disas__retR, /* disas/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivi__retR, /* divi/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-divi/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdiviu__retR, /* diviu/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-diviu/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivl__retR, /* divl/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-divl/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivlu__retR, /* divlu/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-divlu/retR. */
    pvm_specialized_instruction_opcode_drop, /* drop is NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop2, /* drop2 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop3, /* drop3 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_drop4, /* drop4 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_duc, /* duc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_dup, /* dup is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mendhl__retR, /* endhl/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-endhl/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mendsc__retR, /* endsc/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-endsc/retR. */
    pvm_specialized_instruction_opcode_eqc, /* eqc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqi, /* eqi is NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqiu, /* eqiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_eql, /* eql is NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqlu, /* eqlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_eqs, /* eqs is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mexit__retR, /* exit is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-exit/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* exitvm is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-exitvm/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mflush__retR, /* flush is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-flush/retR. */
    pvm_specialized_instruction_opcode_formati__nR__retR, /* formati/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_formatiu__nR__retR, /* formatiu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_formatl__nR__retR, /* formatl/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_formatlu__nR__retR, /* formatlu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_fromr, /* fromr is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gei, /* gei is NOT potentially defective. */
    pvm_specialized_instruction_opcode_geiu, /* geiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gel, /* gel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gelu, /* gelu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ges, /* ges is NOT potentially defective. */
    pvm_specialized_instruction_opcode_getenv, /* getenv is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gti, /* gti is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtiu, /* gtiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtl, /* gtl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gtlu, /* gtlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_gts, /* gts is NOT potentially defective. */
    pvm_specialized_instruction_opcode_indent__retR, /* indent/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mioflags__retR, /* ioflags is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ioflags/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miogetb__retR, /* iogetb/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-iogetb/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miosetb__retR, /* iosetb is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-iosetb/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miosize__retR, /* iosize is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-iosize/retR. */
    pvm_specialized_instruction_opcode_isa, /* isa is NOT potentially defective. */
    pvm_specialized_instruction_opcode_itoi__nR, /* itoi/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_itoiu__nR, /* itoiu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_itol__nR, /* itol/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_itolu__nR, /* itolu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutoi__nR, /* iutoi/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutoiu__nR, /* iutoiu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutol__nR, /* iutol/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_iutolu__nR, /* iutolu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lei, /* lei is NOT potentially defective. */
    pvm_specialized_instruction_opcode_leiu, /* leiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lel, /* lel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lelu, /* lelu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_les, /* les is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lti, /* lti is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltiu, /* ltiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltl, /* ltl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltlu, /* ltlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltoi__nR, /* ltoi/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltoiu__nR, /* ltoiu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltol__nR, /* ltol/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ltolu__nR, /* ltolu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lts, /* lts is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutoi__nR, /* lutoi/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutoiu__nR, /* lutoiu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutol__nR, /* lutol/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_lutolu__nR, /* lutolu/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmap__retR, /* map is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-map/retR. */
    pvm_specialized_instruction_opcode_mgetios, /* mgetios is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetm, /* mgetm is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgeto, /* mgeto is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgets, /* mgets is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetsel, /* mgetsel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetsiz, /* mgetsiz is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mgetw, /* mgetw is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mka, /* mka is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mko, /* mko is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mksct, /* mksct is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktya, /* mktya is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyany, /* mktyany is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyc, /* mktyc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyi, /* mktyi is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyo, /* mktyo is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktys, /* mktys is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktysct, /* mktysct is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mktyv, /* mktyv is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mm, /* mm is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodi__retR, /* modi/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-modi/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodiu__retR, /* modiu/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-modiu/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodl__retR, /* modl/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-modl/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodlu__retR, /* modlu/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-modlu/retR. */
    pvm_specialized_instruction_opcode_msetios, /* msetios is NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetm, /* msetm is NOT potentially defective. */
    pvm_specialized_instruction_opcode_mseto, /* mseto is NOT potentially defective. */
    pvm_specialized_instruction_opcode_msets, /* msets is NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetsel, /* msetsel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetsiz, /* msetsiz is NOT potentially defective. */
    pvm_specialized_instruction_opcode_msetw, /* msetw is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmuli__retR, /* muli is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-muli/retR. */
    pvm_specialized_instruction_opcode_muliu, /* muliu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmull__retR, /* mull is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-mull/retR. */
    pvm_specialized_instruction_opcode_mullu, /* mullu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_muls, /* muls is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nec, /* nec is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegi__retR, /* negi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-negi/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegiu__retR, /* negiu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-negiu/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegl__retR, /* negl is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-negl/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mneglu__retR, /* neglu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-neglu/retR. */
    pvm_specialized_instruction_opcode_nei, /* nei is NOT potentially defective. */
    pvm_specialized_instruction_opcode_neiu, /* neiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nel, /* nel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nelu, /* nelu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nes, /* nes is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip, /* nip is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip2, /* nip2 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nip3, /* nip3 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nn, /* nn is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nnn, /* nnn is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nop, /* nop is NOT potentially defective. */
    pvm_specialized_instruction_opcode_not, /* not is NOT potentially defective. */
    pvm_specialized_instruction_opcode_note__nR, /* note/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_nrot, /* nrot is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetbt, /* ogetbt is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetm, /* ogetm is NOT potentially defective. */
    pvm_specialized_instruction_opcode_ogetu, /* ogetu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mopen__retR, /* open is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-open/retR. */
    pvm_specialized_instruction_opcode_or, /* or is NOT potentially defective. */
    pvm_specialized_instruction_opcode_osetm, /* osetm is NOT potentially defective. */
    pvm_specialized_instruction_opcode_over, /* over is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pec, /* pec is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdi__nR__retR, /* peekdi/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekdi/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdiu__nR__retR, /* peekdiu/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekdiu/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdl__nR__retR, /* peekdl/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekdl/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdlu__nR__retR, /* peekdlu/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekdlu/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeki__nR__nR__nR__retR, /* peeki/nR/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peeki/nR/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekiu__nR__nR__retR, /* peekiu/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekiu/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekl__nR__nR__nR__retR, /* peekl/nR/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peekl/nR/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeklu__nR__nR__retR, /* peeklu/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peeklu/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeks__retR, /* peeks is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-peeks/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedi__nR__retR, /* pokedi/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokedi/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokediu__nR__retR, /* pokediu/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokediu/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedl__nR__retR, /* pokedl/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokedl/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedlu__nR__retR, /* pokedlu/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokedlu/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokei__nR__nR__nR__retR, /* pokei/nR/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokei/nR/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokeiu__nR__nR__retR, /* pokeiu/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokeiu/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokel__nR__nR__nR__retR, /* pokel/nR/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokel/nR/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokelu__nR__nR__retR, /* pokelu/nR/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokelu/nR/nR/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokes__retR, /* pokes is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pokes/retR. */
    pvm_specialized_instruction_opcode_pope, /* pope is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popend, /* popend is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popexite, /* popexite is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popf__nR, /* popf/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopios__retR, /* popios is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-popios/retR. */
    pvm_specialized_instruction_opcode_popoac, /* popoac is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopob__retR, /* popob is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-popob/retR. */
    pvm_specialized_instruction_opcode_popobc, /* popobc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoc, /* popoc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popod, /* popod is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popoi, /* popoi is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopom__retR, /* popom is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-popom/retR. */
    pvm_specialized_instruction_opcode_popoo, /* popoo is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popopp, /* popopp is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popr___rrR, /* popr/%rR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_popvar__nR__nR, /* popvar/nR/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowi__retR, /* powi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-powi/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowiu__retR, /* powiu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-powiu/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowl__retR, /* powl is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-powl/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowlu__retR, /* powlu is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-powlu/retR. */
    pvm_specialized_instruction_opcode_printi__nR__retR, /* printi/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_printiu__nR__retR, /* printiu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_printl__nR__retR, /* printl/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_printlu__nR__retR, /* printlu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_prints__retR, /* prints/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mprolog__retR, /* prolog is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-prolog/retR. */
    pvm_specialized_instruction_opcode_push__nR, /* push/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_push__lR, /* push/lR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_push32__nR, /* push32/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_push32__lR, /* push32/lR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushe__lR, /* pushe/lR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushend, /* pushend is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushf__nR, /* pushf/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushhi__nR, /* pushhi/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushhi__lR, /* pushhi/lR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpushios__retR, /* pushios is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pushios/retR. */
    pvm_specialized_instruction_opcode_pushlo__nR, /* pushlo/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushlo__lR, /* pushlo/lR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoac, /* pushoac is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushob, /* pushob is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushobc, /* pushobc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoc, /* pushoc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushod, /* pushod is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoi, /* pushoi is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushom, /* pushom is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushoo, /* pushoo is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushopp, /* pushopp is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushr___rrR, /* pushr/%rR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpushtopvar__nR__retR, /* pushtopvar/nR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-pushtopvar/nR/retR. */
    pvm_specialized_instruction_opcode_pushvar__n0__n0, /* pushvar/n0/n0 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n1, /* pushvar/n0/n1 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n2, /* pushvar/n0/n2 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n3, /* pushvar/n0/n3 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n4, /* pushvar/n0/n4 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__n5, /* pushvar/n0/n5 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__n0__nR, /* pushvar/n0/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n0, /* pushvar/nR/n0 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n1, /* pushvar/nR/n1 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n2, /* pushvar/nR/n2 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n3, /* pushvar/nR/n3 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n4, /* pushvar/nR/n4 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__n5, /* pushvar/nR/n5 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_pushvar__nR__nR, /* pushvar/nR/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_quake, /* quake is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mraise__retR, /* raise is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-raise/retR. */
    pvm_specialized_instruction_opcode_rand, /* rand is NOT potentially defective. */
    pvm_specialized_instruction_opcode_regvar, /* regvar is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mreloc__retR, /* reloc is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-reloc/retR. */
    pvm_specialized_instruction_opcode_restorer___rrR, /* restorer/%rR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR, /* return is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-return/retR. */
    pvm_specialized_instruction_opcode_revn__n3, /* revn/n3 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_revn__n4, /* revn/n4 is NOT potentially defective. */
    pvm_specialized_instruction_opcode_revn__nR, /* revn/nR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_rot, /* rot is NOT potentially defective. */
    pvm_specialized_instruction_opcode_saver___rrR, /* saver/%rR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_sconc, /* sconc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_sel, /* sel is NOT potentially defective. */
    pvm_specialized_instruction_opcode_setr___rrR, /* setr/%rR is NOT potentially defective. */
    pvm_specialized_instruction_opcode_siz, /* siz is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msleep__retR, /* sleep is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-sleep/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msmodi__retR, /* smodi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-smodi/retR. */
    pvm_specialized_instruction_opcode_spropc, /* spropc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_sproph, /* sproph is NOT potentially defective. */
    pvm_specialized_instruction_opcode_sprops, /* sprops is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msref__retR, /* sref is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-sref/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefi__retR, /* srefi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-srefi/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefia__retR, /* srefia is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-srefia/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefio__retR, /* srefio is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-srefio/retR. */
    pvm_specialized_instruction_opcode_srefmnt, /* srefmnt is NOT potentially defective. */
    pvm_specialized_instruction_opcode_srefnt, /* srefnt is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefo__retR, /* srefo is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-srefo/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msset__retR, /* sset is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-sset/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msseti__retR, /* sseti is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-sseti/retR. */
    pvm_specialized_instruction_opcode_strace__nR__retR, /* strace/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mstrref__retR, /* strref is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-strref/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mstrset__retR, /* strset is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-strset/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubi__retR, /* subi is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-subi/retR. */
    pvm_specialized_instruction_opcode_subiu, /* subiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubl__retR, /* subl is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-subl/retR. */
    pvm_specialized_instruction_opcode_sublu, /* sublu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubstr__retR, /* substr is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-substr/retR. */
    pvm_specialized_instruction_opcode_swap, /* swap is NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgti, /* swapgti is NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtiu, /* swapgtiu is NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtl, /* swapgtl is NOT potentially defective. */
    pvm_specialized_instruction_opcode_swapgtlu, /* swapgtlu is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msync__retR, /* sync is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-sync/retR. */
    pvm_specialized_instruction_opcode_time, /* time is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tor, /* tor is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tuck, /* tuck is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyagetb, /* tyagetb is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyagett, /* tyagett is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyisc, /* tyisc is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tyissct, /* tyissct is NOT potentially defective. */
    pvm_specialized_instruction_opcode_typof, /* typof is NOT potentially defective. */
    pvm_specialized_instruction_opcode_tysctn, /* tysctn is NOT potentially defective. */
    pvm_specialized_instruction_opcode_unmap, /* unmap is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR, /* unreachable is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-unreachable/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mureloc__retR, /* ureloc is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-ureloc/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mwrite__retR, /* write/retR is POTENTIALLY DEFECTIVE, and replaced by !REPLACEMENT-write/retR. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maddi__retR, /* !REPLACEMENT-addi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maddl__retR, /* !REPLACEMENT-addl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mains__retR, /* !REPLACEMENT-ains/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maref__retR, /* !REPLACEMENT-aref/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_marefo__retR, /* !REPLACEMENT-arefo/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_marem__retR, /* !REPLACEMENT-arem/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_maset__retR, /* !REPLACEMENT-aset/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mba__fR__retR, /* !REPLACEMENT-ba/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbn__fR__retR, /* !REPLACEMENT-bn/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnn__fR__retR, /* !REPLACEMENT-bnn/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzi__fR__retR, /* !REPLACEMENT-bnzi/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnziu__fR__retR, /* !REPLACEMENT-bnziu/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzl__fR__retR, /* !REPLACEMENT-bnzl/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzlu__fR__retR, /* !REPLACEMENT-bnzlu/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsli__retR, /* !REPLACEMENT-bsli/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsliu__retR, /* !REPLACEMENT-bsliu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsll__retR, /* !REPLACEMENT-bsll/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbsllu__retR, /* !REPLACEMENT-bsllu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzi__fR__retR, /* !REPLACEMENT-bzi/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbziu__fR__retR, /* !REPLACEMENT-bziu/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzl__fR__retR, /* !REPLACEMENT-bzl/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mbzlu__fR__retR, /* !REPLACEMENT-bzlu/fR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mcall__retR, /* !REPLACEMENT-call/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mclose__retR, /* !REPLACEMENT-close/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivi__retR, /* !REPLACEMENT-divi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdiviu__retR, /* !REPLACEMENT-diviu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivl__retR, /* !REPLACEMENT-divl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mdivlu__retR, /* !REPLACEMENT-divlu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mendhl__retR, /* !REPLACEMENT-endhl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mendsc__retR, /* !REPLACEMENT-endsc/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mexit__retR, /* !REPLACEMENT-exit/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR, /* !REPLACEMENT-exitvm/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mflush__retR, /* !REPLACEMENT-flush/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mioflags__retR, /* !REPLACEMENT-ioflags/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miogetb__retR, /* !REPLACEMENT-iogetb/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miosetb__retR, /* !REPLACEMENT-iosetb/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_miosize__retR, /* !REPLACEMENT-iosize/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmap__retR, /* !REPLACEMENT-map/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodi__retR, /* !REPLACEMENT-modi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodiu__retR, /* !REPLACEMENT-modiu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodl__retR, /* !REPLACEMENT-modl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmodlu__retR, /* !REPLACEMENT-modlu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmuli__retR, /* !REPLACEMENT-muli/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mmull__retR, /* !REPLACEMENT-mull/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegi__retR, /* !REPLACEMENT-negi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegiu__retR, /* !REPLACEMENT-negiu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mnegl__retR, /* !REPLACEMENT-negl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mneglu__retR, /* !REPLACEMENT-neglu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mopen__retR, /* !REPLACEMENT-open/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdi__nR__retR, /* !REPLACEMENT-peekdi/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdiu__nR__retR, /* !REPLACEMENT-peekdiu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdl__nR__retR, /* !REPLACEMENT-peekdl/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdlu__nR__retR, /* !REPLACEMENT-peekdlu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeki__nR__nR__nR__retR, /* !REPLACEMENT-peeki/nR/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekiu__nR__nR__retR, /* !REPLACEMENT-peekiu/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekl__nR__nR__nR__retR, /* !REPLACEMENT-peekl/nR/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeklu__nR__nR__retR, /* !REPLACEMENT-peeklu/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeks__retR, /* !REPLACEMENT-peeks/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedi__nR__retR, /* !REPLACEMENT-pokedi/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokediu__nR__retR, /* !REPLACEMENT-pokediu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedl__nR__retR, /* !REPLACEMENT-pokedl/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedlu__nR__retR, /* !REPLACEMENT-pokedlu/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokei__nR__nR__nR__retR, /* !REPLACEMENT-pokei/nR/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokeiu__nR__nR__retR, /* !REPLACEMENT-pokeiu/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokel__nR__nR__nR__retR, /* !REPLACEMENT-pokel/nR/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokelu__nR__nR__retR, /* !REPLACEMENT-pokelu/nR/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpokes__retR, /* !REPLACEMENT-pokes/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopios__retR, /* !REPLACEMENT-popios/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopob__retR, /* !REPLACEMENT-popob/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpopom__retR, /* !REPLACEMENT-popom/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowi__retR, /* !REPLACEMENT-powi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowiu__retR, /* !REPLACEMENT-powiu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowl__retR, /* !REPLACEMENT-powl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpowlu__retR, /* !REPLACEMENT-powlu/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mprolog__retR, /* !REPLACEMENT-prolog/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpushios__retR, /* !REPLACEMENT-pushios/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mpushtopvar__nR__retR, /* !REPLACEMENT-pushtopvar/nR/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mraise__retR, /* !REPLACEMENT-raise/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mreloc__retR, /* !REPLACEMENT-reloc/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR, /* !REPLACEMENT-return/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msleep__retR, /* !REPLACEMENT-sleep/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msmodi__retR, /* !REPLACEMENT-smodi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msref__retR, /* !REPLACEMENT-sref/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefi__retR, /* !REPLACEMENT-srefi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefia__retR, /* !REPLACEMENT-srefia/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefio__retR, /* !REPLACEMENT-srefio/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msrefo__retR, /* !REPLACEMENT-srefo/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msset__retR, /* !REPLACEMENT-sset/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msseti__retR, /* !REPLACEMENT-sseti/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mstrref__retR, /* !REPLACEMENT-strref/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mstrset__retR, /* !REPLACEMENT-strset/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubi__retR, /* !REPLACEMENT-subi/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubl__retR, /* !REPLACEMENT-subl/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msubstr__retR, /* !REPLACEMENT-substr/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_msync__retR, /* !REPLACEMENT-sync/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR, /* !REPLACEMENT-unreachable/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mureloc__retR, /* !REPLACEMENT-ureloc/retR is NOT potentially defective. */
    pvm_specialized_instruction_opcode__eREPLACEMENT_mwrite__retR /* !REPLACEMENT-write/retR is NOT potentially defective. */
  };
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
const jitter_uint
pvm_call_related_specialized_instruction_ids []
= {
    pvm_specialized_instruction_opcode_call__retR,
    pvm_specialized_instruction_opcode_prolog,
    pvm_specialized_instruction_opcode_return,
    pvm_specialized_instruction_opcode_write__retR
  };

const jitter_uint
pvm_call_related_specialized_instruction_id_no
= sizeof (pvm_call_related_specialized_instruction_ids) / sizeof (jitter_uint);

const bool
pvm_specialized_instruction_call_relateds []
= {
    0, /* !INVALID */
    0, /* !BEGINBASICBLOCK */
    0, /* !EXITVM */
    0, /* !DATALOCATIONS */
    0, /* !NOP */
    0, /* !UNREACHABLE0 */
    0, /* !UNREACHABLE1 */
    0, /* !PRETENDTOJUMPANYWHERE */
    0, /* addi */
    0, /* addiu */
    0, /* addl */
    0, /* addlu */
    0, /* ains */
    0, /* and */
    0, /* aref */
    0, /* arefo */
    0, /* arem */
    0, /* aset */
    0, /* asettb */
    0, /* atr */
    0, /* ba/fR */
    0, /* bandi */
    0, /* bandiu */
    0, /* bandl */
    0, /* bandlu */
    0, /* beghl/retR */
    0, /* begsc/retR */
    0, /* bn/fR */
    0, /* bnn/fR */
    0, /* bnoti */
    0, /* bnotiu */
    0, /* bnotl */
    0, /* bnotlu */
    0, /* bnzi/fR */
    0, /* bnziu/fR */
    0, /* bnzl/fR */
    0, /* bnzlu/fR */
    0, /* bori */
    0, /* boriu */
    0, /* borl */
    0, /* borlu */
    0, /* bsli */
    0, /* bsliu */
    0, /* bsll */
    0, /* bsllu */
    0, /* bsri */
    0, /* bsriu */
    0, /* bsrl */
    0, /* bsrlu */
    0, /* bxori */
    0, /* bxoriu */
    0, /* bxorl */
    0, /* bxorlu */
    0, /* bzi/fR */
    0, /* bziu/fR */
    0, /* bzl/fR */
    0, /* bzlu/fR */
    1, /* call/retR */
    0, /* canary */
    0, /* close */
    0, /* ctos */
    0, /* disas/retR */
    0, /* divi/retR */
    0, /* diviu/retR */
    0, /* divl/retR */
    0, /* divlu/retR */
    0, /* drop */
    0, /* drop2 */
    0, /* drop3 */
    0, /* drop4 */
    0, /* duc */
    0, /* dup */
    0, /* endhl/retR */
    0, /* endsc/retR */
    0, /* eqc */
    0, /* eqi */
    0, /* eqiu */
    0, /* eql */
    0, /* eqlu */
    0, /* eqs */
    0, /* exit */
    0, /* exitvm */
    0, /* flush */
    0, /* formati/nR/retR */
    0, /* formatiu/nR/retR */
    0, /* formatl/nR/retR */
    0, /* formatlu/nR/retR */
    0, /* fromr */
    0, /* gei */
    0, /* geiu */
    0, /* gel */
    0, /* gelu */
    0, /* ges */
    0, /* getenv */
    0, /* gti */
    0, /* gtiu */
    0, /* gtl */
    0, /* gtlu */
    0, /* gts */
    0, /* indent/retR */
    0, /* ioflags */
    0, /* iogetb/retR */
    0, /* iosetb */
    0, /* iosize */
    0, /* isa */
    0, /* itoi/nR */
    0, /* itoiu/nR */
    0, /* itol/nR */
    0, /* itolu/nR */
    0, /* iutoi/nR */
    0, /* iutoiu/nR */
    0, /* iutol/nR */
    0, /* iutolu/nR */
    0, /* lei */
    0, /* leiu */
    0, /* lel */
    0, /* lelu */
    0, /* les */
    0, /* lti */
    0, /* ltiu */
    0, /* ltl */
    0, /* ltlu */
    0, /* ltoi/nR */
    0, /* ltoiu/nR */
    0, /* ltol/nR */
    0, /* ltolu/nR */
    0, /* lts */
    0, /* lutoi/nR */
    0, /* lutoiu/nR */
    0, /* lutol/nR */
    0, /* lutolu/nR */
    0, /* map */
    0, /* mgetios */
    0, /* mgetm */
    0, /* mgeto */
    0, /* mgets */
    0, /* mgetsel */
    0, /* mgetsiz */
    0, /* mgetw */
    0, /* mka */
    0, /* mko */
    0, /* mksct */
    0, /* mktya */
    0, /* mktyany */
    0, /* mktyc */
    0, /* mktyi */
    0, /* mktyo */
    0, /* mktys */
    0, /* mktysct */
    0, /* mktyv */
    0, /* mm */
    0, /* modi/retR */
    0, /* modiu/retR */
    0, /* modl/retR */
    0, /* modlu/retR */
    0, /* msetios */
    0, /* msetm */
    0, /* mseto */
    0, /* msets */
    0, /* msetsel */
    0, /* msetsiz */
    0, /* msetw */
    0, /* muli */
    0, /* muliu */
    0, /* mull */
    0, /* mullu */
    0, /* muls */
    0, /* nec */
    0, /* negi */
    0, /* negiu */
    0, /* negl */
    0, /* neglu */
    0, /* nei */
    0, /* neiu */
    0, /* nel */
    0, /* nelu */
    0, /* nes */
    0, /* nip */
    0, /* nip2 */
    0, /* nip3 */
    0, /* nn */
    0, /* nnn */
    0, /* nop */
    0, /* not */
    0, /* note/nR */
    0, /* nrot */
    0, /* ogetbt */
    0, /* ogetm */
    0, /* ogetu */
    0, /* open */
    0, /* or */
    0, /* osetm */
    0, /* over */
    0, /* pec */
    0, /* peekdi/nR */
    0, /* peekdiu/nR */
    0, /* peekdl/nR */
    0, /* peekdlu/nR */
    0, /* peeki/nR/nR/nR */
    0, /* peekiu/nR/nR */
    0, /* peekl/nR/nR/nR */
    0, /* peeklu/nR/nR */
    0, /* peeks */
    0, /* pokedi/nR */
    0, /* pokediu/nR */
    0, /* pokedl/nR */
    0, /* pokedlu/nR */
    0, /* pokei/nR/nR/nR */
    0, /* pokeiu/nR/nR */
    0, /* pokel/nR/nR/nR */
    0, /* pokelu/nR/nR */
    0, /* pokes */
    0, /* pope */
    0, /* popend */
    0, /* popexite */
    0, /* popf/nR */
    0, /* popios */
    0, /* popoac */
    0, /* popob */
    0, /* popobc */
    0, /* popoc */
    0, /* popod */
    0, /* popoi */
    0, /* popom */
    0, /* popoo */
    0, /* popopp */
    0, /* popr/%rR */
    0, /* popvar/nR/nR */
    0, /* powi */
    0, /* powiu */
    0, /* powl */
    0, /* powlu */
    0, /* printi/nR/retR */
    0, /* printiu/nR/retR */
    0, /* printl/nR/retR */
    0, /* printlu/nR/retR */
    0, /* prints/retR */
    1, /* prolog */
    0, /* push/nR */
    0, /* push/lR */
    0, /* push32/nR */
    0, /* push32/lR */
    0, /* pushe/lR */
    0, /* pushend */
    0, /* pushf/nR */
    0, /* pushhi/nR */
    0, /* pushhi/lR */
    0, /* pushios */
    0, /* pushlo/nR */
    0, /* pushlo/lR */
    0, /* pushoac */
    0, /* pushob */
    0, /* pushobc */
    0, /* pushoc */
    0, /* pushod */
    0, /* pushoi */
    0, /* pushom */
    0, /* pushoo */
    0, /* pushopp */
    0, /* pushr/%rR */
    0, /* pushtopvar/nR */
    0, /* pushvar/n0/n0 */
    0, /* pushvar/n0/n1 */
    0, /* pushvar/n0/n2 */
    0, /* pushvar/n0/n3 */
    0, /* pushvar/n0/n4 */
    0, /* pushvar/n0/n5 */
    0, /* pushvar/n0/nR */
    0, /* pushvar/nR/n0 */
    0, /* pushvar/nR/n1 */
    0, /* pushvar/nR/n2 */
    0, /* pushvar/nR/n3 */
    0, /* pushvar/nR/n4 */
    0, /* pushvar/nR/n5 */
    0, /* pushvar/nR/nR */
    0, /* quake */
    0, /* raise */
    0, /* rand */
    0, /* regvar */
    0, /* reloc */
    0, /* restorer/%rR */
    1, /* return */
    0, /* revn/n3 */
    0, /* revn/n4 */
    0, /* revn/nR */
    0, /* rot */
    0, /* saver/%rR */
    0, /* sconc */
    0, /* sel */
    0, /* setr/%rR */
    0, /* siz */
    0, /* sleep */
    0, /* smodi */
    0, /* spropc */
    0, /* sproph */
    0, /* sprops */
    0, /* sref */
    0, /* srefi */
    0, /* srefia */
    0, /* srefio */
    0, /* srefmnt */
    0, /* srefnt */
    0, /* srefo */
    0, /* sset */
    0, /* sseti */
    0, /* strace/nR/retR */
    0, /* strref */
    0, /* strset */
    0, /* subi */
    0, /* subiu */
    0, /* subl */
    0, /* sublu */
    0, /* substr */
    0, /* swap */
    0, /* swapgti */
    0, /* swapgtiu */
    0, /* swapgtl */
    0, /* swapgtlu */
    0, /* sync */
    0, /* time */
    0, /* tor */
    0, /* tuck */
    0, /* tyagetb */
    0, /* tyagett */
    0, /* tyisc */
    0, /* tyissct */
    0, /* typof */
    0, /* tysctn */
    0, /* unmap */
    0, /* unreachable */
    0, /* ureloc */
    1, /* write/retR */
    0, /* !REPLACEMENT-addi/retR */
    0, /* !REPLACEMENT-addl/retR */
    0, /* !REPLACEMENT-ains/retR */
    0, /* !REPLACEMENT-aref/retR */
    0, /* !REPLACEMENT-arefo/retR */
    0, /* !REPLACEMENT-arem/retR */
    0, /* !REPLACEMENT-aset/retR */
    0, /* !REPLACEMENT-ba/fR/retR */
    0, /* !REPLACEMENT-bn/fR/retR */
    0, /* !REPLACEMENT-bnn/fR/retR */
    0, /* !REPLACEMENT-bnzi/fR/retR */
    0, /* !REPLACEMENT-bnziu/fR/retR */
    0, /* !REPLACEMENT-bnzl/fR/retR */
    0, /* !REPLACEMENT-bnzlu/fR/retR */
    0, /* !REPLACEMENT-bsli/retR */
    0, /* !REPLACEMENT-bsliu/retR */
    0, /* !REPLACEMENT-bsll/retR */
    0, /* !REPLACEMENT-bsllu/retR */
    0, /* !REPLACEMENT-bzi/fR/retR */
    0, /* !REPLACEMENT-bziu/fR/retR */
    0, /* !REPLACEMENT-bzl/fR/retR */
    0, /* !REPLACEMENT-bzlu/fR/retR */
    0, /* !REPLACEMENT-call/retR */
    0, /* !REPLACEMENT-close/retR */
    0, /* !REPLACEMENT-divi/retR */
    0, /* !REPLACEMENT-diviu/retR */
    0, /* !REPLACEMENT-divl/retR */
    0, /* !REPLACEMENT-divlu/retR */
    0, /* !REPLACEMENT-endhl/retR */
    0, /* !REPLACEMENT-endsc/retR */
    0, /* !REPLACEMENT-exit/retR */
    0, /* !REPLACEMENT-exitvm/retR */
    0, /* !REPLACEMENT-flush/retR */
    0, /* !REPLACEMENT-ioflags/retR */
    0, /* !REPLACEMENT-iogetb/retR */
    0, /* !REPLACEMENT-iosetb/retR */
    0, /* !REPLACEMENT-iosize/retR */
    0, /* !REPLACEMENT-map/retR */
    0, /* !REPLACEMENT-modi/retR */
    0, /* !REPLACEMENT-modiu/retR */
    0, /* !REPLACEMENT-modl/retR */
    0, /* !REPLACEMENT-modlu/retR */
    0, /* !REPLACEMENT-muli/retR */
    0, /* !REPLACEMENT-mull/retR */
    0, /* !REPLACEMENT-negi/retR */
    0, /* !REPLACEMENT-negiu/retR */
    0, /* !REPLACEMENT-negl/retR */
    0, /* !REPLACEMENT-neglu/retR */
    0, /* !REPLACEMENT-open/retR */
    0, /* !REPLACEMENT-peekdi/nR/retR */
    0, /* !REPLACEMENT-peekdiu/nR/retR */
    0, /* !REPLACEMENT-peekdl/nR/retR */
    0, /* !REPLACEMENT-peekdlu/nR/retR */
    0, /* !REPLACEMENT-peeki/nR/nR/nR/retR */
    0, /* !REPLACEMENT-peekiu/nR/nR/retR */
    0, /* !REPLACEMENT-peekl/nR/nR/nR/retR */
    0, /* !REPLACEMENT-peeklu/nR/nR/retR */
    0, /* !REPLACEMENT-peeks/retR */
    0, /* !REPLACEMENT-pokedi/nR/retR */
    0, /* !REPLACEMENT-pokediu/nR/retR */
    0, /* !REPLACEMENT-pokedl/nR/retR */
    0, /* !REPLACEMENT-pokedlu/nR/retR */
    0, /* !REPLACEMENT-pokei/nR/nR/nR/retR */
    0, /* !REPLACEMENT-pokeiu/nR/nR/retR */
    0, /* !REPLACEMENT-pokel/nR/nR/nR/retR */
    0, /* !REPLACEMENT-pokelu/nR/nR/retR */
    0, /* !REPLACEMENT-pokes/retR */
    0, /* !REPLACEMENT-popios/retR */
    0, /* !REPLACEMENT-popob/retR */
    0, /* !REPLACEMENT-popom/retR */
    0, /* !REPLACEMENT-powi/retR */
    0, /* !REPLACEMENT-powiu/retR */
    0, /* !REPLACEMENT-powl/retR */
    0, /* !REPLACEMENT-powlu/retR */
    0, /* !REPLACEMENT-prolog/retR */
    0, /* !REPLACEMENT-pushios/retR */
    0, /* !REPLACEMENT-pushtopvar/nR/retR */
    0, /* !REPLACEMENT-raise/retR */
    0, /* !REPLACEMENT-reloc/retR */
    0, /* !REPLACEMENT-return/retR */
    0, /* !REPLACEMENT-sleep/retR */
    0, /* !REPLACEMENT-smodi/retR */
    0, /* !REPLACEMENT-sref/retR */
    0, /* !REPLACEMENT-srefi/retR */
    0, /* !REPLACEMENT-srefia/retR */
    0, /* !REPLACEMENT-srefio/retR */
    0, /* !REPLACEMENT-srefo/retR */
    0, /* !REPLACEMENT-sset/retR */
    0, /* !REPLACEMENT-sseti/retR */
    0, /* !REPLACEMENT-strref/retR */
    0, /* !REPLACEMENT-strset/retR */
    0, /* !REPLACEMENT-subi/retR */
    0, /* !REPLACEMENT-subl/retR */
    0, /* !REPLACEMENT-substr/retR */
    0, /* !REPLACEMENT-sync/retR */
    0, /* !REPLACEMENT-unreachable/retR */
    0, /* !REPLACEMENT-ureloc/retR */
    0 /* !REPLACEMENT-write/retR */
  };

#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT


void
pvm_rewrite (struct jitter_mutable_routine *jitter_mutable_routine_p)
{
  JITTTER_REWRITE_FUNCTION_PROLOG_;

/* User-specified code, rewriter part: beginning. */

/* User-specified code, rewriter part: end */


//asm volatile ("\n# checking swap-drop-to-nip");
//fprintf (stderr, "Trying rule 1 of 10, \"swap-drop-to-nip\" (line 6484)\n");
/* Rewrite rule "swap-drop-to-nip" */
#line 6484 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6481 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, swap)
#line 6482 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule swap-drop-to-nip (line 6484) fires...\n");
#line 6484 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip);
    //fprintf (stderr, "  ...End of the rule swap-drop-to-nip\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking rot-rot-to-nrot");
//fprintf (stderr, "Trying rule 2 of 10, \"rot-rot-to-nrot\" (line 6490)\n");
/* Rewrite rule "rot-rot-to-nrot" */
#line 6490 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6487 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, rot)
#line 6488 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, rot)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule rot-rot-to-nrot (line 6490) fires...\n");
#line 6490 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nrot\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nrot);
    //fprintf (stderr, "  ...End of the rule rot-rot-to-nrot\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip-nip-to-nip2");
//fprintf (stderr, "Trying rule 3 of 10, \"nip-nip-to-nip2\" (line 6496)\n");
/* Rewrite rule "nip-nip-to-nip2" */
#line 6496 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6493 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip)
#line 6494 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip-nip-to-nip2 (line 6496) fires...\n");
#line 6496 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip2\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip2);
    //fprintf (stderr, "  ...End of the rule nip-nip-to-nip2\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking nip2-nip-to-nip3");
//fprintf (stderr, "Trying rule 4 of 10, \"nip2-nip-to-nip3\" (line 6502)\n");
/* Rewrite rule "nip2-nip-to-nip3" */
#line 6502 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6499 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, nip2)
#line 6500 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, nip)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule nip2-nip-to-nip3 (line 6502) fires...\n");
#line 6502 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction nip3\n");
    JITTER_RULE_APPEND_INSTRUCTION_(nip3);
    //fprintf (stderr, "  ...End of the rule nip2-nip-to-nip3\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop-drop-to-drop2");
//fprintf (stderr, "Trying rule 5 of 10, \"drop-drop-to-drop2\" (line 6508)\n");
/* Rewrite rule "drop-drop-to-drop2" */
#line 6508 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6505 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop)
#line 6506 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop-drop-to-drop2 (line 6508) fires...\n");
#line 6508 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop2\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop2);
    //fprintf (stderr, "  ...End of the rule drop-drop-to-drop2\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop2-drop-to-drop3");
//fprintf (stderr, "Trying rule 6 of 10, \"drop2-drop-to-drop3\" (line 6514)\n");
/* Rewrite rule "drop2-drop-to-drop3" */
#line 6514 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6511 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop2)
#line 6512 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop2-drop-to-drop3 (line 6514) fires...\n");
#line 6514 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop3\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop3);
    //fprintf (stderr, "  ...End of the rule drop2-drop-to-drop3\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking drop3-drop-to-drop4");
//fprintf (stderr, "Trying rule 7 of 10, \"drop3-drop-to-drop4\" (line 6520)\n");
/* Rewrite rule "drop3-drop-to-drop4" */
#line 6520 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6517 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, drop3)
#line 6518 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule drop3-drop-to-drop4 (line 6520) fires...\n");
#line 6520 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction drop4\n");
    JITTER_RULE_APPEND_INSTRUCTION_(drop4);
    //fprintf (stderr, "  ...End of the rule drop3-drop-to-drop4\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking swap-over-to-tuck");
//fprintf (stderr, "Trying rule 8 of 10, \"swap-over-to-tuck\" (line 6526)\n");
/* Rewrite rule "swap-over-to-tuck" */
#line 6526 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6523 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, swap)
#line 6524 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, over)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule swap-over-to-tuck (line 6526) fires...\n");
#line 6526 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction tuck\n");
    JITTER_RULE_APPEND_INSTRUCTION_(tuck);
    //fprintf (stderr, "  ...End of the rule swap-over-to-tuck\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking rot-swap-to-quake");
//fprintf (stderr, "Trying rule 9 of 10, \"rot-swap-to-quake\" (line 6532)\n");
/* Rewrite rule "rot-swap-to-quake" */
#line 6532 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6529 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, rot)
#line 6530 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, swap)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule rot-swap-to-quake (line 6532) fires...\n");
#line 6532 "../../libpoke/pvm.jitter"
    //fprintf (stderr, "    rewrite: adding instruction quake\n");
    JITTER_RULE_APPEND_INSTRUCTION_(quake);
    //fprintf (stderr, "  ...End of the rule rot-swap-to-quake\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//asm volatile ("\n# checking push-drop-to-nop");
//fprintf (stderr, "Trying rule 10 of 10, \"push-drop-to-nop\" (line 6537)\n");
/* Rewrite rule "push-drop-to-nop" */
#line 6537 "../../libpoke/pvm.jitter"
JITTER_RULE_BEGIN(2)
  JITTER_RULE_BEGIN_PLACEHOLDER_DECLARATIONS
    JITTER_RULE_DECLARE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DECLARATIONS
  JITTER_RULE_BEGIN_CONDITIONS
    /* Check opcodes first: they are likely not to match, and in */
    /* that case we want to fail as early as possible. */
#line 6535 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(0, push)
#line 6536 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_OPCODE(1, drop)
    /* Check arguments, binding placeholders.  We don't have to worry */
    /* about arity, since the opcodes match if we're here. */
#line 6535 "../../libpoke/pvm.jitter"
    JITTER_RULE_CONDITION_MATCH_PLACEHOLDER(0, 0, a)
    /* Rule guard. */
    JITTER_RULE_CONDITION(
      true
                         )
  JITTER_RULE_END_CONDITIONS
  JITTER_RULE_BEGIN_PLACEHOLDER_CLONING
    JITTER_RULE_CLONE_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_CLONING
  JITTER_RULE_BEGIN_BODY
  //fprintf (stderr, "* The rule push-drop-to-nop (line 6537) fires...\n");
    //fprintf (stderr, "  ...End of the rule push-drop-to-nop\n");
  JITTER_RULE_END_BODY
  JITTER_RULE_BEGIN_PLACEHOLDER_DESTRUCTION
    JITTER_RULE_DESTROY_PLACEHOLDER_(a);
  JITTER_RULE_END_PLACEHOLDER_DESTRUCTION
JITTER_RULE_END

//fprintf (stderr, "No more rules to try\n");
}


//#include <jitter/jitter-fatal.h>

//#include <jitter/jitter.h>
//#include <jitter/jitter-instruction.h>
//#include <jitter/jitter-specialize.h>

//#include "pvm-vm.h"
//#include "pvm-meta-instructions.h"
//#include "pvm-specialized-instructions.h"


/* Recognizer function prototypes. */
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ains (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_and (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arem (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_asettb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_atr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_beghl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_begsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnoti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_boriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsll (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsllu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsri (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxoriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_canary (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_close (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ctos (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_disas (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_diviu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_duc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endhl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eql (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqs (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exit (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_flush (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formati (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formati__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_fromr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_geiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ges (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_getenv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_indent (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ioflags (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iogetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_isa (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_leiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_les (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_map (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgeto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mka (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mko (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mksct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktya (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyany (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktys (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktysct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mseto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mullu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muls (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neglu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_not (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nrot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetbt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_open (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_or (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_osetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_over (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeks (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pope (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popexite (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prints (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_quake (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_raise (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rand (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_regvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_reloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sconc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_siz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sleep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_smodi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_spropc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sproph (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sprops (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefia (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefio (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefmnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sseti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sublu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_substr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sync (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_time (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tor (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tuck (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagett (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyisc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyissct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_typof (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tysctn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unmap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ureloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_write (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
  __attribute__ ((pure));


/* Recognizer function definitions. */
inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_addlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_addlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ains (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ains;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_and (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_and;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_aref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_arefo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_arem (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_arem;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_aset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_aset;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_asettb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_asettb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_atr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_atr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_ba__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ba__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ba__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bandlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bandlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_beghl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_beghl__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_begsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_begsc__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bn__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bn__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnn__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnn__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnn__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnoti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnoti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnotlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnotlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzi__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzi__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnziu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnziu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzl__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzl__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bnzlu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bnzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bnzlu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bori;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_boriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_boriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_borl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_borlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_borlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsli;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsliu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsll (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsll;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsllu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsllu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsri (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsri;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsrl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bsrlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bsrlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxori (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxori;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxoriu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxoriu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxorl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bxorlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bxorlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzi__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzi__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzi__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bziu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bziu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bziu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzl__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzl__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzl__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_bzlu__fR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_bzlu__fR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_bzlu__fR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_call (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_call__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_canary (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_canary;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_close (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_close;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ctos (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ctos;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_disas (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_disas__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divi__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_diviu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_diviu__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divl__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_divlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_divlu__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_drop4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_drop4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_duc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_duc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_dup (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_dup;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endhl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_endhl__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_endsc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_endsc__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eql (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eql;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_eqs (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_eqs;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exit (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_exit;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_exitvm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_exitvm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_flush (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_flush;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formati (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_formati__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formati__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_formati__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_formatiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_formatiu__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_formatl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_formatl__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_formatlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_formatlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_formatlu__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_fromr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_fromr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_geiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_geiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ges (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ges;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_getenv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_getenv;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gtlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_gts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_gts;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_indent (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_indent__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ioflags (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ioflags;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iogetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iogetb__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iosetb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iosize (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iosize;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_isa (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_isa;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_itolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_itolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_itolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_iutolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_iutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_iutolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_leiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_leiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_les (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_les;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_ltolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ltolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ltolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lts (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lts;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutoi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutoi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutoiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutoiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutoiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutol__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutol__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutol__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_lutolu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_lutolu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_lutolu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_map (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_map;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgeto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgeto;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgets;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetsel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetsiz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mgetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mgetw;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mka (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mka;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mko (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mko;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mksct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mksct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktya (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktya;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyany (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyany;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktys (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktys;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktysct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktysct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mktyv (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mktyv;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modi__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modiu__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modl__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_modlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_modlu__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mseto (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mseto;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msets (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msets;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetsel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetsiz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetsiz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_msetw (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_msetw;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muli (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muli;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muliu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muliu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mull (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mull;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_mullu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_mullu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_muls (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_muls;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nec;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_negl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_negl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neglu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_neglu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nei;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_neiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_neiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nelu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nes;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nip3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nip3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nnn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nnn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nop (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nop;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_not (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_not;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_note__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_note__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_note__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_nrot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_nrot;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetbt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetbt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ogetu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ogetu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_open (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_open;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_or (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_or;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_osetm (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_osetm;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_over (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_over;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pec (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pec;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdiu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdl__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekdlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekdlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekdlu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeki__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeki__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeki__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekiu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekiu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peekl__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peekl__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peekl__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeklu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_peeklu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeklu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeklu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_peeks (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_peeks;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokediu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokediu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokediu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedl__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokedlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokedlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokedlu__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokei__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokei__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokei__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokeiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokeiu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokeiu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokeiu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokel__nR__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokel__nR__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokel__nR__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokelu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pokelu__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokelu__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokelu__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pokes (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pokes;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pope (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pope;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popend;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popexite (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popexite;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popf__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popf__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoac;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popob;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popobc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popod;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popom;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popoo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popopp;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_popr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_popvar__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_popvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_popvar__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_powlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_powlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printi__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printi__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printiu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printiu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printiu__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printl__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printl__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printl__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_printlu__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_printlu__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_printlu__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prints (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_prints__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_prolog (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_prolog;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_push__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_push__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_push32__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_push32__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push32__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_push32__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_push32__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushe__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushe__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushe__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushend (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushend;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushf__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushf__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushf__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushhi__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushhi__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushhi__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushhi__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushhi__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushios (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushios;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushlo__nR (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_label)
      && (res = pvm_recognize_specialized_instruction_pushlo__lR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushlo__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushlo__lR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushlo__lR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoac (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoac;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushob (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushob;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushobc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushobc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushod (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushod;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushom (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushom;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushoo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushoo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushopp (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushopp;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_pushr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushtopvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushtopvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushtopvar__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__n0__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n0;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n1;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__n5;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__n0__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__n0__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 0 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n0 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 1 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n1 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 2 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n2 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 5 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__n5 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_pushvar__nR__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n0 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n0;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n1 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n1;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n2 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n2;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__n5 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__n5;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_pushvar__nR__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_pushvar__nR__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_quake (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_quake;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_raise (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_raise;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rand (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_rand;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_regvar (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_regvar;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_reloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_reloc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_restorer___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_restorer___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_restorer___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_return (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_return;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 3 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_revn__n3 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal && (* ps)->literal.fixnum == 4 && enable_fast_literals)
      && (res = pvm_recognize_specialized_instruction_revn__n4 (ps + 1, enable_fast_literals)))
    goto done;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_revn__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n3 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__n3;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__n4 (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__n4;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_revn__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_revn__nR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_rot (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_rot;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_saver___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_saver___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_saver___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sconc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sconc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sel (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sel;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_register_id)
      && (res = pvm_recognize_specialized_instruction_setr___rrR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_setr___rrR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_setr___rrR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_siz (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_siz;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sleep (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sleep;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_smodi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_smodi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_spropc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_spropc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sproph (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sproph;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sprops (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sprops;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefia (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefia;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefio (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefio;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefmnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefmnt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefnt (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefnt;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_srefo (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_srefo;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sset;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sseti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sseti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  enum pvm_specialized_instruction_opcode res = pvm_specialized_instruction_opcode__eINVALID;
  if (((* ps)->type == jitter_parameter_type_literal)
      && (res = pvm_recognize_specialized_instruction_strace__nR (ps + 1, enable_fast_literals)))
    goto done;
done:
  return res;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strace__nR (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_strace__nR__retR;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strref (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_strref;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_strset (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_strset;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subi (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subi;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_subl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_subl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sublu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sublu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_substr (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_substr;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swap;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgti (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgti;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtiu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtiu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtl (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtl;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_swapgtlu (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_swapgtlu;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_sync (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_sync;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_time (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_time;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tor (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tor;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tuck (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tuck;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagetb (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyagetb;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyagett (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyagett;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyisc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyisc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tyissct (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tyissct;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_typof (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_typof;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_tysctn (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_tysctn;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unmap (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_unmap;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_unreachable (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_unreachable;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_ureloc (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_ureloc;
}

inline static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction_write (struct jitter_parameter ** const ps,
                                               bool enable_fast_literals)
{
  /* The prefix is a full specialized instruction.  We're done recognizing it. */
  return pvm_specialized_instruction_opcode_write__retR;
}



/* Recognizer entry point. */
static enum pvm_specialized_instruction_opcode
pvm_recognize_specialized_instruction (struct jitter_mutable_routine *p,
                                            const struct jitter_instruction *ins)
{
  bool fl = ! p->options.slow_literals_only;
  const struct jitter_meta_instruction *mi = ins->meta_instruction;
  switch (mi->id)
    {
    case pvm_meta_instruction_id_addi:
      return pvm_recognize_specialized_instruction_addi (ins->parameters, fl);
    case pvm_meta_instruction_id_addiu:
      return pvm_recognize_specialized_instruction_addiu (ins->parameters, fl);
    case pvm_meta_instruction_id_addl:
      return pvm_recognize_specialized_instruction_addl (ins->parameters, fl);
    case pvm_meta_instruction_id_addlu:
      return pvm_recognize_specialized_instruction_addlu (ins->parameters, fl);
    case pvm_meta_instruction_id_ains:
      return pvm_recognize_specialized_instruction_ains (ins->parameters, fl);
    case pvm_meta_instruction_id_and:
      return pvm_recognize_specialized_instruction_and (ins->parameters, fl);
    case pvm_meta_instruction_id_aref:
      return pvm_recognize_specialized_instruction_aref (ins->parameters, fl);
    case pvm_meta_instruction_id_arefo:
      return pvm_recognize_specialized_instruction_arefo (ins->parameters, fl);
    case pvm_meta_instruction_id_arem:
      return pvm_recognize_specialized_instruction_arem (ins->parameters, fl);
    case pvm_meta_instruction_id_aset:
      return pvm_recognize_specialized_instruction_aset (ins->parameters, fl);
    case pvm_meta_instruction_id_asettb:
      return pvm_recognize_specialized_instruction_asettb (ins->parameters, fl);
    case pvm_meta_instruction_id_atr:
      return pvm_recognize_specialized_instruction_atr (ins->parameters, fl);
    case pvm_meta_instruction_id_ba:
      return pvm_recognize_specialized_instruction_ba (ins->parameters, fl);
    case pvm_meta_instruction_id_bandi:
      return pvm_recognize_specialized_instruction_bandi (ins->parameters, fl);
    case pvm_meta_instruction_id_bandiu:
      return pvm_recognize_specialized_instruction_bandiu (ins->parameters, fl);
    case pvm_meta_instruction_id_bandl:
      return pvm_recognize_specialized_instruction_bandl (ins->parameters, fl);
    case pvm_meta_instruction_id_bandlu:
      return pvm_recognize_specialized_instruction_bandlu (ins->parameters, fl);
    case pvm_meta_instruction_id_beghl:
      return pvm_recognize_specialized_instruction_beghl (ins->parameters, fl);
    case pvm_meta_instruction_id_begsc:
      return pvm_recognize_specialized_instruction_begsc (ins->parameters, fl);
    case pvm_meta_instruction_id_bn:
      return pvm_recognize_specialized_instruction_bn (ins->parameters, fl);
    case pvm_meta_instruction_id_bnn:
      return pvm_recognize_specialized_instruction_bnn (ins->parameters, fl);
    case pvm_meta_instruction_id_bnoti:
      return pvm_recognize_specialized_instruction_bnoti (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotiu:
      return pvm_recognize_specialized_instruction_bnotiu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotl:
      return pvm_recognize_specialized_instruction_bnotl (ins->parameters, fl);
    case pvm_meta_instruction_id_bnotlu:
      return pvm_recognize_specialized_instruction_bnotlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzi:
      return pvm_recognize_specialized_instruction_bnzi (ins->parameters, fl);
    case pvm_meta_instruction_id_bnziu:
      return pvm_recognize_specialized_instruction_bnziu (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzl:
      return pvm_recognize_specialized_instruction_bnzl (ins->parameters, fl);
    case pvm_meta_instruction_id_bnzlu:
      return pvm_recognize_specialized_instruction_bnzlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bori:
      return pvm_recognize_specialized_instruction_bori (ins->parameters, fl);
    case pvm_meta_instruction_id_boriu:
      return pvm_recognize_specialized_instruction_boriu (ins->parameters, fl);
    case pvm_meta_instruction_id_borl:
      return pvm_recognize_specialized_instruction_borl (ins->parameters, fl);
    case pvm_meta_instruction_id_borlu:
      return pvm_recognize_specialized_instruction_borlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsli:
      return pvm_recognize_specialized_instruction_bsli (ins->parameters, fl);
    case pvm_meta_instruction_id_bsliu:
      return pvm_recognize_specialized_instruction_bsliu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsll:
      return pvm_recognize_specialized_instruction_bsll (ins->parameters, fl);
    case pvm_meta_instruction_id_bsllu:
      return pvm_recognize_specialized_instruction_bsllu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsri:
      return pvm_recognize_specialized_instruction_bsri (ins->parameters, fl);
    case pvm_meta_instruction_id_bsriu:
      return pvm_recognize_specialized_instruction_bsriu (ins->parameters, fl);
    case pvm_meta_instruction_id_bsrl:
      return pvm_recognize_specialized_instruction_bsrl (ins->parameters, fl);
    case pvm_meta_instruction_id_bsrlu:
      return pvm_recognize_specialized_instruction_bsrlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bxori:
      return pvm_recognize_specialized_instruction_bxori (ins->parameters, fl);
    case pvm_meta_instruction_id_bxoriu:
      return pvm_recognize_specialized_instruction_bxoriu (ins->parameters, fl);
    case pvm_meta_instruction_id_bxorl:
      return pvm_recognize_specialized_instruction_bxorl (ins->parameters, fl);
    case pvm_meta_instruction_id_bxorlu:
      return pvm_recognize_specialized_instruction_bxorlu (ins->parameters, fl);
    case pvm_meta_instruction_id_bzi:
      return pvm_recognize_specialized_instruction_bzi (ins->parameters, fl);
    case pvm_meta_instruction_id_bziu:
      return pvm_recognize_specialized_instruction_bziu (ins->parameters, fl);
    case pvm_meta_instruction_id_bzl:
      return pvm_recognize_specialized_instruction_bzl (ins->parameters, fl);
    case pvm_meta_instruction_id_bzlu:
      return pvm_recognize_specialized_instruction_bzlu (ins->parameters, fl);
    case pvm_meta_instruction_id_call:
      return pvm_recognize_specialized_instruction_call (ins->parameters, fl);
    case pvm_meta_instruction_id_canary:
      return pvm_recognize_specialized_instruction_canary (ins->parameters, fl);
    case pvm_meta_instruction_id_close:
      return pvm_recognize_specialized_instruction_close (ins->parameters, fl);
    case pvm_meta_instruction_id_ctos:
      return pvm_recognize_specialized_instruction_ctos (ins->parameters, fl);
    case pvm_meta_instruction_id_disas:
      return pvm_recognize_specialized_instruction_disas (ins->parameters, fl);
    case pvm_meta_instruction_id_divi:
      return pvm_recognize_specialized_instruction_divi (ins->parameters, fl);
    case pvm_meta_instruction_id_diviu:
      return pvm_recognize_specialized_instruction_diviu (ins->parameters, fl);
    case pvm_meta_instruction_id_divl:
      return pvm_recognize_specialized_instruction_divl (ins->parameters, fl);
    case pvm_meta_instruction_id_divlu:
      return pvm_recognize_specialized_instruction_divlu (ins->parameters, fl);
    case pvm_meta_instruction_id_drop:
      return pvm_recognize_specialized_instruction_drop (ins->parameters, fl);
    case pvm_meta_instruction_id_drop2:
      return pvm_recognize_specialized_instruction_drop2 (ins->parameters, fl);
    case pvm_meta_instruction_id_drop3:
      return pvm_recognize_specialized_instruction_drop3 (ins->parameters, fl);
    case pvm_meta_instruction_id_drop4:
      return pvm_recognize_specialized_instruction_drop4 (ins->parameters, fl);
    case pvm_meta_instruction_id_duc:
      return pvm_recognize_specialized_instruction_duc (ins->parameters, fl);
    case pvm_meta_instruction_id_dup:
      return pvm_recognize_specialized_instruction_dup (ins->parameters, fl);
    case pvm_meta_instruction_id_endhl:
      return pvm_recognize_specialized_instruction_endhl (ins->parameters, fl);
    case pvm_meta_instruction_id_endsc:
      return pvm_recognize_specialized_instruction_endsc (ins->parameters, fl);
    case pvm_meta_instruction_id_eqc:
      return pvm_recognize_specialized_instruction_eqc (ins->parameters, fl);
    case pvm_meta_instruction_id_eqi:
      return pvm_recognize_specialized_instruction_eqi (ins->parameters, fl);
    case pvm_meta_instruction_id_eqiu:
      return pvm_recognize_specialized_instruction_eqiu (ins->parameters, fl);
    case pvm_meta_instruction_id_eql:
      return pvm_recognize_specialized_instruction_eql (ins->parameters, fl);
    case pvm_meta_instruction_id_eqlu:
      return pvm_recognize_specialized_instruction_eqlu (ins->parameters, fl);
    case pvm_meta_instruction_id_eqs:
      return pvm_recognize_specialized_instruction_eqs (ins->parameters, fl);
    case pvm_meta_instruction_id_exit:
      return pvm_recognize_specialized_instruction_exit (ins->parameters, fl);
    case pvm_meta_instruction_id_exitvm:
      return pvm_recognize_specialized_instruction_exitvm (ins->parameters, fl);
    case pvm_meta_instruction_id_flush:
      return pvm_recognize_specialized_instruction_flush (ins->parameters, fl);
    case pvm_meta_instruction_id_formati:
      return pvm_recognize_specialized_instruction_formati (ins->parameters, fl);
    case pvm_meta_instruction_id_formatiu:
      return pvm_recognize_specialized_instruction_formatiu (ins->parameters, fl);
    case pvm_meta_instruction_id_formatl:
      return pvm_recognize_specialized_instruction_formatl (ins->parameters, fl);
    case pvm_meta_instruction_id_formatlu:
      return pvm_recognize_specialized_instruction_formatlu (ins->parameters, fl);
    case pvm_meta_instruction_id_fromr:
      return pvm_recognize_specialized_instruction_fromr (ins->parameters, fl);
    case pvm_meta_instruction_id_gei:
      return pvm_recognize_specialized_instruction_gei (ins->parameters, fl);
    case pvm_meta_instruction_id_geiu:
      return pvm_recognize_specialized_instruction_geiu (ins->parameters, fl);
    case pvm_meta_instruction_id_gel:
      return pvm_recognize_specialized_instruction_gel (ins->parameters, fl);
    case pvm_meta_instruction_id_gelu:
      return pvm_recognize_specialized_instruction_gelu (ins->parameters, fl);
    case pvm_meta_instruction_id_ges:
      return pvm_recognize_specialized_instruction_ges (ins->parameters, fl);
    case pvm_meta_instruction_id_getenv:
      return pvm_recognize_specialized_instruction_getenv (ins->parameters, fl);
    case pvm_meta_instruction_id_gti:
      return pvm_recognize_specialized_instruction_gti (ins->parameters, fl);
    case pvm_meta_instruction_id_gtiu:
      return pvm_recognize_specialized_instruction_gtiu (ins->parameters, fl);
    case pvm_meta_instruction_id_gtl:
      return pvm_recognize_specialized_instruction_gtl (ins->parameters, fl);
    case pvm_meta_instruction_id_gtlu:
      return pvm_recognize_specialized_instruction_gtlu (ins->parameters, fl);
    case pvm_meta_instruction_id_gts:
      return pvm_recognize_specialized_instruction_gts (ins->parameters, fl);
    case pvm_meta_instruction_id_indent:
      return pvm_recognize_specialized_instruction_indent (ins->parameters, fl);
    case pvm_meta_instruction_id_ioflags:
      return pvm_recognize_specialized_instruction_ioflags (ins->parameters, fl);
    case pvm_meta_instruction_id_iogetb:
      return pvm_recognize_specialized_instruction_iogetb (ins->parameters, fl);
    case pvm_meta_instruction_id_iosetb:
      return pvm_recognize_specialized_instruction_iosetb (ins->parameters, fl);
    case pvm_meta_instruction_id_iosize:
      return pvm_recognize_specialized_instruction_iosize (ins->parameters, fl);
    case pvm_meta_instruction_id_isa:
      return pvm_recognize_specialized_instruction_isa (ins->parameters, fl);
    case pvm_meta_instruction_id_itoi:
      return pvm_recognize_specialized_instruction_itoi (ins->parameters, fl);
    case pvm_meta_instruction_id_itoiu:
      return pvm_recognize_specialized_instruction_itoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_itol:
      return pvm_recognize_specialized_instruction_itol (ins->parameters, fl);
    case pvm_meta_instruction_id_itolu:
      return pvm_recognize_specialized_instruction_itolu (ins->parameters, fl);
    case pvm_meta_instruction_id_iutoi:
      return pvm_recognize_specialized_instruction_iutoi (ins->parameters, fl);
    case pvm_meta_instruction_id_iutoiu:
      return pvm_recognize_specialized_instruction_iutoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_iutol:
      return pvm_recognize_specialized_instruction_iutol (ins->parameters, fl);
    case pvm_meta_instruction_id_iutolu:
      return pvm_recognize_specialized_instruction_iutolu (ins->parameters, fl);
    case pvm_meta_instruction_id_lei:
      return pvm_recognize_specialized_instruction_lei (ins->parameters, fl);
    case pvm_meta_instruction_id_leiu:
      return pvm_recognize_specialized_instruction_leiu (ins->parameters, fl);
    case pvm_meta_instruction_id_lel:
      return pvm_recognize_specialized_instruction_lel (ins->parameters, fl);
    case pvm_meta_instruction_id_lelu:
      return pvm_recognize_specialized_instruction_lelu (ins->parameters, fl);
    case pvm_meta_instruction_id_les:
      return pvm_recognize_specialized_instruction_les (ins->parameters, fl);
    case pvm_meta_instruction_id_lti:
      return pvm_recognize_specialized_instruction_lti (ins->parameters, fl);
    case pvm_meta_instruction_id_ltiu:
      return pvm_recognize_specialized_instruction_ltiu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltl:
      return pvm_recognize_specialized_instruction_ltl (ins->parameters, fl);
    case pvm_meta_instruction_id_ltlu:
      return pvm_recognize_specialized_instruction_ltlu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltoi:
      return pvm_recognize_specialized_instruction_ltoi (ins->parameters, fl);
    case pvm_meta_instruction_id_ltoiu:
      return pvm_recognize_specialized_instruction_ltoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_ltol:
      return pvm_recognize_specialized_instruction_ltol (ins->parameters, fl);
    case pvm_meta_instruction_id_ltolu:
      return pvm_recognize_specialized_instruction_ltolu (ins->parameters, fl);
    case pvm_meta_instruction_id_lts:
      return pvm_recognize_specialized_instruction_lts (ins->parameters, fl);
    case pvm_meta_instruction_id_lutoi:
      return pvm_recognize_specialized_instruction_lutoi (ins->parameters, fl);
    case pvm_meta_instruction_id_lutoiu:
      return pvm_recognize_specialized_instruction_lutoiu (ins->parameters, fl);
    case pvm_meta_instruction_id_lutol:
      return pvm_recognize_specialized_instruction_lutol (ins->parameters, fl);
    case pvm_meta_instruction_id_lutolu:
      return pvm_recognize_specialized_instruction_lutolu (ins->parameters, fl);
    case pvm_meta_instruction_id_map:
      return pvm_recognize_specialized_instruction_map (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetios:
      return pvm_recognize_specialized_instruction_mgetios (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetm:
      return pvm_recognize_specialized_instruction_mgetm (ins->parameters, fl);
    case pvm_meta_instruction_id_mgeto:
      return pvm_recognize_specialized_instruction_mgeto (ins->parameters, fl);
    case pvm_meta_instruction_id_mgets:
      return pvm_recognize_specialized_instruction_mgets (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetsel:
      return pvm_recognize_specialized_instruction_mgetsel (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetsiz:
      return pvm_recognize_specialized_instruction_mgetsiz (ins->parameters, fl);
    case pvm_meta_instruction_id_mgetw:
      return pvm_recognize_specialized_instruction_mgetw (ins->parameters, fl);
    case pvm_meta_instruction_id_mka:
      return pvm_recognize_specialized_instruction_mka (ins->parameters, fl);
    case pvm_meta_instruction_id_mko:
      return pvm_recognize_specialized_instruction_mko (ins->parameters, fl);
    case pvm_meta_instruction_id_mksct:
      return pvm_recognize_specialized_instruction_mksct (ins->parameters, fl);
    case pvm_meta_instruction_id_mktya:
      return pvm_recognize_specialized_instruction_mktya (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyany:
      return pvm_recognize_specialized_instruction_mktyany (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyc:
      return pvm_recognize_specialized_instruction_mktyc (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyi:
      return pvm_recognize_specialized_instruction_mktyi (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyo:
      return pvm_recognize_specialized_instruction_mktyo (ins->parameters, fl);
    case pvm_meta_instruction_id_mktys:
      return pvm_recognize_specialized_instruction_mktys (ins->parameters, fl);
    case pvm_meta_instruction_id_mktysct:
      return pvm_recognize_specialized_instruction_mktysct (ins->parameters, fl);
    case pvm_meta_instruction_id_mktyv:
      return pvm_recognize_specialized_instruction_mktyv (ins->parameters, fl);
    case pvm_meta_instruction_id_mm:
      return pvm_recognize_specialized_instruction_mm (ins->parameters, fl);
    case pvm_meta_instruction_id_modi:
      return pvm_recognize_specialized_instruction_modi (ins->parameters, fl);
    case pvm_meta_instruction_id_modiu:
      return pvm_recognize_specialized_instruction_modiu (ins->parameters, fl);
    case pvm_meta_instruction_id_modl:
      return pvm_recognize_specialized_instruction_modl (ins->parameters, fl);
    case pvm_meta_instruction_id_modlu:
      return pvm_recognize_specialized_instruction_modlu (ins->parameters, fl);
    case pvm_meta_instruction_id_msetios:
      return pvm_recognize_specialized_instruction_msetios (ins->parameters, fl);
    case pvm_meta_instruction_id_msetm:
      return pvm_recognize_specialized_instruction_msetm (ins->parameters, fl);
    case pvm_meta_instruction_id_mseto:
      return pvm_recognize_specialized_instruction_mseto (ins->parameters, fl);
    case pvm_meta_instruction_id_msets:
      return pvm_recognize_specialized_instruction_msets (ins->parameters, fl);
    case pvm_meta_instruction_id_msetsel:
      return pvm_recognize_specialized_instruction_msetsel (ins->parameters, fl);
    case pvm_meta_instruction_id_msetsiz:
      return pvm_recognize_specialized_instruction_msetsiz (ins->parameters, fl);
    case pvm_meta_instruction_id_msetw:
      return pvm_recognize_specialized_instruction_msetw (ins->parameters, fl);
    case pvm_meta_instruction_id_muli:
      return pvm_recognize_specialized_instruction_muli (ins->parameters, fl);
    case pvm_meta_instruction_id_muliu:
      return pvm_recognize_specialized_instruction_muliu (ins->parameters, fl);
    case pvm_meta_instruction_id_mull:
      return pvm_recognize_specialized_instruction_mull (ins->parameters, fl);
    case pvm_meta_instruction_id_mullu:
      return pvm_recognize_specialized_instruction_mullu (ins->parameters, fl);
    case pvm_meta_instruction_id_muls:
      return pvm_recognize_specialized_instruction_muls (ins->parameters, fl);
    case pvm_meta_instruction_id_nec:
      return pvm_recognize_specialized_instruction_nec (ins->parameters, fl);
    case pvm_meta_instruction_id_negi:
      return pvm_recognize_specialized_instruction_negi (ins->parameters, fl);
    case pvm_meta_instruction_id_negiu:
      return pvm_recognize_specialized_instruction_negiu (ins->parameters, fl);
    case pvm_meta_instruction_id_negl:
      return pvm_recognize_specialized_instruction_negl (ins->parameters, fl);
    case pvm_meta_instruction_id_neglu:
      return pvm_recognize_specialized_instruction_neglu (ins->parameters, fl);
    case pvm_meta_instruction_id_nei:
      return pvm_recognize_specialized_instruction_nei (ins->parameters, fl);
    case pvm_meta_instruction_id_neiu:
      return pvm_recognize_specialized_instruction_neiu (ins->parameters, fl);
    case pvm_meta_instruction_id_nel:
      return pvm_recognize_specialized_instruction_nel (ins->parameters, fl);
    case pvm_meta_instruction_id_nelu:
      return pvm_recognize_specialized_instruction_nelu (ins->parameters, fl);
    case pvm_meta_instruction_id_nes:
      return pvm_recognize_specialized_instruction_nes (ins->parameters, fl);
    case pvm_meta_instruction_id_nip:
      return pvm_recognize_specialized_instruction_nip (ins->parameters, fl);
    case pvm_meta_instruction_id_nip2:
      return pvm_recognize_specialized_instruction_nip2 (ins->parameters, fl);
    case pvm_meta_instruction_id_nip3:
      return pvm_recognize_specialized_instruction_nip3 (ins->parameters, fl);
    case pvm_meta_instruction_id_nn:
      return pvm_recognize_specialized_instruction_nn (ins->parameters, fl);
    case pvm_meta_instruction_id_nnn:
      return pvm_recognize_specialized_instruction_nnn (ins->parameters, fl);
    case pvm_meta_instruction_id_nop:
      return pvm_recognize_specialized_instruction_nop (ins->parameters, fl);
    case pvm_meta_instruction_id_not:
      return pvm_recognize_specialized_instruction_not (ins->parameters, fl);
    case pvm_meta_instruction_id_note:
      return pvm_recognize_specialized_instruction_note (ins->parameters, fl);
    case pvm_meta_instruction_id_nrot:
      return pvm_recognize_specialized_instruction_nrot (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetbt:
      return pvm_recognize_specialized_instruction_ogetbt (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetm:
      return pvm_recognize_specialized_instruction_ogetm (ins->parameters, fl);
    case pvm_meta_instruction_id_ogetu:
      return pvm_recognize_specialized_instruction_ogetu (ins->parameters, fl);
    case pvm_meta_instruction_id_open:
      return pvm_recognize_specialized_instruction_open (ins->parameters, fl);
    case pvm_meta_instruction_id_or:
      return pvm_recognize_specialized_instruction_or (ins->parameters, fl);
    case pvm_meta_instruction_id_osetm:
      return pvm_recognize_specialized_instruction_osetm (ins->parameters, fl);
    case pvm_meta_instruction_id_over:
      return pvm_recognize_specialized_instruction_over (ins->parameters, fl);
    case pvm_meta_instruction_id_pec:
      return pvm_recognize_specialized_instruction_pec (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdi:
      return pvm_recognize_specialized_instruction_peekdi (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdiu:
      return pvm_recognize_specialized_instruction_peekdiu (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdl:
      return pvm_recognize_specialized_instruction_peekdl (ins->parameters, fl);
    case pvm_meta_instruction_id_peekdlu:
      return pvm_recognize_specialized_instruction_peekdlu (ins->parameters, fl);
    case pvm_meta_instruction_id_peeki:
      return pvm_recognize_specialized_instruction_peeki (ins->parameters, fl);
    case pvm_meta_instruction_id_peekiu:
      return pvm_recognize_specialized_instruction_peekiu (ins->parameters, fl);
    case pvm_meta_instruction_id_peekl:
      return pvm_recognize_specialized_instruction_peekl (ins->parameters, fl);
    case pvm_meta_instruction_id_peeklu:
      return pvm_recognize_specialized_instruction_peeklu (ins->parameters, fl);
    case pvm_meta_instruction_id_peeks:
      return pvm_recognize_specialized_instruction_peeks (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedi:
      return pvm_recognize_specialized_instruction_pokedi (ins->parameters, fl);
    case pvm_meta_instruction_id_pokediu:
      return pvm_recognize_specialized_instruction_pokediu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedl:
      return pvm_recognize_specialized_instruction_pokedl (ins->parameters, fl);
    case pvm_meta_instruction_id_pokedlu:
      return pvm_recognize_specialized_instruction_pokedlu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokei:
      return pvm_recognize_specialized_instruction_pokei (ins->parameters, fl);
    case pvm_meta_instruction_id_pokeiu:
      return pvm_recognize_specialized_instruction_pokeiu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokel:
      return pvm_recognize_specialized_instruction_pokel (ins->parameters, fl);
    case pvm_meta_instruction_id_pokelu:
      return pvm_recognize_specialized_instruction_pokelu (ins->parameters, fl);
    case pvm_meta_instruction_id_pokes:
      return pvm_recognize_specialized_instruction_pokes (ins->parameters, fl);
    case pvm_meta_instruction_id_pope:
      return pvm_recognize_specialized_instruction_pope (ins->parameters, fl);
    case pvm_meta_instruction_id_popend:
      return pvm_recognize_specialized_instruction_popend (ins->parameters, fl);
    case pvm_meta_instruction_id_popexite:
      return pvm_recognize_specialized_instruction_popexite (ins->parameters, fl);
    case pvm_meta_instruction_id_popf:
      return pvm_recognize_specialized_instruction_popf (ins->parameters, fl);
    case pvm_meta_instruction_id_popios:
      return pvm_recognize_specialized_instruction_popios (ins->parameters, fl);
    case pvm_meta_instruction_id_popoac:
      return pvm_recognize_specialized_instruction_popoac (ins->parameters, fl);
    case pvm_meta_instruction_id_popob:
      return pvm_recognize_specialized_instruction_popob (ins->parameters, fl);
    case pvm_meta_instruction_id_popobc:
      return pvm_recognize_specialized_instruction_popobc (ins->parameters, fl);
    case pvm_meta_instruction_id_popoc:
      return pvm_recognize_specialized_instruction_popoc (ins->parameters, fl);
    case pvm_meta_instruction_id_popod:
      return pvm_recognize_specialized_instruction_popod (ins->parameters, fl);
    case pvm_meta_instruction_id_popoi:
      return pvm_recognize_specialized_instruction_popoi (ins->parameters, fl);
    case pvm_meta_instruction_id_popom:
      return pvm_recognize_specialized_instruction_popom (ins->parameters, fl);
    case pvm_meta_instruction_id_popoo:
      return pvm_recognize_specialized_instruction_popoo (ins->parameters, fl);
    case pvm_meta_instruction_id_popopp:
      return pvm_recognize_specialized_instruction_popopp (ins->parameters, fl);
    case pvm_meta_instruction_id_popr:
      return pvm_recognize_specialized_instruction_popr (ins->parameters, fl);
    case pvm_meta_instruction_id_popvar:
      return pvm_recognize_specialized_instruction_popvar (ins->parameters, fl);
    case pvm_meta_instruction_id_powi:
      return pvm_recognize_specialized_instruction_powi (ins->parameters, fl);
    case pvm_meta_instruction_id_powiu:
      return pvm_recognize_specialized_instruction_powiu (ins->parameters, fl);
    case pvm_meta_instruction_id_powl:
      return pvm_recognize_specialized_instruction_powl (ins->parameters, fl);
    case pvm_meta_instruction_id_powlu:
      return pvm_recognize_specialized_instruction_powlu (ins->parameters, fl);
    case pvm_meta_instruction_id_printi:
      return pvm_recognize_specialized_instruction_printi (ins->parameters, fl);
    case pvm_meta_instruction_id_printiu:
      return pvm_recognize_specialized_instruction_printiu (ins->parameters, fl);
    case pvm_meta_instruction_id_printl:
      return pvm_recognize_specialized_instruction_printl (ins->parameters, fl);
    case pvm_meta_instruction_id_printlu:
      return pvm_recognize_specialized_instruction_printlu (ins->parameters, fl);
    case pvm_meta_instruction_id_prints:
      return pvm_recognize_specialized_instruction_prints (ins->parameters, fl);
    case pvm_meta_instruction_id_prolog:
      return pvm_recognize_specialized_instruction_prolog (ins->parameters, fl);
    case pvm_meta_instruction_id_push:
      return pvm_recognize_specialized_instruction_push (ins->parameters, fl);
    case pvm_meta_instruction_id_push32:
      return pvm_recognize_specialized_instruction_push32 (ins->parameters, fl);
    case pvm_meta_instruction_id_pushe:
      return pvm_recognize_specialized_instruction_pushe (ins->parameters, fl);
    case pvm_meta_instruction_id_pushend:
      return pvm_recognize_specialized_instruction_pushend (ins->parameters, fl);
    case pvm_meta_instruction_id_pushf:
      return pvm_recognize_specialized_instruction_pushf (ins->parameters, fl);
    case pvm_meta_instruction_id_pushhi:
      return pvm_recognize_specialized_instruction_pushhi (ins->parameters, fl);
    case pvm_meta_instruction_id_pushios:
      return pvm_recognize_specialized_instruction_pushios (ins->parameters, fl);
    case pvm_meta_instruction_id_pushlo:
      return pvm_recognize_specialized_instruction_pushlo (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoac:
      return pvm_recognize_specialized_instruction_pushoac (ins->parameters, fl);
    case pvm_meta_instruction_id_pushob:
      return pvm_recognize_specialized_instruction_pushob (ins->parameters, fl);
    case pvm_meta_instruction_id_pushobc:
      return pvm_recognize_specialized_instruction_pushobc (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoc:
      return pvm_recognize_specialized_instruction_pushoc (ins->parameters, fl);
    case pvm_meta_instruction_id_pushod:
      return pvm_recognize_specialized_instruction_pushod (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoi:
      return pvm_recognize_specialized_instruction_pushoi (ins->parameters, fl);
    case pvm_meta_instruction_id_pushom:
      return pvm_recognize_specialized_instruction_pushom (ins->parameters, fl);
    case pvm_meta_instruction_id_pushoo:
      return pvm_recognize_specialized_instruction_pushoo (ins->parameters, fl);
    case pvm_meta_instruction_id_pushopp:
      return pvm_recognize_specialized_instruction_pushopp (ins->parameters, fl);
    case pvm_meta_instruction_id_pushr:
      return pvm_recognize_specialized_instruction_pushr (ins->parameters, fl);
    case pvm_meta_instruction_id_pushtopvar:
      return pvm_recognize_specialized_instruction_pushtopvar (ins->parameters, fl);
    case pvm_meta_instruction_id_pushvar:
      return pvm_recognize_specialized_instruction_pushvar (ins->parameters, fl);
    case pvm_meta_instruction_id_quake:
      return pvm_recognize_specialized_instruction_quake (ins->parameters, fl);
    case pvm_meta_instruction_id_raise:
      return pvm_recognize_specialized_instruction_raise (ins->parameters, fl);
    case pvm_meta_instruction_id_rand:
      return pvm_recognize_specialized_instruction_rand (ins->parameters, fl);
    case pvm_meta_instruction_id_regvar:
      return pvm_recognize_specialized_instruction_regvar (ins->parameters, fl);
    case pvm_meta_instruction_id_reloc:
      return pvm_recognize_specialized_instruction_reloc (ins->parameters, fl);
    case pvm_meta_instruction_id_restorer:
      return pvm_recognize_specialized_instruction_restorer (ins->parameters, fl);
    case pvm_meta_instruction_id_return:
      return pvm_recognize_specialized_instruction_return (ins->parameters, fl);
    case pvm_meta_instruction_id_revn:
      return pvm_recognize_specialized_instruction_revn (ins->parameters, fl);
    case pvm_meta_instruction_id_rot:
      return pvm_recognize_specialized_instruction_rot (ins->parameters, fl);
    case pvm_meta_instruction_id_saver:
      return pvm_recognize_specialized_instruction_saver (ins->parameters, fl);
    case pvm_meta_instruction_id_sconc:
      return pvm_recognize_specialized_instruction_sconc (ins->parameters, fl);
    case pvm_meta_instruction_id_sel:
      return pvm_recognize_specialized_instruction_sel (ins->parameters, fl);
    case pvm_meta_instruction_id_setr:
      return pvm_recognize_specialized_instruction_setr (ins->parameters, fl);
    case pvm_meta_instruction_id_siz:
      return pvm_recognize_specialized_instruction_siz (ins->parameters, fl);
    case pvm_meta_instruction_id_sleep:
      return pvm_recognize_specialized_instruction_sleep (ins->parameters, fl);
    case pvm_meta_instruction_id_smodi:
      return pvm_recognize_specialized_instruction_smodi (ins->parameters, fl);
    case pvm_meta_instruction_id_spropc:
      return pvm_recognize_specialized_instruction_spropc (ins->parameters, fl);
    case pvm_meta_instruction_id_sproph:
      return pvm_recognize_specialized_instruction_sproph (ins->parameters, fl);
    case pvm_meta_instruction_id_sprops:
      return pvm_recognize_specialized_instruction_sprops (ins->parameters, fl);
    case pvm_meta_instruction_id_sref:
      return pvm_recognize_specialized_instruction_sref (ins->parameters, fl);
    case pvm_meta_instruction_id_srefi:
      return pvm_recognize_specialized_instruction_srefi (ins->parameters, fl);
    case pvm_meta_instruction_id_srefia:
      return pvm_recognize_specialized_instruction_srefia (ins->parameters, fl);
    case pvm_meta_instruction_id_srefio:
      return pvm_recognize_specialized_instruction_srefio (ins->parameters, fl);
    case pvm_meta_instruction_id_srefmnt:
      return pvm_recognize_specialized_instruction_srefmnt (ins->parameters, fl);
    case pvm_meta_instruction_id_srefnt:
      return pvm_recognize_specialized_instruction_srefnt (ins->parameters, fl);
    case pvm_meta_instruction_id_srefo:
      return pvm_recognize_specialized_instruction_srefo (ins->parameters, fl);
    case pvm_meta_instruction_id_sset:
      return pvm_recognize_specialized_instruction_sset (ins->parameters, fl);
    case pvm_meta_instruction_id_sseti:
      return pvm_recognize_specialized_instruction_sseti (ins->parameters, fl);
    case pvm_meta_instruction_id_strace:
      return pvm_recognize_specialized_instruction_strace (ins->parameters, fl);
    case pvm_meta_instruction_id_strref:
      return pvm_recognize_specialized_instruction_strref (ins->parameters, fl);
    case pvm_meta_instruction_id_strset:
      return pvm_recognize_specialized_instruction_strset (ins->parameters, fl);
    case pvm_meta_instruction_id_subi:
      return pvm_recognize_specialized_instruction_subi (ins->parameters, fl);
    case pvm_meta_instruction_id_subiu:
      return pvm_recognize_specialized_instruction_subiu (ins->parameters, fl);
    case pvm_meta_instruction_id_subl:
      return pvm_recognize_specialized_instruction_subl (ins->parameters, fl);
    case pvm_meta_instruction_id_sublu:
      return pvm_recognize_specialized_instruction_sublu (ins->parameters, fl);
    case pvm_meta_instruction_id_substr:
      return pvm_recognize_specialized_instruction_substr (ins->parameters, fl);
    case pvm_meta_instruction_id_swap:
      return pvm_recognize_specialized_instruction_swap (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgti:
      return pvm_recognize_specialized_instruction_swapgti (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtiu:
      return pvm_recognize_specialized_instruction_swapgtiu (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtl:
      return pvm_recognize_specialized_instruction_swapgtl (ins->parameters, fl);
    case pvm_meta_instruction_id_swapgtlu:
      return pvm_recognize_specialized_instruction_swapgtlu (ins->parameters, fl);
    case pvm_meta_instruction_id_sync:
      return pvm_recognize_specialized_instruction_sync (ins->parameters, fl);
    case pvm_meta_instruction_id_time:
      return pvm_recognize_specialized_instruction_time (ins->parameters, fl);
    case pvm_meta_instruction_id_tor:
      return pvm_recognize_specialized_instruction_tor (ins->parameters, fl);
    case pvm_meta_instruction_id_tuck:
      return pvm_recognize_specialized_instruction_tuck (ins->parameters, fl);
    case pvm_meta_instruction_id_tyagetb:
      return pvm_recognize_specialized_instruction_tyagetb (ins->parameters, fl);
    case pvm_meta_instruction_id_tyagett:
      return pvm_recognize_specialized_instruction_tyagett (ins->parameters, fl);
    case pvm_meta_instruction_id_tyisc:
      return pvm_recognize_specialized_instruction_tyisc (ins->parameters, fl);
    case pvm_meta_instruction_id_tyissct:
      return pvm_recognize_specialized_instruction_tyissct (ins->parameters, fl);
    case pvm_meta_instruction_id_typof:
      return pvm_recognize_specialized_instruction_typof (ins->parameters, fl);
    case pvm_meta_instruction_id_tysctn:
      return pvm_recognize_specialized_instruction_tysctn (ins->parameters, fl);
    case pvm_meta_instruction_id_unmap:
      return pvm_recognize_specialized_instruction_unmap (ins->parameters, fl);
    case pvm_meta_instruction_id_unreachable:
      return pvm_recognize_specialized_instruction_unreachable (ins->parameters, fl);
    case pvm_meta_instruction_id_ureloc:
      return pvm_recognize_specialized_instruction_ureloc (ins->parameters, fl);
    case pvm_meta_instruction_id_write:
      return pvm_recognize_specialized_instruction_write (ins->parameters, fl);
    default:
      jitter_fatal ("invalid meta-instruction id %i", (int)mi->id);
    }
  __builtin_unreachable ();
}

/* Specializer entry point: the only non-static function here. */
int
pvm_specialize_instruction (struct jitter_mutable_routine *p,
                                 const struct jitter_instruction *ins)
{
  enum pvm_specialized_instruction_opcode opcode
    = pvm_recognize_specialized_instruction (p, ins);
  if (opcode == pvm_specialized_instruction_opcode__eINVALID)
    jitter_fatal ("specialization failed: %s", ins->meta_instruction->name);

#ifdef JITTER_HAVE_DEFECT_REPLACEMENT
  /* Replace the opcode with its non-defective counterpart. */
  opcode = pvm_replacement_table [opcode];
#endif // #ifdef JITTER_HAVE_DEFECT_REPLACEMENT

  jitter_add_specialized_instruction_opcode (p, opcode);


  /* FIXME: in the old shell-based generator I grouped specialized instructions by
     their "residual parameter map", yielding a switch with a lot of different
     specialized instructions mapping to the same case.  I should redo that here. */
  switch (opcode)
    {
    /* !INVALID. */
    case pvm_specialized_instruction_opcode__eINVALID:
      break;

    /* !BEGINBASICBLOCK. */
    case pvm_specialized_instruction_opcode__eBEGINBASICBLOCK:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* !EXITVM. */
    case pvm_specialized_instruction_opcode__eEXITVM:
      break;

    /* !DATALOCATIONS. */
    case pvm_specialized_instruction_opcode__eDATALOCATIONS:
      break;

    /* !NOP. */
    case pvm_specialized_instruction_opcode__eNOP:
      break;

    /* !UNREACHABLE0. */
    case pvm_specialized_instruction_opcode__eUNREACHABLE0:
      break;

    /* !UNREACHABLE1. */
    case pvm_specialized_instruction_opcode__eUNREACHABLE1:
      break;

    /* !PRETENDTOJUMPANYWHERE. */
    case pvm_specialized_instruction_opcode__ePRETENDTOJUMPANYWHERE:
      break;

    /* addi. */
    case pvm_specialized_instruction_opcode_addi:
      break;

    /* addiu. */
    case pvm_specialized_instruction_opcode_addiu:
      break;

    /* addl. */
    case pvm_specialized_instruction_opcode_addl:
      break;

    /* addlu. */
    case pvm_specialized_instruction_opcode_addlu:
      break;

    /* ains. */
    case pvm_specialized_instruction_opcode_ains:
      break;

    /* and. */
    case pvm_specialized_instruction_opcode_and:
      break;

    /* aref. */
    case pvm_specialized_instruction_opcode_aref:
      break;

    /* arefo. */
    case pvm_specialized_instruction_opcode_arefo:
      break;

    /* arem. */
    case pvm_specialized_instruction_opcode_arem:
      break;

    /* aset. */
    case pvm_specialized_instruction_opcode_aset:
      break;

    /* asettb. */
    case pvm_specialized_instruction_opcode_asettb:
      break;

    /* atr. */
    case pvm_specialized_instruction_opcode_atr:
      break;

    /* ba/fR. */
    case pvm_specialized_instruction_opcode_ba__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bandi. */
    case pvm_specialized_instruction_opcode_bandi:
      break;

    /* bandiu. */
    case pvm_specialized_instruction_opcode_bandiu:
      break;

    /* bandl. */
    case pvm_specialized_instruction_opcode_bandl:
      break;

    /* bandlu. */
    case pvm_specialized_instruction_opcode_bandlu:
      break;

    /* beghl/retR. */
    case pvm_specialized_instruction_opcode_beghl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* begsc/retR. */
    case pvm_specialized_instruction_opcode_begsc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* bn/fR. */
    case pvm_specialized_instruction_opcode_bn__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bnn/fR. */
    case pvm_specialized_instruction_opcode_bnn__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bnoti. */
    case pvm_specialized_instruction_opcode_bnoti:
      break;

    /* bnotiu. */
    case pvm_specialized_instruction_opcode_bnotiu:
      break;

    /* bnotl. */
    case pvm_specialized_instruction_opcode_bnotl:
      break;

    /* bnotlu. */
    case pvm_specialized_instruction_opcode_bnotlu:
      break;

    /* bnzi/fR. */
    case pvm_specialized_instruction_opcode_bnzi__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bnziu/fR. */
    case pvm_specialized_instruction_opcode_bnziu__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bnzl/fR. */
    case pvm_specialized_instruction_opcode_bnzl__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bnzlu/fR. */
    case pvm_specialized_instruction_opcode_bnzlu__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bori. */
    case pvm_specialized_instruction_opcode_bori:
      break;

    /* boriu. */
    case pvm_specialized_instruction_opcode_boriu:
      break;

    /* borl. */
    case pvm_specialized_instruction_opcode_borl:
      break;

    /* borlu. */
    case pvm_specialized_instruction_opcode_borlu:
      break;

    /* bsli. */
    case pvm_specialized_instruction_opcode_bsli:
      break;

    /* bsliu. */
    case pvm_specialized_instruction_opcode_bsliu:
      break;

    /* bsll. */
    case pvm_specialized_instruction_opcode_bsll:
      break;

    /* bsllu. */
    case pvm_specialized_instruction_opcode_bsllu:
      break;

    /* bsri. */
    case pvm_specialized_instruction_opcode_bsri:
      break;

    /* bsriu. */
    case pvm_specialized_instruction_opcode_bsriu:
      break;

    /* bsrl. */
    case pvm_specialized_instruction_opcode_bsrl:
      break;

    /* bsrlu. */
    case pvm_specialized_instruction_opcode_bsrlu:
      break;

    /* bxori. */
    case pvm_specialized_instruction_opcode_bxori:
      break;

    /* bxoriu. */
    case pvm_specialized_instruction_opcode_bxoriu:
      break;

    /* bxorl. */
    case pvm_specialized_instruction_opcode_bxorl:
      break;

    /* bxorlu. */
    case pvm_specialized_instruction_opcode_bxorlu:
      break;

    /* bzi/fR. */
    case pvm_specialized_instruction_opcode_bzi__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bziu/fR. */
    case pvm_specialized_instruction_opcode_bziu__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bzl/fR. */
    case pvm_specialized_instruction_opcode_bzl__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* bzlu/fR. */
    case pvm_specialized_instruction_opcode_bzlu__fR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* call/retR. */
    case pvm_specialized_instruction_opcode_call__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* canary. */
    case pvm_specialized_instruction_opcode_canary:
      break;

    /* close. */
    case pvm_specialized_instruction_opcode_close:
      break;

    /* ctos. */
    case pvm_specialized_instruction_opcode_ctos:
      break;

    /* disas/retR. */
    case pvm_specialized_instruction_opcode_disas__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divi/retR. */
    case pvm_specialized_instruction_opcode_divi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* diviu/retR. */
    case pvm_specialized_instruction_opcode_diviu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divl/retR. */
    case pvm_specialized_instruction_opcode_divl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* divlu/retR. */
    case pvm_specialized_instruction_opcode_divlu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* drop. */
    case pvm_specialized_instruction_opcode_drop:
      break;

    /* drop2. */
    case pvm_specialized_instruction_opcode_drop2:
      break;

    /* drop3. */
    case pvm_specialized_instruction_opcode_drop3:
      break;

    /* drop4. */
    case pvm_specialized_instruction_opcode_drop4:
      break;

    /* duc. */
    case pvm_specialized_instruction_opcode_duc:
      break;

    /* dup. */
    case pvm_specialized_instruction_opcode_dup:
      break;

    /* endhl/retR. */
    case pvm_specialized_instruction_opcode_endhl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* endsc/retR. */
    case pvm_specialized_instruction_opcode_endsc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* eqc. */
    case pvm_specialized_instruction_opcode_eqc:
      break;

    /* eqi. */
    case pvm_specialized_instruction_opcode_eqi:
      break;

    /* eqiu. */
    case pvm_specialized_instruction_opcode_eqiu:
      break;

    /* eql. */
    case pvm_specialized_instruction_opcode_eql:
      break;

    /* eqlu. */
    case pvm_specialized_instruction_opcode_eqlu:
      break;

    /* eqs. */
    case pvm_specialized_instruction_opcode_eqs:
      break;

    /* exit. */
    case pvm_specialized_instruction_opcode_exit:
      break;

    /* exitvm. */
    case pvm_specialized_instruction_opcode_exitvm:
      break;

    /* flush. */
    case pvm_specialized_instruction_opcode_flush:
      break;

    /* formati/nR/retR. */
    case pvm_specialized_instruction_opcode_formati__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* formatiu/nR/retR. */
    case pvm_specialized_instruction_opcode_formatiu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* formatl/nR/retR. */
    case pvm_specialized_instruction_opcode_formatl__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* formatlu/nR/retR. */
    case pvm_specialized_instruction_opcode_formatlu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* fromr. */
    case pvm_specialized_instruction_opcode_fromr:
      break;

    /* gei. */
    case pvm_specialized_instruction_opcode_gei:
      break;

    /* geiu. */
    case pvm_specialized_instruction_opcode_geiu:
      break;

    /* gel. */
    case pvm_specialized_instruction_opcode_gel:
      break;

    /* gelu. */
    case pvm_specialized_instruction_opcode_gelu:
      break;

    /* ges. */
    case pvm_specialized_instruction_opcode_ges:
      break;

    /* getenv. */
    case pvm_specialized_instruction_opcode_getenv:
      break;

    /* gti. */
    case pvm_specialized_instruction_opcode_gti:
      break;

    /* gtiu. */
    case pvm_specialized_instruction_opcode_gtiu:
      break;

    /* gtl. */
    case pvm_specialized_instruction_opcode_gtl:
      break;

    /* gtlu. */
    case pvm_specialized_instruction_opcode_gtlu:
      break;

    /* gts. */
    case pvm_specialized_instruction_opcode_gts:
      break;

    /* indent/retR. */
    case pvm_specialized_instruction_opcode_indent__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* ioflags. */
    case pvm_specialized_instruction_opcode_ioflags:
      break;

    /* iogetb/retR. */
    case pvm_specialized_instruction_opcode_iogetb__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* iosetb. */
    case pvm_specialized_instruction_opcode_iosetb:
      break;

    /* iosize. */
    case pvm_specialized_instruction_opcode_iosize:
      break;

    /* isa. */
    case pvm_specialized_instruction_opcode_isa:
      break;

    /* itoi/nR. */
    case pvm_specialized_instruction_opcode_itoi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* itoiu/nR. */
    case pvm_specialized_instruction_opcode_itoiu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* itol/nR. */
    case pvm_specialized_instruction_opcode_itol__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* itolu/nR. */
    case pvm_specialized_instruction_opcode_itolu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* iutoi/nR. */
    case pvm_specialized_instruction_opcode_iutoi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* iutoiu/nR. */
    case pvm_specialized_instruction_opcode_iutoiu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* iutol/nR. */
    case pvm_specialized_instruction_opcode_iutol__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* iutolu/nR. */
    case pvm_specialized_instruction_opcode_iutolu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* lei. */
    case pvm_specialized_instruction_opcode_lei:
      break;

    /* leiu. */
    case pvm_specialized_instruction_opcode_leiu:
      break;

    /* lel. */
    case pvm_specialized_instruction_opcode_lel:
      break;

    /* lelu. */
    case pvm_specialized_instruction_opcode_lelu:
      break;

    /* les. */
    case pvm_specialized_instruction_opcode_les:
      break;

    /* lti. */
    case pvm_specialized_instruction_opcode_lti:
      break;

    /* ltiu. */
    case pvm_specialized_instruction_opcode_ltiu:
      break;

    /* ltl. */
    case pvm_specialized_instruction_opcode_ltl:
      break;

    /* ltlu. */
    case pvm_specialized_instruction_opcode_ltlu:
      break;

    /* ltoi/nR. */
    case pvm_specialized_instruction_opcode_ltoi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* ltoiu/nR. */
    case pvm_specialized_instruction_opcode_ltoiu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* ltol/nR. */
    case pvm_specialized_instruction_opcode_ltol__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* ltolu/nR. */
    case pvm_specialized_instruction_opcode_ltolu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* lts. */
    case pvm_specialized_instruction_opcode_lts:
      break;

    /* lutoi/nR. */
    case pvm_specialized_instruction_opcode_lutoi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* lutoiu/nR. */
    case pvm_specialized_instruction_opcode_lutoiu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* lutol/nR. */
    case pvm_specialized_instruction_opcode_lutol__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* lutolu/nR. */
    case pvm_specialized_instruction_opcode_lutolu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* map. */
    case pvm_specialized_instruction_opcode_map:
      break;

    /* mgetios. */
    case pvm_specialized_instruction_opcode_mgetios:
      break;

    /* mgetm. */
    case pvm_specialized_instruction_opcode_mgetm:
      break;

    /* mgeto. */
    case pvm_specialized_instruction_opcode_mgeto:
      break;

    /* mgets. */
    case pvm_specialized_instruction_opcode_mgets:
      break;

    /* mgetsel. */
    case pvm_specialized_instruction_opcode_mgetsel:
      break;

    /* mgetsiz. */
    case pvm_specialized_instruction_opcode_mgetsiz:
      break;

    /* mgetw. */
    case pvm_specialized_instruction_opcode_mgetw:
      break;

    /* mka. */
    case pvm_specialized_instruction_opcode_mka:
      break;

    /* mko. */
    case pvm_specialized_instruction_opcode_mko:
      break;

    /* mksct. */
    case pvm_specialized_instruction_opcode_mksct:
      break;

    /* mktya. */
    case pvm_specialized_instruction_opcode_mktya:
      break;

    /* mktyany. */
    case pvm_specialized_instruction_opcode_mktyany:
      break;

    /* mktyc. */
    case pvm_specialized_instruction_opcode_mktyc:
      break;

    /* mktyi. */
    case pvm_specialized_instruction_opcode_mktyi:
      break;

    /* mktyo. */
    case pvm_specialized_instruction_opcode_mktyo:
      break;

    /* mktys. */
    case pvm_specialized_instruction_opcode_mktys:
      break;

    /* mktysct. */
    case pvm_specialized_instruction_opcode_mktysct:
      break;

    /* mktyv. */
    case pvm_specialized_instruction_opcode_mktyv:
      break;

    /* mm. */
    case pvm_specialized_instruction_opcode_mm:
      break;

    /* modi/retR. */
    case pvm_specialized_instruction_opcode_modi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* modiu/retR. */
    case pvm_specialized_instruction_opcode_modiu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* modl/retR. */
    case pvm_specialized_instruction_opcode_modl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* modlu/retR. */
    case pvm_specialized_instruction_opcode_modlu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* msetios. */
    case pvm_specialized_instruction_opcode_msetios:
      break;

    /* msetm. */
    case pvm_specialized_instruction_opcode_msetm:
      break;

    /* mseto. */
    case pvm_specialized_instruction_opcode_mseto:
      break;

    /* msets. */
    case pvm_specialized_instruction_opcode_msets:
      break;

    /* msetsel. */
    case pvm_specialized_instruction_opcode_msetsel:
      break;

    /* msetsiz. */
    case pvm_specialized_instruction_opcode_msetsiz:
      break;

    /* msetw. */
    case pvm_specialized_instruction_opcode_msetw:
      break;

    /* muli. */
    case pvm_specialized_instruction_opcode_muli:
      break;

    /* muliu. */
    case pvm_specialized_instruction_opcode_muliu:
      break;

    /* mull. */
    case pvm_specialized_instruction_opcode_mull:
      break;

    /* mullu. */
    case pvm_specialized_instruction_opcode_mullu:
      break;

    /* muls. */
    case pvm_specialized_instruction_opcode_muls:
      break;

    /* nec. */
    case pvm_specialized_instruction_opcode_nec:
      break;

    /* negi. */
    case pvm_specialized_instruction_opcode_negi:
      break;

    /* negiu. */
    case pvm_specialized_instruction_opcode_negiu:
      break;

    /* negl. */
    case pvm_specialized_instruction_opcode_negl:
      break;

    /* neglu. */
    case pvm_specialized_instruction_opcode_neglu:
      break;

    /* nei. */
    case pvm_specialized_instruction_opcode_nei:
      break;

    /* neiu. */
    case pvm_specialized_instruction_opcode_neiu:
      break;

    /* nel. */
    case pvm_specialized_instruction_opcode_nel:
      break;

    /* nelu. */
    case pvm_specialized_instruction_opcode_nelu:
      break;

    /* nes. */
    case pvm_specialized_instruction_opcode_nes:
      break;

    /* nip. */
    case pvm_specialized_instruction_opcode_nip:
      break;

    /* nip2. */
    case pvm_specialized_instruction_opcode_nip2:
      break;

    /* nip3. */
    case pvm_specialized_instruction_opcode_nip3:
      break;

    /* nn. */
    case pvm_specialized_instruction_opcode_nn:
      break;

    /* nnn. */
    case pvm_specialized_instruction_opcode_nnn:
      break;

    /* nop. */
    case pvm_specialized_instruction_opcode_nop:
      break;

    /* not. */
    case pvm_specialized_instruction_opcode_not:
      break;

    /* note/nR. */
    case pvm_specialized_instruction_opcode_note__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* nrot. */
    case pvm_specialized_instruction_opcode_nrot:
      break;

    /* ogetbt. */
    case pvm_specialized_instruction_opcode_ogetbt:
      break;

    /* ogetm. */
    case pvm_specialized_instruction_opcode_ogetm:
      break;

    /* ogetu. */
    case pvm_specialized_instruction_opcode_ogetu:
      break;

    /* open. */
    case pvm_specialized_instruction_opcode_open:
      break;

    /* or. */
    case pvm_specialized_instruction_opcode_or:
      break;

    /* osetm. */
    case pvm_specialized_instruction_opcode_osetm:
      break;

    /* over. */
    case pvm_specialized_instruction_opcode_over:
      break;

    /* pec. */
    case pvm_specialized_instruction_opcode_pec:
      break;

    /* peekdi/nR. */
    case pvm_specialized_instruction_opcode_peekdi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* peekdiu/nR. */
    case pvm_specialized_instruction_opcode_peekdiu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* peekdl/nR. */
    case pvm_specialized_instruction_opcode_peekdl__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* peekdlu/nR. */
    case pvm_specialized_instruction_opcode_peekdlu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* peeki/nR/nR/nR. */
    case pvm_specialized_instruction_opcode_peeki__nR__nR__nR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    /* peekiu/nR/nR. */
    case pvm_specialized_instruction_opcode_peekiu__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* peekl/nR/nR/nR. */
    case pvm_specialized_instruction_opcode_peekl__nR__nR__nR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    /* peeklu/nR/nR. */
    case pvm_specialized_instruction_opcode_peeklu__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* peeks. */
    case pvm_specialized_instruction_opcode_peeks:
      break;

    /* pokedi/nR. */
    case pvm_specialized_instruction_opcode_pokedi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pokediu/nR. */
    case pvm_specialized_instruction_opcode_pokediu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pokedl/nR. */
    case pvm_specialized_instruction_opcode_pokedl__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pokedlu/nR. */
    case pvm_specialized_instruction_opcode_pokedlu__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pokei/nR/nR/nR. */
    case pvm_specialized_instruction_opcode_pokei__nR__nR__nR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    /* pokeiu/nR/nR. */
    case pvm_specialized_instruction_opcode_pokeiu__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* pokel/nR/nR/nR. */
    case pvm_specialized_instruction_opcode_pokel__nR__nR__nR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      break;

    /* pokelu/nR/nR. */
    case pvm_specialized_instruction_opcode_pokelu__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* pokes. */
    case pvm_specialized_instruction_opcode_pokes:
      break;

    /* pope. */
    case pvm_specialized_instruction_opcode_pope:
      break;

    /* popend. */
    case pvm_specialized_instruction_opcode_popend:
      break;

    /* popexite. */
    case pvm_specialized_instruction_opcode_popexite:
      break;

    /* popf/nR. */
    case pvm_specialized_instruction_opcode_popf__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* popios. */
    case pvm_specialized_instruction_opcode_popios:
      break;

    /* popoac. */
    case pvm_specialized_instruction_opcode_popoac:
      break;

    /* popob. */
    case pvm_specialized_instruction_opcode_popob:
      break;

    /* popobc. */
    case pvm_specialized_instruction_opcode_popobc:
      break;

    /* popoc. */
    case pvm_specialized_instruction_opcode_popoc:
      break;

    /* popod. */
    case pvm_specialized_instruction_opcode_popod:
      break;

    /* popoi. */
    case pvm_specialized_instruction_opcode_popoi:
      break;

    /* popom. */
    case pvm_specialized_instruction_opcode_popom:
      break;

    /* popoo. */
    case pvm_specialized_instruction_opcode_popoo:
      break;

    /* popopp. */
    case pvm_specialized_instruction_opcode_popopp:
      break;

    /* popr/%rR. */
    case pvm_specialized_instruction_opcode_popr___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* popvar/nR/nR. */
    case pvm_specialized_instruction_opcode_popvar__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* powi. */
    case pvm_specialized_instruction_opcode_powi:
      break;

    /* powiu. */
    case pvm_specialized_instruction_opcode_powiu:
      break;

    /* powl. */
    case pvm_specialized_instruction_opcode_powl:
      break;

    /* powlu. */
    case pvm_specialized_instruction_opcode_powlu:
      break;

    /* printi/nR/retR. */
    case pvm_specialized_instruction_opcode_printi__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* printiu/nR/retR. */
    case pvm_specialized_instruction_opcode_printiu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* printl/nR/retR. */
    case pvm_specialized_instruction_opcode_printl__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* printlu/nR/retR. */
    case pvm_specialized_instruction_opcode_printlu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* prints/retR. */
    case pvm_specialized_instruction_opcode_prints__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* prolog. */
    case pvm_specialized_instruction_opcode_prolog:
      /* This is a callee instruction. */
      break;

    /* push/nR. */
    case pvm_specialized_instruction_opcode_push__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* push/lR. */
    case pvm_specialized_instruction_opcode_push__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* push32/nR. */
    case pvm_specialized_instruction_opcode_push32__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* push32/lR. */
    case pvm_specialized_instruction_opcode_push32__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* pushe/lR. */
    case pvm_specialized_instruction_opcode_pushe__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* pushend. */
    case pvm_specialized_instruction_opcode_pushend:
      break;

    /* pushf/nR. */
    case pvm_specialized_instruction_opcode_pushf__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pushhi/nR. */
    case pvm_specialized_instruction_opcode_pushhi__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pushhi/lR. */
    case pvm_specialized_instruction_opcode_pushhi__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* pushios. */
    case pvm_specialized_instruction_opcode_pushios:
      break;

    /* pushlo/nR. */
    case pvm_specialized_instruction_opcode_pushlo__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pushlo/lR. */
    case pvm_specialized_instruction_opcode_pushlo__lR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      break;

    /* pushoac. */
    case pvm_specialized_instruction_opcode_pushoac:
      break;

    /* pushob. */
    case pvm_specialized_instruction_opcode_pushob:
      break;

    /* pushobc. */
    case pvm_specialized_instruction_opcode_pushobc:
      break;

    /* pushoc. */
    case pvm_specialized_instruction_opcode_pushoc:
      break;

    /* pushod. */
    case pvm_specialized_instruction_opcode_pushod:
      break;

    /* pushoi. */
    case pvm_specialized_instruction_opcode_pushoi:
      break;

    /* pushom. */
    case pvm_specialized_instruction_opcode_pushom:
      break;

    /* pushoo. */
    case pvm_specialized_instruction_opcode_pushoo:
      break;

    /* pushopp. */
    case pvm_specialized_instruction_opcode_pushopp:
      break;

    /* pushr/%rR. */
    case pvm_specialized_instruction_opcode_pushr___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* pushtopvar/nR. */
    case pvm_specialized_instruction_opcode_pushtopvar__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* pushvar/n0/n0. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n0:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/n1. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n1:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/n2. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n2:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/n3. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n3:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/n4. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n4:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/n5. */
    case pvm_specialized_instruction_opcode_pushvar__n0__n5:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/n0/nR. */
    case pvm_specialized_instruction_opcode_pushvar__n0__nR:
      /* j:0 residual_no:2 */
      /* Argument 0 (0-based, of 2) is non-residual */
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* pushvar/nR/n0. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n0:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/n1. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n1:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/n2. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n2:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/n3. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n3:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/n4. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n4:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/n5. */
    case pvm_specialized_instruction_opcode_pushvar__nR__n5:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Argument 1 (0-based, of 2) is non-residual */
      break;

    /* pushvar/nR/nR. */
    case pvm_specialized_instruction_opcode_pushvar__nR__nR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      break;

    /* quake. */
    case pvm_specialized_instruction_opcode_quake:
      break;

    /* raise. */
    case pvm_specialized_instruction_opcode_raise:
      break;

    /* rand. */
    case pvm_specialized_instruction_opcode_rand:
      break;

    /* regvar. */
    case pvm_specialized_instruction_opcode_regvar:
      break;

    /* reloc. */
    case pvm_specialized_instruction_opcode_reloc:
      break;

    /* restorer/%rR. */
    case pvm_specialized_instruction_opcode_restorer___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* return. */
    case pvm_specialized_instruction_opcode_return:
      break;

    /* revn/n3. */
    case pvm_specialized_instruction_opcode_revn__n3:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* revn/n4. */
    case pvm_specialized_instruction_opcode_revn__n4:
      /* j:0 residual_no:1 */
      /* Argument 0 (0-based, of 1) is non-residual */
      break;

    /* revn/nR. */
    case pvm_specialized_instruction_opcode_revn__nR:
      /* j:0 residual_no:1 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      break;

    /* rot. */
    case pvm_specialized_instruction_opcode_rot:
      break;

    /* saver/%rR. */
    case pvm_specialized_instruction_opcode_saver___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* sconc. */
    case pvm_specialized_instruction_opcode_sconc:
      break;

    /* sel. */
    case pvm_specialized_instruction_opcode_sel:
      break;

    /* setr/%rR. */
    case pvm_specialized_instruction_opcode_setr___rrR:
      /* j:0 residual_no:1 */
      /* A slow register is passed as a residual literal offset. */
      jitter_add_specialized_instruction_literal (p, PVM_SLOW_REGISTER_OFFSET(r, ins->parameters[0]->register_index));
      break;

    /* siz. */
    case pvm_specialized_instruction_opcode_siz:
      break;

    /* sleep. */
    case pvm_specialized_instruction_opcode_sleep:
      break;

    /* smodi. */
    case pvm_specialized_instruction_opcode_smodi:
      break;

    /* spropc. */
    case pvm_specialized_instruction_opcode_spropc:
      break;

    /* sproph. */
    case pvm_specialized_instruction_opcode_sproph:
      break;

    /* sprops. */
    case pvm_specialized_instruction_opcode_sprops:
      break;

    /* sref. */
    case pvm_specialized_instruction_opcode_sref:
      break;

    /* srefi. */
    case pvm_specialized_instruction_opcode_srefi:
      break;

    /* srefia. */
    case pvm_specialized_instruction_opcode_srefia:
      break;

    /* srefio. */
    case pvm_specialized_instruction_opcode_srefio:
      break;

    /* srefmnt. */
    case pvm_specialized_instruction_opcode_srefmnt:
      break;

    /* srefnt. */
    case pvm_specialized_instruction_opcode_srefnt:
      break;

    /* srefo. */
    case pvm_specialized_instruction_opcode_srefo:
      break;

    /* sset. */
    case pvm_specialized_instruction_opcode_sset:
      break;

    /* sseti. */
    case pvm_specialized_instruction_opcode_sseti:
      break;

    /* strace/nR/retR. */
    case pvm_specialized_instruction_opcode_strace__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* strref. */
    case pvm_specialized_instruction_opcode_strref:
      break;

    /* strset. */
    case pvm_specialized_instruction_opcode_strset:
      break;

    /* subi. */
    case pvm_specialized_instruction_opcode_subi:
      break;

    /* subiu. */
    case pvm_specialized_instruction_opcode_subiu:
      break;

    /* subl. */
    case pvm_specialized_instruction_opcode_subl:
      break;

    /* sublu. */
    case pvm_specialized_instruction_opcode_sublu:
      break;

    /* substr. */
    case pvm_specialized_instruction_opcode_substr:
      break;

    /* swap. */
    case pvm_specialized_instruction_opcode_swap:
      break;

    /* swapgti. */
    case pvm_specialized_instruction_opcode_swapgti:
      break;

    /* swapgtiu. */
    case pvm_specialized_instruction_opcode_swapgtiu:
      break;

    /* swapgtl. */
    case pvm_specialized_instruction_opcode_swapgtl:
      break;

    /* swapgtlu. */
    case pvm_specialized_instruction_opcode_swapgtlu:
      break;

    /* sync. */
    case pvm_specialized_instruction_opcode_sync:
      break;

    /* time. */
    case pvm_specialized_instruction_opcode_time:
      break;

    /* tor. */
    case pvm_specialized_instruction_opcode_tor:
      break;

    /* tuck. */
    case pvm_specialized_instruction_opcode_tuck:
      break;

    /* tyagetb. */
    case pvm_specialized_instruction_opcode_tyagetb:
      break;

    /* tyagett. */
    case pvm_specialized_instruction_opcode_tyagett:
      break;

    /* tyisc. */
    case pvm_specialized_instruction_opcode_tyisc:
      break;

    /* tyissct. */
    case pvm_specialized_instruction_opcode_tyissct:
      break;

    /* typof. */
    case pvm_specialized_instruction_opcode_typof:
      break;

    /* tysctn. */
    case pvm_specialized_instruction_opcode_tysctn:
      break;

    /* unmap. */
    case pvm_specialized_instruction_opcode_unmap:
      break;

    /* unreachable. */
    case pvm_specialized_instruction_opcode_unreachable:
      break;

    /* ureloc. */
    case pvm_specialized_instruction_opcode_ureloc:
      break;

    /* write/retR. */
    case pvm_specialized_instruction_opcode_write__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-addi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_maddi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-addl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_maddl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ains/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mains__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-aref/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_maref__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-arefo/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_marefo__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-arem/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_marem__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-aset/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_maset__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ba/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mba__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bn/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbn__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bnn/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbnn__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bnzi/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzi__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bnziu/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbnziu__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bnzl/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzl__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bnzlu/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbnzlu__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bsli/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbsli__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bsliu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbsliu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bsll/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbsll__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bsllu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbsllu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bzi/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbzi__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bziu/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbziu__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bzl/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbzl__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-bzlu/fR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mbzlu__fR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_label_index (p, ins->parameters[0]->label_as_index);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-call/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mcall__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    /* !REPLACEMENT-close/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mclose__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-divi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mdivi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-diviu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mdiviu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-divl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mdivl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-divlu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mdivlu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-endhl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mendhl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-endsc/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mendsc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-exit/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mexit__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-exitvm/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mexitvm__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-flush/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mflush__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ioflags/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mioflags__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-iogetb/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_miogetb__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-iosetb/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_miosetb__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-iosize/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_miosize__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-map/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmap__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-modi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmodi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-modiu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmodiu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-modl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmodl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-modlu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmodlu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-muli/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmuli__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-mull/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mmull__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-negi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mnegi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-negiu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mnegiu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-negl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mnegl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-neglu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mneglu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-open/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mopen__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekdi/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdi__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekdiu/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdiu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekdl/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdl__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekdlu/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekdlu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peeki/nR/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeki__nR__nR__nR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekiu/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekiu__nR__nR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peekl/nR/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeekl__nR__nR__nR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peeklu/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeklu__nR__nR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-peeks/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpeeks__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokedi/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedi__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokediu/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokediu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokedl/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedl__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokedlu/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokedlu__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokei/nR/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokei__nR__nR__nR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokeiu/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokeiu__nR__nR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokel/nR/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokel__nR__nR__nR__retR:
      /* j:0 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:4 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[2]->literal.ufixnum);
      /* j:3 residual_no:4 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokelu/nR/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokelu__nR__nR__retR:
      /* j:0 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:3 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[1]->literal.ufixnum);
      /* j:2 residual_no:3 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pokes/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpokes__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-popios/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpopios__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-popob/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpopob__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-popom/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpopom__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-powi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpowi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-powiu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpowiu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-powl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpowl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-powlu/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpowlu__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-prolog/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mprolog__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a callee instruction. */
      break;

    /* !REPLACEMENT-pushios/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpushios__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-pushtopvar/nR/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mpushtopvar__nR__retR:
      /* j:0 residual_no:2 */
      jitter_add_specialized_instruction_literal (p, ins->parameters[0]->literal.ufixnum);
      /* j:1 residual_no:2 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-raise/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mraise__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-reloc/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mreloc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-return/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mreturn__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-sleep/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msleep__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-smodi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msmodi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-sref/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msref__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-srefi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msrefi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-srefia/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msrefia__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-srefio/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msrefio__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-srefo/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msrefo__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-sset/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msset__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-sseti/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msseti__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-strref/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mstrref__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-strset/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mstrset__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-subi/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msubi__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-subl/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msubl__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-substr/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msubstr__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-sync/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_msync__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-unreachable/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_munreachable__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-ureloc/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mureloc__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      break;

    /* !REPLACEMENT-write/retR. */
    case pvm_specialized_instruction_opcode__eREPLACEMENT_mwrite__retR:
      /* j:0 residual_no:1 */
      /* Non-relocatable or callee [[?????FIXME: Do I want this?????]] instruction. 
         Special return-address parameter whose correct value will be patched in at specialization time. */
      jitter_add_specialized_instruction_literal (p, -1);
      /* This is a caller instruction. */
      break;

    default:
      jitter_fatal ("invalid specialized instruction opcode %i", (int)opcode);
    }
  return 1; // FIXME: I should rethink this return value.
}

void
pvm_state_initialize_with_slow_registers
   (struct pvm_state *jitter_state,
    jitter_uint jitter_slow_register_no_per_class)
{
  struct pvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->pvm_state_backing;
  struct pvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_pvm_Vv9tlAxnoJ_state_runtime;

  /* Initialize The Array. */
  jitter_state_backing->jitter_slow_register_no_per_class
    = jitter_slow_register_no_per_class;
  jitter_state_backing->jitter_array
    = jitter_xmalloc (PVM_ARRAY_SIZE(jitter_state_backing
                         ->jitter_slow_register_no_per_class));

  /* Initialize special-purpose data. */
  pvm_initialize_special_purpose_data (PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = pvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* Make the stack backing for stack . */
  jitter_stack_initialize_tos_backing(& jitter_state_backing->jitter_stack_stack_backing,
                                      sizeof (pvm_val),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_TOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              stack, jitter_state_backing->jitter_stack_stack_backing);
  /* Make the stack backing for returnstack . */
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_returnstack_backing,
                                      sizeof (pvm_val),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_NTOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);
  /* Make the stack backing for exceptionstack . */
  jitter_stack_initialize_ntos_backing(& jitter_state_backing->jitter_stack_exceptionstack_backing,
                                      sizeof (struct pvm_exception_handler),
                                      65536,
                                      NULL,
                                      1,
                                      1);
  JITTER_STACK_NTOS_INITIALIZE(struct pvm_exception_handler, jitter_state_runtime-> ,
                              exceptionstack, jitter_state_backing->jitter_stack_exceptionstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* No need to initialise r-class fast registers. */

  /* Initialise slow registers. */
  pvm_initialize_slow_registers
     (jitter_state->pvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);

  /* Link this new state to the list of states. */
  JITTER_LIST_LINK_LAST (pvm_state, links, & pvm_vm->states, jitter_state);

  /* User code for state initialization. */
#line 1092 "../../libpoke/pvm.jitter"
#line 1092 "../../libpoke/pvm.jitter"

      jitter_state_backing->vm = NULL;
      jitter_state_backing->canary_stack = NULL;
      jitter_state_backing->canary_returnstack = NULL;
      jitter_state_backing->canary_exceptionstack = NULL;
      jitter_state_backing->exit_code = PVM_EXIT_OK;
      jitter_state_backing->result_value = PVM_NULL;
      jitter_state_runtime->endian = IOS_ENDIAN_MSB;
      jitter_state_runtime->nenc = IOS_NENC_2;
      jitter_state_runtime->pretty_print = 0;
      jitter_state_runtime->omode = PVM_PRINT_FLAT;
      jitter_state_runtime->obase = 10;
      jitter_state_runtime->omaps = 0;
      jitter_state_runtime->odepth = 0;
      jitter_state_runtime->oindent = 2;
      jitter_state_runtime->oacutoff = 0;
  
  /* End of the user code for state initialization. */

}

void
pvm_state_reset
   (struct pvm_state *jitter_state)
{
  struct pvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->pvm_state_backing;
  struct pvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_pvm_Vv9tlAxnoJ_state_runtime;

  /* No need to touch The Array, which already exists. */
  /* No need to touch special-purpose data, which already exist. */

  /* Set the initial program point to an invalid value, for defensiveness. */
  jitter_state_backing->initial_program_point = NULL;

  /* Set the initial exit status. */
  jitter_state_backing->exit_status
    = pvm_exit_status_never_executed;

  /* Initialize stack backing and stack runtime data structures, pointing
     to memory from the backings. */
  /* The stack backing for stack already exists, and does
     not require element initialisation. */
  JITTER_STACK_TOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              stack, jitter_state_backing->jitter_stack_stack_backing);
  /* The stack backing for returnstack already exists, and does
     not require element initialisation. */
  JITTER_STACK_NTOS_INITIALIZE(pvm_val, jitter_state_runtime-> ,
                              returnstack, jitter_state_backing->jitter_stack_returnstack_backing);
  /* The stack backing for exceptionstack already exists, and does
     not require element initialisation. */
  JITTER_STACK_NTOS_INITIALIZE(struct pvm_exception_handler, jitter_state_runtime-> ,
                              exceptionstack, jitter_state_backing->jitter_stack_exceptionstack_backing);

  /* Initialise the link register. */
  jitter_state_runtime->_jitter_link.label = NULL;

  /* No need to initialise r-class fast registers. */

  /* Initialise slow registers. */
  pvm_initialize_slow_registers
     (jitter_state->pvm_state_backing.jitter_array,
      0 /* overwrite any already existing rank */,
      jitter_state_backing->jitter_slow_register_no_per_class);


  /* No need to touch links within the global list of states:
     this state already exists and is already linked. */

  /* The user supplied no explicit code for state reset: use
     finalisation code followed by initialisation code. */
/* User finalisation. */
{
#line 1112 "../../libpoke/pvm.jitter"
#line 1112 "../../libpoke/pvm.jitter"

   /* Finalize extra state here.  */
  
}
/* User Initialisation. */{
#line 1092 "../../libpoke/pvm.jitter"
#line 1092 "../../libpoke/pvm.jitter"

      jitter_state_backing->vm = NULL;
      jitter_state_backing->canary_stack = NULL;
      jitter_state_backing->canary_returnstack = NULL;
      jitter_state_backing->canary_exceptionstack = NULL;
      jitter_state_backing->exit_code = PVM_EXIT_OK;
      jitter_state_backing->result_value = PVM_NULL;
      jitter_state_runtime->endian = IOS_ENDIAN_MSB;
      jitter_state_runtime->nenc = IOS_NENC_2;
      jitter_state_runtime->pretty_print = 0;
      jitter_state_runtime->omode = PVM_PRINT_FLAT;
      jitter_state_runtime->obase = 10;
      jitter_state_runtime->omaps = 0;
      jitter_state_runtime->odepth = 0;
      jitter_state_runtime->oindent = 2;
      jitter_state_runtime->oacutoff = 0;
  
}
  /* End of the user code for state finalisation followed by
     initialisation. */
}

void
pvm_state_finalize (struct pvm_state *jitter_state)
{
  struct pvm_state_backing * const jitter_state_backing
    __attribute__ ((unused))
    = & jitter_state->pvm_state_backing;
  struct pvm_state_runtime * const jitter_state_runtime
    __attribute__ ((unused))
    = & jitter_state->_pvm_Vv9tlAxnoJ_state_runtime;

  /* User code for state finalization. */
#line 1112 "../../libpoke/pvm.jitter"
#line 1112 "../../libpoke/pvm.jitter"

   /* Finalize extra state here.  */
  
  /* End of the user code for state finalization. */

  /* Finalize stack backings -- There is no need to finalize the stack
     runtime data structures, as they hold no heap data of their own. */
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_stack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_returnstack_backing);
  jitter_stack_finalize_backing (& jitter_state_backing->jitter_stack_exceptionstack_backing);


  /* Unlink this state from the list of states. */
  JITTER_LIST_UNLINK (pvm_state, links, & pvm_vm->states, jitter_state);

  /* Finalize special-purpose data. */
  pvm_finalize_special_purpose_data (PVM_ARRAY_TO_SPECIAL_PURPOSE_STATE_DATA (jitter_state_backing->jitter_array));

  /* Finalize the Array. */
  free ((void *) jitter_state_backing->jitter_array);

}

